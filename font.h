
#ifndef FONT_INCLUDED
#define FONT_INCLUDED

#include "libs/engine/graphics.h"
#include "util.h"
#include "stdint.h"

GFX_Font *
Load_font(Arena *tmp_arena, uint8_t *filedata, int64_t filedata_len, float height);


#endif // FONT_INCLUDED


#ifdef FONT_IMPLEMENTATION
#ifndef FONT_IMPLEMENTATION_INCLUDED
#define FONT_IMPLEMENTATION_INCLUDED


#include "libs/engine/madmath.h"

size_t
FONT__strlen(const char *str) {
    size_t len = 0;
    for (;  str[len]; len+=1);
    return len;
}

Arena *FONT__arena = NULL;

#define STBTT_ifloor(x)   ((int) Floor(x))
#define STBTT_iceil(x)    ((int) Ceil(x))
#define STBTT_sqrt(x)      Sqrt(x)
#define STBTT_pow(x,y)     Pow(x,y)
#define STBTT_fmod(x,y)    Mod(x,y)
#define STBTT_cos(x)       Cos(x)
#define STBTT_acos(x)      Acos(x)
#define STBTT_fabs(x)      Abs(x)

#define STBTT_malloc(x,u)  ((void)(u),ARENA_Push(FONT__arena, x))
#define STBTT_free(x,u)    ((void)(u),0)

#define STBTT_assert(x)    ASSERT(x)

#define STBTT_strlen(x)    FONT__strlen(x)

void *
memcpy(void *dest_, const void *src_, size_t n);
void *
memset(void *dst_, int c, size_t n);

#define STBTT_memcpy(a, b, c) memcpy(a, b, c)
#define STBTT_memset(a, b, c) memset(a, b, c)

#define STB_TRUETYPE_IMPLEMENTATION
#include "libs/stb_truetype.h"




#define FONT__ATLAS_SIZE 1024


GFX_Font *
Load_font(Arena *tmp_arena, uint8_t *filedata, int64_t filedata_len, float height) {

    FONT__arena = tmp_arena;

    static GFX_Font FONT__font;
    static int64_t FONT__ranges[2];
    static GFX_GlyphInfo FONT__glyph_infos[96];
    
    uint8_t *atlas8  = ARENA_Array(tmp_arena, uint8_t, FONT__ATLAS_SIZE*FONT__ATLAS_SIZE);
    uint8_t *atlas32 = ARENA_Array(tmp_arena, uint8_t, FONT__ATLAS_SIZE*FONT__ATLAS_SIZE*4);

    // We lie saying that the height is FONT__ATLAS_SIZE-4 because we reserve the last
    // 4 rows to store a tiny white square
	int result = stbtt_BakeFontBitmap(
        filedata, 0, height, atlas8, FONT__ATLAS_SIZE, FONT__ATLAS_SIZE-4,
        32, 96, (stbtt_bakedchar*)(FONT__glyph_infos));

    if (result <= 0) return NULL;

	for (int i = 0; i < FONT__ATLAS_SIZE; i+=1) {
		for (int j = 0; j < FONT__ATLAS_SIZE; j+=1) {
			atlas32[i*FONT__ATLAS_SIZE*4 + j*4 + 0] = 0xFF; // R
			atlas32[i*FONT__ATLAS_SIZE*4 + j*4 + 1] = 0xFF; // G
			atlas32[i*FONT__ATLAS_SIZE*4 + j*4 + 2] = 0xFF; // B
			uint8_t A = atlas8[i*FONT__ATLAS_SIZE + j];
			atlas32[i*FONT__ATLAS_SIZE*4 + j*4 + 3] = A; // A
		}
	}

    for (int i = FONT__ATLAS_SIZE-4; i < FONT__ATLAS_SIZE; i+=1) {
        for (int j = 0; j < 4; j+=1) {
			atlas32[i*FONT__ATLAS_SIZE*4 + j*4 + 0] = 0xFF; // R
			atlas32[i*FONT__ATLAS_SIZE*4 + j*4 + 1] = 0xFF; // G
			atlas32[i*FONT__ATLAS_SIZE*4 + j*4 + 2] = 0xFF; // B
			atlas32[i*FONT__ATLAS_SIZE*4 + j*4 + 3] = 0xFF; // A
        }
    }

	// Make the texture
    GLuint texture;
	glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, FONT__ATLAS_SIZE, FONT__ATLAS_SIZE, 0,
        GL_RGBA, GL_UNSIGNED_BYTE, atlas32);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    
    FONT__ranges[0] = 32;
    FONT__ranges[1] = 126;
    FONT__font.ranges = FONT__ranges;
    FONT__font.total_ranges = 1;
    FONT__font.count     = 96;
    FONT__font.fallback  = 0;
    FONT__font.texture_w = FONT__ATLAS_SIZE;
    FONT__font.texture_h = FONT__ATLAS_SIZE;
    FONT__font.texture   = texture;
    FONT__font.height = height;
    FONT__font.has_white = true;
    FONT__font.white_x = 1.0f/(float)FONT__ATLAS_SIZE;
    FONT__font.white_y = ((float)FONT__ATLAS_SIZE-3)/(float)FONT__ATLAS_SIZE;
    FONT__font.white_w = 2.0f/(float)FONT__ATLAS_SIZE;
    FONT__font.white_h = 2.0f/(float)FONT__ATLAS_SIZE;
    FONT__font.glyph_infos = FONT__glyph_infos;

    // NOTE(Tano) the yoff on stbtt ajust the position to the baseline, but the default font
    // on graphics.h adjusts to the maximum top, this is why we iterate hera and add the
    // height, but this stills not giving the same result...
    // TODO(Tano) fix this or change the graphics.h default font to adjust to baseline
    for (int i = 0; i < 96; i+=1) FONT__glyph_infos[i].yoff += height;

    return &FONT__font;
}


#endif // FONT_IMPLEMENTATION_INCLUDED
#endif // FONT_IMPLEMENTATION


