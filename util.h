
// Util structures and functions used across multiple tools

#ifndef UTIL_INCLUDED
#define UTIL_INCLUDED

#include <stdbool.h>
#include <stdint.h>


// ASSERT

#ifdef __clang__
#define ASSERT(x)  do {if (!(x)) __builtin_trap();} while(0)
#else
#define ASSERT(x)  do {if (!(x)){*((volatile int*)0) = 0;}} while(0)
#endif






// STRINGS stuff

typedef struct {
    char   *data;
    int64_t len;
} String;

#define STR_LIT(str)   (String){(char *)(str), sizeof((str))-1}
#define STR(str, len)  (String){(char *)(str), (len)}
#define STR_VARG(s)   (int)s.len, s.data


bool
STR_Match(String a, String b);


bool
STR_Contains(String s, String sub_s, bool case_insensitive);

uint64_t
STR_Hash(String s);






// ARENA Stuff
// Fixed linear arena


typedef struct {
    uint8_t *memory;
    uint64_t cursor;
    uint64_t cap;
} Arena;



void
ARENA_Init(Arena *arena, void *memory_chunk, uint64_t size);


void
ARENA_Clear(Arena *arena);


void *
ARENA_Push_aligned(Arena *arena, uint64_t size, uint64_t alignement);

void *
ARENA_Push(Arena *arena, uint64_t size);


#define ARENA_New(arena, T)                 (T*)ARENA_Push(arena, sizeof(T))
#define ARENA_Array(arena, T, total_items)  (T*)ARENA_Push(arena, sizeof(T)*total_items)





// HASHMAP Stuff

#define HASHMAP_MAX_ITEMS_PER_BUCKET 16
typedef struct HashMapBucket HashMapBucket;
struct HashMapBucket {
    uint64_t items_count;
    uint64_t keys[HASHMAP_MAX_ITEMS_PER_BUCKET];
    HashMapBucket *next;
    uint64_t values[HASHMAP_MAX_ITEMS_PER_BUCKET];
};


typedef struct {
    uint64_t total_slots;
    HashMapBucket *slots[];
} HashMap;


static inline uint64_t
HashMap_String_key(String s) {
    return STR_Hash(s);
}

HashMap *
HashMap_New(Arena *arena, uint64_t total_slots);

uint64_t *
HashMap_Lookup(HashMap *hashmap, uint64_t key);

bool
HashMap_Delete(HashMap *hashmap, uint64_t key);

void
HashMap_Delete_all_values(HashMap *hashmap);

uint64_t *
HashMap_Lookup_or_create(Arena *arena, HashMap *hashmap, uint64_t key, bool *found);

bool
HashMap_Insert(Arena *arena, HashMap *hashmap, uint64_t key, uint64_t value, bool overwrite);

#endif // UTIL_INCLUDED


#ifdef UTIL_IMPLEMENTATION
#ifndef UTIL_IMPLEMENTATION_INCLUDED
#define UTIL_IMPLEMENTATION_INCLUDED







// ------------------------------------
// STRING
// -----------------------------------

bool
STR_Match(String a, String b) {
    if (a.len != b.len) return false;
    for (int64_t i = 0, len = a.len; i < len; i+=1) {
        if (a.data[i] != b.data[i]) return false;
    }
    return true;
}


// djb2  http://www.cse.yorku.ca/~oz/hash.html
uint64_t
STR_Hash(String s) {
    uint64_t hash = 5381;
    for (int cursor = 0; cursor < s.len; cursor+=1) {
        uint64_t c = s.data[cursor];
        hash = ((hash << 5) + hash) + c; /* hash * 33 + c */
    }
    return hash;
}

char
Char_to_lower(char c) {
    if (c >= 'A' && c <= 'Z') {
        return c-'A'+'a';
    }
    return c;
}


bool
STR_Contains(String s, String sub_s, bool case_insensitive) {
    for (int64_t cursor = 0; cursor < s.len-sub_s.len; cursor  += 1) {
        bool found = true;
        for (int64_t cursor2 = 0; cursor2 < sub_s.len; cursor2 += 1) {
            char c0 = s.data[cursor+cursor2];
            char c1 = sub_s.data[cursor2];
            if (case_insensitive) {
                c0 = Char_to_lower(c0);
                c1 = Char_to_lower(c1);
            }
            if (c0 != c1) {
                found = false;
                break;
            }
        }
        if (found) return true;
    }
    return false;
}





// ------------------------------------
// ARENA
// -----------------------------------

void
ARENA_Init(Arena *arena, void *memory_chunk, uint64_t size) {
    arena->memory = memory_chunk;
    arena->cursor = 0;
    arena->cap    = size;
}


void
ARENA_Clear(Arena *arena) {
    arena->cursor = 0;
}

void *
ARENA_Push_aligned(Arena *arena, uint64_t size, uint64_t alignement) {

    uintptr_t memory_int_aligned = (uintptr_t)&(arena->memory[arena->cursor]);
    uintptr_t memory_rem = memory_int_aligned%alignement;
    if (memory_rem > 0) {
        memory_int_aligned += (alignement-memory_rem);
    }
    ASSERT(memory_int_aligned%alignement == 0);

    uint64_t cursor = memory_int_aligned-((uintptr_t)(arena->memory));
    if ((cursor+size) > arena->cap) return NULL;

    uint8_t *result = &(arena->memory[cursor]);
    arena->cursor = cursor+size;

    return (void*)result;
}

void *
ARENA_Push(Arena *arena, uint64_t size) {
    return ARENA_Push_aligned(arena, size, 8);
}

#define ARENA_New(arena, T)                 (T*)ARENA_Push(arena, sizeof(T))
#define ARENA_Array(arena, T, total_items)  (T*)ARENA_Push(arena, sizeof(T)*total_items)








// ------------------------------------
// HASH MAP
// -----------------------------------




HashMap *
HashMap_New(Arena *arena, uint64_t total_slots) {
    HashMap *result = ARENA_Push_aligned(arena, sizeof(HashMap)+sizeof(void*)*total_slots, 8);
    if (result != NULL) {
        result->total_slots = total_slots;
        for (int64_t i = 0; i < (int64_t)total_slots; i += 1) {
            result->slots[i] = NULL;
        }
    }
    return result;
}

uint64_t *
HashMap_Lookup(HashMap *hashmap, uint64_t key) {
    uint64_t slot_pos = key%hashmap->total_slots;
    for (HashMapBucket *bucket = hashmap->slots[slot_pos]; bucket; bucket = bucket->next) {
        for (int64_t i = 0; i < bucket->items_count; i += 1) {
            if (bucket->keys[i] == key) {
                return &(bucket->values[i]);
            }
        }
    }
    return NULL;
}

bool
HashMap_Delete(HashMap *hashmap, uint64_t key) {
    uint64_t slot_pos = key%hashmap->total_slots;
    for (HashMapBucket *bucket = hashmap->slots[slot_pos]; bucket; bucket = bucket->next) {
        for (int64_t i = 0; i < bucket->items_count; i += 1) {
            if (bucket->keys[i] == key) {
                int64_t last_item_index = bucket->items_count-1;
                bucket->keys[i]   = bucket->keys[last_item_index];
                bucket->values[i] = bucket->values[last_item_index];
                bucket->items_count -= 1;
                return true;
            }
        }
    }
    return false;
}


void
HashMap_Delete_all_values(HashMap *hashmap) {
    int64_t total_slots = hashmap->total_slots;
    for (int64_t slot_i = 0; slot_i < total_slots; slot_i+=1) {
        for (HashMapBucket *bucket = hashmap->slots[slot_i]; bucket; bucket = bucket->next) {
            bucket->items_count = 0;
        }
    }
}


uint64_t *
HashMap_Lookup_or_create(Arena *arena, HashMap *hashmap, uint64_t key, bool *found) {
    uint64_t slot_pos = key%hashmap->total_slots;
    HashMapBucket **append_bucket = NULL;
    HashMapBucket *target_bucket  = NULL;

    if (hashmap->slots[slot_pos]) {

        HashMapBucket *bucket = hashmap->slots[slot_pos];
        for (;;) {

            // Iterate over the bucket items
            for (int64_t i = 0; i < bucket->items_count; i += 1) {
                if (bucket->keys[i] == key) {
                    if (found) *found = true;
                    return &(bucket->values[i]);
                }
            }

            // Check if the bucket has empty items to target it in case the key is not found
            if (!target_bucket && bucket->items_count < HASHMAP_MAX_ITEMS_PER_BUCKET) {
                target_bucket = bucket;
            }

            // If the next bucket is NULL we end
            if (bucket->next == NULL) {
                break;
            }

            // Advance the bucket
            bucket = bucket->next;
        }
        
        if (!target_bucket) {
            append_bucket = &(bucket->next);
        }
    }
    else {
        append_bucket = &(hashmap->slots[slot_pos]);
    }
    
    // If append bucket is not null we have to create a new bucket and append it to the
    // append_bucket pointer
    if (append_bucket) {
        target_bucket = ARENA_New(arena, HashMapBucket);
        if (target_bucket) {
            target_bucket->items_count = 0;
            target_bucket->next        = NULL;
            *append_bucket = target_bucket;
        }
    }
    
    uint64_t *result = NULL;

    // If a target bucket exist we add a new item to it
    if (target_bucket) {
        target_bucket->keys[target_bucket->items_count]   = key;
        target_bucket->values[target_bucket->items_count] = 0;
        result = &(target_bucket->values[target_bucket->items_count]);
        target_bucket->items_count += 1;
    }
    
    // If we are here we didn't find the key
    if (found) *found = false;
    return result;

}


bool
HashMap_Insert(Arena *arena, HashMap *hashmap, uint64_t key, uint64_t value, bool overwrite) {
    bool found = false;
    uint64_t *val_ptr = HashMap_Lookup_or_create(arena, hashmap, key, &found);
    if (!val_ptr) return false;
    if (found && !overwrite) return false;
    *val_ptr = value;
    return true;
}


#endif // UTIL_IMPLEMENTATION_INCLUDED
#endif // UTIL_IMPLEMENTATION

