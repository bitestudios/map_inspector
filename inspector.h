
#ifndef INSPECTOR_INCLUDED
#define INSPECTOR_INCLUDED

#include "util.h"


typedef struct Area Area;

typedef struct {
    String  name;
    String  module;
    int64_t value;
    int64_t estimated_size;
    Area *area;
} Symbol;


typedef struct SymbolNode SymbolNode;
struct SymbolNode {
    SymbolNode *next;
    Symbol      symbol;
};


typedef struct {
    uint64_t total_nodes;
    SymbolNode *first;
    SymbolNode *last;
} SymbolList;

struct Area {
    String     name;
    SymbolList symbols;
    int64_t    address;
    int64_t    size;
};

typedef struct AreaNode AreaNode;
struct AreaNode {
    AreaNode *next;
    Area      area;
};

typedef struct {
    uint64_t total_nodes;
    AreaNode *first;
    AreaNode *last;
} AreaList;


typedef struct {
    AreaList areas;
    int64_t start_address;
    int64_t size;
    Symbol **symbols;
    int64_t symbols_count;
} BinaryMap;




bool
Gen_binary_map_from_memory(Arena *arena, BinaryMap *binary_map, char *file_data, uint64_t file_size);

enum {
    SORT_KIND_SIZE_LESS_FIRST,
    SORT_KIND_SIZE_GREATER_FIRST,
    SORT_KIND_ADDRESS_LESS_FIRST,
    SORT_KIND_ADDRESS_GREATER_FIRST,
};

void
Sort_symbols_by(BinaryMap *binary_map, int64_t sort_kind);



#endif // INSPECTOR_INCLUDED

#ifdef  INSPECTOR_IMPLEMENTATION
#ifndef INSPECTOR_IMPLEMENTATION_INCLUDED
#define INSPECTOR_IMPLEMENTATION_INCLUDED


typedef struct {
    char *data;
    int64_t cursor;
    int64_t size;
    int64_t current_line;
} Stream;




bool
Char_match(char c, const char *chars) {
    for (int64_t i = 0; chars[i]; i+=1) {
        if (c == chars[i]) return true;
    }
    return false;
}


bool
Is_space(char c) {
    return Char_match(c, " \r\t");
}


bool
Is_hex_char(char c) {
    return Char_match(c, "0123456789ABCDEFabcdef");
}


bool
String_to_int_hex(String s, int64_t *result_) {
    int64_t result = 0;
    if (s.len <= 0) return false;
    for (int64_t i = 0; i < s.len; i += 1) {
        char c = s.data[i];
        int64_t int_val = 0;
        if (c >= '0' && c <= '9') {
            int_val = (c-'0');
        }
        else if (c >= 'A' && c <= 'F') {
            int_val = (c-'A')+10;
        }
        else if (c >= 'a' && c <= 'f') {
            int_val = (c-'a')+10;
        }
        else {
            return false;
        }
        result = result*16+int_val;
    }
    *result_ = result;
    return true;
}


void
Eat_spaces(Stream *stream) {
    char   *data   = stream->data;
    int64_t cursor = stream->cursor;
    int64_t size   = stream->size;
    for (;;) {
        if (cursor >= size) break;
        if (!Is_space(data[cursor])) break;
        cursor += 1;
    }
    stream->cursor = cursor;
}



void
Eat_line(Stream *stream) {
    char   *data   = stream->data;
    int64_t cursor = stream->cursor;
    int64_t size   = stream->size;
    for (;;) {
        if (cursor >= size) break;
        if (data[cursor] == '\n') {
            cursor += 1;
            stream->current_line += 1;
            break;
        }
        cursor += 1;
    }
    stream->cursor = cursor;
}


String
Get_token(Stream *stream) {
    Eat_spaces(stream);

    char   *data   = stream->data;
    int64_t cursor = stream->cursor;
    int64_t size   = stream->size;
    for (;;) {
        if (cursor >= size) break;
        if (Char_match(data[cursor], " \t\r\n")) break;
        cursor += 1;
    }
    String result;
    result.data = &data[stream->cursor];
    result.len  = cursor-stream->cursor;

    stream->cursor = cursor;

    return result;
}



bool
Stream_end(Stream stream) {
    return (stream.cursor == stream.size);
}




bool
SymbolList_Add_symbol(Arena *arena, SymbolList *list, Symbol symbol) {
    SymbolNode *symbol_node = ARENA_New(arena, SymbolNode);
    if (symbol_node == NULL) false;
    symbol_node->next   = NULL;
    symbol_node->symbol = symbol;
    if (list->last) {
        list->last->next = symbol_node;
        list->last       = symbol_node;
    }
    else {
        list->first = symbol_node;
        list->last  = symbol_node;
    }
    list->total_nodes += 1;
    return true;
}




bool
AreaList_Add_area(Arena *arena, AreaList *list, Area area) {
    AreaNode *area_node = ARENA_New(arena, AreaNode);
    if (area_node == NULL) false;
    area_node->next = NULL;
    area_node->area = area;
    if (list->last) {
        list->last->next = area_node;
        list->last       = area_node;
    }
    else {
        list->first = area_node;
        list->last  = area_node;
    }
    list->total_nodes += 1;
    return true;
}




static inline void
qsort_(void *base_, size_t nitems, size_t size, int (*compar)(const void *, const void*)) {
    unsigned char *base = base_;
    if (nitems < 2) return;

    unsigned char *pivot = &base[(nitems/2)*size];

    size_t i, j;
    for (i=0, j=nitems-1; ;i+=1, j-=1) {
        while (i < nitems && compar(&base[i*size], pivot) < 0) i+=1;
        while (0 < j      && compar(pivot, &base[j*size]) < 0) j-=1;

        if (i >= j) break;

        if (&base[i*size] == pivot) {
            pivot = &base[j*size];
        }else if (&base[j*size] == pivot) {
            pivot = &base[i*size];
        }
        for (size_t byte_i = 0; byte_i < size; byte_i+=1) {
            unsigned char tmp = base[i*size+byte_i];
            base[i*size+byte_i] = base[j*size+byte_i];
            base[j*size+byte_i] = tmp;
        }
    }
    
    qsort_(base, i, size, compar);
    qsort_(&base[i*size], nitems-i, size, compar);
}


static inline int
Compare_symbol_by_size_less_first(const void *l, const void *r) {
    const Symbol *symbol_l = *(const Symbol **)l;
    const Symbol *symbol_r = *(const Symbol **)r;
    return (symbol_l->estimated_size - symbol_r->estimated_size);
}


static inline int
Compare_symbol_by_size_greater_first(const void *l, const void *r) {
    const Symbol *symbol_l = *(const Symbol **)l;
    const Symbol *symbol_r = *(const Symbol **)r;
    return (symbol_r->estimated_size - symbol_l->estimated_size);
}


static inline int
Compare_symbol_by_address_less_first(const void *l, const void *r) {
    const Symbol *symbol_l = *(const Symbol **)l;
    const Symbol *symbol_r = *(const Symbol **)r;
    return (symbol_l->value - symbol_r->value);
}


static inline int
Compare_symbol_by_address_greater_first(const void *l, const void *r) {
    const Symbol *symbol_l = *(const Symbol **)l;
    const Symbol *symbol_r = *(const Symbol **)r;
    return (symbol_r->value - symbol_l->value);
}


bool
Gen_binary_map_from_memory(Arena *arena, BinaryMap *binary_map, char *file_data, uint64_t file_size) {

    AreaList areas = {0};
    
    bool parse_error = false;
    Stream stream = {.data = file_data, .cursor = 0, .current_line = 1, .size = file_size};
    for (;!Stream_end(stream);) {

        String token_string = Get_token(&stream);
        if (STR_Match(token_string, STR_LIT("Area"))) {
            bool area_parse_error = false;

            // We dont care about the other stuff in the line
            Eat_line(&stream);
            // Here should be just "---------"
            Eat_line(&stream);

            String area_name = Get_token(&stream);
            // When the token is . is followed by .ABS., at least on my code...
            if (STR_Match(area_name, STR_LIT("."))) {
                area_name = Get_token(&stream);
            }

            int64_t area_address  = 0;
            String  area_addr_str = {0};
            if (!area_parse_error) {
                area_addr_str = Get_token(&stream);
                if (!String_to_int_hex(area_addr_str, &area_address)) {
                    ASSERT(false);
                    area_parse_error = true;
                }
            }

            int64_t area_size    = 0;
            String area_size_str = {0};
            if (!area_parse_error) {
                area_size_str = Get_token(&stream);
                if (!String_to_int_hex(area_size_str, &area_size)) {
                    ASSERT(false);
                    area_parse_error = true;
                }
            }

            if (!area_parse_error) {
                bool    area_already_exist = false;
                Area   *current_area       = NULL;
                for (AreaNode *area_node = areas.first; area_node; area_node = area_node->next) {
                    if (STR_Match(area_name, area_node->area.name)) {
                        area_already_exist = true;
                        current_area = &(area_node->area);
                        break;
                    }
                }
                
                if (!area_already_exist) {
                    Area area = {.name = area_name, .address = area_address, .size = area_size};
                    AreaList_Add_area(arena, &areas, area);
                    current_area = &(areas.last->area);
                }

                // End the area info
                Eat_line(&stream);
                // White space
                Eat_line(&stream);
                // Headers
                Eat_line(&stream);
                // Here should be just "---------"
                Eat_line(&stream);

                for (;!Stream_end(stream);) {
                    String  symbol_addr_str = Get_token(&stream);
                    int64_t symbol_addr     = 0;
                    if (!String_to_int_hex(symbol_addr_str, &symbol_addr)) {
                        break;
                    }
                    String symbol_name  = Get_token(&stream);
                    String module_name  = Get_token(&stream);
                    Symbol symbol = {
                        .name   = symbol_name,
                        .value  = symbol_addr,
                        .module = module_name,
                        .area   = current_area
                    };

                    SymbolList_Add_symbol(arena, &(current_area->symbols), symbol);

                    Eat_line(&stream);
                }
            }

            if (area_parse_error) {
                parse_error = true;
            }
        }
        Eat_line(&stream);
    }

    if (!parse_error) {
        // Estimate binary start/end
        int64_t binary_start = 0x7FFFFFFFFFFFFFFF;
        int64_t binary_end   = 0;
        for (AreaNode *area_node = areas.first; area_node; area_node = area_node->next) {
            Area area = (area_node->area);
            if (area.size > 0) {
                int64_t area_end = area.address+area.size;
                if (area_node->area.address < binary_start) {
                    binary_start = area.address;
                }
                if (area_end > binary_end) {
                    binary_end   = area_end;
                }
            }
        }

        // Estimate symbol sizes
        for (AreaNode *area_node = areas.first; area_node; area_node = area_node->next) {
            int64_t area_start = area_node->area.address;
            int64_t area_end   = area_start + area_node->area.size;

            if (area_end > area_start) {
                SymbolNode *symbol_node = area_node->area.symbols.first;
                for (; symbol_node; symbol_node = symbol_node->next) {
                    int64_t symbol_start = symbol_node->symbol.value;
                    int64_t symbol_end   = symbol_start;
                    if (symbol_node->next) {
                        symbol_end = symbol_node->next->symbol.value;
                    }
                    else {
                        symbol_end = area_end;
                    }
                    symbol_node->symbol.estimated_size = symbol_end-symbol_start;
                    if (symbol_node->symbol.estimated_size < 0) {
                        symbol_node->symbol.estimated_size = 0;
                    }
                }
                
            }
        }
        
        // Count the total of symbols
        int64_t symbols_count = 0;
        for (AreaNode *area_node = areas.first; area_node; area_node = area_node->next) {
            symbols_count += area_node->area.symbols.total_nodes;
        }
        
        binary_map->areas         = areas;
        binary_map->start_address = binary_start;
        binary_map->size          = binary_end-binary_start;

        binary_map->symbols = ARENA_Array(arena, Symbol *, symbols_count);
        binary_map->symbols_count = symbols_count;

        // Make an array of pointers of symbols
        int64_t symbol_i = 0;
        for (AreaNode *area_node = areas.first; area_node; area_node = area_node->next) {
            SymbolNode *symbol_node = area_node->area.symbols.first;
            for (; symbol_node; symbol_node = symbol_node->next) {
                binary_map->symbols[symbol_i] = &(symbol_node->symbol);
                symbol_i += 1;
            }
        }

        return true;
    }

    return false;
}



void
Sort_symbols_by(BinaryMap *binary_map, int64_t sort_kind) {
    if (sort_kind == SORT_KIND_SIZE_LESS_FIRST) {
        qsort_(binary_map->symbols, binary_map->symbols_count, sizeof(Symbol*), Compare_symbol_by_size_less_first);
    }
    else if (sort_kind == SORT_KIND_SIZE_GREATER_FIRST) {
        qsort_(binary_map->symbols, binary_map->symbols_count, sizeof(Symbol*), Compare_symbol_by_size_greater_first);
    }
    else if (sort_kind == SORT_KIND_ADDRESS_LESS_FIRST) {
        qsort_(binary_map->symbols, binary_map->symbols_count, sizeof(Symbol*), Compare_symbol_by_address_less_first);
    }
    else if (sort_kind == SORT_KIND_ADDRESS_GREATER_FIRST) {
        qsort_(binary_map->symbols, binary_map->symbols_count, sizeof(Symbol*), Compare_symbol_by_address_greater_first);
    }
}

#endif // INSPECTOR_IMPLEMENTATION_INCLUDED
#endif //  INSPECTOR_IMPLEMENTATION

#if 0

#include <stdio.h>

int
main() {
    static uint8_t arena_memory[16*1024*1024];
    Arena arena;
    ARENA_Init(&arena, arena_memory, 16*1024*1024);

    FILE *file_handle = fopen("obj/cpcpool.map", "rb");
    fseek(file_handle, 0, SEEK_END);
    int64_t file_size = ftell(file_handle);
    fseek(file_handle, 0, SEEK_SET);
    char *file_data = ARENA_Array(&arena, char, file_size);
    fread(file_data, file_size, 1, file_handle);

    BinaryMap binary_map;
    bool success = Gen_binary_map_from_memory(&arena, &binary_map, file_data, file_size);
    if (success) {
        printf("Binary start: 0x%lX\n", binary_map.start_address);
        printf("Binary size:  %ld\n\n", binary_map.size);
        
        for (AreaNode *area_node = binary_map.areas.first; area_node; area_node = area_node->next) {

            printf("\narea name:    %.*s\n", STR_VARG(area_node->area.name));
            printf("area address: 0x%lX\n", area_node->area.address);
            printf("area size:    0x%lX\n", area_node->area.size);

            SymbolNode *symbol_node = area_node->area.symbols.first;
            for (; symbol_node; symbol_node = symbol_node->next) {
                printf("symbol name:    %.*s\n", STR_VARG(symbol_node->symbol.name));
                printf("symbol address: 0x%lX\n", symbol_node->symbol.value);
                printf("symbol size:    %ld\n", symbol_node->symbol.estimated_size);
                printf("symbol module:  %.*s\n", STR_VARG(symbol_node->symbol.module));
            }
        }
    }

    
    return 0;
}

#endif

