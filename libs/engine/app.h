
// TODO
//
// Hunt all the fprintf and push all to a error buffer
//
// Implement this hack, to unlock MSAA on windows
// https://www.khronos.org/opengl/wiki/Creating_an_OpenGL_Context_(WGL)#Proper_Context_Creation
//
// Manage hdpi
//


// MAP OF THE FILE
//
// - HEADER (All API's function calls)
// - IMPLEMENTATION
// 	- GENERAL IMPLEMENTATIONS (Calling to the per platform implementations when needed)
//	- PER PLATFORM NEEDED IMPLEMENTATIONS
// 	  - Windows
// 	  - Linux
// 	  - Wasm
//



#ifndef APP_INCLUDE
#define APP_INCLUDE

#include <stdint.h>
#include <stdbool.h>


#ifndef APP_PUBLIC
	#define APP_PUBLIC 
#endif

#ifndef APP_INTERNAL
	#define APP_INTERNAL static
#endif




///////////////////////////////////////////////////////////////////////////
//
//
//
// Basic GL application API
//
//
//
///////////////////////////////////////////////////////////////////////////




/* Based on GLFW */
typedef enum {
    APP_KEY_INVALID          = 0,
    APP_KEY_SPACE            = 32,
    APP_KEY_APOSTROPHE       = 39,  /* ' */
    APP_KEY_COMMA            = 44,  /* , */
    APP_KEY_MINUS            = 45,  /* - */
    APP_KEY_PERIOD           = 46,  /* . */
    APP_KEY_SLASH            = 47,  /* / */
    APP_KEY_0                = 48,
    APP_KEY_1                = 49,
    APP_KEY_2                = 50,
    APP_KEY_3                = 51,
    APP_KEY_4                = 52,
    APP_KEY_5                = 53,
    APP_KEY_6                = 54,
    APP_KEY_7                = 55,
    APP_KEY_8                = 56,
    APP_KEY_9                = 57,
    APP_KEY_SEMICOLON        = 59,  /* ; */
    APP_KEY_EQUAL            = 61,  /* = */
    APP_KEY_A                = 65,
    APP_KEY_B                = 66,
    APP_KEY_C                = 67,
    APP_KEY_D                = 68,
    APP_KEY_E                = 69,
    APP_KEY_F                = 70,
    APP_KEY_G                = 71,
    APP_KEY_H                = 72,
    APP_KEY_I                = 73,
    APP_KEY_J                = 74,
    APP_KEY_K                = 75,
    APP_KEY_L                = 76,
    APP_KEY_M                = 77,
    APP_KEY_N                = 78,
    APP_KEY_O                = 79,
    APP_KEY_P                = 80,
    APP_KEY_Q                = 81,
    APP_KEY_R                = 82,
    APP_KEY_S                = 83,
    APP_KEY_T                = 84,
    APP_KEY_U                = 85,
    APP_KEY_V                = 86,
    APP_KEY_W                = 87,
    APP_KEY_X                = 88,
    APP_KEY_Y                = 89,
    APP_KEY_Z                = 90,
    APP_KEY_LEFT_BRACKET     = 91,  /* [ */
    APP_KEY_BACKSLASH        = 92,  /* \ */
    APP_KEY_RIGHT_BRACKET    = 93,  /* ] */
    APP_KEY_GRAVE_ACCENT     = 96,  /* ` */
    APP_KEY_WORLD_1          = 161, /* non-US #1 */
    APP_KEY_WORLD_2          = 162, /* non-US #2 */
    APP_KEY_ESCAPE           = 256,
    APP_KEY_ENTER            = 257,
    APP_KEY_TAB              = 258,
    APP_KEY_BACKSPACE        = 259,
    APP_KEY_INSERT           = 260,
    APP_KEY_DELETE           = 261,
    APP_KEY_RIGHT            = 262,
    APP_KEY_LEFT             = 263,
    APP_KEY_DOWN             = 264,
    APP_KEY_UP               = 265,
    APP_KEY_PAGE_UP          = 266,
    APP_KEY_PAGE_DOWN        = 267,
    APP_KEY_HOME             = 268,
    APP_KEY_END              = 269,
    APP_KEY_CAPS_LOCK        = 280,
    APP_KEY_SCROLL_LOCK      = 281,
    APP_KEY_NUM_LOCK         = 282,
    APP_KEY_PRINT_SCREEN     = 283,
    APP_KEY_PAUSE            = 284,
    APP_KEY_F1               = 290,
    APP_KEY_F2               = 291,
    APP_KEY_F3               = 292,
    APP_KEY_F4               = 293,
    APP_KEY_F5               = 294,
    APP_KEY_F6               = 295,
    APP_KEY_F7               = 296,
    APP_KEY_F8               = 297,
    APP_KEY_F9               = 298,
    APP_KEY_F10              = 299,
    APP_KEY_F11              = 300,
    APP_KEY_F12              = 301,
    APP_KEY_F13              = 302,
    APP_KEY_F14              = 303,
    APP_KEY_F15              = 304,
    APP_KEY_F16              = 305,
    APP_KEY_F17              = 306,
    APP_KEY_F18              = 307,
    APP_KEY_F19              = 308,
    APP_KEY_F20              = 309,
    APP_KEY_F21              = 310,
    APP_KEY_F22              = 311,
    APP_KEY_F23              = 312,
    APP_KEY_F24              = 313,
    APP_KEY_F25              = 314,
    APP_KEY_KP_0             = 320,
    APP_KEY_KP_1             = 321,
    APP_KEY_KP_2             = 322,
    APP_KEY_KP_3             = 323,
    APP_KEY_KP_4             = 324,
    APP_KEY_KP_5             = 325,
    APP_KEY_KP_6             = 326,
    APP_KEY_KP_7             = 327,
    APP_KEY_KP_8             = 328,
    APP_KEY_KP_9             = 329,
    APP_KEY_KP_DECIMAL       = 330,
    APP_KEY_KP_DIVIDE        = 331,
    APP_KEY_KP_MULTIPLY      = 332,
    APP_KEY_KP_SUBTRACT      = 333,
    APP_KEY_KP_ADD           = 334,
    APP_KEY_KP_ENTER         = 335,
    APP_KEY_KP_EQUAL         = 336,
    APP_KEY_LEFT_SHIFT       = 340,
    APP_KEY_LEFT_CONTROL     = 341,
    APP_KEY_LEFT_ALT         = 342,
    APP_KEY_LEFT_SUPER       = 343,
    APP_KEY_RIGHT_SHIFT      = 344,
    APP_KEY_RIGHT_CONTROL    = 345,
    APP_KEY_RIGHT_ALT        = 346,
    APP_KEY_RIGHT_SUPER      = 347,
    APP_KEY_MENU             = 348,
	APP_MAX_KEYCODES,
} APP_KeyCode;


typedef enum {
	APP_MOUSE_BUTTON_LEFT   = 0,
	APP_MOUSE_BUTTON_RIGHT  = 1,
	APP_MOUSE_BUTTON_MIDDLE = 2,
	APP_MOUSE_BUTTON_MAX = 3,
} APP_MouseButton;


typedef enum {
	APP_TOUCH_STATE_INVALID = 0x0,
	APP_TOUCH_STATE_BEGIN   = 0x1,
	APP_TOUCH_STATE_MOVE    = 0x2,
	APP_TOUCH_STATE_END     = 0x4,
	APP_TOUCH_STATE_CANCEL  = 0x8,
} APP_TouchState;


typedef enum {
	APP_STATE_INVALID  = 0x0, // 
	APP_STATE_UP       = 0x1, // is up
	APP_STATE_PRESSED  = 0x2, // changed from up to down
	APP_STATE_DOWN     = 0x4, // is down
	APP_STATE_RELEASED = 0x8, // changed from down to up
	APP_STATE_REPEAT   = 0x10, // The os sended another keyPressed event but was already down
} APP_KeyState;


typedef enum {
	APP_NO_ERROR = 0,
} APP_ErrorCode;


enum {
    // key field
    APP_EVENT_KEY_PRESSED   = 1,
    APP_EVENT_KEY_RELEASED,
    APP_EVENT_KEY_REPEAT,
    
    // text input
    APP_EVENT_TEXT_INPUT,

    // mouse field
    APP_EVENT_MOUSE_MOVE,
    APP_EVENT_MOUSE_BUTTON_PRESSED,
    APP_EVENT_MOUSE_BUTTON_RELEASED,
    APP_EVENT_MOUSE_WHEEL_X,
    APP_EVENT_MOUSE_WHEEL_Y,

    // touch field
    APP_EVENT_TOUCH_BEGIN,
    APP_EVENT_TOUCH_MOVE,
    APP_EVENT_TOUCH_END,
    APP_EVENT_TOUCH_CANCEL,

    // window field
    APP_EVENT_WINDOW_RESIZED,
    APP_EVENT_WINDOW_TO_FULLSCREEN,
    APP_EVENT_WINDOW_TO_WINDOW,
    APP_EVENT_WINDOW_DPI_CHANGED,
    APP_EVENT_WINDOW_FOCUS_LOST,
    APP_EVENT_WINDOW_FOCUS_GAINED,

    // Drag and drop files
    APP_EVENT_FILE_DROPPED,

    // no struct specified
    APP_EVENT_QUIT,
};


typedef struct {
    uint64_t type;
    union {

        struct {
            uint32_t id;
        } key;

        struct {
            char     data[4];
            int32_t  count;
        } text_input;

        struct {
            uint32_t button;
            float    x;
            float    y;
            float    wheel_x;
            float    wheel_y;
        } mouse;

        struct {
            uint32_t id;
            float    x;
            float    y;
        } touch;

        struct {
            int32_t width;
            int32_t height;
            float   dpi_scale;
        } window;

        struct {
            const char *filepath;
        } file_dropped;

    };
} APP_Event;



// Creates a windows based on the description passed.
// This must be called before any window operation.
// To close the window properly, APP_Shutdown must be called
// at the end.
APP_PUBLIC int
APP_Init(const char *title, int width, int height);

// Destroys the window
APP_PUBLIC void
APP_Shutdown(void);



// Gets the window width
APP_PUBLIC int
APP_Get_window_width(void);

// Gets the window height
APP_PUBLIC int
APP_Get_window_height(void);

// Gets if the windows was resized in the last frame
APP_PUBLIC bool
APP_Window_resized(void);



// Gets the state of the key
// if the key is down
//
APP_PUBLIC unsigned char
APP_Get_key_state(APP_KeyCode key);




// Gets a null terminated utf8 string with the last text send by the keyboard
// Puts the size of the array in bytes in the count parameter if isn't a NULL pointer, BUT a NULL
// pointer is allowed 
APP_PUBLIC const char *
APP_Get_text_input(int *count);




// Gets the mouse x position
APP_PUBLIC float
APP_Get_mouse_x(void);

// Gets the mouse y position (up is 0)
APP_PUBLIC float
APP_Get_mouse_y(void);




// Gets if the mouse button is down
APP_PUBLIC unsigned char
APP_Get_mouse_button_state(APP_MouseButton button);





// Gets the mouse wheel delta
APP_PUBLIC float
APP_Get_wheel_x(void);

// Gets the mouse wheel delta
APP_PUBLIC float
APP_Get_wheel_y(void);




// Gets the touch x
APP_PUBLIC float
APP_Get_touch_x(int id);

// Gets the touch y
APP_PUBLIC float
APP_Get_touch_y(int id);

// Gets the touch state
APP_PUBLIC unsigned char
APP_Get_touch_state(int id);


// The pointer is a mix between mouse and touch, tracks the mouse or the touch(0) depending on the
// last interaction

APP_PUBLIC float
APP_Get_pointer_x(void);

APP_PUBLIC float
APP_Get_pointer_y(void);

APP_PUBLIC unsigned char
APP_Get_pointer_state(void);





// Changes to fullscreen or windowed depending if is true (fullscreen) or
// false (windowed).
//
// The resize event will be triggered when this is called.
APP_PUBLIC void
APP_Set_fullscreen(bool enable);


// Shows or hides the mouse depending on the parameter.
// true  = shows
// false = hides
APP_PUBLIC void
APP_Show_mouse(bool show);


// Changes the window title.
APP_PUBLIC void
APP_Set_window_title(const char *title);



APP_PUBLIC const char *
APP_Get_file_dropped_path(void);


APP_PUBLIC bool
APP_Quit_requested(void);


// Changes the swap interval between the backbuffer and the front buffer
// This must be called after Setup_GL
//
// 0 disables the swap interval so unlimits the fps rate.
//
// NOTE: This doesn't have any effect on WASM.
APP_PUBLIC void
APP_Set_swap_interval(unsigned int interval);


APP_PUBLIC void
APP_Swap_buffers(void);



// Runs the application loop, recives a application frame handler
// which will execute every frame.
// If the handler returns 0 the application should continue normally
// If the handler returns 1 the application should finish.
// If the handler returns <0 the application will have an abnormal finish.
//
// Take into account that this function don't does any resource cleanup
// when the handler decides tu finish, all resource cleanup should be
// done inside the frame handler.
//
// This function varies in implementaton between wasm and other platforms
// because in javascript we need to use the frame handler as a event listener.
// Look at platform_wasm.h, WAFN_application_frame().
// 
// The way to call this function from main should be:
//
// int
// main(...) {
//	....
//   return Run_application_loop(My_frame_handler);
// }
APP_PUBLIC int
APP_Run_application_loop(int(*application_frame)(void));



// Returns the nanoseconds passed from the start of the app
APP_PUBLIC int64_t
APP_Time(void);


APP_PUBLIC int64_t
APP_Frame_duration(int samples);


// Tries to sleep the amount of nanoseconds submitted, note that on some platforms it won't be
// that precise or won't work at all (ex: wasm).
APP_PUBLIC void
APP_Try_sleep(int64_t ns);






#if defined (WIN32) || defined (_WIN32)
    /* Windows */
	#define APP_WINDOWS (1)
    #define APIENTRY __stdcall
#elif defined (__wasm__)
	/* Wasm */
	#define APP_WASM (1)
	#define APP__WA_JS(ret, name, args, ...) extern __attribute__((import_module("JS"), import_name(#name "|" #args "|" #__VA_ARGS__))) ret name args;
#elif defined(__linux__) || defined(__unix__)
    /* Linux */
	#define APP_LINUX (1)
    #define APIENTRY 
#endif

#if defined(APP_WASM)
	#define APP__EXPORT(func_name) __attribute__((export_name(#func_name)))
#else
	#define APP__EXPORT(func_name)
#endif

#ifdef __wasm__
    #define APP_ENTRY __attribute__((visibility("default"), export_name("_start")))
#else
    #define APP_ENTRY
#endif

#define __gl_h_ 1
#define __gl32_h_ 1
#define __gl31_h_ 1
#define __GL_H__ 1
#define __glext_h_ 1
#define __GLEXT_H_ 1
#define __gltypes_h_ 1
#define __glcorearb_h_ 1
#define __gl_glcorearb_h_ 1
#define GL_APIENTRY APIENTRY

typedef unsigned int  GLenum;
typedef unsigned int  GLuint;
typedef int  GLsizei;
typedef char  GLchar;
typedef intptr_t  GLintptr;
typedef intptr_t GLsizeiptr;
typedef double  GLclampd;
typedef unsigned short  GLushort;
typedef unsigned char  GLubyte;
typedef unsigned char  GLboolean;
typedef uint64_t  GLuint64;
typedef double  GLdouble;
typedef unsigned short  GLhalf;
typedef float  GLclampf;
typedef unsigned int  GLbitfield;
typedef signed char  GLbyte;
typedef short  GLshort;
typedef void  GLvoid;
typedef int64_t  GLint64;
typedef float  GLfloat;
typedef struct __GLsync * GLsync;
typedef int  GLint;
#define GL_INT_2_10_10_10_REV 0x8D9F
#define GL_R32F 0x822E
#define GL_PROGRAM_POINT_SIZE 0x8642
#define GL_STENCIL_ATTACHMENT 0x8D20
#define GL_DEPTH_ATTACHMENT 0x8D00
#define GL_COLOR_ATTACHMENT2 0x8CE2
#define GL_COLOR_ATTACHMENT0 0x8CE0
#define GL_R16F 0x822D
#define GL_COLOR_ATTACHMENT22 0x8CF6
#define GL_DRAW_FRAMEBUFFER 0x8CA9
#define GL_FRAMEBUFFER_COMPLETE 0x8CD5
#define GL_NUM_EXTENSIONS 0x821D
#define GL_INFO_LOG_LENGTH 0x8B84
#define GL_VERTEX_SHADER 0x8B31
#define GL_INCR 0x1E02
#define GL_DYNAMIC_DRAW 0x88E8
#define GL_STATIC_DRAW 0x88E4
#define GL_TEXTURE_CUBE_MAP_POSITIVE_Z 0x8519
#define GL_TEXTURE_CUBE_MAP 0x8513
#define GL_FUNC_SUBTRACT 0x800A
#define GL_FUNC_REVERSE_SUBTRACT 0x800B
#define GL_CONSTANT_COLOR 0x8001
#define GL_DECR_WRAP 0x8508
#define GL_LINEAR_MIPMAP_LINEAR 0x2703
#define GL_ELEMENT_ARRAY_BUFFER 0x8893
#define GL_SHORT 0x1402
#define GL_DEPTH_TEST 0x0B71
#define GL_TEXTURE_CUBE_MAP_NEGATIVE_Y 0x8518
#define GL_LINK_STATUS 0x8B82
#define GL_TEXTURE_CUBE_MAP_POSITIVE_Y 0x8517
#define GL_SAMPLE_ALPHA_TO_COVERAGE 0x809E
#define GL_RGBA16F 0x881A
#define GL_CONSTANT_ALPHA 0x8003
#define GL_READ_FRAMEBUFFER 0x8CA8
#define GL_TEXTURE0 0x84C0
#define GL_TEXTURE_MIN_LOD 0x813A
#define GL_CLAMP_TO_EDGE 0x812F
#define GL_UNSIGNED_SHORT_5_6_5 0x8363
#define GL_TEXTURE_WRAP_R 0x8072
#define GL_UNSIGNED_SHORT_5_5_5_1 0x8034
#define GL_NEAREST_MIPMAP_NEAREST 0x2700
#define GL_UNSIGNED_SHORT_4_4_4_4 0x8033
#define GL_SRC_ALPHA_SATURATE 0x0308
#define GL_STREAM_DRAW 0x88E0
#define GL_ONE 1
#define GL_NEAREST_MIPMAP_LINEAR 0x2702
#define GL_RGB10_A2 0x8059
#define GL_RGBA8 0x8058
#define GL_COLOR_ATTACHMENT1 0x8CE1
#define GL_RGBA4 0x8056
#define GL_RGB8 0x8051
#define GL_ARRAY_BUFFER 0x8892
#define GL_STENCIL 0x1802
#define GL_TEXTURE_2D 0x0DE1
#define GL_DEPTH 0x1801
#define GL_FRONT 0x0404
#define GL_STENCIL_BUFFER_BIT 0x00000400
#define GL_REPEAT 0x2901
#define GL_RGBA 0x1908
#define GL_TEXTURE_CUBE_MAP_POSITIVE_X 0x8515
#define GL_DECR 0x1E03
#define GL_FRAGMENT_SHADER 0x8B30
#define GL_FLOAT 0x1406
#define GL_TEXTURE_MAX_LOD 0x813B
#define GL_DEPTH_COMPONENT 0x1902
#define GL_ONE_MINUS_DST_ALPHA 0x0305
#define GL_COLOR 0x1800
#define GL_TEXTURE_2D_ARRAY 0x8C1A
#define GL_TRIANGLES 0x0004
#define GL_UNSIGNED_BYTE 0x1401
#define GL_TEXTURE_MAG_FILTER 0x2800
#define GL_ONE_MINUS_CONSTANT_ALPHA 0x8004
#define GL_NONE 0
#define GL_SRC_COLOR 0x0300
#define GL_BYTE 0x1400
#define GL_TEXTURE_CUBE_MAP_NEGATIVE_Z 0x851A
#define GL_LINE_STRIP 0x0003
#define GL_TEXTURE_3D 0x806F
#define GL_CW 0x0900
#define GL_LINEAR 0x2601
#define GL_RENDERBUFFER 0x8D41
#define GL_GEQUAL 0x0206
#define GL_COLOR_BUFFER_BIT 0x00004000
#define GL_RGBA32F 0x8814
#define GL_BLEND 0x0BE2
#define GL_ONE_MINUS_SRC_ALPHA 0x0303
#define GL_ONE_MINUS_CONSTANT_COLOR 0x8002
#define GL_TEXTURE_WRAP_T 0x2803
#define GL_TEXTURE_WRAP_S 0x2802
#define GL_TEXTURE_MIN_FILTER 0x2801
#define GL_LINEAR_MIPMAP_NEAREST 0x2701
#define GL_EXTENSIONS 0x1F03
#define GL_NO_ERROR 0
#define GL_REPLACE 0x1E01
#define GL_KEEP 0x1E00
#define GL_CCW 0x0901
#define GL_TEXTURE_CUBE_MAP_NEGATIVE_X 0x8516
#define GL_RGB 0x1907
#define GL_TRIANGLE_STRIP 0x0005
#define GL_TRIANGLE_FAN 0x0006
#define GL_FALSE 0
#define GL_ZERO 0
#define GL_CULL_FACE 0x0B44
#define GL_INVERT 0x150A
#define GL_INT 0x1404
#define GL_UNSIGNED_INT 0x1405
#define GL_UNSIGNED_SHORT 0x1403
#define GL_NEAREST 0x2600
#define GL_SCISSOR_TEST 0x0C11
#define GL_LEQUAL 0x0203
#define GL_STENCIL_TEST 0x0B90
#define GL_DITHER 0x0BD0
#define GL_DEPTH_COMPONENT16 0x81A5
#define GL_EQUAL 0x0202
#define GL_FRAMEBUFFER 0x8D40
#define GL_RGB5 0x8050
#define GL_LINES 0x0001
#define GL_DEPTH_BUFFER_BIT 0x00000100
#define GL_SRC_ALPHA 0x0302
#define GL_INCR_WRAP 0x8507
#define GL_LESS 0x0201
#define GL_MULTISAMPLE 0x809D
#define GL_FRAMEBUFFER_BINDING 0x8CA6
#define GL_BACK 0x0405
#define GL_ALWAYS 0x0207
#define GL_FUNC_ADD 0x8006
#define GL_ONE_MINUS_DST_COLOR 0x0307
#define GL_NOTEQUAL 0x0205
#define GL_DST_COLOR 0x0306
#define GL_COMPILE_STATUS 0x8B81
#define GL_RED 0x1903
#define GL_COLOR_ATTACHMENT3 0x8CE3
#define GL_DST_ALPHA 0x0304
#define GL_RGB5_A1 0x8057
#define GL_GREATER 0x0204
#define GL_POLYGON_OFFSET_FILL 0x8037
#define GL_TRUE 1
#define GL_NEVER 0x0200
#define GL_POINTS 0x0000
#define GL_ONE_MINUS_SRC_COLOR 0x0301
#define GL_MIRRORED_REPEAT 0x8370
#define GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS 0x8B4D
#define GL_R11F_G11F_B10F 0x8C3A
#define GL_UNSIGNED_INT_10F_11F_11F_REV 0x8C3B
#define GL_RGBA32UI 0x8D70
#define GL_RGB32UI 0x8D71
#define GL_RGBA16UI 0x8D76
#define GL_RGB16UI 0x8D77
#define GL_RGBA8UI 0x8D7C
#define GL_RGB8UI 0x8D7D
#define GL_RGBA32I 0x8D82
#define GL_RGB32I 0x8D83
#define GL_RGBA16I 0x8D88
#define GL_RGB16I 0x8D89
#define GL_RGBA8I 0x8D8E
#define GL_RGB8I 0x8D8F
#define GL_RED_INTEGER 0x8D94
#define GL_RG 0x8227
#define GL_RG_INTEGER 0x8228
#define GL_R8 0x8229 
#define GL_R16 0x822A
#define GL_RG8 0x822B
#define GL_RG16 0x822C
#define GL_R16F 0x822D
#define GL_R32F 0x822E
#define GL_RG16F 0x822F
#define GL_RG32F 0x8230
#define GL_R8I 0x8231
#define GL_R8UI 0x8232
#define GL_R16I 0x8233
#define GL_R16UI 0x8234
#define GL_R32I 0x8235
#define GL_R32UI 0x8236
#define GL_RG8I 0x8237
#define GL_RG8UI 0x8238
#define GL_RG16I 0x8239
#define GL_RG16UI 0x823A
#define GL_RG32I 0x823B
#define GL_RG32UI 0x823C
#define GL_RGBA_INTEGER 0x8D99
#define GL_R8_SNORM 0x8F94
#define GL_RG8_SNORM 0x8F95
#define GL_RGB8_SNORM 0x8F96
#define GL_RGBA8_SNORM 0x8F97
#define GL_R16_SNORM 0x8F98
#define GL_RG16_SNORM 0x8F99
#define GL_RGB16_SNORM 0x8F9A
#define GL_RGBA16_SNORM 0x8F9B
#define GL_RGBA16 0x805B
#define GL_MAX_TEXTURE_SIZE 0x0D33
#define GL_MAX_CUBE_MAP_TEXTURE_SIZE 0x851C
#define GL_MAX_3D_TEXTURE_SIZE 0x8073
#define GL_MAX_ARRAY_TEXTURE_LAYERS 0x88FF
#define GL_MAX_VERTEX_ATTRIBS 0x8869
#define GL_CLAMP_TO_BORDER 0x812D
#define GL_TEXTURE_BORDER_COLOR 0x1004
#define GL_CURRENT_PROGRAM 0x8B8D
#define GL_MAX_VERTEX_UNIFORM_VECTORS 0x8DFB
#define GL_UNPACK_ALIGNMENT 0x0CF5
#define GL_PACK_ALIGNMENT 0x0D05

#define APP__GL_FUNCS \
    APP__GL_XMACRO(glBindVertexArray,                 void, (GLuint array)) \
    APP__GL_XMACRO(glFramebufferTextureLayer,         void, (GLenum target, GLenum attachment, GLuint texture, GLint level, GLint layer)) \
    APP__GL_XMACRO(glGenFramebuffers,                 void, (GLsizei n, GLuint * framebuffers)) \
    APP__GL_XMACRO(glBindFramebuffer,                 void, (GLenum target, GLuint framebuffer)) \
    APP__GL_XMACRO(glBindRenderbuffer,                void, (GLenum target, GLuint renderbuffer)) \
    APP__GL_XMACRO(glGetStringi,                      const GLubyte *, (GLenum name, GLuint index)) \
    APP__GL_XMACRO(glClearBufferfi,                   void, (GLenum buffer, GLint drawbuffer, GLfloat depth, GLint stencil)) \
    APP__GL_XMACRO(glClearBufferfv,                   void, (GLenum buffer, GLint drawbuffer, const GLfloat * value)) \
    APP__GL_XMACRO(glClearBufferuiv,                  void, (GLenum buffer, GLint drawbuffer, const GLuint * value)) \
    APP__GL_XMACRO(glClearBufferiv,                   void, (GLenum buffer, GLint drawbuffer, const GLint * value)) \
    APP__GL_XMACRO(glDeleteRenderbuffers,             void, (GLsizei n, const GLuint * renderbuffers)) \
    APP__GL_XMACRO(glUniform1i,                       void, (GLint location, GLint v0)) \
    APP__GL_XMACRO(glUniform2i,                       void, (GLint location, GLint v0, GLint v1)) \
    APP__GL_XMACRO(glUniform1f,                       void, (GLint location, GLfloat v0)) \
    APP__GL_XMACRO(glUniform2f,                       void, (GLint location, GLfloat v0, GLfloat v1)) \
    APP__GL_XMACRO(glUniform3f,                       void, (GLint location, GLfloat v0, GLfloat v1, GLfloat v2)) \
    APP__GL_XMACRO(glUniform4f,                       void, (GLint location, const GLfloat v0, const GLfloat v1, const GLfloat v2, const GLfloat v3)) \
    APP__GL_XMACRO(glUniform1fv,                      void, (GLint location, GLsizei count, const GLfloat * value)) \
    APP__GL_XMACRO(glUniform2fv,                      void, (GLint location, GLsizei count, const GLfloat * value)) \
    APP__GL_XMACRO(glUniform3fv,                      void, (GLint location, GLsizei count, const GLfloat * value)) \
    APP__GL_XMACRO(glUniform4fv,                      void, (GLint location, GLsizei count, const GLfloat * value)) \
    APP__GL_XMACRO(glUniformMatrix4fv,                void, (GLint location, GLsizei count, GLboolean transpose, const GLfloat * value)) \
    APP__GL_XMACRO(glUseProgram,                      void, (GLuint program)) \
    APP__GL_XMACRO(glShaderSource,                    void, (GLuint shader, GLsizei count, const GLchar *const* string, const GLint * length)) \
    APP__GL_XMACRO(glLinkProgram,                     void, (GLuint program)) \
    APP__GL_XMACRO(glGetUniformLocation,              GLint, (GLuint program, const GLchar * name)) \
    APP__GL_XMACRO(glGetShaderiv,                     void, (GLuint shader, GLenum pname, GLint * params)) \
    APP__GL_XMACRO(glGetProgramInfoLog,               void, (GLuint program, GLsizei bufSize, GLsizei * length, GLchar * infoLog)) \
    APP__GL_XMACRO(glGetAttribLocation,               GLint, (GLuint program, const GLchar * name)) \
    APP__GL_XMACRO(glDisableVertexAttribArray,        void, (GLuint index)) \
    APP__GL_XMACRO(glDeleteShader,                    void, (GLuint shader)) \
    APP__GL_XMACRO(glDeleteProgram,                   void, (GLuint program)) \
    APP__GL_XMACRO(glCompileShader,                   void, (GLuint shader)) \
    APP__GL_XMACRO(glStencilFuncSeparate,             void, (GLenum face, GLenum func, GLint ref, GLuint mask)) \
    APP__GL_XMACRO(glStencilOpSeparate,               void, (GLenum face, GLenum sfail, GLenum dpfail, GLenum dppass)) \
    APP__GL_XMACRO(glRenderbufferStorageMultisample,  void, (GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height)) \
    APP__GL_XMACRO(glDrawBuffers,                     void, (GLsizei n, const GLenum * bufs)) \
    APP__GL_XMACRO(glVertexAttribDivisor,             void, (GLuint index, GLuint divisor)) \
    APP__GL_XMACRO(glBufferSubData,                   void, (GLenum target, GLintptr offset, GLsizeiptr size, const void * data)) \
    APP__GL_XMACRO(glGenBuffers,                      void, (GLsizei n, GLuint * buffers)) \
    APP__GL_XMACRO(glCheckFramebufferStatus,          GLenum, (GLenum target)) \
    APP__GL_XMACRO(glFramebufferRenderbuffer,         void, (GLenum target, GLenum attachment, GLenum renderbuffertarget, GLuint renderbuffer)) \
    APP__GL_XMACRO(glCompressedTexImage2D,            void, (GLenum target, GLint level, GLenum internalformat, GLsizei width, GLsizei height, GLint border, GLsizei imageSize, const void * data)) \
    APP__GL_XMACRO(glCompressedTexImage3D,            void, (GLenum target, GLint level, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth, GLint border, GLsizei imageSize, const void * data)) \
    APP__GL_XMACRO(glActiveTexture,                   void, (GLenum texture)) \
    APP__GL_XMACRO(glTexSubImage2D,                   void, (GLenum target, GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, GLenum type, const void * pixels)) \
    APP__GL_XMACRO(glTexSubImage3D,                   void, (GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLenum type, const void * pixels)) \
    APP__GL_XMACRO(glRenderbufferStorage,             void, (GLenum target, GLenum internalformat, GLsizei width, GLsizei height)) \
    APP__GL_XMACRO(glGenerateMipmap,                  void, (GLenum target)) \
    APP__GL_XMACRO(glGenTextures,                     void, (GLsizei n, GLuint * textures)) \
    APP__GL_XMACRO(glPolygonOffset,                   void, (GLfloat factor, GLfloat units)) \
    APP__GL_XMACRO(glDrawElements,                    void, (GLenum mode, GLsizei count, GLenum type, const void * indices)) \
    APP__GL_XMACRO(glDeleteFramebuffers,              void, (GLsizei n, const GLuint * framebuffers)) \
    APP__GL_XMACRO(glBlendEquationSeparate,           void, (GLenum modeRGB, GLenum modeAlpha)) \
    APP__GL_XMACRO(glDeleteTextures,                  void, (GLsizei n, const GLuint * textures)) \
    APP__GL_XMACRO(glGetProgramiv,                    void, (GLuint program, GLenum pname, GLint * params)) \
    APP__GL_XMACRO(glBindTexture,                     void, (GLenum target, GLuint texture)) \
    APP__GL_XMACRO(glTexImage3D,                      void, (GLenum target, GLint level, GLint internalformat, GLsizei width, GLsizei height, GLsizei depth, GLint border, GLenum format, GLenum type, const void * pixels)) \
    APP__GL_XMACRO(glCreateShader,                    GLuint, (GLenum type)) \
    APP__GL_XMACRO(glClearDepth,                      void, (GLdouble depth)) \
    APP__GL_XMACRO(glFramebufferTexture2D,            void, (GLenum target, GLenum attachment, GLenum textarget, GLuint texture, GLint level)) \
    APP__GL_XMACRO(glCreateProgram,                   GLuint, (void)) \
    APP__GL_XMACRO(glViewport,                        void, (GLint x, GLint y, GLsizei width, GLsizei height)) \
    APP__GL_XMACRO(glDeleteBuffers,                   void, (GLsizei n, const GLuint * buffers)) \
    APP__GL_XMACRO(glDrawArrays,                      void, (GLenum mode, GLint first, GLsizei count)) \
    APP__GL_XMACRO(glDrawElementsInstanced,           void, (GLenum mode, GLsizei count, GLenum type, const void * indices, GLsizei instancecount)) \
    APP__GL_XMACRO(glVertexAttribPointer,             void, (GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const void * pointer)) \
    APP__GL_XMACRO(glDisable,                         void, (GLenum cap)) \
    APP__GL_XMACRO(glColorMask,                       void, (GLboolean red, GLboolean green, GLboolean blue, GLboolean alpha)) \
    APP__GL_XMACRO(glColorMaski,                      void, (GLuint buf, GLboolean red, GLboolean green, GLboolean blue, GLboolean alpha)) \
    APP__GL_XMACRO(glBindBuffer,                      void, (GLenum target, GLuint buffer)) \
    APP__GL_XMACRO(glDeleteVertexArrays,              void, (GLsizei n, const GLuint * arrays)) \
    APP__GL_XMACRO(glDepthMask,                       void, (GLboolean flag)) \
    APP__GL_XMACRO(glDrawArraysInstanced,             void, (GLenum mode, GLint first, GLsizei count, GLsizei instancecount)) \
    APP__GL_XMACRO(glClearStencil,                    void, (GLint s)) \
    APP__GL_XMACRO(glScissor,                         void, (GLint x, GLint y, GLsizei width, GLsizei height)) \
    APP__GL_XMACRO(glGenRenderbuffers,                void, (GLsizei n, GLuint * renderbuffers)) \
    APP__GL_XMACRO(glBufferData,                      void, (GLenum target, GLsizeiptr size, const void * data, GLenum usage)) \
    APP__GL_XMACRO(glBlendFuncSeparate,               void, (GLenum sfactorRGB, GLenum dfactorRGB, GLenum sfactorAlpha, GLenum dfactorAlpha)) \
    APP__GL_XMACRO(glTexParameteri,                   void, (GLenum target, GLenum pname, GLint param)) \
    APP__GL_XMACRO(glGetIntegerv,                     void, (GLenum pname, GLint * data)) \
    APP__GL_XMACRO(glEnable,                          void, (GLenum cap)) \
    APP__GL_XMACRO(glBlitFramebuffer,                 void, (GLint srcX0, GLint srcY0, GLint srcX1, GLint srcY1, GLint dstX0, GLint dstY0, GLint dstX1, GLint dstY1, GLbitfield mask, GLenum filter)) \
    APP__GL_XMACRO(glStencilMask,                     void, (GLuint mask)) \
    APP__GL_XMACRO(glAttachShader,                    void, (GLuint program, GLuint shader)) \
    APP__GL_XMACRO(glDetachShader,                    void, (GLuint program, GLuint shader)) \
    APP__GL_XMACRO(glGetError,                        GLenum, (void)) \
    APP__GL_XMACRO(glClearColor,                      void, (GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha)) \
    APP__GL_XMACRO(glBlendColor,                      void, (GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha)) \
    APP__GL_XMACRO(glTexParameterf,                   void, (GLenum target, GLenum pname, GLfloat param)) \
    APP__GL_XMACRO(glTexParameterfv,                  void, (GLenum target, GLenum pname, GLfloat* params)) \
    APP__GL_XMACRO(glGetShaderInfoLog,                void, (GLuint shader, GLsizei bufSize, GLsizei * length, GLchar * infoLog)) \
    APP__GL_XMACRO(glDepthFunc,                       void, (GLenum func)) \
    APP__GL_XMACRO(glStencilOp ,                      void, (GLenum fail, GLenum zfail, GLenum zpass)) \
    APP__GL_XMACRO(glStencilFunc,                     void, (GLenum func, GLint ref, GLuint mask)) \
    APP__GL_XMACRO(glEnableVertexAttribArray,         void, (GLuint index)) \
    APP__GL_XMACRO(glBlendFunc,                       void, (GLenum sfactor, GLenum dfactor)) \
    APP__GL_XMACRO(glReadBuffer,                      void, (GLenum src)) \
    APP__GL_XMACRO(glClear,                           void, (GLbitfield mask)) \
    APP__GL_XMACRO(glTexImage2D,                      void, (GLenum target, GLint level, GLint internalformat, GLsizei width, GLsizei height, GLint border, GLenum format, GLenum type, const void * pixels)) \
    APP__GL_XMACRO(glGenVertexArrays,                 void, (GLsizei n, GLuint * arrays)) \
    APP__GL_XMACRO(glFrontFace,                       void, (GLenum mode)) \
    APP__GL_XMACRO(glCullFace,                        void, (GLenum mode)) \
    APP__GL_XMACRO(glFinish,                          void, (void)) \
	APP__GL_XMACRO(glPixelStorei,                     void, (GLenum pname, GLint param))


#if defined(WIN32) || defined(APP_LINUX)
	// generate GL function pointer typedefs
	#define APP__GL_XMACRO(name, ret, args) typedef ret (GL_APIENTRY* PFN_ ## name) args;
	APP__GL_FUNCS
	#undef APP__GL_XMACRO
	
	// generate GL extern function pointers
	#define APP__GL_XMACRO(name, ret, args) extern PFN_ ## name name;
	APP__GL_FUNCS
	#undef APP__GL_XMACRO

#elif defined(APP_WASM)

	// This doesn't exist on webgl
	void glBindVertexArray(GLuint array) {}

//    APP__WA_JS(void, glFramebufferTextureLayer, (GLenum target, GLenum attachment, GLuint texture, GLint level, GLint layer), {
//
//	});

    APP__WA_JS(void, glGenFramebuffers, (GLsizei n, GLuint * framebuffers), {
		checkHeap();
		for (var i = 0; i < n; ++i) {
			var framebuffer = Module.GLctx.createFramebuffer();
			if (!framebuffer)
			{
				Module.GLrecordError(0x0502); // GL_INVALID_OPERATION
				while(i < n) HEAP32[(((ids)+(i++*4))>>2)]=0;
				return;
			}
			var id = Module.GLgetNewId(GLframebuffers);
			framebuffer.name = id;
			Module.GLframebuffers[id] = framebuffer;
			HEAP32[(((framebuffers)+(i*4))>>2)] = id;
		}
	})

    APP__WA_JS(void, glBindFramebuffer, (GLenum target, GLuint framebuffer), {
		Module.GLctx.bindFramebuffer(target, framebuffer ? Module.GLframebuffers[framebuffer] : null);
	})

//    APP__WA_JS(void, glBindRenderbuffer, (GLenum target, GLuint renderbuffer), {
//
//	});

//    APP__WA_JS(const GLubyte *, glGetStringi, (GLenum name, GLuint index), {
//
//	});

//    APP__WA_JS(void, glClearBufferfi, (GLenum buffer, GLint drawbuffer, GLfloat depth, GLint stencil), {
//
//	});

//    APP__WA_JS(void, glClearBufferfv, (GLenum buffer, GLint drawbuffer, const GLfloat * value), {
//
//	});
//
//    APP__WA_JS(void, glClearBufferuiv, (GLenum buffer, GLint drawbuffer, const GLuint * value), {
//
//	});
//
//    APP__WA_JS(void, glClearBufferiv, (GLenum buffer, GLint drawbuffer, const GLint * value), {
//
//	});

//    APP__WA_JS(void, glDeleteRenderbuffers,(GLsizei n, const GLuint * renderbuffers), {
//
//	});

    APP__WA_JS(void, glUniform1i, (GLint location, GLint v0), {
		Module.GLctx.uniform1i(Module.GLuniforms[location], v0);
	})

    APP__WA_JS(void, glUniform2i, (GLint location, GLint v0, GLint v1), {
		Module.GLctx.uniform2i(Module.GLuniforms[location], v0, v1);
	})

    APP__WA_JS(void, glUniform1f, (GLint location, GLfloat v0), {
		Module.GLctx.uniform1f(Module.GLuniforms[location], v0);
	})

    APP__WA_JS(void, glUniform2f, (GLint location, GLfloat v0, GLfloat v1), {
		Module.GLctx.uniform2f(Module.GLuniforms[location], v0, v1);
	})

    APP__WA_JS(void, glUniform3f, (GLint location, GLfloat v0, GLfloat v1, GLfloat v2), {
		Module.GLctx.uniform3f(Module.GLuniforms[location], v0, v1, v2);
	})

    APP__WA_JS(void, glUniform4f, (GLint location, const GLfloat v0, const GLfloat v1, const GLfloat v2, const GLfloat v3), {
		Module.GLctx.uniform4f(Module.GLuniforms[loccation], v0, v1, v2, v3);
	})

//    APP__WA_JS(void, glUniform1fv, (GLint location, GLsizei count, const GLfloat * value), {
//
//	});
//
//    APP__WA_JS(void, glUniform2fv, (GLint location, GLsizei count, const GLfloat * value), {
//
//	});

    APP__WA_JS(void, glUniform3fv, (GLint location, GLsizei count, const GLfloat * value), {
		checkHeap();
		var view;
		if (3*count <= Module.GLMINI_TEMP_BUFFER_SIZE) {
			// avoid allocation when uploading few enough uniforms
			view = Module.GLminiTempBufferViews[3*count-1];
			for (var ptr = value>>2, i = 0; i != 3*count; i++) {
				view[i] = HEAPF32[ptr+i];
			}
		}
		else view = HEAPF32.subarray((value)>>2,(value+count*12)>>2);
		Module.GLctx.uniform3fv(Module.GLuniforms[location], view);
	})

//    APP__WA_JS(void, glUniform4fv, (GLint location, GLsizei count, const GLfloat * value), {
//
//	});

    APP__WA_JS(void, glUniformMatrix4fv, (GLint location, GLsizei count, GLboolean transpose, const GLfloat * value), {
		checkHeap();
		count<<=4;
		var view;
		if (count <= Module.GLMINI_TEMP_BUFFER_SIZE)
		{
			// avoid allocation when uploading few enough uniforms
			view = Module.GLminiTempBufferViews[count-1];
			for (var ptr = value>>2, i = 0; i != count; i += 4)
			{
				view[i  ] = HEAPF32[ptr+i  ];
				view[i+1] = HEAPF32[ptr+i+1];
				view[i+2] = HEAPF32[ptr+i+2];
				view[i+3] = HEAPF32[ptr+i+3];
			}
		}
		else view = HEAPF32.subarray((value)>>2,(value+count*4)>>2);
		Module.GLctx.uniformMatrix4fv(Module.GLuniforms[location], !!transpose, view);
	})

    APP__WA_JS(void, glUseProgram, (GLuint program), {
		Module.GLctx.useProgram(program ? Module.GLprograms[program] : null);
	})

    APP__WA_JS(void, glShaderSource, (GLuint shader, GLsizei count, const GLchar *const* string, const GLint * length), {
		checkHeap();
		var source = Module.GLgetSource(shader, count, string, length);
		Module.GLctx.shaderSource(Module.GLshaders[shader], source);
	})
			
    APP__WA_JS(void, glLinkProgram, (GLuint program), {
		Module.GLctx.linkProgram(Module.GLprograms[program]);
		Module.GLprogramInfos[program] = null; // uniforms no longer keep the same names after linking
		Module.GLpopulateUniformTable(program);;
	})

    APP__WA_JS(GLint, glGetUniformLocation, (GLuint program, const GLchar * name), {
		checkHeap();
		name = Pointer_stringify(name);

		var arrayOffset = 0;
		if (name.indexOf(']', name.length-1) !== -1) {
			// If user passed an array accessor "[index]", parse the array index off the accessor.
			var ls = name.lastIndexOf('[');
			var arrayIndex = name.slice(ls+1, -1);
			if (arrayIndex.length > 0)
			{
				arrayOffset = parseInt(arrayIndex);
				if (arrayOffset < 0) return -1;
			}
			name = name.slice(0, ls);
		}

		var ptable = Module.GLprogramInfos[program];
		if (!ptable) return -1;
		var utable = ptable.uniforms;
		var uniformInfo = utable[name]; // returns pair [ dimension_of_uniform_array, uniform_location ]
		if (uniformInfo && arrayOffset < uniformInfo[0]) {
			// Check if user asked for an out-of-bounds element, i.e. for 'vec4 colors[3];' user could ask for 'colors[10]' which should return -1.
			return uniformInfo[1] + arrayOffset;
		}
		return -1;
	})

    APP__WA_JS(void, glGetShaderiv, (GLuint shader, GLenum pname, GLint * params), {
		checkHeap();
		if (!params) {
			// GLES2 specification does not specify how to behave if params is a null pointer. Since calling this function does not make sense
			// if params == null, issue a GL error to notify user about it.
			Module.GLrecordError(0x0501); // GL_INVALID_VALUE
			return;
		}
		if (pname == 0x8B84) { // GL_INFO_LOG_LENGTH
			var log = Module.GLctx.getShaderInfoLog(Module.GLshaders[shader]);
			if (log === null) log = '(unknown error)';
			HEAP32[((params)>>2)] = log.length + 1;
		}
		else if (pname == 0x8B88) { // GL_SHADER_SOURCE_LENGTH
			var source = Module.GLctx.getShaderSource(Module.GLshaders[shader]);
			var sourceLength = (source === null || source.length == 0) ? 0 : source.length + 1;
			HEAP32[((params)>>2)] = sourceLength;
		}
		else HEAP32[((params)>>2)] = Module.GLctx.getShaderParameter(Module.GLshaders[shader], pname);
	})

    APP__WA_JS(void, glGetProgramInfoLog, (GLuint program, GLsizei bufSize, GLsizei * length, GLchar * infoLog), {
		checkHeap();
		var log = Module.GLctx.getProgramInfoLog(Module.GLprograms[program]);
		if (log === null) log = '(unknown error)';
		if (bufSize > 0 && infoLog)
		{
			var numBytesWrittenExclNull = stringToUTF8Array(log, infoLog, bufSize);
			if (length) HEAP32[((length)>>2)]=numBytesWrittenExclNull;
		}
		else if (length) HEAP32[((length)>>2)]=0;
	})

    APP__WA_JS(GLint, glGetAttribLocation, (GLuint program, const GLchar * name), {
		checkHeap();
		program = Module.GLprograms[program];
		name = Pointer_stringify(name);
		return Module.GLctx.getAttribLocation(program, name);
	})

    APP__WA_JS(void, glDisableVertexAttribArray, (GLuint index), {
		Module.GLctx.disableVertexAttribArray(index);
	})

    APP__WA_JS(void, glDeleteShader, (GLuint shader), {
		if (!shader) return;
		var shader_object = Module.GLshaders[shader];
		if (!shader_object)
		{
			// glDeleteShader actually signals an error when deleting a nonexisting object, unlike some other GL delete functions.
			Module.GLrecordError(0x0501); // GL_INVALID_VALUE
			return;
		}
		Module.GLctx.deleteShader(shader_object);
		Module.GLshaders[shader] = null;
	})

    APP__WA_JS(void, glDeleteProgram, (GLuint program), {
		if (!program) return;
		var program_object = Module.GLprograms[program];
		if (!program_object) 
		{
			// glDeleteProgram actually signals an error when deleting a nonexisting object, unlike some other GL delete functions.
			Module.GLrecordError(0x0501); // GL_INVALID_VALUE
			return;
		}
		Module.GLctx.deleteProgram(program_object);
		program_object.name = 0;
		Module.GLprograms[program] = null;
		Module.GLprogramInfos[program] = null;
	})

    APP__WA_JS(void, glCompileShader, (GLuint shader), {
		Module.GLctx.compileShader(Module.GLshaders[shader]);
	})

//    APP__WA_JS(void, glStencilFuncSeparate, (GLenum face, GLenum func, GLint ref, GLuint mask), {
//
//	});
//
//    APP__WA_JS(void, glStencilOpSeparate, (GLenum face, GLenum sfail, GLenum dpfail, GLenum dppass), {
//
//	});

//    APP__WA_JS(void, glRenderbufferStorageMultisample, (GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height), {
//
//	});

//    APP__WA_JS(void, glDrawBuffers, (GLsizei n, const GLenum * bufs), {
//
//	});

//    APP__WA_JS(void, glVertexAttribDivisor, (GLuint index, GLuint divisor), {
//
//	});

    APP__WA_JS(void, glBufferSubData, (GLenum target, GLintptr offset, GLsizeiptr size, const void * data), {
		checkHeap();
		Module.GLctx.bufferSubData(target, offset, HEAPU8.subarray(data, data+size));
	})

    APP__WA_JS(void, glGenBuffers, (GLsizei n, GLuint *buffers), {
		checkHeap();
		for (var i = 0; i < n; i++) {
			var buffer = Module.GLctx.createBuffer();
			if (!buffer) {
				Module.GLrecordError(0x0502); // GL_INVALID_OPERATION
				while(i < n) HEAP32[(((buffers)+(i++*4))>>2)]=0;
				return;
			}
			var id = Module.GLgetNewId(Module.GLbuffers);
			buffer.name = id;
			Module.GLbuffers[id] = buffer;
			HEAP32[(((buffers)+(i*4))>>2)]=id;
		}
	})

//    APP__WA_JS(GLenum, glCheckFramebufferStatus, (GLenum target), {
//
//	});

//    APP__WA_JS(void, glFramebufferRenderbuffer, (GLenum target, GLenum attachment, GLenum renderbuffertarget, GLuint renderbuffer), {
//
//	});

//    APP__WA_JS(void, glCompressedTexImage2D, (GLenum target, GLint level, GLenum internalformat, GLsizei width, GLsizei height, GLint border, GLsizei imageSize, const void * data), {
//
//	});
//
//    APP__WA_JS(void, glCompressedTexImage3D, (GLenum target, GLint level, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth, GLint border, GLsizei imageSize, const void * data), {
//
//	});

    APP__WA_JS(void, glGenerateMipmap, (GLenum target), {
		Module.GLctx.generateMipmap(target);
	})

    APP__WA_JS(void, glActiveTexture, (GLenum texture), {
		Module.GLctx.activeTexture(texture);
	})

    APP__WA_JS(void, glTexSubImage2D, (GLenum target, GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, GLenum type, const void * pixels), {
		checkHeap();
		var pixelData = null;
		if (pixels) pixelData = Module.webGLGetTexPixelData(type, format, width, height, pixels, 0);
		Module.GLctx.texSubImage2D(target, level, xoffset, yoffset, width, height, format, type, pixelData);
	})

//    APP__WA_JS(void, glTexSubImage3D, (GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLenum type, const void * pixels), {
//
//	});

//    APP__WA_JS(void, glRenderbufferStorage, (GLenum target, GLenum internalformat, GLsizei width, GLsizei height), {
//
//	});

    APP__WA_JS(void, glGenTextures, (GLsizei n, GLuint * textures), {
		checkHeap();
		for (var i = 0; i < n; i++) {
			var texture = Module.GLctx.createTexture();
			if (!texture) {
				// GLES + EGL specs don't specify what should happen here, so best to issue an error and create IDs with 0.
				Module.GLrecordError(0x0502); // GL_INVALID_OPERATION
				while(i < n) HEAP32[(((textures)+(i++*4))>>2)]=0;
				return;
			}
			var id = Module.GLgetNewId(Module.GLtextures);
			texture.name = id;
			Module.GLtextures[id] = texture;
			HEAP32[(((textures)+(i*4))>>2)]=id;
		}
	})

//    APP__WA_JS(void, glPolygonOffset, (GLfloat factor, GLfloat units), {
//
//	});

    APP__WA_JS(void, glDrawElements, (GLenum mode, GLsizei count, GLenum type, const void * indices), {
		Module.GLctx.drawElements(mode, count, type, indices);
	})

    APP__WA_JS(void, glDeleteFramebuffers, (GLsizei n, const GLuint *framebuffers), {
		checkHeap();
		for (var i = 0; i < n; ++i)
		{
			var id = HEAP32[(((framebuffers)+(i*4))>>2)];
			var framebuffer = Module.GLframebuffers[id];
			if (!framebuffer) continue; // GL spec: "glDeleteFramebuffers silently ignores 0s and names that do not correspond to existing framebuffer objects".
			Module.GLctx.deleteFramebuffer(framebuffer);
			framebuffer.name = 0;
			Module.GLframebuffers[id] = null;
		}
	})

    APP__WA_JS(void, glBlendEquationSeparate, (GLenum modeRGB, GLenum modeAlpha), {
		Module.GLctx.blendEquationSeparate(modeRGB, modeAlpha);
	})

    APP__WA_JS(void, glDeleteTextures, (GLsizei n, const GLuint *textures), {
		checkHeap();
		for (var i = 0; i < n; i++) {
			var id = HEAP32[(((textures)+(i*4))>>2)];
			var texture = Module.GLtextures[id];
			if (!texture) continue; // GL spec: "glDeleteTextures silently ignores 0s and names that do not correspond to existing textures".
			Module.GLctx.deleteTexture(texture);
			texture.name = 0;
			Module.GLtextures[id] = null;
		}
	})

    APP__WA_JS(void, glGetProgramiv, (GLuint program, GLenum pname, GLint * params), {
		checkHeap();
		if (!params) {
			// GLES2 specification does not specify how to behave if params is a null pointer. Since calling this function does not make sense
			// if params == null, issue a GL error to notify user about it.
			Module.GLrecordError(0x0501); // GL_INVALID_VALUE
			return;
		}

		if (program >= GLcounter) {
			Module.GLrecordError(0x0501); // GL_INVALID_VALUE
			return;
		}

		var ptable = Module.GLprogramInfos[program];
		if (!ptable) {
			Module.GLrecordError(0x0502); //GL_INVALID_OPERATION
			return;
		}

		if (pname == 0x8B84) { // GL_INFO_LOG_LENGTH
			var log = Module.GLctx.getProgramInfoLog(Module.GLprograms[program]);
			if (log === null) log = '(unknown error)';
			HEAP32[((params)>>2)] = log.length + 1;
		}
		else if (pname == 0x8B87) { //GL_ACTIVE_UNIFORM_MAX_LENGTH
			HEAP32[((params)>>2)] = ptable.maxUniformLength;
		}
		else if (pname == 0x8B8A) { //GL_ACTIVE_ATTRIBUTE_MAX_LENGTH
			if (ptable.maxAttributeLength == -1) {
				program = Module.GLprograms[program];
				var numAttribs = Module.GLctx.getProgramParameter(program, Module.GLctx.ACTIVE_ATTRIBUTES);
				ptable.maxAttributeLength = 0; // Spec says if there are no active attribs, 0 must be returned.
				for (var i = 0; i < numAttribs; ++i) {
					var activeAttrib = Module.GLctx.getActiveAttrib(program, i);
					ptable.maxAttributeLength = Math.max(ptable.maxAttributeLength, activeAttrib.name.length+1);
				}
			}
			HEAP32[((params)>>2)] = ptable.maxAttributeLength;
		}
		else if (pname == 0x8A35) { //GL_ACTIVE_UNIFORM_BLOCK_MAX_NAME_LENGTH
			if (ptable.maxUniformBlockNameLength == -1) {
				program = Module.GLprograms[program];
				var numBlocks = Module.GLctx.getProgramParameter(program, GLctx.ACTIVE_UNIFORM_BLOCKS);
				ptable.maxUniformBlockNameLength = 0;
				for (var i = 0; i < numBlocks; ++i) {
					var activeBlockName = Module.GLctx.getActiveUniformBlockName(program, i);
					ptable.maxUniformBlockNameLength = Math.max(ptable.maxUniformBlockNameLength, activeBlockName.length+1);
				}
			}
			HEAP32[((params)>>2)] = ptable.maxUniformBlockNameLength;
		} else {
			HEAP32[((params)>>2)] = Module.GLctx.getProgramParameter(Module.GLprograms[program], pname);
		}
	})

    APP__WA_JS(void, glBindTexture, (GLenum target, GLuint texture), {
		Module.GLctx.bindTexture(target, texture ? Module.GLtextures[texture] : null);
	})

    APP__WA_JS(void, glTexImage2D, (GLenum target, GLint level, GLint internalFormat, GLsizei width, GLsizei height, GLint border, GLenum format, GLenum type, const void * pixels), {
		checkHeap();
		var pixelData = null;
		if (pixels) pixelData = Module.webGLGetTexPixelData(type, format, width, height, pixels, internalFormat);
		Module.GLctx.texImage2D(target, level, internalFormat, width, height, border, format, type, pixelData);
	})

//    APP__WA_JS(void, glTexImage3D, (GLenum target, GLint level, GLint internalformat, GLsizei width, GLsizei height, GLsizei depth, GLint border, GLenum format, GLenum type, const void * pixels), {
//
//	});

    APP__WA_JS(GLuint, glCreateShader, (GLenum type), {
		var id = Module.GLgetNewId(Module.GLshaders);
		Module.GLshaders[id] = Module.GLctx.createShader(type);
		return id;
	})

//    APP__WA_JS(void, glClearDepth, (GLdouble depth), {
//
//	});

    APP__WA_JS(void, glFramebufferTexture2D, (GLenum target, GLenum attachment, GLenum textarget, GLuint texture, GLint level), {
		Module.GLctx.framebufferTexture2D(target, attachment, textarget, Module.GLtextures[texture], level);
	})

    APP__WA_JS(GLuint, glCreateProgram, (void), {
		var id = Module.GLgetNewId(Module.GLprograms);
		var program = Module.GLctx.createProgram();
		program.name = id;
		Module.GLprograms[id] = program;
		return id;
	})

    APP__WA_JS(void, glViewport, (GLint x, GLint y, GLsizei width, GLsizei height), {
		Module.GLctx.viewport(x, y, width, height);
	})

    APP__WA_JS(void, glDeleteBuffers, (GLsizei n, const GLuint *buffers), {
		checkHeap();
		for (var i = 0; i < n; i++) {
			var id = HEAP32[(((buffers)+(i*4))>>2)];
			var buffer = Module.GLbuffers[id];

			// From spec: "glDeleteBuffers silently ignores 0's and names that do not correspond to existing buffer objects."
			if (!buffer) continue;

			Module.GLctx.deleteBuffer(buffer);
			buffer.name = 0;
			Module.GLbuffers[id] = null;
		}
	})

    APP__WA_JS(void, glDrawArrays, (GLenum mode, GLint first, GLsizei count), {
		Module.GLctx.drawArrays(mode, first, count);
	})

//    APP__WA_JS(void, glDrawElementsInstanced, (GLenum mode, GLsizei count, GLenum type, const void * indices, GLsizei instancecount), {
//
//	});

    APP__WA_JS(void, glVertexAttribPointer, (GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const void * pointer), {
		Module.GLctx.vertexAttribPointer(index, size, type, !!normalized, stride, pointer);
	})

    APP__WA_JS(void, glDisable, (GLenum cap), {
		Module.GLctx.disable(cap);
	})

    APP__WA_JS(void, glColorMask, (GLboolean red, GLboolean green, GLboolean blue, GLboolean alpha), {
		Module.GLctx.colorMask(!!red, !!green, !!blue, !!alpha);
	})

//    APP__WA_JS(void, glColorMaski, (GLuint buf, GLboolean red, GLboolean green, GLboolean blue, GLboolean alpha), {
//
//	});

    APP__WA_JS(void, glBindBuffer, (GLenum target, GLuint buffer), {
		Module.GLctx.bindBuffer(target, buffer ? Module.GLbuffers[buffer] : null); 
	})

//    APP__WA_JS(void, glDeleteVertexArrays, (GLsizei n, const GLuint *arrays), {
//
//	});

    APP__WA_JS(void, glDepthMask, (GLboolean flag), {
		Module.GLctx.depthMask(!!flag);
	})

//    APP__WA_JS(void, glDrawArraysInstanced, (GLenum mode, GLint first, GLsizei count, GLsizei instancecount), {
//
//	});

//    APP__WA_JS(void, glClearStencil, (GLint s), {
//
//	});

    APP__WA_JS(void, glScissor, (GLint x, GLint y, GLsizei width, GLsizei height), {
		Module.GLctx.scissor(x, y, width, height);
	})

//    APP__WA_JS(void, glGenRenderbuffers, (GLsizei n, GLuint * renderbuffers), {
//
//	});

    APP__WA_JS(void, glBufferData, (GLenum target, GLsizeiptr size, const void *data, GLenum usage), {
		checkHeap();
		if (!data) Module.GLctx.bufferData(target, size, usage);
		else Module.GLctx.bufferData(target, HEAPU8.subarray(data, data+size), usage);
	})

    APP__WA_JS(void, glBlendFuncSeparate, (GLenum sfactorRGB, GLenum dfactorRGB, GLenum sfactorAlpha, GLenum dfactorAlpha), {
		Module.GLctx.blendFuncSeparate(sfactorRGB, dfactorRGB, sfactorAlpha, dfactorAlpha);
	})

    APP__WA_JS(void, glTexParameteri, (GLenum target, GLenum pname, GLint param), {
		Module.GLctx.texParameteri(target, pname, param);
	})

    APP__WA_JS(void, glGetIntegerv, (GLenum pname, GLint *data), {
		Module.webGLGet(pname, data, 'Integer');
	})

    APP__WA_JS(void, glEnable, (GLenum cap), {
		Module.GLctx.enable(cap);
	})

//    APP__WA_JS(void, glBlitFramebuffer, (GLint srcX0, GLint srcY0, GLint srcX1, GLint srcY1, GLint dstX0, GLint dstY0, GLint dstX1, GLint dstY1, GLbitfield mask, GLenum filter), {
//
//	});

//    APP__WA_JS(void, glStencilMask, (GLuint mask), {
//
//	});

    APP__WA_JS(void, glAttachShader, (GLuint program, GLuint shader), {
		Module.GLctx.attachShader(Module.GLprograms[program], Module.GLshaders[shader]);
	})

    APP__WA_JS(void, glDetachShader, (GLuint program, GLuint shader), {
		Module.GLctx.detachShader(Module.GLprograms[program], Module.GLshaders[shader]);
	})

    APP__WA_JS(GLenum, glGetError, (void), {
		if (Module.GLlastError) {
			var e = Module.GLlastError;
			Module.GLlastError = 0;
			return e;
		}
		return Module.GLctx.getError();
	})

    APP__WA_JS(void, glClearColor, (GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha), {
		Module.GLctx.clearColor(red, green, blue, alpha);
	})

    APP__WA_JS(void, glBlendColor, (GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha), {
		Module.GLctx.blendColor(red, green, blue, alpha);
	})

//    APP__WA_JS(void, glTexParameterf, (GLenum target, GLenum pname, GLfloat param), {
//
//	});
//
//    APP__WA_JS(void, glTexParameterfv, (GLenum target, GLenum pname, GLfloat* params), {
//
//	});

    APP__WA_JS(void, glGetShaderInfoLog, (GLuint shader, GLsizei bufSize, GLsizei *length, GLchar *infoLog), {
		checkHeap();
		var log = Module.GLctx.getShaderInfoLog(Module.GLshaders[shader]);
		if (log === null) log = '(unknown error)';
		if (bufSize > 0 && infoLog)
		{
			var numBytesWrittenExclNull = stringToUTF8Array(log, infoLog, bufSize);
			if (length) HEAP32[((length)>>2)] = numBytesWrittenExclNull;
		}
		else if (length) HEAP32[((length)>>2)] = 0;
	})

    APP__WA_JS(void, glDepthFunc, (GLenum func), {
		Module.GLctx.depthFunc(func);
	})

//    APP__WA_JS(void, glStencilOp, (GLenum fail, GLenum zfail, GLenum zpass), {
//
//	});
//
//    APP__WA_JS(void, glStencilFunc, (GLenum func, GLint ref, GLuint mask), {
//
//	});

    APP__WA_JS(void, glEnableVertexAttribArray, (GLuint index), {
		Module.GLctx.enableVertexAttribArray(index);
	})

    APP__WA_JS(void, glBlendFunc, (GLenum sfactor, GLenum dfactor), {
		Module.GLctx.blendFunc(sfactor, dfactor);
	})

//    APP__WA_JS(void, glReadBuffer, (GLenum src), {
//
//	});

    APP__WA_JS(void, glClear, (GLbitfield mask), {
		Module.GLctx.clear(mask);
	})

	// This doesn't exist on webgl
    void glGenVertexArrays(GLsizei n, GLuint * arrays) {}

//    APP__WA_JS(void, glFrontFace, (GLenum mode), {
//
//	});
//
//    APP__WA_JS(void, glCullFace, (GLenum mode), {
//
//	});

//    APP__WA_JS(void, glFinish, (void), {
//
//	});

	APP__WA_JS(void, glPixelStorei, (GLenum pname, GLint param), {
		if (pname == 0x0D05) Module.GLpackAlignment = param; //GL_PACK_ALIGNMENT
		else if (pname == 0x0cf5) Module.GLunpackAlignment = param; //GL_UNPACK_ALIGNMENT
		Module.GLctx.pixelStorei(pname, param);
	})

#endif




#endif // APP_INCLUDE





/////////////////////////////////////////////////////////////////////////////////
//
//
//
//    IMPLEMENTATION STARTS HERE
//
// | | | | | | | | | | | | | | | | | |
// V V V V V V V V V V V V V V V V V V
/////////////////////////////////////////////////////////////////////////////////



#ifdef APP_IMPLEMENTATION
#ifndef APP_IMPLEMENTATION_INCLUDED
#define APP_IMPLEMENTATION_INCLUDED


#ifdef __wasm__
#define APP__ASSERT(x) do {if (!(x)) __builtin_trap();} while(0)
#else
#define APP__ASSERT(x) do {if (!(x)) *((volatile int *)(0)) = 1;} while(0)
#endif




#ifndef APP__TEXT_INPUT_BUFFER_MAX
	#define APP__TEXT_INPUT_BUFFER_MAX 1024
#endif

#ifndef APP__DRAG_AND_DROP_BUFFER_MAX
    #define APP__DRAG_AND_DROP_BUFFER_MAX 2048
#endif

APP_INTERNAL const char APP__default_text_input_filter[] = {
	0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25,
	26, 27, 28, 29, 30, 31, 127};




#ifndef APP__MAX_TOUCH_POINTS
	#define APP__MAX_TOUCH_POINTS 10
#endif

#define APP__MAX_FRAME_DURATION_SAMPLES 255

//
// Global struct that will contain all the crossplatform application data
//
APP_INTERNAL struct {

	int(*frame_callback)(void);
    void(*on_event)(const APP_Event *);

	int  w_width;
	int  w_height;
	int  default_width;
	int  default_height;
	bool window_resized;
	bool fullscreen_changed;
    unsigned char keyboard[APP_MAX_KEYCODES];
	struct {
		float x;
		float y;
		unsigned char buttons[3];
		float wheel_x;
		float wheel_y;
	} mouse;

	bool focus;

	// NOTE(Tano): We may split this into SOA (structures of arrays) to get more performance but
	// anyway...
	struct {
		int id;
		float x;
		float y;
		float force;
		unsigned char state;
	} touch_points[APP__MAX_TOUCH_POINTS];

	struct {
		float x;
		float y;
		unsigned char state;
	} pointer;

	bool quit_requested;

	char text_input_buffer[APP__TEXT_INPUT_BUFFER_MAX];
	unsigned int text_input_bytes_count;

	const char *text_input_filter;
	unsigned int text_input_filter_len;

    char drag_and_drop_buffer[APP__DRAG_AND_DROP_BUFFER_MAX];

    bool        file_dropped;
    const char *file_dropped_path;

	// Manage frame duration and statistics
	int64_t prev_time;
	int64_t frame_duration_samples[APP__MAX_FRAME_DURATION_SAMPLES];
	int frame_duration_samples_count;
	int frame_duration_samples_head;

    unsigned int swap_interval;
} APP__data = {0};








APP__EXPORT(APP__Handle_frame) int
APP__Handle_frame(void);

APP_INTERNAL void
APP__On_event(const APP_Event *);

#if defined (APP_LINUX)

	APP_INTERNAL int     APP__linux_Init(const char *title, int width, int height);
	APP_INTERNAL void    APP__linux_Shutdown(void);
	APP_INTERNAL void    APP__linux_Process_events(void);
	APP_INTERNAL void    APP__linux_Swap_buffers(void);
	APP_INTERNAL void    APP__linux_Set_fullscreen(bool enable);
	APP_INTERNAL void    APP__linux_Set_swap_interval(unsigned int interval);
	APP_INTERNAL void    APP__linux_Show_mouse(bool show);
	APP_INTERNAL void    APP__linux_Set_window_title(const char *title);
	APP_INTERNAL int64_t APP__linux_Time(void);
	APP_INTERNAL void    APP__linux_Sleep(int64_t ns);

#elif defined (APP_WINDOWS)

	APP_INTERNAL int     APP__win32_Init(const char *title, int width, int height);
	APP_INTERNAL void    APP__win32_Shutdown(void);
	APP_INTERNAL void    APP__win32_Process_events(void);
	APP_INTERNAL void    APP__win32_Swap_buffers(void);
	APP_INTERNAL void    APP__win32_Set_fullscreen(bool enable);
	APP_INTERNAL void    APP__win32_Set_swap_interval(unsigned int interval);
	APP_INTERNAL void    APP__win32_Show_mouse(bool show);
	APP_INTERNAL void    APP__win32_Set_window_title(const char *title);
	APP_INTERNAL int64_t APP__win32_Time(void);
	APP_INTERNAL void    APP__win32_Sleep(int64_t ns);

#elif defined (APP_WASM)

	static struct {
		int64_t initial_time;
	} APP__wasm_data = {0};

	APP_INTERNAL int     APP__wasm_Init(const char *title, int width, int height);
	APP_INTERNAL void    APP__wasm_Shutdown(void);
	APP_INTERNAL void    APP__wasm_Process_events(void);
	APP_INTERNAL void    APP__wasm_Set_fullscreen(bool enable);
	APP_INTERNAL void    APP__wasm_Show_mouse(bool show);
	APP_INTERNAL void    APP__wasm_Set_window_title(const char *title);
	APP_INTERNAL int64_t APP__wasm_Time(void);
#endif

APP_PUBLIC int
APP_Init(const char *title, int width, int height) {

	APP__data.text_input_filter = APP__default_text_input_filter,
	APP__data.text_input_filter_len = sizeof(APP__default_text_input_filter),

	APP__data.focus = true;

	// Init the touch points id
	for (int i = 0; i < APP__MAX_TOUCH_POINTS; i+=1) {
		APP__data.touch_points[i].id = -1;
	}

	#if defined (APP_LINUX)
		return APP__linux_Init(title, width, height);
	#elif defined (APP_WINDOWS)
		return APP__win32_Init(title, width, height);
	#elif defined (APP_WASM)
		return APP__wasm_Init(title, width, height);
	#endif

	//APP__data.prev_time = APP_Time();
}

// Destroys the window
APP_PUBLIC void
APP_Shutdown(void) {
	#if defined (APP_LINUX)
		APP__linux_Shutdown();
	#elif defined (APP_WINDOWS)
		APP__win32_Shutdown();
	#elif defined (APP_WASM)
		APP__wasm_Shutdown();
	#endif
}


APP_PUBLIC int
APP_Get_window_width(void) {
	return APP__data.w_width;
}


APP_PUBLIC int
APP_Get_window_height(void) {
	return APP__data.w_height;
}

APP_PUBLIC bool
APP_Window_resized(void) {
	return APP__data.window_resized;
}

APP_PUBLIC bool
APP_Quit_requested(void) {
	return APP__data.quit_requested;
}

APP_PUBLIC unsigned char
APP_Get_key_state(APP_KeyCode key) {
    if (key <= APP_KEY_INVALID || key >= APP_MAX_KEYCODES) return 0;
	return APP__data.keyboard[key];
}


APP_PUBLIC const char *
APP_Get_text_input(int *bytes_count) {
    if (bytes_count) *bytes_count = APP__data.text_input_bytes_count;
	return APP__data.text_input_buffer;
}



APP_PUBLIC float
APP_Get_mouse_x(void) {
	float result = APP__data.mouse.x;
	return result;
}


APP_PUBLIC float
APP_Get_mouse_y(void) {
	float result = APP__data.mouse.y;
	return result;
}



APP_PUBLIC unsigned char
APP_Get_mouse_button_state(APP_MouseButton button) {
    if (button < 0 || button >= APP_MOUSE_BUTTON_MAX) return 0;
	return APP__data.mouse.buttons[button];
}


APP_PUBLIC float
APP_Get_wheel_x(void) {
	return APP__data.mouse.wheel_x;
}

APP_PUBLIC float
APP_Get_wheel_y(void) {
	return APP__data.mouse.wheel_y;
}


APP_PUBLIC float
APP_Get_touch_x(int id) {
	if (id < 0 || id >= APP__MAX_TOUCH_POINTS) return 0.0f;
	return APP__data.touch_points[id].x;
}

APP_PUBLIC float
APP_Get_touch_y(int id) {
	if (id < 0 || id >= APP__MAX_TOUCH_POINTS) return 0.0f;
	return APP__data.touch_points[id].y;
}

APP_PUBLIC unsigned char
APP_Get_touch_state(int id) {
	if (id < 0 || id >= APP__MAX_TOUCH_POINTS) return APP_TOUCH_STATE_INVALID;
	return APP__data.touch_points[id].state;
}

APP_PUBLIC float
APP_Get_pointer_x(void) {
	return APP__data.pointer.x;
}

APP_PUBLIC float
APP_Get_pointer_y(void) {
	return APP__data.pointer.y;
}

APP_PUBLIC unsigned char
APP_Get_pointer_state(void) {
	return APP__data.pointer.state;
}


APP_PUBLIC const char *
APP_Get_file_dropped_path(void) {
    return APP__data.file_dropped_path;
}


APP_PUBLIC void
APP_Set_fullscreen(bool enable) {
	#if defined (APP_LINUX)
		APP__linux_Set_fullscreen(enable);
	#elif defined (APP_WINDOWS)
		APP__win32_Set_fullscreen(enable);
	#elif defined (APP_WASM)
		APP__wasm_Set_fullscreen(enable);
	#endif
}


APP_PUBLIC void
APP_Set_swap_interval(unsigned int interval) {
    APP__data.swap_interval = interval;
	#if defined (APP_LINUX)
		APP__linux_Set_swap_interval(interval);
	#elif defined (APP_WINDOWS)
		APP__win32_Set_swap_interval(interval);
	#elif defined (APP_WASM)
		// This doesn't work on wasm
	#endif
}


APP_PUBLIC void
APP_Swap_buffers(void) {
	#if defined (APP_LINUX)
	    APP__linux_Swap_buffers();
	#elif defined (APP_WINDOWS)
	    APP__win32_Swap_buffers();
	#elif defined (APP_WASM)
		// This doesn't work on wasm
	#endif
}

APP_PUBLIC void
APP_Show_mouse(bool show) {
	#if defined (APP_LINUX)
		APP__linux_Show_mouse(show);
	#elif defined (APP_WINDOWS)
		APP__win32_Show_mouse(show);
	#elif defined (APP_WASM)
		APP__wasm_Show_mouse(show);
	#endif
}

APP_PUBLIC void
APP_Set_window_title(const char *title) {
	#if defined (APP_LINUX)
		APP__linux_Set_window_title(title);
	#elif defined (APP_WINDOWS)
		APP__win32_Set_window_title(title);
	#elif defined (APP_WASM)
		APP__wasm_Set_window_title(title);
	#endif
}



APP_PUBLIC int64_t
APP_Time(void) {
	#if defined (APP_LINUX)
		return APP__linux_Time();
	#elif defined (APP_WINDOWS)
		return APP__win32_Time();
	#elif defined (APP_WASM)
		return APP__wasm_Time();
	#endif
}

APP_PUBLIC int64_t
APP_Frame_duration(int samples) {
	
	if (APP__data.frame_duration_samples_count < samples) {
		samples = APP__data.frame_duration_samples_count;
	}
	if (samples == 0) {
		return 0;
	}

	int head = APP__data.frame_duration_samples_head;
	int64_t acum = 0;
	for (int i = 0; i < samples; i+=1) {
		acum += APP__data.frame_duration_samples[(head+i)%APP__MAX_FRAME_DURATION_SAMPLES];
	}
	
	acum /= (int64_t)samples;

	return acum;
}


APP_PUBLIC void
APP_Try_sleep(int64_t ns) {
	#if defined (APP_LINUX)
		APP__linux_Sleep(ns);
	#elif defined (APP_WINDOWS)
		APP__win32_Sleep(ns);
	#elif defined (APP_WASM)
		// Nothing
	#endif
}


APP_INTERNAL bool
APP__Is_char_filtered(char c) {
	const int total = APP__data.text_input_filter_len;
	const char *filter = APP__data.text_input_filter;
	for (int i = 0; i < total; ++i) {
		if (c == filter[i]) return true;
	}
	return false;
}

APP_INTERNAL void
APP__Put_keys_up(void) {
	// The keys that are DOWN go UP
	for (int i = 0; i < APP_MAX_KEYCODES; ++i) {
		if (APP__data.keyboard[i] == APP_STATE_PRESSED ||
			APP__data.keyboard[i] == APP_STATE_DOWN) {

			APP__data.keyboard[i] = APP_STATE_RELEASED;
		}
	}
	// The same with mouse buttons
	for (int i = 0; i < 3; ++i) {
		if (APP__data.mouse.buttons[i] == APP_STATE_PRESSED ||
			APP__data.keyboard[i] == APP_STATE_DOWN) {

			APP__data.mouse.buttons[i] = APP_STATE_RELEASED;
		}
	}
}


APP_INTERNAL void
APP__On_event(const APP_Event *event) {

    // key field
    if (event->type == APP_EVENT_KEY_PRESSED) {

		if (APP__data.keyboard[event->key.id] & APP_STATE_PRESSED ||
			APP__data.keyboard[event->key.id] & APP_STATE_DOWN) {

			APP__data.keyboard[event->key.id] = APP_STATE_DOWN;
		}
		else {
			APP__data.keyboard[event->key.id] = APP_STATE_PRESSED;
		}

    }
    else if (event->type == APP_EVENT_KEY_RELEASED) {
		APP__data.keyboard[event->key.id] = APP_STATE_RELEASED;
    }
    else if (event->type == APP_EVENT_KEY_REPEAT) {
		APP__data.keyboard[event->key.id] |= APP_STATE_REPEAT;
    }
    // text input
    if (event->type == APP_EVENT_TEXT_INPUT) {
		if (event->text_input.count &&
            !APP__Is_char_filtered(event->text_input.data[0]) &&
			APP__data.text_input_bytes_count+event->text_input.count < APP__TEXT_INPUT_BUFFER_MAX) {

			for (int i = 0; i < event->text_input.count; i+=1) {
				APP__data.text_input_buffer[APP__data.text_input_bytes_count+i] =
                    event->text_input.data[i];
			}
			APP__data.text_input_bytes_count = APP__data.text_input_bytes_count + event->text_input.count;
			APP__data.text_input_buffer[APP__data.text_input_bytes_count] = '\0';
		}
    }
    // mouse field
    else if (event->type == APP_EVENT_MOUSE_MOVE) {
        APP__data.mouse.x   = event->mouse.x;
        APP__data.mouse.y   = event->mouse.y;
        APP__data.pointer.x = event->mouse.x;
        APP__data.pointer.y = event->mouse.y;
    }
    else if (event->type == APP_EVENT_MOUSE_BUTTON_PRESSED) {
	    if (event->mouse.button < 0 || event->mouse.button >= APP_MOUSE_BUTTON_MAX) return;
	    if (APP__data.mouse.buttons[event->mouse.button] == APP_STATE_PRESSED ||
	    	APP__data.mouse.buttons[event->mouse.button] == APP_STATE_DOWN) {

	    	APP__data.mouse.buttons[event->mouse.button] = APP_STATE_DOWN;
	    }
	    else {
	    	APP__data.mouse.buttons[event->mouse.button] = APP_STATE_PRESSED;
	    }

	    // Handle pointer case
	    if (event->mouse.button == APP_MOUSE_BUTTON_LEFT) {
	    	if (APP__data.pointer.state & APP_STATE_PRESSED) {
	    		APP__data.pointer.state = APP_STATE_DOWN;
	    	}
	    	else {
	    		APP__data.pointer.state = (APP_STATE_PRESSED|APP_STATE_DOWN);
	    	}
	    }
    }
    else if (event->type == APP_EVENT_MOUSE_BUTTON_RELEASED) {
	    if (event->mouse.button < 0 || event->mouse.button >= APP_MOUSE_BUTTON_MAX) return;

        // Handle mouse down and up in the same frame
	    if (APP__data.mouse.buttons[event->mouse.button] & APP_STATE_PRESSED) {
            APP__data.mouse.buttons[event->mouse.button] = APP_STATE_UP;
	        if (event->mouse.button == APP_MOUSE_BUTTON_LEFT) {
	        	APP__data.pointer.state = APP_STATE_UP;
            }
        }
	    else {
            APP__data.mouse.buttons[event->mouse.button] = APP_STATE_RELEASED;
	        if (event->mouse.button == APP_MOUSE_BUTTON_LEFT) {
	        	if (!(APP__data.pointer.state & APP_STATE_UP)) {
	        	    APP__data.pointer.state = (APP_STATE_RELEASED|APP_STATE_UP);
                }
	        }
        }
    }
    // Touch field
    else if (event->type == APP_EVENT_TOUCH_BEGIN ||
             event->type == APP_EVENT_TOUCH_MOVE  ||
             event->type == APP_EVENT_TOUCH_END   ||
             event->type == APP_EVENT_TOUCH_CANCEL) {

	    int target_index = -1;
	    for (int i = 0; i < APP__MAX_TOUCH_POINTS; i+=1) {
	    	// If the touch point already exists and has a valid state we target it
	    	if (APP__data.touch_points[i].id == event->touch.id) {
	    		target_index = i;
	    		break;
	    	}
	    	// We set the target to the first invalid point that we see
	    	else if (APP__data.touch_points[i].state == APP_TOUCH_STATE_INVALID && target_index < 0) {
                target_index = i;
            }
	    }
		if      (event->type == APP_EVENT_TOUCH_BEGIN ) APP__data.touch_points[target_index].state = APP_TOUCH_STATE_BEGIN;
		else if (event->type == APP_EVENT_TOUCH_MOVE  ) APP__data.touch_points[target_index].state = APP_TOUCH_STATE_MOVE;
		else if (event->type == APP_EVENT_TOUCH_END   ) APP__data.touch_points[target_index].state = APP_TOUCH_STATE_END;
		else if (event->type == APP_EVENT_TOUCH_CANCEL) APP__data.touch_points[target_index].state = APP_TOUCH_STATE_CANCEL;
		APP__data.touch_points[target_index].id = event->touch.id;
		APP__data.touch_points[target_index].x  = event->touch.x;
		APP__data.touch_points[target_index].y  = event->touch.y;
		APP__data.pointer.x = event->touch.x;
		APP__data.pointer.y = event->touch.y;

		if (event->type == APP_EVENT_TOUCH_BEGIN || event->type == APP_EVENT_TOUCH_MOVE) {
            // This was thought carefully
            // Multiple events can be triggered between frames so we only must set the state
            // If the pointer isn't already down
			if (!(APP__data.pointer.state & APP_STATE_DOWN)) {
				APP__data.pointer.state = (APP_STATE_PRESSED|APP_STATE_DOWN);
			}
		}
		else {
            // We discard this if the pointer is already up
			if (!(APP__data.pointer.state & APP_STATE_UP)) {
			    APP__data.pointer.state = (APP_STATE_RELEASED|APP_STATE_UP);
            }
		}
    }
    else if (event->type == APP_EVENT_MOUSE_WHEEL_X) {
        APP__data.mouse.wheel_x = event->mouse.wheel_x;
    }
    else if (event->type == APP_EVENT_MOUSE_WHEEL_Y) {
        APP__data.mouse.wheel_y = event->mouse.wheel_y;
    }
    // window field
    else if (event->type == APP_EVENT_WINDOW_RESIZED) {
        APP__data.w_width  = event->window.width;
        APP__data.w_height = event->window.height;
        APP__data.window_resized = true;
    }
    else if (event->type == APP_EVENT_WINDOW_TO_FULLSCREEN) {

    }
    else if (event->type == APP_EVENT_WINDOW_TO_WINDOW) {

    }
    else if (event->type == APP_EVENT_WINDOW_DPI_CHANGED) {

    }
    else if (event->type == APP_EVENT_WINDOW_FOCUS_LOST) {
        // Puts all the keys down to up because will never notify the up event
        APP__Put_keys_up();
    }
    else if (event->type == APP_EVENT_WINDOW_FOCUS_GAINED) {

    }
    else if (event->type == APP_EVENT_FILE_DROPPED) {
        APP__data.file_dropped = true;
        APP__data.file_dropped_path = event->file_dropped.filepath;
    }
    // quit
    else if (event->type == APP_EVENT_QUIT) {
        APP__data.quit_requested = true;
    }
}


APP_INTERNAL void
APP__Emit_event(const APP_Event *event) {
    if (APP__data.on_event) APP__data.on_event(event);
    else APP__On_event(event);
}


APP_INTERNAL void
APP__Clear_events(void) {

	// Clear the text input stuff
	APP__data.text_input_buffer[0] = '\0';
	APP__data.text_input_bytes_count = 0;

	// We have to reset the window resize event every frame.
	APP__data.window_resized = false;

	// We must trigger the windows resize event if we went to fullscreen, the right dimensions
	// are informed when the fullscreen has changed.
	// On x11 and wasm the resize event is triggered anyway, so this doesn't matter.
	if (APP__data.fullscreen_changed == true) {
		APP__data.fullscreen_changed = false;
		APP__data.window_resized = true;
	}
	
	// The keys that are UP must be NONE the next frame
	for (int i = 0; i < APP_MAX_KEYCODES; ++i) {
		if (APP__data.keyboard[i] & APP_STATE_REPEAT) {
			APP__data.keyboard[i] &= (~APP_STATE_REPEAT);
		}
		if (APP__data.keyboard[i] & APP_STATE_PRESSED) {
			APP__data.keyboard[i] = APP_STATE_DOWN;
		}
		if (APP__data.keyboard[i] & APP_STATE_RELEASED) {
			APP__data.keyboard[i] = APP_STATE_UP;
		}
	}

	// The same with mouse buttons
	for (int i = 0; i < 3; i+=1) {
		if (APP__data.mouse.buttons[i] & APP_STATE_PRESSED) {
			APP__data.mouse.buttons[i] = APP_STATE_DOWN;
		}
		if (APP__data.mouse.buttons[i] & APP_STATE_RELEASED) {
			APP__data.mouse.buttons[i] = APP_STATE_UP;
		}
	}

	APP__data.mouse.wheel_x = 0.0f;
	APP__data.mouse.wheel_y = 0.0f;

	// Clear the touch points
	for (int i = 0; i < APP__MAX_TOUCH_POINTS; i+=1) {
		if (APP__data.touch_points[i].state & (APP_TOUCH_STATE_END|APP_TOUCH_STATE_CANCEL)) {
			APP__data.touch_points[i].state = APP_TOUCH_STATE_INVALID;
			APP__data.touch_points[i].id = -1;
		}
	}

	if (APP__data.pointer.state & APP_STATE_PRESSED)  APP__data.pointer.state = APP_STATE_DOWN;
	if (APP__data.pointer.state & APP_STATE_RELEASED) APP__data.pointer.state = APP_STATE_UP;
	if (APP__data.pointer.state == APP_STATE_INVALID) APP__data.pointer.state = APP_STATE_UP;

    // Clear file dropped
    APP__data.file_dropped = false;
    APP__data.file_dropped_path = NULL;
}




int
APP__Handle_frame(void) {

	int64_t time_now = APP_Time();
	int64_t frame_duration = time_now - APP__data.prev_time;
	APP__data.prev_time = time_now;

	int head = APP__data.frame_duration_samples_head;
	head = (head + APP__MAX_FRAME_DURATION_SAMPLES - 1) % APP__MAX_FRAME_DURATION_SAMPLES;
	APP__data.frame_duration_samples[head] = frame_duration;
	APP__data.frame_duration_samples_head  = head;
	if (APP__data.frame_duration_samples_count < APP__MAX_FRAME_DURATION_SAMPLES) {
		APP__data.frame_duration_samples_count += 1;
	}

	#if defined (APP_LINUX)
		APP__linux_Process_events();
	#elif defined (APP_WINDOWS)
		APP__win32_Process_events();
	#elif defined (APP_WASM)
		APP__wasm_Process_events();
	#endif
	
	
	int result = -1;
	if (APP__data.frame_callback) {
		result = APP__data.frame_callback();
	}

	APP__Clear_events();

	// If the focus has changed to not focus
	// Puts all the keys down to up because will never notify the up event
	if (!APP__data.focus) {
		APP__Put_keys_up();
	}

	return result;
}









// HERE STARTS THE PLATFORM SPECIFIC CODE










/////////////////////////////////////////////////////////////////////////////////
//
//
//
//                     WINDOWS IMPLEMENTATION
//
//
//
////////////////////////////////////////////////////////////////////////////////
#if defined(APP_WINDOWS)

// https://www.opengl.org/archives/resources/code/samples/win32_tutorial/
// https://github.com/floooh/sokol/blob/master/sokol_app.h

#include <windows.h>
#include <stdio.h>
#include <windowsx.h> // GET_X_LPARAM ...


typedef HGLRC (WINAPI * PFN_wglCreateContext)(HDC);
typedef BOOL (WINAPI * PFN_wglDeleteContext)(HGLRC);
typedef PROC (WINAPI * PFN_wglGetProcAddress)(LPCSTR);
typedef HDC (WINAPI * PFN_wglGetCurrentDC)(void);
typedef BOOL (WINAPI * PFN_wglMakeCurrent)(HDC,HGLRC);
typedef BOOL (WINAPI * PFNWGLSWAPINTERVALEXTPROC)(int);
#define WGL_NUMBER_PIXEL_FORMATS_ARB 0x2000
#define WGL_SUPPORT_OPENGL_ARB 0x2010
#define WGL_DRAW_TO_WINDOW_ARB 0x2001
#define WGL_PIXEL_TYPE_ARB 0x2013
#define WGL_TYPE_RGBA_ARB 0x202b
#define WGL_ACCELERATION_ARB 0x2003
#define WGL_NO_ACCELERATION_ARB 0x2025
#define WGL_RED_BITS_ARB 0x2015
#define WGL_GREEN_BITS_ARB 0x2017
#define WGL_BLUE_BITS_ARB 0x2019
#define WGL_ALPHA_BITS_ARB 0x201b
#define WGL_DEPTH_BITS_ARB 0x2022
#define WGL_STENCIL_BITS_ARB 0x2023
#define WGL_DOUBLE_BUFFER_ARB 0x2011
#define WGL_SAMPLES_ARB 0x2042
#define WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB 0x00000002
#define WGL_CONTEXT_PROFILE_MASK_ARB 0x9126
#define WGL_CONTEXT_CORE_PROFILE_BIT_ARB 0x00000001
#define WGL_CONTEXT_MAJOR_VERSION_ARB 0x2091
#define WGL_CONTEXT_MINOR_VERSION_ARB 0x2092
#define WGL_CONTEXT_FLAGS_ARB 0x2094
#define ERROR_INVALID_VERSION_ARB 0x2095
#define ERROR_INVALID_PROFILE_ARB 0x2096
#define ERROR_INCOMPATIBLE_DEVICE_CONTEXTS_ARB 0x2054


// generate GL function pointers
#define APP__GL_XMACRO(name, ret, args) PFN_ ## name name;
	APP__GL_FUNCS
#undef APP__GL_XMACRO



static struct {
	HWND hwnd;
	HDC  hdc;
	HWND msg_hwnd;
	HDC  msg_hdc;
	wchar_t window_title_wide[128];

	HGLRC gl_ctx;
	HINSTANCE opengl32;
	PFN_wglCreateContext wglCreateContext;
	PFN_wglDeleteContext wglDeleteContext;
	PFN_wglGetProcAddress wglGetProcAddress;
	PFN_wglGetCurrentDC wglGetCurrentDC;
	PFN_wglMakeCurrent wglMakeCurrent;
    PFNWGLSWAPINTERVALEXTPROC wglSwapIntervalEXT;

	// Time tracking
	LARGE_INTEGER initial_time;
	LARGE_INTEGER time_freq;
} APP__win32_data = {0};


#define APP__MAX_KEYMAPPING 512

static APP_KeyCode APP__key_mapping[APP__MAX_KEYMAPPING] = {
    /* same as GLFW */
    [0x00B] = APP_KEY_0,
    [0x002] = APP_KEY_1,
    [0x003] = APP_KEY_2,
    [0x004] = APP_KEY_3,
    [0x005] = APP_KEY_4,
    [0x006] = APP_KEY_5,
    [0x007] = APP_KEY_6,
    [0x008] = APP_KEY_7,
    [0x009] = APP_KEY_8,
    [0x00A] = APP_KEY_9,
    [0x01E] = APP_KEY_A,
    [0x030] = APP_KEY_B,
    [0x02E] = APP_KEY_C,
    [0x020] = APP_KEY_D,
    [0x012] = APP_KEY_E,
    [0x021] = APP_KEY_F,
    [0x022] = APP_KEY_G,
    [0x023] = APP_KEY_H,
    [0x017] = APP_KEY_I,
    [0x024] = APP_KEY_J,
    [0x025] = APP_KEY_K,
    [0x026] = APP_KEY_L,
    [0x032] = APP_KEY_M,
    [0x031] = APP_KEY_N,
    [0x018] = APP_KEY_O,
    [0x019] = APP_KEY_P,
    [0x010] = APP_KEY_Q,
    [0x013] = APP_KEY_R,
    [0x01F] = APP_KEY_S,
    [0x014] = APP_KEY_T,
    [0x016] = APP_KEY_U,
    [0x02F] = APP_KEY_V,
    [0x011] = APP_KEY_W,
    [0x02D] = APP_KEY_X,
    [0x015] = APP_KEY_Y,
    [0x02C] = APP_KEY_Z,
    [0x028] = APP_KEY_APOSTROPHE,
    [0x02B] = APP_KEY_BACKSLASH,
    [0x033] = APP_KEY_COMMA,
    [0x00D] = APP_KEY_EQUAL,
    [0x029] = APP_KEY_GRAVE_ACCENT,
    [0x01A] = APP_KEY_LEFT_BRACKET,
    [0x00C] = APP_KEY_MINUS,
    [0x034] = APP_KEY_PERIOD,
    [0x01B] = APP_KEY_RIGHT_BRACKET,
    [0x027] = APP_KEY_SEMICOLON,
    [0x035] = APP_KEY_SLASH,
    [0x056] = APP_KEY_WORLD_2,
    [0x00E] = APP_KEY_BACKSPACE,
    [0x153] = APP_KEY_DELETE,
    [0x14F] = APP_KEY_END,
    [0x01C] = APP_KEY_ENTER,
    [0x001] = APP_KEY_ESCAPE,
    [0x147] = APP_KEY_HOME,
    [0x152] = APP_KEY_INSERT,
    [0x15D] = APP_KEY_MENU,
    [0x151] = APP_KEY_PAGE_DOWN,
    [0x149] = APP_KEY_PAGE_UP,
    [0x045] = APP_KEY_PAUSE,
    [0x146] = APP_KEY_PAUSE,
    [0x039] = APP_KEY_SPACE,
    [0x00F] = APP_KEY_TAB,
    [0x03A] = APP_KEY_CAPS_LOCK,
    [0x145] = APP_KEY_NUM_LOCK,
    [0x046] = APP_KEY_SCROLL_LOCK,
    [0x03B] = APP_KEY_F1,
    [0x03C] = APP_KEY_F2,
    [0x03D] = APP_KEY_F3,
    [0x03E] = APP_KEY_F4,
    [0x03F] = APP_KEY_F5,
    [0x040] = APP_KEY_F6,
    [0x041] = APP_KEY_F7,
    [0x042] = APP_KEY_F8,
    [0x043] = APP_KEY_F9,
    [0x044] = APP_KEY_F10,
    [0x057] = APP_KEY_F11,
    [0x058] = APP_KEY_F12,
    [0x064] = APP_KEY_F13,
    [0x065] = APP_KEY_F14,
    [0x066] = APP_KEY_F15,
    [0x067] = APP_KEY_F16,
    [0x068] = APP_KEY_F17,
    [0x069] = APP_KEY_F18,
    [0x06A] = APP_KEY_F19,
    [0x06B] = APP_KEY_F20,
    [0x06C] = APP_KEY_F21,
    [0x06D] = APP_KEY_F22,
    [0x06E] = APP_KEY_F23,
    [0x076] = APP_KEY_F24,
    [0x038] = APP_KEY_LEFT_ALT,
    [0x01D] = APP_KEY_LEFT_CONTROL,
    [0x02A] = APP_KEY_LEFT_SHIFT,
    [0x15B] = APP_KEY_LEFT_SUPER,
    [0x137] = APP_KEY_PRINT_SCREEN,
    [0x138] = APP_KEY_RIGHT_ALT,
    [0x11D] = APP_KEY_RIGHT_CONTROL,
    [0x036] = APP_KEY_RIGHT_SHIFT,
    [0x15C] = APP_KEY_RIGHT_SUPER,
    [0x150] = APP_KEY_DOWN,
    [0x14B] = APP_KEY_LEFT,
    [0x14D] = APP_KEY_RIGHT,
    [0x148] = APP_KEY_UP,
    [0x052] = APP_KEY_KP_0,
    [0x04F] = APP_KEY_KP_1,
    [0x050] = APP_KEY_KP_2,
    [0x051] = APP_KEY_KP_3,
    [0x04B] = APP_KEY_KP_4,
    [0x04C] = APP_KEY_KP_5,
    [0x04D] = APP_KEY_KP_6,
    [0x047] = APP_KEY_KP_7,
    [0x048] = APP_KEY_KP_8,
    [0x049] = APP_KEY_KP_9,
    [0x04E] = APP_KEY_KP_ADD,
    [0x053] = APP_KEY_KP_DECIMAL,
    [0x135] = APP_KEY_KP_DIVIDE,
    [0x11C] = APP_KEY_KP_ENTER,
    [0x037] = APP_KEY_KP_MULTIPLY,
    [0x04A] = APP_KEY_KP_SUBTRACT,
};


APP_INTERNAL LONG WINAPI
APP__Window_proc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	static bool mouse_tracked = false;

	switch (uMsg) {
	    case WM_CLOSE:
            APP__Emit_event(&(APP_Event){.type = APP_EVENT_QUIT});
	        break;
	    case WM_SYSCOMMAND:
	        break;
	    case WM_ERASEBKGND:
	        return 1;
	    case WM_SIZE: {
            APP__Emit_event(&(APP_Event){
                .type = APP_EVENT_WINDOW_RESIZED,
                .window = {.width = LOWORD(lParam), .height = HIWORD(lParam)}
            });
	        break;
		}
	    case WM_SETCURSOR:
	        break;
	    case WM_LBUTTONDOWN: {
            APP__Emit_event(&(APP_Event){
                .type = APP_EVENT_MOUSE_BUTTON_PRESSED,
                .mouse = {.button = APP_MOUSE_BUTTON_LEFT}
            });
	        break;
		}
	    case WM_RBUTTONDOWN: {
            APP__Emit_event(&(APP_Event){
                .type = APP_EVENT_MOUSE_BUTTON_PRESSED,
                .mouse = {.button = APP_MOUSE_BUTTON_RIGHT}
            });
	        break;
		}
	    case WM_MBUTTONDOWN: {
            APP__Emit_event(&(APP_Event){
                .type = APP_EVENT_MOUSE_BUTTON_PRESSED,
                .mouse = {.button = APP_MOUSE_BUTTON_MIDDLE}
            });
	        break;
		}
	    case WM_LBUTTONUP: {
            APP__Emit_event(&(APP_Event){
                .type = APP_EVENT_MOUSE_BUTTON_RELEASED,
                .mouse = {.button = APP_MOUSE_BUTTON_LEFT}
            });
	        break;
		}
	    case WM_RBUTTONUP: {
            APP__Emit_event(&(APP_Event){
                .type = APP_EVENT_MOUSE_BUTTON_RELEASED,
                .mouse = {.button = APP_MOUSE_BUTTON_RIGHT}
            });
	        break;
		}
	    case WM_MBUTTONUP: {
            APP__Emit_event(&(APP_Event){
                .type = APP_EVENT_MOUSE_BUTTON_RELEASED,
                .mouse = {.button = APP_MOUSE_BUTTON_MIDDLE}
            });
	        break;
		}
		case  WM_MOUSEMOVE: {
            APP__Emit_event(&(APP_Event){
                .type = APP_EVENT_MOUSE_MOVE,
                .mouse = {.x = (float)GET_X_LPARAM(lParam), .y = (float)GET_Y_LPARAM(lParam)}
            });
			if (!mouse_tracked) {
				TRACKMOUSEEVENT tme;
				memset(&tme, 0, sizeof(tme));
				tme.cbSize = sizeof(tme);
				tme.dwFlags = TME_LEAVE;
				tme.hwndTrack = APP__win32_data.hwnd;
				TrackMouseEvent(&tme);
			}
			break;
		}
	    case WM_INPUT:
	        break;
	    case WM_MOUSELEAVE: {
			mouse_tracked = false;
            APP__Emit_event(&(APP_Event){
                .type = APP_EVENT_WINDOW_FOCUS_LOST
            });
	        break;
		}
	    case WM_MOUSEWHEEL:
            APP__Emit_event(&(APP_Event){
                .type = APP_EVENT_MOUSE_WHEEL_Y,
                .mouse = {.wheel_y = (float)((SHORT)HIWORD(wParam))}
            });
	        break;
	    case WM_MOUSEHWHEEL:
            APP__Emit_event(&(APP_Event){
                .type = APP_EVENT_MOUSE_WHEEL_X,
                .mouse = {.wheel_x = (float)((SHORT)HIWORD(wParam))}
            });
	        break;
	    case WM_CHAR: {
			wchar_t input_char = (wchar_t)wParam;
			char multibyte_buffer[5] = "";
			int multibyte_len = WideCharToMultiByte(CP_UTF8, 0, &input_char, 1, multibyte_buffer, 5, NULL, NULL);
            if (multibyte_len > 0 && multibyte_len <= 4) {
                APP__Emit_event(&(APP_Event){
                    .type = APP_EVENT_TEXT_INPUT,
                    .text_input = {
                        .data[0] = multibyte_buffer[0],
                        .data[1] = multibyte_buffer[1],
                        .data[2] = multibyte_buffer[2],
                        .data[3] = multibyte_buffer[3],
                        .count = multibyte_len,
                    }
                });
            }
			break;
		 }
        case WM_KEYDOWN:
        case WM_SYSKEYDOWN: {
			int key = (int)(HIWORD(lParam)&0x1FF);
			if (key >= 0 && key < APP__MAX_KEYMAPPING) {
				int key_id = APP__key_mapping[key];
                APP__Emit_event(&(APP_Event){
                    .type = APP_EVENT_KEY_PRESSED,
                    .key = {.id = key_id}
                });
                APP__Emit_event(&(APP_Event){
                    .type = APP_EVENT_KEY_REPEAT,
                    .key = {.id = key_id}
                });
			}
            break;
		}
        case WM_KEYUP:
        case WM_SYSKEYUP: {
			int key = (int)(HIWORD(lParam)&0x1FF);
			if (key >= 0 && key < APP__MAX_KEYMAPPING) {
				int key_id = APP__key_mapping[key];
                APP__Emit_event(&(APP_Event){
                    .type = APP_EVENT_KEY_RELEASED,
                    .key = {.id = key_id}
                });
			}
			break;
		}
		case WM_KILLFOCUS: {
            APP__Emit_event(&(APP_Event){
                .type = APP_EVENT_WINDOW_FOCUS_LOST
            });
			break;
		}
	    case WM_ENTERSIZEMOVE:
	        break;
	    case WM_EXITSIZEMOVE:
	        break;
	    case WM_TIMER:
	        //    SwapBuffers(hdc);
	        break;
	    case WM_DROPFILES:
	        break;
	    default:
	        break;
	}
	
    return DefWindowProcW(hWnd, uMsg, wParam, lParam);
} 


/*
 * Extracted from sokol_app.h _sAPP_win32_uwp_utf8_to_wide
 *
 */
APP_INTERNAL bool
APP__String_to_wide(const char* src, wchar_t* dst, int dst_num_bytes) {
    APP__ASSERT(src && dst && (dst_num_bytes > 1));
    memset(dst, 0, (size_t)dst_num_bytes);
    const int dst_chars = dst_num_bytes / (int)sizeof(wchar_t);
    const int dst_needed = MultiByteToWideChar(CP_UTF8, 0, src, -1, 0, 0);
    if ((dst_needed > 0) && (dst_needed < dst_chars)) {
        MultiByteToWideChar(CP_UTF8, 0, src, -1, dst, dst_chars);
        return true;
    }
    else {
        /* input string doesn't fit into destination buffer */
        return false;
    }
}


APP_INTERNAL void *
APP__Load_gl_function(const char *func_name) {
    void* proc_addr = (void*)APP__win32_data.wglGetProcAddress(func_name);
	if (proc_addr == NULL) {
		proc_addr = (void*) GetProcAddress(APP__win32_data.opengl32, func_name);
	}
    if (proc_addr == NULL) {
		printf("CANNOT LOAD %s\n", func_name);
	}
    return proc_addr;
}



APP_INTERNAL int
APP__win32_Init(const char *title, int width, int height) {

	APP__data.default_width  = width;
	APP__data.default_height = height;
	APP__data.w_width  = width;
	APP__data.w_height = height;
	
	APP__String_to_wide(
			title,
			APP__win32_data.window_title_wide,
			sizeof(APP__win32_data.window_title_wide));

    WNDCLASSW wndclassw;
    memset(&wndclassw, 0, sizeof(wndclassw));
    wndclassw.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
    wndclassw.lpfnWndProc = (WNDPROC) APP__Window_proc;
    wndclassw.hInstance = GetModuleHandleW(NULL);
    wndclassw.hCursor = LoadCursor(NULL, IDC_ARROW);
    wndclassw.hIcon = LoadIcon(NULL, IDI_WINLOGO);
    wndclassw.lpszClassName = L"APP";
    RegisterClassW(&wndclassw);


    DWORD win_style;
    const DWORD win_ex_style = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;
    RECT rect = { 0, 0, 0, 0 };
    win_style = WS_CLIPSIBLINGS |
				WS_CLIPCHILDREN |
				WS_CAPTION |
				WS_SYSMENU |
				WS_MINIMIZEBOX |
				WS_MAXIMIZEBOX |
				WS_SIZEBOX;

    rect.right  = width;
    rect.bottom = height;

    AdjustWindowRectEx(&rect, win_style, FALSE, win_ex_style);
    const int win_width = rect.right - rect.left;
    const int win_height = rect.bottom - rect.top;
    APP__win32_data.hwnd = CreateWindowExW(
        win_ex_style,                 /* dwExStyle */
        L"APP",                       /* lpClassName */
        APP__win32_data.window_title_wide, /* lpWindowName */
        win_style,                    /* dwStyle */
        CW_USEDEFAULT,                /* X */
        CW_USEDEFAULT,                /* Y */
        win_width,                    /* nWidth */
        win_height,                   /* nHeight */
        NULL,                         /* hWndParent */
        NULL,                         /* hMenu */
        GetModuleHandle(NULL),        /* hInstance */
        NULL);                        /* lParam */
    ShowWindow(APP__win32_data.hwnd, SW_SHOW);
    APP__win32_data.hdc = GetDC(APP__win32_data.hwnd);
	if (!APP__win32_data.hdc) {
		APP_Shutdown();
		return -1;
	}

	// SETUP GL
	{
		APP__win32_data.opengl32 = LoadLibraryA("opengl32.dll");
		if (!APP__win32_data.opengl32) {
		    fprintf(stderr, "Failed to load opengl32.dll\n");
			APP_Shutdown();
			return -1;
		}

		APP__win32_data.wglCreateContext = (PFN_wglCreateContext)(void*)GetProcAddress(APP__win32_data.opengl32, "wglCreateContext");
		if (!APP__win32_data.wglCreateContext) {
		    fprintf(stderr, "Failed to get proc address wglCreateContext\n");
			APP_Shutdown();
			return -1;
		}

		APP__win32_data.wglDeleteContext  = (PFN_wglDeleteContext)(void*)GetProcAddress(APP__win32_data.opengl32, "wglDeleteContext");
		if (!APP__win32_data.wglDeleteContext) {
		    fprintf(stderr, "Failed to get proc address wglDeleteContext\n");
			APP_Shutdown();
			return -1;
		}

		APP__win32_data.wglGetProcAddress = (PFN_wglGetProcAddress)(void*)GetProcAddress(APP__win32_data.opengl32, "wglGetProcAddress");
		if (!APP__win32_data.wglGetProcAddress) {
		    fprintf(stderr, "Failed to get proc address wglGetProcAddress\n");
			APP_Shutdown();
			return -1;
		}

		APP__win32_data.wglGetCurrentDC = (PFN_wglGetCurrentDC)(void*)GetProcAddress(APP__win32_data.opengl32, "wglGetCurrentDC");
		if (!APP__win32_data.wglGetCurrentDC) {
		    fprintf(stderr, "Failed to get proc address wglGetCurrentDC\n");
			APP_Shutdown();
			return -1;
		}

		APP__win32_data.wglMakeCurrent = (PFN_wglMakeCurrent)(void*)GetProcAddress(APP__win32_data.opengl32, "wglMakeCurrent");
		if (!APP__win32_data.wglMakeCurrent) {
		    fprintf(stderr, "Failed to get proc address wglMakeCurrent\n");
			APP_Shutdown();
			return -1;
		}
    	

		PIXELFORMATDESCRIPTOR pfd;
    	/* there is no guarantee that the contents of the stack that become
    	   the pfd are zeroed, therefore _make sure_ to clear these bits. */
    	memset(&pfd, 0, sizeof(pfd));
    	pfd.nSize        = sizeof(pfd);
    	pfd.nVersion     = 1;
    	pfd.dwFlags      = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
    	pfd.iPixelType   = PFD_TYPE_RGBA;
    	pfd.cColorBits   = 24;
		pfd.cDepthBits   = 24;

    	int pf = ChoosePixelFormat(APP__win32_data.hdc, &pfd);
    	if (pf == 0) {
			fprintf(stderr, "ChoosePixelFormat() failed: Cannot find a suitable pixel format."); 
			APP_Shutdown();
			return -1;
    	} 
 
    	if (!SetPixelFormat(APP__win32_data.hdc, pf, &pfd)) {
			fprintf(stderr, "SetPixelFormat() failed: Cannot set format specified.");
			APP_Shutdown();
			return -1;
    	} 

    	DescribePixelFormat(APP__win32_data.hdc, pf, sizeof(PIXELFORMATDESCRIPTOR), &pfd);

		APP__win32_data.gl_ctx = APP__win32_data.wglCreateContext(APP__win32_data.hdc);
		if (!APP__win32_data.gl_ctx) {
			const DWORD err = GetLastError();
			if (err == (0xc0070000 | ERROR_INVALID_VERSION_ARB)) {
				fprintf(stderr, "WGL: Driver does not support OpenGL version 3.3\n");
				APP_Shutdown();
				return -1;
			}
			else if (err == (0xc0070000 | ERROR_INVALID_PROFILE_ARB)) {
				fprintf(stderr, "WGL: Driver does not support the requested OpenGL profile");
				APP_Shutdown();
				return -1;
			}
			else if (err == (0xc0070000 | ERROR_INCOMPATIBLE_DEVICE_CONTEXTS_ARB)) {
				fprintf(stderr, "WGL: The share context is not compatible with the requested context");
				APP_Shutdown();
				return -1;
			}
			else {
				fprintf(stderr, "WGL: Failed to create OpenGL context");
				APP_Shutdown();
				return -1;
			}
		}
		APP__win32_data.wglMakeCurrent(APP__win32_data.hdc, APP__win32_data.gl_ctx);
		

		// What the fuck is this (Tano)
		//if (_sapp.wgl.ext_swap_control) {
		//	/* FIXME: dwmIsCompositionEnabled() (see GLFW) */
		//	_sapp.wgl.SwapIntervalEXT(_sapp.swap_interval);
		//}
		
		APP__win32_data.wglSwapIntervalEXT = (PFNWGLSWAPINTERVALEXTPROC)(void*) APP__win32_data.wglGetProcAddress("wglSwapIntervalEXT");
		if (!APP__win32_data.wglSwapIntervalEXT) {
		    fprintf(stderr, "Failed to get proc address wglSwapIntervalEXT\n");
		}

		if (APP__win32_data.wglSwapIntervalEXT) {
            APP__win32_data.wglSwapIntervalEXT(1);
		}

		// BIND ALL GL FUNCTIONS
		#define APP__GL_XMACRO(name, ret, args) name = (PFN_ ## name) APP__Load_gl_function(#name);
			APP__GL_FUNCS
    	#undef APP__GL_XMACRO
	}

	{ // INIT APP TIME
		QueryPerformanceFrequency(&APP__win32_data.time_freq);
		QueryPerformanceCounter(&APP__win32_data.initial_time);
	}

	return 0;
}

APP_INTERNAL void
APP__win32_Set_fullscreen(bool enable) {
    HMONITOR monitor = MonitorFromWindow(APP__win32_data.hwnd, MONITOR_DEFAULTTONEAREST);
    MONITORINFO minfo;
    memset(&minfo, 0, sizeof(minfo));
    minfo.cbSize = sizeof(MONITORINFO);
    GetMonitorInfo(monitor, &minfo);
    const RECT mr = minfo.rcMonitor;
    const int monitor_w = mr.right - mr.left;
    const int monitor_h = mr.bottom - mr.top;

    const DWORD win_ex_style = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;
    DWORD win_style;
    RECT rect = { 0, 0, 0, 0 };

    if (enable) {
        win_style = WS_POPUP | WS_SYSMENU | WS_VISIBLE;
        rect.right = monitor_w;
        rect.bottom = monitor_h;
    }
    else {
        win_style = WS_CLIPSIBLINGS | WS_CLIPCHILDREN | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_SIZEBOX;
        rect.right  = APP__data.default_width;
        rect.bottom = APP__data.default_height;
    }

    AdjustWindowRectEx(&rect, win_style, FALSE, win_ex_style);

    int win_width = rect.right - rect.left;
    int win_height = rect.bottom - rect.top;

    if (!enable) {
        rect.left = (monitor_w - win_width) / 2;
        rect.top = (monitor_h - win_height) / 2;
    }

    SetWindowLongPtr(APP__win32_data.hwnd, GWL_STYLE, win_style);
    SetWindowPos(APP__win32_data.hwnd, HWND_TOP, mr.left + rect.left, mr.top + rect.top, win_width, win_height, SWP_SHOWWINDOW | SWP_FRAMECHANGED);
	
	// Seems that when we call SetWindowPos it calls internally to our Window_proc with the WM_SIZE
	// event, but Set_fullscreen is called from the frame, and Clear_events clears all events after
	// the frame callback, so the application wouldn't notice the resize.
	//
	// To fix this we set fullscreen_changed to true and clear events will handle this case
	// correctly.
	//
	// Tano 15-08-2021
	APP__data.fullscreen_changed = true;
}

APP_INTERNAL void
APP__win32_Show_mouse(bool show) {
    ShowCursor((BOOL)show);
}

APP_INTERNAL void
APP__win32_Set_window_title(const char *title) {
	APP__String_to_wide(
			title,
			APP__win32_data.window_title_wide,
			sizeof(APP__win32_data.window_title_wide)
	);
    SetWindowTextW(APP__win32_data.hwnd, APP__win32_data.window_title_wide);
}



APP_INTERNAL void
APP__win32_Shutdown() {
	{ // Cleanup GL
		APP__win32_data.wglDeleteContext(APP__win32_data.gl_ctx);
    	APP__win32_data.gl_ctx = 0;
    	FreeLibrary(APP__win32_data.opengl32);
		APP__win32_data.opengl32 = 0;
	}
    DestroyWindow(APP__win32_data.hwnd);
}


APP_INTERNAL void
APP__win32_Process_events() {
	MSG msg;
	while (PeekMessageW(&msg, NULL, 0, 0, PM_REMOVE)) {
		if (WM_QUIT == msg.message) {
			APP__data.quit_requested = true;
			continue;
		}
		else {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}
}

APP_INTERNAL void
APP__win32_Swap_buffers(void) {
	/* get rendered buffer to the screen */
	SwapBuffers(APP__win32_data.hdc);
}

APP_INTERNAL void
APP__win32_Set_swap_interval(unsigned int interval) {
	if (APP__win32_data.wglSwapIntervalEXT) {
		APP__win32_data.wglSwapIntervalEXT(interval);
	}
}

APP_INTERNAL int64_t
APP__win32_Time(void) {
	LARGE_INTEGER qpc_t;
	QueryPerformanceCounter(&qpc_t);

	int64_t counter_diff = qpc_t.QuadPart - APP__win32_data.initial_time.QuadPart;
	int64_t freq         = APP__win32_data.time_freq.QuadPart;
	
	// This is to preserve the precission (from sokol time)
    int64_t q = counter_diff / freq;
    int64_t r = counter_diff % freq;

	const int64_t SECOND = 1000*1000*1000;
	
    return q * SECOND + (r * SECOND) / freq;
}


APP_INTERNAL void
APP__win32_Sleep(int64_t ns) {
    // TODO(Tano) fix this, isn't that good...

    /* Create timer */
    HANDLE timer = CreateWaitableTimer(NULL, TRUE, NULL);
    if (!timer) return;

    /* Set timer properties */
    LARGE_INTEGER li;
    li.QuadPart = -ns/100;
    if(!SetWaitableTimer(timer, &li, 0, NULL, NULL, FALSE)){
    	CloseHandle(timer);
    	return;
    }
    /* Start & wait for timer */
    WaitForSingleObject(timer, INFINITE);
    /* Clean resources */
    CloseHandle(timer);
}














/////////////////////////////////////////////////////////////////////////////////
//
//
//
//                     LINUX IMPLEMENTATION ||||
//                                          VVVV
//
//
////////////////////////////////////////////////////////////////////////////////

#elif defined(APP_LINUX)

#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <dlfcn.h>


// ----------------------------
// Xlib stuff
// ----------------------------

typedef unsigned long XID;
typedef unsigned long Atom;
typedef unsigned long Time;
typedef unsigned long VisualID;
typedef XID Window;
typedef XID Drawable;
typedef XID Pixmap;
typedef XID Cursor;
typedef XID Colormap;
typedef XID GContext;
typedef XID KeySym;
typedef unsigned char KeyCode;
typedef struct _XIM *XIM;
typedef struct _XIC *XIC;
typedef struct Display_ Display;
struct XrmHashBucketRec;
#define Bool int
#define Status int
#define True 1
#define False 0
#define _Xconst const
typedef char *XPointer;

/* http://clang.llvm.org/docs/LanguageExtensions.html#has-attribute */
#ifndef __has_attribute
# define __has_attribute(x) 0  /* Compatibility with non-clang compilers. */
#endif
#ifndef __has_feature
# define __has_feature(x) 0    /* Compatibility with non-clang compilers. */
#endif
#ifndef __has_extension
# define __has_extension(x) 0  /* Compatibility with non-clang compilers. */
#endif

/* Added in X11R6.9, so available in any version of modular xproto */
#if __has_attribute(__sentinel__) || (defined(__GNUC__) && (__GNUC__ >= 4))
# define _X_SENTINEL(x) __attribute__ ((__sentinel__(x)))
#else
# define _X_SENTINEL(x)
#endif /* GNUC >= 4 */

/* Function prototype configuration (see configure for more info) */
#if !defined(NARROWPROTO) && \
    (defined(__linux__) || defined(__FreeBSD__) || defined(__NetBSD__) || defined(__OpenBSD__))
#define NARROWPROTO
#endif
#ifndef FUNCPROTO
#define FUNCPROTO 15
#endif

#ifndef NeedWidePrototypes
#ifdef NARROWPROTO
#define NeedWidePrototypes 0
#else
#define NeedWidePrototypes 1		/* default to make interropt. easier */
#endif
#endif /* NeedWidePrototypes */

/*
 * Extensions need a way to hang private data on some structures.
 */
typedef struct _XExtData {
	int number;		/* number returned by XRegisterExtension */
	struct _XExtData *next;	/* next item on list of data for structure */
	int (*free_private)(	/* called to free private storage */
	struct _XExtData *extension
	);
	XPointer private_data;	/* data private to this extension. */
} XExtData;


/*
 * Visual structure; contains information about colormapping possible.
 */
typedef struct {
	XExtData *ext_data;	/* hook for extension to hang data */
	VisualID visualid;	/* visual id of this visual */
#if defined(__cplusplus) || defined(c_plusplus)
	int c_class;		/* C++ class of screen (monochrome, etc.) */
#else
	int class;		/* class of screen (monochrome, etc.) */
#endif
	unsigned long red_mask, green_mask, blue_mask;	/* mask values */
	int bits_per_rgb;	/* log base 2 of distinct color values */
	int map_entries;	/* color map entries */
} Visual;

/*
 * Data structure for setting window attributes.
 */
typedef struct {
    Pixmap background_pixmap;	/* background or None or ParentRelative */
    unsigned long background_pixel;	/* background pixel */
    Pixmap border_pixmap;	/* border of the window */
    unsigned long border_pixel;	/* border pixel value */
    int bit_gravity;		/* one of bit gravity values */
    int win_gravity;		/* one of the window gravity values */
    int backing_store;		/* NotUseful, WhenMapped, Always */
    unsigned long backing_planes;/* planes to be preserved if possible */
    unsigned long backing_pixel;/* value to use in restoring planes */
    Bool save_under;		/* should bits under be saved? (popups) */
    long event_mask;		/* set of events that should be saved */
    long do_not_propagate_mask;	/* set of events that should not propagate */
    Bool override_redirect;	/* boolean value for override-redirect */
    Colormap colormap;		/* color map to be associated with window */
    Cursor cursor;		/* cursor to be displayed (or None) */
} XSetWindowAttributes;



/*
 * Definitions of specific events.
 */
typedef struct {
	int type;		/* of event */
	unsigned long serial;	/* # of last request processed by server */
	Bool send_event;	/* true if this came from a SendEvent request */
	Display *display;	/* Display the event was read from */
	Window window;	        /* "event" window it is reported relative to */
	Window root;	        /* root window that the event occurred on */
	Window subwindow;	/* child window */
	Time time;		/* milliseconds */
	int x, y;		/* pointer x, y coordinates in event window */
	int x_root, y_root;	/* coordinates relative to root */
	unsigned int state;	/* key or button mask */
	unsigned int keycode;	/* detail */
	Bool same_screen;	/* same screen flag */
} XKeyEvent;
typedef XKeyEvent XKeyPressedEvent;
typedef XKeyEvent XKeyReleasedEvent;

typedef struct {
	int type;		/* of event */
	unsigned long serial;	/* # of last request processed by server */
	Bool send_event;	/* true if this came from a SendEvent request */
	Display *display;	/* Display the event was read from */
	Window window;	        /* "event" window it is reported relative to */
	Window root;	        /* root window that the event occurred on */
	Window subwindow;	/* child window */
	Time time;		/* milliseconds */
	int x, y;		/* pointer x, y coordinates in event window */
	int x_root, y_root;	/* coordinates relative to root */
	unsigned int state;	/* key or button mask */
	unsigned int button;	/* detail */
	Bool same_screen;	/* same screen flag */
} XButtonEvent;
typedef XButtonEvent XButtonPressedEvent;
typedef XButtonEvent XButtonReleasedEvent;

typedef struct {
	int type;		/* of event */
	unsigned long serial;	/* # of last request processed by server */
	Bool send_event;	/* true if this came from a SendEvent request */
	Display *display;	/* Display the event was read from */
	Window window;	        /* "event" window reported relative to */
	Window root;	        /* root window that the event occurred on */
	Window subwindow;	/* child window */
	Time time;		/* milliseconds */
	int x, y;		/* pointer x, y coordinates in event window */
	int x_root, y_root;	/* coordinates relative to root */
	unsigned int state;	/* key or button mask */
	char is_hint;		/* detail */
	Bool same_screen;	/* same screen flag */
} XMotionEvent;
typedef XMotionEvent XPointerMovedEvent;

typedef struct {
	int type;		/* of event */
	unsigned long serial;	/* # of last request processed by server */
	Bool send_event;	/* true if this came from a SendEvent request */
	Display *display;	/* Display the event was read from */
	Window window;	        /* "event" window reported relative to */
	Window root;	        /* root window that the event occurred on */
	Window subwindow;	/* child window */
	Time time;		/* milliseconds */
	int x, y;		/* pointer x, y coordinates in event window */
	int x_root, y_root;	/* coordinates relative to root */
	int mode;		/* NotifyNormal, NotifyGrab, NotifyUngrab */
	int detail;
	/*
	 * NotifyAncestor, NotifyVirtual, NotifyInferior,
	 * NotifyNonlinear,NotifyNonlinearVirtual
	 */
	Bool same_screen;	/* same screen flag */
	Bool focus;		/* boolean focus */
	unsigned int state;	/* key or button mask */
} XCrossingEvent;
typedef XCrossingEvent XEnterWindowEvent;
typedef XCrossingEvent XLeaveWindowEvent;

typedef struct {
	int type;		/* FocusIn or FocusOut */
	unsigned long serial;	/* # of last request processed by server */
	Bool send_event;	/* true if this came from a SendEvent request */
	Display *display;	/* Display the event was read from */
	Window window;		/* window of event */
	int mode;		/* NotifyNormal, NotifyWhileGrabbed,
				   NotifyGrab, NotifyUngrab */
	int detail;
	/*
	 * NotifyAncestor, NotifyVirtual, NotifyInferior,
	 * NotifyNonlinear,NotifyNonlinearVirtual, NotifyPointer,
	 * NotifyPointerRoot, NotifyDetailNone
	 */
} XFocusChangeEvent;
typedef XFocusChangeEvent XFocusInEvent;
typedef XFocusChangeEvent XFocusOutEvent;

/* generated on EnterWindow and FocusIn  when KeyMapState selected */
typedef struct {
	int type;
	unsigned long serial;	/* # of last request processed by server */
	Bool send_event;	/* true if this came from a SendEvent request */
	Display *display;	/* Display the event was read from */
	Window window;
	char key_vector[32];
} XKeymapEvent;

typedef struct {
	int type;
	unsigned long serial;	/* # of last request processed by server */
	Bool send_event;	/* true if this came from a SendEvent request */
	Display *display;	/* Display the event was read from */
	Window window;
	int x, y;
	int width, height;
	int count;		/* if non-zero, at least this many more */
} XExposeEvent;

typedef struct {
	int type;
	unsigned long serial;	/* # of last request processed by server */
	Bool send_event;	/* true if this came from a SendEvent request */
	Display *display;	/* Display the event was read from */
	Drawable drawable;
	int x, y;
	int width, height;
	int count;		/* if non-zero, at least this many more */
	int major_code;		/* core is CopyArea or CopyPlane */
	int minor_code;		/* not defined in the core */
} XGraphicsExposeEvent;

typedef struct {
	int type;
	unsigned long serial;	/* # of last request processed by server */
	Bool send_event;	/* true if this came from a SendEvent request */
	Display *display;	/* Display the event was read from */
	Drawable drawable;
	int major_code;		/* core is CopyArea or CopyPlane */
	int minor_code;		/* not defined in the core */
} XNoExposeEvent;

typedef struct {
	int type;
	unsigned long serial;	/* # of last request processed by server */
	Bool send_event;	/* true if this came from a SendEvent request */
	Display *display;	/* Display the event was read from */
	Window window;
	int state;		/* Visibility state */
} XVisibilityEvent;

typedef struct {
	int type;
	unsigned long serial;	/* # of last request processed by server */
	Bool send_event;	/* true if this came from a SendEvent request */
	Display *display;	/* Display the event was read from */
	Window parent;		/* parent of the window */
	Window window;		/* window id of window created */
	int x, y;		/* window location */
	int width, height;	/* size of window */
	int border_width;	/* border width */
	Bool override_redirect;	/* creation should be overridden */
} XCreateWindowEvent;

typedef struct {
	int type;
	unsigned long serial;	/* # of last request processed by server */
	Bool send_event;	/* true if this came from a SendEvent request */
	Display *display;	/* Display the event was read from */
	Window event;
	Window window;
} XDestroyWindowEvent;

typedef struct {
	int type;
	unsigned long serial;	/* # of last request processed by server */
	Bool send_event;	/* true if this came from a SendEvent request */
	Display *display;	/* Display the event was read from */
	Window event;
	Window window;
	Bool from_configure;
} XUnmapEvent;

typedef struct {
	int type;
	unsigned long serial;	/* # of last request processed by server */
	Bool send_event;	/* true if this came from a SendEvent request */
	Display *display;	/* Display the event was read from */
	Window event;
	Window window;
	Bool override_redirect;	/* boolean, is override set... */
} XMapEvent;

typedef struct {
	int type;
	unsigned long serial;	/* # of last request processed by server */
	Bool send_event;	/* true if this came from a SendEvent request */
	Display *display;	/* Display the event was read from */
	Window parent;
	Window window;
} XMapRequestEvent;

typedef struct {
	int type;
	unsigned long serial;	/* # of last request processed by server */
	Bool send_event;	/* true if this came from a SendEvent request */
	Display *display;	/* Display the event was read from */
	Window event;
	Window window;
	Window parent;
	int x, y;
	Bool override_redirect;
} XReparentEvent;

typedef struct {
	int type;
	unsigned long serial;	/* # of last request processed by server */
	Bool send_event;	/* true if this came from a SendEvent request */
	Display *display;	/* Display the event was read from */
	Window event;
	Window window;
	int x, y;
	int width, height;
	int border_width;
	Window above;
	Bool override_redirect;
} XConfigureEvent;

typedef struct {
	int type;
	unsigned long serial;	/* # of last request processed by server */
	Bool send_event;	/* true if this came from a SendEvent request */
	Display *display;	/* Display the event was read from */
	Window event;
	Window window;
	int x, y;
} XGravityEvent;

typedef struct {
	int type;
	unsigned long serial;	/* # of last request processed by server */
	Bool send_event;	/* true if this came from a SendEvent request */
	Display *display;	/* Display the event was read from */
	Window window;
	int width, height;
} XResizeRequestEvent;

typedef struct {
	int type;
	unsigned long serial;	/* # of last request processed by server */
	Bool send_event;	/* true if this came from a SendEvent request */
	Display *display;	/* Display the event was read from */
	Window parent;
	Window window;
	int x, y;
	int width, height;
	int border_width;
	Window above;
	int detail;		/* Above, Below, TopIf, BottomIf, Opposite */
	unsigned long value_mask;
} XConfigureRequestEvent;

typedef struct {
	int type;
	unsigned long serial;	/* # of last request processed by server */
	Bool send_event;	/* true if this came from a SendEvent request */
	Display *display;	/* Display the event was read from */
	Window event;
	Window window;
	int place;		/* PlaceOnTop, PlaceOnBottom */
} XCirculateEvent;

typedef struct {
	int type;
	unsigned long serial;	/* # of last request processed by server */
	Bool send_event;	/* true if this came from a SendEvent request */
	Display *display;	/* Display the event was read from */
	Window parent;
	Window window;
	int place;		/* PlaceOnTop, PlaceOnBottom */
} XCirculateRequestEvent;

typedef struct {
	int type;
	unsigned long serial;	/* # of last request processed by server */
	Bool send_event;	/* true if this came from a SendEvent request */
	Display *display;	/* Display the event was read from */
	Window window;
	Atom atom;
	Time time;
	int state;		/* NewValue, Deleted */
} XPropertyEvent;

typedef struct {
	int type;
	unsigned long serial;	/* # of last request processed by server */
	Bool send_event;	/* true if this came from a SendEvent request */
	Display *display;	/* Display the event was read from */
	Window window;
	Atom selection;
	Time time;
} XSelectionClearEvent;

typedef struct {
	int type;
	unsigned long serial;	/* # of last request processed by server */
	Bool send_event;	/* true if this came from a SendEvent request */
	Display *display;	/* Display the event was read from */
	Window owner;
	Window requestor;
	Atom selection;
	Atom target;
	Atom property;
	Time time;
} XSelectionRequestEvent;

typedef struct {
	int type;
	unsigned long serial;	/* # of last request processed by server */
	Bool send_event;	/* true if this came from a SendEvent request */
	Display *display;	/* Display the event was read from */
	Window requestor;
	Atom selection;
	Atom target;
	Atom property;		/* ATOM or None */
	Time time;
} XSelectionEvent;

typedef struct {
	int type;
	unsigned long serial;	/* # of last request processed by server */
	Bool send_event;	/* true if this came from a SendEvent request */
	Display *display;	/* Display the event was read from */
	Window window;
	Colormap colormap;	/* COLORMAP or None */
#if defined(__cplusplus) || defined(c_plusplus)
	Bool c_new;		/* C++ */
#else
	Bool new;
#endif
	int state;		/* ColormapInstalled, ColormapUninstalled */
} XColormapEvent;

typedef struct {
	int type;
	unsigned long serial;	/* # of last request processed by server */
	Bool send_event;	/* true if this came from a SendEvent request */
	Display *display;	/* Display the event was read from */
	Window window;
	Atom message_type;
	int format;
	union {
		char b[20];
		short s[10];
		long l[5];
		} data;
} XClientMessageEvent;

typedef struct {
	int type;
	unsigned long serial;	/* # of last request processed by server */
	Bool send_event;	/* true if this came from a SendEvent request */
	Display *display;	/* Display the event was read from */
	Window window;		/* unused */
	int request;		/* one of MappingModifier, MappingKeyboard,
				   MappingPointer */
	int first_keycode;	/* first keycode */
	int count;		/* defines range of change w. first_keycode*/
} XMappingEvent;

typedef struct {
	int type;
	Display *display;	/* Display the event was read from */
	XID resourceid;		/* resource id */
	unsigned long serial;	/* serial number of failed request */
	unsigned char error_code;	/* error code of failed request */
	unsigned char request_code;	/* Major op-code of failed request */
	unsigned char minor_code;	/* Minor op-code of failed request */
} XErrorEvent;

typedef struct {
	int type;
	unsigned long serial;	/* # of last request processed by server */
	Bool send_event;	/* true if this came from a SendEvent request */
	Display *display;/* Display the event was read from */
	Window window;	/* window on which event was requested in event mask */
} XAnyEvent;


/***************************************************************
 *
 * GenericEvent.  This event is the standard event for all newer extensions.
 */

typedef struct
    {
    int            type;         /* of event. Always GenericEvent */
    unsigned long  serial;       /* # of last request processed */
    Bool           send_event;   /* true if from SendEvent request */
    Display        *display;     /* Display the event was read from */
    int            extension;    /* major opcode of extension that caused the event */
    int            evtype;       /* actual event type. */
    } XGenericEvent;

typedef struct {
    int            type;         /* of event. Always GenericEvent */
    unsigned long  serial;       /* # of last request processed */
    Bool           send_event;   /* true if from SendEvent request */
    Display        *display;     /* Display the event was read from */
    int            extension;    /* major opcode of extension that caused the event */
    int            evtype;       /* actual event type. */
    unsigned int   cookie;
    void           *data;
} XGenericEventCookie;

/*
 * this union is defined so Xlib can always use the same sized
 * event structure internally, to avoid memory fragmentation.
 */
typedef union _XEvent {
        int type;		/* must not be changed; first element */
	XAnyEvent xany;
	XKeyEvent xkey;
	XButtonEvent xbutton;
	XMotionEvent xmotion;
	XCrossingEvent xcrossing;
	XFocusChangeEvent xfocus;
	XExposeEvent xexpose;
	XGraphicsExposeEvent xgraphicsexpose;
	XNoExposeEvent xnoexpose;
	XVisibilityEvent xvisibility;
	XCreateWindowEvent xcreatewindow;
	XDestroyWindowEvent xdestroywindow;
	XUnmapEvent xunmap;
	XMapEvent xmap;
	XMapRequestEvent xmaprequest;
	XReparentEvent xreparent;
	XConfigureEvent xconfigure;
	XGravityEvent xgravity;
	XResizeRequestEvent xresizerequest;
	XConfigureRequestEvent xconfigurerequest;
	XCirculateEvent xcirculate;
	XCirculateRequestEvent xcirculaterequest;
	XPropertyEvent xproperty;
	XSelectionClearEvent xselectionclear;
	XSelectionRequestEvent xselectionrequest;
	XSelectionEvent xselection;
	XColormapEvent xcolormap;
	XClientMessageEvent xclient;
	XMappingEvent xmapping;
	XErrorEvent xerror;
	XKeymapEvent xkeymap;
	XGenericEvent xgeneric;
	XGenericEventCookie xcookie;
	long pad[24];
} XEvent;



/*****************************************************************
 * RESERVED RESOURCE AND CONSTANT DEFINITIONS
 *****************************************************************/

#ifndef None
#define None                 0L	/* universal null resource or null atom */
#endif

#define ParentRelative       1L	/* background pixmap in CreateWindow
				    and ChangeWindowAttributes */

#define CopyFromParent       0L	/* border pixmap in CreateWindow
				       and ChangeWindowAttributes
				   special VisualID and special window
				       class passed to CreateWindow */

#define PointerWindow        0L	/* destination window in SendEvent */
#define InputFocus           1L	/* destination window in SendEvent */

#define PointerRoot          1L	/* focus window in SetInputFocus */

#define AnyPropertyType      0L	/* special Atom, passed to GetProperty */

#define AnyKey		     0L	/* special Key Code, passed to GrabKey */

#define AnyButton            0L	/* special Button Code, passed to GrabButton */

#define AllTemporary         0L	/* special Resource ID passed to KillClient */

#define CurrentTime          0L	/* special Time */

#define NoSymbol	     0L	/* special KeySym */

/*****************************************************************
 * EVENT DEFINITIONS
 *****************************************************************/

/* Input Event Masks. Used as event-mask window attribute and as arguments
   to Grab requests.  Not to be confused with event names.  */

#define NoEventMask			0L
#define KeyPressMask			(1L<<0)
#define KeyReleaseMask			(1L<<1)
#define ButtonPressMask			(1L<<2)
#define ButtonReleaseMask		(1L<<3)
#define EnterWindowMask			(1L<<4)
#define LeaveWindowMask			(1L<<5)
#define PointerMotionMask		(1L<<6)
#define PointerMotionHintMask		(1L<<7)
#define Button1MotionMask		(1L<<8)
#define Button2MotionMask		(1L<<9)
#define Button3MotionMask		(1L<<10)
#define Button4MotionMask		(1L<<11)
#define Button5MotionMask		(1L<<12)
#define ButtonMotionMask		(1L<<13)
#define KeymapStateMask			(1L<<14)
#define ExposureMask			(1L<<15)
#define VisibilityChangeMask		(1L<<16)
#define StructureNotifyMask		(1L<<17)
#define ResizeRedirectMask		(1L<<18)
#define SubstructureNotifyMask		(1L<<19)
#define SubstructureRedirectMask	(1L<<20)
#define FocusChangeMask			(1L<<21)
#define PropertyChangeMask		(1L<<22)
#define ColormapChangeMask		(1L<<23)
#define OwnerGrabButtonMask		(1L<<24)

/* Event names.  Used in "type" field in XEvent structures.  Not to be
confused with event masks above.  They start from 2 because 0 and 1
are reserved in the protocol for errors and replies. */

#define KeyPress		2
#define KeyRelease		3
#define ButtonPress		4
#define ButtonRelease		5
#define MotionNotify		6
#define EnterNotify		7
#define LeaveNotify		8
#define FocusIn			9
#define FocusOut		10
#define KeymapNotify		11
#define Expose			12
#define GraphicsExpose		13
#define NoExpose		14
#define VisibilityNotify	15
#define CreateNotify		16
#define DestroyNotify		17
#define UnmapNotify		18
#define MapNotify		19
#define MapRequest		20
#define ReparentNotify		21
#define ConfigureNotify		22
#define ConfigureRequest	23
#define GravityNotify		24
#define ResizeRequest		25
#define CirculateNotify		26
#define CirculateRequest	27
#define PropertyNotify		28
#define SelectionClear		29
#define SelectionRequest	30
#define SelectionNotify		31
#define ColormapNotify		32
#define ClientMessage		33
#define MappingNotify		34
#define GenericEvent		35
#define LASTEvent		36	/* must be bigger than any event # */


/* Key masks. Used as modifiers to GrabButton and GrabKey, results of QueryPointer,
   state in various key-, mouse-, and button-related events. */

#define ShiftMask		(1<<0)
#define LockMask		(1<<1)
#define ControlMask		(1<<2)
#define Mod1Mask		(1<<3)
#define Mod2Mask		(1<<4)
#define Mod3Mask		(1<<5)
#define Mod4Mask		(1<<6)
#define Mod5Mask		(1<<7)

/* modifier names.  Used to build a SetModifierMapping request or
   to read a GetModifierMapping request.  These correspond to the
   masks defined above. */
#define ShiftMapIndex		0
#define LockMapIndex		1
#define ControlMapIndex		2
#define Mod1MapIndex		3
#define Mod2MapIndex		4
#define Mod3MapIndex		5
#define Mod4MapIndex		6
#define Mod5MapIndex		7


/* button masks.  Used in same manner as Key masks above. Not to be confused
   with button names below. */

#define Button1Mask		(1<<8)
#define Button2Mask		(1<<9)
#define Button3Mask		(1<<10)
#define Button4Mask		(1<<11)
#define Button5Mask		(1<<12)

#define AnyModifier		(1<<15)  /* used in GrabButton, GrabKey */


/* button names. Used as arguments to GrabButton and as detail in ButtonPress
   and ButtonRelease events.  Not to be confused with button masks above.
   Note that 0 is already defined above as "AnyButton".  */

#define Button1			1
#define Button2			2
#define Button3			3
#define Button4			4
#define Button5			5

/* Notify modes */

#define NotifyNormal		0
#define NotifyGrab		1
#define NotifyUngrab		2
#define NotifyWhileGrabbed	3

#define NotifyHint		1	/* for MotionNotify events */

/* Notify detail */

#define NotifyAncestor		0
#define NotifyVirtual		1
#define NotifyInferior		2
#define NotifyNonlinear		3
#define NotifyNonlinearVirtual	4
#define NotifyPointer		5
#define NotifyPointerRoot	6
#define NotifyDetailNone	7

/* Visibility notify */

#define VisibilityUnobscured		0
#define VisibilityPartiallyObscured	1
#define VisibilityFullyObscured		2

/* Circulation request */

#define PlaceOnTop		0
#define PlaceOnBottom		1

/* protocol families */

#define FamilyInternet		0	/* IPv4 */
#define FamilyDECnet		1
#define FamilyChaos		2
#define FamilyInternet6		6	/* IPv6 */

/* authentication families not tied to a specific protocol */
#define FamilyServerInterpreted 5

/* Property notification */

#define PropertyNewValue	0
#define PropertyDelete		1

/* Color Map notification */

#define ColormapUninstalled	0
#define ColormapInstalled	1

/* GrabPointer, GrabButton, GrabKeyboard, GrabKey Modes */

#define GrabModeSync		0
#define GrabModeAsync		1

/* GrabPointer, GrabKeyboard reply status */

#define GrabSuccess		0
#define AlreadyGrabbed		1
#define GrabInvalidTime		2
#define GrabNotViewable		3
#define GrabFrozen		4

/* AllowEvents modes */

#define AsyncPointer		0
#define SyncPointer		1
#define ReplayPointer		2
#define AsyncKeyboard		3
#define SyncKeyboard		4
#define ReplayKeyboard		5
#define AsyncBoth		6
#define SyncBoth		7

/*****************************************************************
 * ERROR CODES
 *****************************************************************/

#define Success		   0	/* everything's okay */
#define BadRequest	   1	/* bad request code */
#define BadValue	   2	/* int parameter out of range */
#define BadWindow	   3	/* parameter not a Window */
#define BadPixmap	   4	/* parameter not a Pixmap */
#define BadAtom		   5	/* parameter not an Atom */
#define BadCursor	   6	/* parameter not a Cursor */
#define BadFont		   7	/* parameter not a Font */
#define BadMatch	   8	/* parameter mismatch */
#define BadDrawable	   9	/* parameter not a Pixmap or Window */
#define BadAccess	  10	/* depending on context:
				 - key/button already grabbed
				 - attempt to free an illegal
				   cmap entry
				- attempt to store into a read-only
				   color map entry.
 				- attempt to modify the access control
				   list from other than the local host.
				*/
#define BadAlloc	  11	/* insufficient resources */
#define BadColor	  12	/* no such colormap */
#define BadGC		  13	/* parameter not a GC */
#define BadIDChoice	  14	/* choice not in range or already used */
#define BadName		  15	/* font or color name doesn't exist */
#define BadLength	  16	/* Request length incorrect */
#define BadImplementation 17	/* server is defective */

#define FirstExtensionError	128
#define LastExtensionError	255

/*****************************************************************
 * WINDOW DEFINITIONS
 *****************************************************************/

/* Window classes used by CreateWindow */
/* Note that CopyFromParent is already defined as 0 above */

#define InputOutput		1
#define InputOnly		2

/* Window attributes for CreateWindow and ChangeWindowAttributes */

#define CWBackPixmap		(1L<<0)
#define CWBackPixel		(1L<<1)
#define CWBorderPixmap		(1L<<2)
#define CWBorderPixel           (1L<<3)
#define CWBitGravity		(1L<<4)
#define CWWinGravity		(1L<<5)
#define CWBackingStore          (1L<<6)
#define CWBackingPlanes	        (1L<<7)
#define CWBackingPixel	        (1L<<8)
#define CWOverrideRedirect	(1L<<9)
#define CWSaveUnder		(1L<<10)
#define CWEventMask		(1L<<11)
#define CWDontPropagate	        (1L<<12)
#define CWColormap		(1L<<13)
#define CWCursor	        (1L<<14)

/* ConfigureWindow structure */

#define CWX			(1<<0)
#define CWY			(1<<1)
#define CWWidth			(1<<2)
#define CWHeight		(1<<3)
#define CWBorderWidth		(1<<4)
#define CWSibling		(1<<5)
#define CWStackMode		(1<<6)


/* Bit Gravity */

#define ForgetGravity		0
#define NorthWestGravity	1
#define NorthGravity		2
#define NorthEastGravity	3
#define WestGravity		4
#define CenterGravity		5
#define EastGravity		6
#define SouthWestGravity	7
#define SouthGravity		8
#define SouthEastGravity	9
#define StaticGravity		10

/* Window gravity + bit gravity above */

#define UnmapGravity		0

/* Used in CreateWindow for backing-store hint */

#define NotUseful               0
#define WhenMapped              1
#define Always                  2

/* Used in GetWindowAttributes reply */

#define IsUnmapped		0
#define IsUnviewable		1
#define IsViewable		2

/* Used in ChangeSaveSet */

#define SetModeInsert           0
#define SetModeDelete           1

/* Used in ChangeCloseDownMode */

#define DestroyAll              0
#define RetainPermanent         1
#define RetainTemporary         2

/* Window stacking method (in configureWindow) */

#define Above                   0
#define Below                   1
#define TopIf                   2
#define BottomIf                3
#define Opposite                4

/* Circulation direction */

#define RaiseLowest             0
#define LowerHighest            1

/* Property modes */

#define PropModeReplace         0
#define PropModePrepend         1
#define PropModeAppend          2

/*****************************************************************
 * GRAPHICS DEFINITIONS
 *****************************************************************/

/* graphics functions, as in GC.alu */

#define	GXclear			0x0		/* 0 */
#define GXand			0x1		/* src AND dst */
#define GXandReverse		0x2		/* src AND NOT dst */
#define GXcopy			0x3		/* src */
#define GXandInverted		0x4		/* NOT src AND dst */
#define	GXnoop			0x5		/* dst */
#define GXxor			0x6		/* src XOR dst */
#define GXor			0x7		/* src OR dst */
#define GXnor			0x8		/* NOT src AND NOT dst */
#define GXequiv			0x9		/* NOT src XOR dst */
#define GXinvert		0xa		/* NOT dst */
#define GXorReverse		0xb		/* src OR NOT dst */
#define GXcopyInverted		0xc		/* NOT src */
#define GXorInverted		0xd		/* NOT src OR dst */
#define GXnand			0xe		/* NOT src OR NOT dst */
#define GXset			0xf		/* 1 */

/* LineStyle */

#define LineSolid		0
#define LineOnOffDash		1
#define LineDoubleDash		2

/* capStyle */

#define CapNotLast		0
#define CapButt			1
#define CapRound		2
#define CapProjecting		3

/* joinStyle */

#define JoinMiter		0
#define JoinRound		1
#define JoinBevel		2

/* fillStyle */

#define FillSolid		0
#define FillTiled		1
#define FillStippled		2
#define FillOpaqueStippled	3

/* fillRule */

#define EvenOddRule		0
#define WindingRule		1

/* subwindow mode */

#define ClipByChildren		0
#define IncludeInferiors	1

/* SetClipRectangles ordering */

#define Unsorted		0
#define YSorted			1
#define YXSorted		2
#define YXBanded		3

/* CoordinateMode for drawing routines */

#define CoordModeOrigin		0	/* relative to the origin */
#define CoordModePrevious       1	/* relative to previous point */

/* Polygon shapes */

#define Complex			0	/* paths may intersect */
#define Nonconvex		1	/* no paths intersect, but not convex */
#define Convex			2	/* wholly convex */

/* Arc modes for PolyFillArc */

#define ArcChord		0	/* join endpoints of arc */
#define ArcPieSlice		1	/* join endpoints to center of arc */

/* GC components: masks used in CreateGC, CopyGC, ChangeGC, OR'ed into
   GC.stateChanges */

#define GCFunction              (1L<<0)
#define GCPlaneMask             (1L<<1)
#define GCForeground            (1L<<2)
#define GCBackground            (1L<<3)
#define GCLineWidth             (1L<<4)
#define GCLineStyle             (1L<<5)
#define GCCapStyle              (1L<<6)
#define GCJoinStyle		(1L<<7)
#define GCFillStyle		(1L<<8)
#define GCFillRule		(1L<<9)
#define GCTile			(1L<<10)
#define GCStipple		(1L<<11)
#define GCTileStipXOrigin	(1L<<12)
#define GCTileStipYOrigin	(1L<<13)
#define GCFont 			(1L<<14)
#define GCSubwindowMode		(1L<<15)
#define GCGraphicsExposures     (1L<<16)
#define GCClipXOrigin		(1L<<17)
#define GCClipYOrigin		(1L<<18)
#define GCClipMask		(1L<<19)
#define GCDashOffset		(1L<<20)
#define GCDashList		(1L<<21)
#define GCArcMode		(1L<<22)

#define GCLastBit		22
/*****************************************************************
 * FONTS
 *****************************************************************/

/* used in QueryFont -- draw direction */

#define FontLeftToRight		0
#define FontRightToLeft		1

#define FontChange		255

/*****************************************************************
 *  IMAGING
 *****************************************************************/

/* ImageFormat -- PutImage, GetImage */

#define XYBitmap		0	/* depth 1, XYFormat */
#define XYPixmap		1	/* depth == drawable depth */
#define ZPixmap			2	/* depth == drawable depth */

/*****************************************************************
 *  COLOR MAP STUFF
 *****************************************************************/

/* For CreateColormap */

#define AllocNone		0	/* create map with no entries */
#define AllocAll		1	/* allocate entire map writeable */


/* Flags used in StoreNamedColor, StoreColors */

#define DoRed			(1<<0)
#define DoGreen			(1<<1)
#define DoBlue			(1<<2)

/*****************************************************************
 * CURSOR STUFF
 *****************************************************************/

/* QueryBestSize Class */

#define CursorShape		0	/* largest size that can be displayed */
#define TileShape		1	/* size tiled fastest */
#define StippleShape		2	/* size stippled fastest */

/*****************************************************************
 * KEYBOARD/POINTER STUFF
 *****************************************************************/

#define AutoRepeatModeOff	0
#define AutoRepeatModeOn	1
#define AutoRepeatModeDefault	2

#define LedModeOff		0
#define LedModeOn		1

/* masks for ChangeKeyboardControl */

#define KBKeyClickPercent	(1L<<0)
#define KBBellPercent		(1L<<1)
#define KBBellPitch		(1L<<2)
#define KBBellDuration		(1L<<3)
#define KBLed			(1L<<4)
#define KBLedMode		(1L<<5)
#define KBKey			(1L<<6)
#define KBAutoRepeatMode	(1L<<7)

#define MappingSuccess     	0
#define MappingBusy        	1
#define MappingFailed		2

#define MappingModifier		0
#define MappingKeyboard		1
#define MappingPointer		2

/*****************************************************************
 * SCREEN SAVER STUFF
 *****************************************************************/

#define DontPreferBlanking	0
#define PreferBlanking		1
#define DefaultBlanking		2

#define DisableScreenSaver	0
#define DisableScreenInterval	0

#define DontAllowExposures	0
#define AllowExposures		1
#define DefaultExposures	2

/* for ForceScreenSaver */

#define ScreenSaverReset 0
#define ScreenSaverActive 1

/*****************************************************************
 * HOSTS AND CONNECTIONS
 *****************************************************************/

/* for ChangeHosts */

#define HostInsert		0
#define HostDelete		1

/* for ChangeAccessControl */

#define EnableAccess		1
#define DisableAccess		0

/* Display classes  used in opening the connection
 * Note that the statically allocated ones are even numbered and the
 * dynamically changeable ones are odd numbered */

#define StaticGray		0
#define GrayScale		1
#define StaticColor		2
#define PseudoColor		3
#define TrueColor		4
#define DirectColor		5


/* Byte order  used in imageByteOrder and bitmapBitOrder */

#define LSBFirst		0
#define MSBFirst		1

#define XIMPreeditArea		0x0001L
#define XIMPreeditCallbacks	0x0002L
#define XIMPreeditPosition	0x0004L
#define XIMPreeditNothing	0x0008L
#define XIMPreeditNone		0x0010L
#define XIMStatusArea		0x0100L
#define XIMStatusCallbacks	0x0200L
#define XIMStatusNothing	0x0400L
#define XIMStatusNone		0x0800L

#define XBufferOverflow		-1
#define XLookupNone		1
#define XLookupChars		2
#define XLookupKeySym		3
#define XLookupBoth		4

#define QueuedAlready 0
#define QueuedAfterReading 1
#define QueuedAfterFlush 2


// ATOMS

#define XA_ATOM ((Atom) 4)


#define APP__X11_PROCS(X) \
X(Display *, XOpenDisplay, (_Xconst char*)) \
X(Window,    XDefaultRootWindow, (Display*)) \
X(Window,    XCreateWindow, (Display*, Window, int, int, unsigned int, unsigned int, unsigned int, int, unsigned int, Visual*, unsigned long, XSetWindowAttributes*)) \
X(int,       XMapWindow, (Display*, Window)) \
X(int,       XStoreName, (Display*, Window, _Xconst char*)) \
X(Atom,      XInternAtom, (Display*, _Xconst char*, Bool)) \
X(Status,    XSetWMProtocols, (Display*, Window, Atom*, int)) \
X(XIM,       XOpenIM, (Display*, struct XrmHashBucketRec*, char*, char*)) \
X(XIC,       XCreateIC, (XIM, ...)) \
X(int,       XChangeProperty, (Display*, Window, Atom, Atom, int, int, _Xconst unsigned char*, int)) \
X(int,       XConvertSelection, (Display*, Atom, Atom, Atom, Window, Time)) \
X(Status,    XSendEvent, (Display*, Window, Bool, long, XEvent*)) \
X(int,       XDefineCursor, (Display*, Window, Cursor)) \
X(int,       XUndefineCursor, (Display*, Window)) \
X(int,       XGetWindowProperty, (Display*, Window, Atom, long, long, Bool, Atom, Atom*, int*, unsigned long*, unsigned long*, unsigned char**)) \
X(int,       XFlush, (Display*)) \
X(void,      XDestroyIC, (XIC)) \
X(Status,    XCloseIM, (XIM)) \
X(int,       XUnmapWindow, (Display*, Window)) \
X(int,       XDestroyWindow, (Display*, Window)) \
X(int,       XCloseDisplay, (Display*)) \
X(KeySym *,  XGetKeyboardMapping, (Display*, unsigned int, int, int*)) \
X(int,       XFree, (void*)) \
X(int,       XPending, (Display*)) \
X(int,       XNextEvent, (Display*, XEvent*)) \
X(int,       Xutf8LookupString, (XIC, XKeyPressedEvent*, char*, int, KeySym*, Status*)) \
X(int,       XEventsQueued, (Display*, int)) \
X(int,       XPeekEvent, (Display*, XEvent*))

#define X(retval, name, params) typedef retval (* PFN_ ## name) params;
APP__X11_PROCS(X)
#undef X

#define X(retval, name, params) PFN_ ## name name = NULL;
APP__X11_PROCS(X)
#undef X


APP_INTERNAL void *APP__libX11 = 0;

APP_INTERNAL int
APP__Load_libX11(void) {
    const char* candidates[] = {"libX11.so", "libX11.so.6", "libX11.so.6.4.0"};
    for (int i = 0; i < 3; i+=1) {
        APP__libX11 = dlopen(candidates[i], RTLD_LAZY);
        if (APP__libX11) break;
    }
    if (!APP__libX11) {
    	fprintf(stderr, "Cannot load libX11.so\n");
    	return -1;
    }

    #define X(retval, name, params) name = (PFN_ ## name) dlsym(APP__libX11, #name);
    APP__X11_PROCS(X)
    #undef X


    #define X(retval, name, params) if (!name) return -1;
    APP__X11_PROCS(X)
    #undef X

    return 0;
}


APP_INTERNAL void
APP__Unload_libX11(void) {
    if (APP__libX11) dlclose(APP__libX11);
    APP__libX11 = 0;
}




// ----------------------------
// libXcursor stuff
// ----------------------------

typedef int	XcursorBool;
typedef uint32_t XcursorUInt;

typedef XcursorUInt	XcursorDim;
typedef XcursorUInt	XcursorPixel;

typedef struct _XcursorImage {
    XcursorUInt	    version;	/* version of the image data */
    XcursorDim	    size;	/* nominal size for matching */
    XcursorDim	    width;	/* actual width */
    XcursorDim	    height;	/* actual height */
    XcursorDim	    xhot;	/* hot spot x (must be inside image) */
    XcursorDim	    yhot;	/* hot spot y (must be inside image) */
    XcursorUInt	    delay;	/* animation delay to next frame (ms) */
    XcursorPixel    *pixels;	/* pointer to pixels */
} XcursorImage;

/*
 * Manage Image objects
 */
typedef XcursorImage *(*PFN_XcursorImageCreate) (int width, int height);
typedef void (*PFN_XcursorImageDestroy) (XcursorImage *image);
typedef Cursor (*PFN_XcursorImageLoadCursor) (Display *dpy, const XcursorImage *image);

PFN_XcursorImageCreate     XcursorImageCreate     = 0;
PFN_XcursorImageDestroy    XcursorImageDestroy    = 0;
PFN_XcursorImageLoadCursor XcursorImageLoadCursor = 0;

APP_INTERNAL void *APP__libXcursor = 0;

APP_INTERNAL int
APP__Load_libXcursor(void) {
    const char* candidates[] = {"libXcursor.so", "libXcursor.so.1"};
    for (int i = 0; i < 2; i+=1) {
        APP__libXcursor = dlopen(candidates[i], RTLD_LAZY);
        if (APP__libXcursor) break;
    }
    if (!APP__libXcursor) {
    	fprintf(stderr, "Cannot load libXcursor.so\n");
    	return -1;
    }

    XcursorImageCreate     = (PFN_XcursorImageCreate    )dlsym(APP__libXcursor, "XcursorImageCreate");
    XcursorImageDestroy    = (PFN_XcursorImageDestroy   )dlsym(APP__libXcursor, "XcursorImageDestroy");
    XcursorImageLoadCursor = (PFN_XcursorImageLoadCursor)dlsym(APP__libXcursor, "XcursorImageLoadCursor");

    if (!XcursorImageCreate    ) return -1;
    if (!XcursorImageDestroy   ) return -1;
    if (!XcursorImageLoadCursor) return -1;

    return 0;
}

APP_INTERNAL void
APP__Unload_libXcursor(void) {
    if (APP__libXcursor) dlclose(APP__libXcursor);
    APP__libXcursor = 0;
}






// -----------------------------------------------
// EGL stuff
// ----------------------------------------------

typedef Display *EGLNativeDisplayType;
typedef Window   EGLNativeWindowType;
typedef void *EGLDisplay;
typedef void *EGLConfig;
typedef void *EGLContext;
typedef void *EGLSurface;
typedef int32_t EGLint;
typedef unsigned int EGLBoolean;

#define EGL_CAST(type, value) ((type) (value))

#define EGL_ALPHA_FORMAT 0x3088
#define EGL_ALPHA_FORMAT_NONPRE 0x308B
#define EGL_ALPHA_FORMAT_PRE 0x308C
#define EGL_ALPHA_MASK_SIZE 0x303E
#define EGL_ALPHA_SIZE 0x3021
#define EGL_BACK_BUFFER 0x3084
#define EGL_BAD_ACCESS 0x3002
#define EGL_BAD_ALLOC 0x3003
#define EGL_BAD_ATTRIBUTE 0x3004
#define EGL_BAD_CONFIG 0x3005
#define EGL_BAD_CONTEXT 0x3006
#define EGL_BAD_CURRENT_SURFACE 0x3007
#define EGL_BAD_DISPLAY 0x3008
#define EGL_BAD_MATCH 0x3009
#define EGL_BAD_NATIVE_PIXMAP 0x300A
#define EGL_BAD_NATIVE_WINDOW 0x300B
#define EGL_BAD_PARAMETER 0x300C
#define EGL_BAD_SURFACE 0x300D
#define EGL_BIND_TO_TEXTURE_RGB 0x3039
#define EGL_BIND_TO_TEXTURE_RGBA 0x303A
#define EGL_BLUE_SIZE 0x3022
#define EGL_BUFFER_DESTROYED 0x3095
#define EGL_BUFFER_PRESERVED 0x3094
#define EGL_BUFFER_SIZE 0x3020
#define EGL_CLIENT_APIS 0x308D
#define EGL_CL_EVENT_HANDLE 0x309C
#define EGL_COLORSPACE 0x3087
#define EGL_COLORSPACE_LINEAR 0x308A
#define EGL_COLORSPACE_sRGB 0x3089
#define EGL_COLOR_BUFFER_TYPE 0x303F
#define EGL_CONDITION_SATISFIED 0x30F6
#define EGL_CONFIG_CAVEAT 0x3027
#define EGL_CONFIG_ID 0x3028
#define EGL_CONFORMANT 0x3042
#define EGL_CONTEXT_CLIENT_TYPE 0x3097
#define EGL_CONTEXT_CLIENT_VERSION 0x3098
#define EGL_CONTEXT_LOST 0x300E
#define EGL_CONTEXT_MAJOR_VERSION 0x3098
#define EGL_CONTEXT_MINOR_VERSION 0x30FB
#define EGL_CONTEXT_OPENGL_COMPATIBILITY_PROFILE_BIT 0x00000002
#define EGL_CONTEXT_OPENGL_CORE_PROFILE_BIT 0x00000001
#define EGL_CONTEXT_OPENGL_DEBUG 0x31B0
#define EGL_CONTEXT_OPENGL_FORWARD_COMPATIBLE 0x31B1
#define EGL_CONTEXT_OPENGL_PROFILE_MASK 0x30FD
#define EGL_CONTEXT_OPENGL_RESET_NOTIFICATION_STRATEGY 0x31BD
#define EGL_CONTEXT_OPENGL_ROBUST_ACCESS 0x31B2
#define EGL_CORE_NATIVE_ENGINE 0x305B
#define EGL_DEFAULT_DISPLAY EGL_CAST(EGLNativeDisplayType,0)
#define EGL_DEPTH_SIZE 0x3025
#define EGL_DISPLAY_SCALING 10000
#define EGL_DONT_CARE EGL_CAST(EGLint,-1)
#define EGL_DRAW 0x3059
#define EGL_EXTENSIONS 0x3055
#define EGL_FALSE 0
#define EGL_FOREVER 0xFFFFFFFFFFFFFFFF
#define EGL_GL_COLORSPACE 0x309D
#define EGL_GL_COLORSPACE_LINEAR 0x308A
#define EGL_GL_COLORSPACE_SRGB 0x3089
#define EGL_GL_RENDERBUFFER 0x30B9
#define EGL_GL_TEXTURE_2D 0x30B1
#define EGL_GL_TEXTURE_3D 0x30B2
#define EGL_GL_TEXTURE_CUBE_MAP_NEGATIVE_X 0x30B4
#define EGL_GL_TEXTURE_CUBE_MAP_NEGATIVE_Y 0x30B6
#define EGL_GL_TEXTURE_CUBE_MAP_NEGATIVE_Z 0x30B8
#define EGL_GL_TEXTURE_CUBE_MAP_POSITIVE_X 0x30B3
#define EGL_GL_TEXTURE_CUBE_MAP_POSITIVE_Y 0x30B5
#define EGL_GL_TEXTURE_CUBE_MAP_POSITIVE_Z 0x30B7
#define EGL_GL_TEXTURE_LEVEL 0x30BC
#define EGL_GL_TEXTURE_ZOFFSET 0x30BD
#define EGL_GREEN_SIZE 0x3023
#define EGL_HEIGHT 0x3056
#define EGL_HORIZONTAL_RESOLUTION 0x3090
#define EGL_IMAGE_PRESERVED 0x30D2
#define EGL_LARGEST_PBUFFER 0x3058
#define EGL_LEVEL 0x3029
#define EGL_LOSE_CONTEXT_ON_RESET 0x31BF
#define EGL_LUMINANCE_BUFFER 0x308F
#define EGL_LUMINANCE_SIZE 0x303D
#define EGL_MATCH_NATIVE_PIXMAP 0x3041
#define EGL_MAX_PBUFFER_HEIGHT 0x302A
#define EGL_MAX_PBUFFER_PIXELS 0x302B
#define EGL_MAX_PBUFFER_WIDTH 0x302C
#define EGL_MAX_SWAP_INTERVAL 0x303C
#define EGL_MIN_SWAP_INTERVAL 0x303B
#define EGL_MIPMAP_LEVEL 0x3083
#define EGL_MIPMAP_TEXTURE 0x3082
#define EGL_MULTISAMPLE_RESOLVE 0x3099
#define EGL_MULTISAMPLE_RESOLVE_BOX 0x309B
#define EGL_MULTISAMPLE_RESOLVE_BOX_BIT 0x0200
#define EGL_MULTISAMPLE_RESOLVE_DEFAULT 0x309A
#define EGL_NATIVE_RENDERABLE 0x302D
#define EGL_NATIVE_VISUAL_ID 0x302E
#define EGL_NATIVE_VISUAL_TYPE 0x302F
#define EGL_NONE 0x3038
#define EGL_NON_CONFORMANT_CONFIG 0x3051
#define EGL_NOT_INITIALIZED 0x3001
#define EGL_NO_CONTEXT EGL_CAST(EGLContext,0)
#define EGL_NO_DISPLAY EGL_CAST(EGLDisplay,0)
#define EGL_NO_IMAGE EGL_CAST(EGLImage,0)
#define EGL_NO_RESET_NOTIFICATION 0x31BE
#define EGL_NO_SURFACE EGL_CAST(EGLSurface,0)
#define EGL_NO_SYNC EGL_CAST(EGLSync,0)
#define EGL_NO_TEXTURE 0x305C
#define EGL_OPENGL_API 0x30A2
#define EGL_OPENGL_BIT 0x0008
#define EGL_OPENGL_ES2_BIT 0x0004
#define EGL_OPENGL_ES3_BIT 0x00000040
#define EGL_OPENGL_ES_API 0x30A0
#define EGL_OPENGL_ES_BIT 0x0001
#define EGL_OPENVG_API 0x30A1
#define EGL_OPENVG_BIT 0x0002
#define EGL_OPENVG_IMAGE 0x3096
#define EGL_PBUFFER_BIT 0x0001
#define EGL_PIXEL_ASPECT_RATIO 0x3092
#define EGL_PIXMAP_BIT 0x0002
#define EGL_READ 0x305A
#define EGL_RED_SIZE 0x3024
#define EGL_RENDERABLE_TYPE 0x3040
#define EGL_RENDER_BUFFER 0x3086
#define EGL_RGB_BUFFER 0x308E
#define EGL_SAMPLES 0x3031
#define EGL_SAMPLE_BUFFERS 0x3032
#define EGL_SIGNALED 0x30F2
#define EGL_SINGLE_BUFFER 0x3085
#define EGL_SLOW_CONFIG 0x3050
#define EGL_STENCIL_SIZE 0x3026
#define EGL_SUCCESS 0x3000
#define EGL_SURFACE_TYPE 0x3033
#define EGL_SWAP_BEHAVIOR 0x3093
#define EGL_SWAP_BEHAVIOR_PRESERVED_BIT 0x0400
#define EGL_SYNC_CL_EVENT 0x30FE
#define EGL_SYNC_CL_EVENT_COMPLETE 0x30FF
#define EGL_SYNC_CONDITION 0x30F8
#define EGL_SYNC_FENCE 0x30F9
#define EGL_SYNC_FLUSH_COMMANDS_BIT 0x0001
#define EGL_SYNC_PRIOR_COMMANDS_COMPLETE 0x30F0
#define EGL_SYNC_STATUS 0x30F1
#define EGL_SYNC_TYPE 0x30F7
#define EGL_TEXTURE_2D 0x305F
#define EGL_TEXTURE_FORMAT 0x3080
#define EGL_TEXTURE_RGB 0x305D
#define EGL_TEXTURE_RGBA 0x305E
#define EGL_TEXTURE_TARGET 0x3081
#define EGL_TIMEOUT_EXPIRED 0x30F5
#define EGL_TRANSPARENT_BLUE_VALUE 0x3035
#define EGL_TRANSPARENT_GREEN_VALUE 0x3036
#define EGL_TRANSPARENT_RED_VALUE 0x3037
#define EGL_TRANSPARENT_RGB 0x3052
#define EGL_TRANSPARENT_TYPE 0x3034
#define EGL_TRUE 1
#define EGL_UNKNOWN EGL_CAST(EGLint,-1)
#define EGL_UNSIGNALED 0x30F3
#define EGL_VENDOR 0x3053
#define EGL_VERSION 0x3054
#define EGL_VERTICAL_RESOLUTION 0x3091
#define EGL_VG_ALPHA_FORMAT 0x3088
#define EGL_VG_ALPHA_FORMAT_NONPRE 0x308B
#define EGL_VG_ALPHA_FORMAT_PRE 0x308C
#define EGL_VG_ALPHA_FORMAT_PRE_BIT 0x0040
#define EGL_VG_COLORSPACE 0x3087
#define EGL_VG_COLORSPACE_LINEAR 0x308A
#define EGL_VG_COLORSPACE_LINEAR_BIT 0x0020
#define EGL_VG_COLORSPACE_sRGB 0x3089
#define EGL_WIDTH 0x3057
#define EGL_WINDOW_BIT 0x0004

typedef EGLDisplay (*PFN_eglGetDisplay)(EGLNativeDisplayType display_id);
typedef EGLBoolean (*PFN_eglInitialize)(EGLDisplay dpy, EGLint * major, EGLint * minor);
typedef EGLBoolean (*PFN_eglChooseConfig)(EGLDisplay dpy, const EGLint * attrib_list, EGLConfig * configs, EGLint config_size, EGLint * num_config);
typedef EGLSurface (*PFN_eglCreateWindowSurface)(EGLDisplay dpy, EGLConfig config, EGLNativeWindowType win, const EGLint * attrib_list);
typedef EGLContext (*PFN_eglCreateContext)(EGLDisplay dpy, EGLConfig config, EGLContext share_context, const EGLint * attrib_list);
typedef EGLBoolean (*PFN_eglMakeCurrent)(EGLDisplay dpy, EGLSurface draw, EGLSurface read, EGLContext ctx);
typedef EGLBoolean (*PFN_eglDestroySurface)(EGLDisplay dpy, EGLSurface surface);
typedef EGLBoolean (*PFN_eglDestroyContext)(EGLDisplay dpy, EGLContext ctx);
typedef EGLBoolean (*PFN_eglTerminate)(EGLDisplay dpy);
typedef EGLBoolean (*PFN_eglSwapBuffers)(EGLDisplay dpy, EGLSurface surface);
typedef EGLBoolean (*PFN_eglSwapInterval)(EGLDisplay dpy, EGLint interval);
typedef EGLint (*PFN_eglGetError)(void);

PFN_eglGetDisplay          eglGetDisplay          = 0;
PFN_eglInitialize          eglInitialize          = 0;
PFN_eglChooseConfig        eglChooseConfig        = 0;
PFN_eglCreateWindowSurface eglCreateWindowSurface = 0;
PFN_eglCreateContext       eglCreateContext       = 0;
PFN_eglMakeCurrent         eglMakeCurrent         = 0;
PFN_eglDestroySurface      eglDestroySurface      = 0;
PFN_eglDestroyContext      eglDestroyContext      = 0;
PFN_eglTerminate           eglTerminate           = 0;
PFN_eglSwapBuffers         eglSwapBuffers         = 0;
PFN_eglSwapInterval        eglSwapInterval        = 0;
PFN_eglGetError            eglGetError            = 0;

APP_INTERNAL void *APP__libEGL = 0;

APP_INTERNAL int
APP__Load_libEGL(void) {
    const char* candidates[] = {"libEGL.so", "libEGL.so.1", "libEGL.so.1.1.0"};
    for (int i = 0; i < 3; i+=1) {
        APP__libEGL = dlopen(candidates[i], RTLD_LAZY);
        if (APP__libEGL) break;
    }
    if (!APP__libEGL) {
    	fprintf(stderr, "Cannot load libEGL.so\n");
    	return -1;
    }

    eglGetDisplay          = (PFN_eglGetDisplay         )dlsym(APP__libEGL, "eglGetDisplay"         );
    eglInitialize          = (PFN_eglInitialize         )dlsym(APP__libEGL, "eglInitialize"         );
    eglChooseConfig        = (PFN_eglChooseConfig       )dlsym(APP__libEGL, "eglChooseConfig"       );
    eglCreateWindowSurface = (PFN_eglCreateWindowSurface)dlsym(APP__libEGL, "eglCreateWindowSurface");
    eglCreateContext       = (PFN_eglCreateContext      )dlsym(APP__libEGL, "eglCreateContext"      );
    eglMakeCurrent         = (PFN_eglMakeCurrent        )dlsym(APP__libEGL, "eglMakeCurrent"        );
    eglDestroySurface      = (PFN_eglDestroySurface     )dlsym(APP__libEGL, "eglDestroySurface"     );
    eglDestroyContext      = (PFN_eglDestroyContext     )dlsym(APP__libEGL, "eglDestroyContext"     );
    eglTerminate           = (PFN_eglTerminate          )dlsym(APP__libEGL, "eglTerminate"          );
    eglSwapBuffers         = (PFN_eglSwapBuffers        )dlsym(APP__libEGL, "eglSwapBuffers"        );
    eglSwapInterval        = (PFN_eglSwapInterval       )dlsym(APP__libEGL, "eglSwapInterval"       );
    eglGetError            = (PFN_eglGetError           )dlsym(APP__libEGL, "eglGetError"           );

    if (!eglGetDisplay         ) return -1;
    if (!eglInitialize         ) return -1;
    if (!eglChooseConfig       ) return -1;
    if (!eglCreateWindowSurface) return -1;
    if (!eglCreateContext      ) return -1;
    if (!eglMakeCurrent        ) return -1;
    if (!eglDestroySurface     ) return -1;
    if (!eglDestroyContext     ) return -1;
    if (!eglTerminate          ) return -1;
    if (!eglSwapBuffers        ) return -1;
    if (!eglSwapInterval       ) return -1;
    if (!eglGetError           ) return -1;

    return 0;
}


APP_INTERNAL void
APP__Unload_libEGL(void) {
    if (APP__libEGL) dlclose(APP__libEGL);
    APP__libEGL = 0;
}





// ------------------------------------------------
// Load GLES
// ------------------------------------------------

APP_INTERNAL void *APP__libGLES = 0;

// generate GL function pointers
#define APP__GL_XMACRO(name, ret, args) PFN_ ## name name;
	APP__GL_FUNCS
#undef APP__GL_XMACRO

APP_INTERNAL void *
APP__Load_gl_proc(const char *func_name) {
    void *proc_addr = dlsym(APP__libGLES, func_name);
    if (proc_addr == NULL) {
		printf("CANNOT LOAD %s\n", func_name);
	}
    return proc_addr;
}


APP_INTERNAL int
APP__Load_libGLES(void) {
    const char* candidates[] = {"libGLESv2.so", "libGLESv2.so.2", "libGLESv2.so.2.1.0"};
    for (int i = 0; i < 3; i+=1) {
        APP__libGLES = dlopen(candidates[i], RTLD_LAZY);
        if (APP__libGLES) break;
    }
    if (!APP__libGLES) {
    	fprintf(stderr, "Cannot load libGLESv2.so\n");
    	return -1;
    }

	// BIND ALL GL FUNCTIONS
	#define APP__GL_XMACRO(name, ret, args) name = (PFN_ ## name) APP__Load_gl_proc(#name);
		APP__GL_FUNCS
    #undef APP__GL_XMACRO

    return 0;
}

APP_INTERNAL void
APP__Unload_libGLES(void) {
    if (APP__libGLES) dlclose(APP__libGLES);
    APP__libGLES = 0;
}


static struct {
	Window root;
	Window  win;
	Display *xdisplay;
	XSetWindowAttributes swa;
    Atom WM_PROTOCOLS;
    Atom WM_DELETE_WINDOW;
	Atom NET_WM_STATE;
    Atom NET_WM_STATE_FULLSCREEN;

    struct {
        Atom XdndAware;
        Atom XdndEnter;
        Atom XdndPosition;
        Atom XdndStatus;
        Atom XdndActionCopy;
        Atom XdndDrop;
        Atom XdndFinished;
        Atom XdndSelection;
        Atom XdndTypeList;
        Atom text_uri_list;

        int    version;
        Atom   format;
        Window source;
    } xdnd;

	// Text input
	XIM xim;
	XIC	xic;

	Cursor hidden_cursor;

	// EGL-related objects
	EGLDisplay egl_display;
	EGLConfig  egl_conf;
	EGLContext egl_context;
	EGLSurface egl_surface;

	// APP TIME
	struct timespec initial_time;

} APP__x11_data = {0};


#define APP__X11_XDND_VERSION 5

APP_INTERNAL int
APP__linux_Init(const char *title, int width, int height) {

	APP__data.default_width  = width;
	APP__data.default_height = height;
	APP__data.w_width  = width;
	APP__data.w_height = height;

    if (APP__Load_libX11() != 0) {
        APP_Shutdown();
        return -1;
    }

    if (APP__Load_libXcursor() != 0) {
        APP_Shutdown();
        return -1;
    }

    if (APP__Load_libEGL() != 0) {
        APP_Shutdown();
        return -1;
    }

    if (APP__Load_libGLES() != 0) {
        APP_Shutdown();
        return -1;
    }


	/* open standard display (primary screen) */
	APP__x11_data.xdisplay = XOpenDisplay(NULL);   
	if (APP__x11_data.xdisplay == NULL) {
	    printf("Error opening X display\n");
	    return -1;
	}
	// get the root window (usually the whole screen)
	APP__x11_data.root = XDefaultRootWindow(APP__x11_data.xdisplay);
	
	// list all events this window accepts
	APP__x11_data.swa.event_mask =
		StructureNotifyMask |
		ExposureMask        |
		PointerMotionMask   |
		KeyPressMask        |
		KeyReleaseMask      |
		ButtonPressMask     |
		FocusChangeMask     |
		ButtonReleaseMask;
	
	// Xlib's window creation
	APP__x11_data.win = XCreateWindow (
			APP__x11_data.xdisplay, APP__x11_data.root, 0, 0, width, height, 0,
			CopyFromParent, InputOutput, CopyFromParent, CWEventMask,
			&APP__x11_data.swa);
	
	XMapWindow(APP__x11_data.xdisplay, APP__x11_data.win);         // make window visible
	XStoreName(APP__x11_data.xdisplay, APP__x11_data.win, title);
    
	// To handle windows close
	APP__x11_data.WM_PROTOCOLS     = XInternAtom(APP__x11_data.xdisplay, "WM_PROTOCOLS", False);
    APP__x11_data.WM_DELETE_WINDOW = XInternAtom(APP__x11_data.xdisplay, "WM_DELETE_WINDOW", False);
	APP__x11_data.NET_WM_STATE     = XInternAtom(APP__x11_data.xdisplay, "_NET_WM_STATE", False);
	APP__x11_data.NET_WM_STATE_FULLSCREEN = XInternAtom(APP__x11_data.xdisplay, "_NET_WM_STATE_FULLSCREEN", False);
    Atom protocols[] = {
        APP__x11_data.WM_DELETE_WINDOW
    };
    XSetWMProtocols(APP__x11_data.xdisplay, APP__x11_data.win, protocols, 1);
    
    // Drag and drop stuff
    APP__x11_data.xdnd.XdndAware      = XInternAtom(APP__x11_data.xdisplay, "XdndAware", False);
    APP__x11_data.xdnd.XdndEnter      = XInternAtom(APP__x11_data.xdisplay, "XdndEnter", False);
    APP__x11_data.xdnd.XdndPosition   = XInternAtom(APP__x11_data.xdisplay, "XdndPosition", False);
    APP__x11_data.xdnd.XdndStatus     = XInternAtom(APP__x11_data.xdisplay, "XdndStatus", False);
    APP__x11_data.xdnd.XdndActionCopy = XInternAtom(APP__x11_data.xdisplay, "XdndActionCopy", False);
    APP__x11_data.xdnd.XdndDrop       = XInternAtom(APP__x11_data.xdisplay, "XdndDrop", False);
    APP__x11_data.xdnd.XdndFinished   = XInternAtom(APP__x11_data.xdisplay, "XdndFinished", False);
    APP__x11_data.xdnd.XdndSelection  = XInternAtom(APP__x11_data.xdisplay, "XdndSelection", False);
    APP__x11_data.xdnd.XdndTypeList   = XInternAtom(APP__x11_data.xdisplay, "XdndTypeList", False);
    APP__x11_data.xdnd.text_uri_list  = XInternAtom(APP__x11_data.xdisplay, "text/uri-list", False);
    /* announce support for drag'n'drop */
    Atom version = APP__X11_XDND_VERSION;
    XChangeProperty(
        APP__x11_data.xdisplay,
        APP__x11_data.win,
        APP__x11_data.xdnd.XdndAware,
        XA_ATOM,
        32,
        PropModeReplace,
        (unsigned char*) &version, 1);
    


	{ // Prepare for keyboard text input
		APP__x11_data.xim = XOpenIM(APP__x11_data.xdisplay, 0, 0, 0);
		APP__x11_data.xic = XCreateIC(APP__x11_data.xim, "inputStyle", XIMPreeditNothing | XIMStatusNothing, NULL);
	}

	{ // Create a invisible cursor
		const int w = 16;
    	const int h = 16;
    	XcursorImage* img = XcursorImageCreate(w, h);
		if (img == NULL) {
			APP_Shutdown();
			return -1;
		}
    	img->xhot = 0;
    	img->yhot = 0;
		for (int i = 0; i < w*h; i+=1) img->pixels[i] = 0; // Set all to 0

    	APP__x11_data.hidden_cursor = XcursorImageLoadCursor(APP__x11_data.xdisplay, img);
    	XcursorImageDestroy(img);
	}

	{ // Setup OpenGL

		EGLint attr[] = {
    	    EGL_SURFACE_TYPE,    EGL_WINDOW_BIT,
    	    EGL_RED_SIZE,        8,
    	    EGL_GREEN_SIZE,      8,
    	    EGL_BLUE_SIZE,       8,
			EGL_DEPTH_SIZE,      24,
            // NOTE(Tano) This is for multisampling but doesn't work on the pinephone :/
            // EGL_SAMPLE_BUFFERS, 1,
            // EGL_SAMPLES, 2,
    	    EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT, /* If one needs GLES2 */
    	    EGL_NONE
    	};

    	EGLint num_config;
    	EGLint major, minor;

    	EGLint ctxattr[] = {
    	   EGL_CONTEXT_CLIENT_VERSION, 2,
    	   EGL_NONE
    	};

    	APP__x11_data.egl_display = eglGetDisplay((EGLNativeDisplayType)APP__x11_data.xdisplay);
    	if (APP__x11_data.egl_display == EGL_NO_DISPLAY) {
    	    fprintf(stderr, "Error getting EGL display\n");
			APP_Shutdown();
    	    return -1;
    	}

    	if (eglInitialize(APP__x11_data.egl_display, &major, &minor) != EGL_TRUE) {
    	    fprintf(stderr, "Error initializing EGL\n");
			APP_Shutdown();
    	    return -1;
    	}
    	// printf("EGL major: %d, minor %d\n", major, minor);

    	/* create EGL rendering context */
    	if (!eglChooseConfig(APP__x11_data.egl_display, attr, &APP__x11_data.egl_conf, 1, &num_config )) {
    	    fprintf(stderr, "Failed to choose config (eglError: %x)\n", eglGetError());
			APP_Shutdown();
    	    return -1;
    	}

    	if (num_config != 1) {
    	    return -1;
    	}

    	APP__x11_data.egl_surface = eglCreateWindowSurface(
				APP__x11_data.egl_display,
				APP__x11_data.egl_conf,
				APP__x11_data.win,
				NULL
		);
    	if (APP__x11_data.egl_surface == EGL_NO_SURFACE) {
    	    fprintf(stderr, "CreateWindowSurface, EGL eglError: %d\n", eglGetError());
			APP_Shutdown();
    	    return -1;
    	}

    	APP__x11_data.egl_context = eglCreateContext(
				APP__x11_data.egl_display,
				APP__x11_data.egl_conf,
				EGL_NO_CONTEXT,
				ctxattr
		);
    	if (APP__x11_data.egl_context == EGL_NO_CONTEXT) {
    	    fprintf(stderr, "CreateContext, EGL eglError: %d\n", eglGetError() );
			APP_Shutdown();
    	    return -1;
    	}
		
		//
		// WARNING LEAK!!!!
		// This generates a leak, no idea why....
		//
		if (eglMakeCurrent(
				APP__x11_data.egl_display,
				APP__x11_data.egl_surface,
				APP__x11_data.egl_surface,
				APP__x11_data.egl_context
		) != EGL_TRUE) {
    	    fprintf(stderr, "MakeCurrent, EGL eglError: %d\n", eglGetError() );
			APP_Shutdown();
    	    return -1;
		}
	}


	APP_Set_swap_interval(1);
	
    clock_gettime(CLOCK_MONOTONIC_RAW, &APP__x11_data.initial_time);


	return 0;
}


APP_INTERNAL unsigned long
APP__X11_get_window_property(Window window, Atom property, Atom type, unsigned char** value) {
    Atom actual_type;
    int actual_format;
    unsigned long item_count, bytes_after;
    XGetWindowProperty(APP__x11_data.xdisplay,
                       window,
                       property,
                       0,
                       ~((unsigned long)0), // LONG_MAX
                       False,
                       type,
                       &actual_type,
                       &actual_format,
                       &item_count,
                       &bytes_after,
                       value);
    return item_count;
}
 

APP_INTERNAL void
APP__X11_send_event(Atom type, int a, int b, int c, int d, int e) {
    XEvent event = {0};

    event.type = ClientMessage;
    event.xclient.window = APP__x11_data.win;
    event.xclient.format = 32;
    event.xclient.message_type = type;
    event.xclient.data.l[0] = a;
    event.xclient.data.l[1] = b;
    event.xclient.data.l[2] = c;
    event.xclient.data.l[3] = d;
    event.xclient.data.l[4] = e;

    XSendEvent(APP__x11_data.xdisplay, APP__x11_data.root,
               False,
               SubstructureNotifyMask | SubstructureRedirectMask,
               &event);
}

APP_INTERNAL void
APP__linux_Set_fullscreen(bool enable) {
	/* NOTE: this function must be called after XMapWindow (which happens in _sAPP_x11_show_window()) */
    if (APP__x11_data.NET_WM_STATE && APP__x11_data.NET_WM_STATE_FULLSCREEN) {
        if (enable) {
            const int _NET_WM_STATE_ADD = 1;
            APP__X11_send_event(APP__x11_data.NET_WM_STATE,
                                _NET_WM_STATE_ADD,
                                APP__x11_data.NET_WM_STATE_FULLSCREEN,
                                0, 1, 0);
        }
        else {
            const int _NET_WM_STATE_REMOVE = 0;
            APP__X11_send_event(APP__x11_data.NET_WM_STATE,
                                _NET_WM_STATE_REMOVE,
                                APP__x11_data.NET_WM_STATE_FULLSCREEN,
                                0, 1, 0);
        }
    }
    XFlush(APP__x11_data.xdisplay);
}


APP_INTERNAL void
APP__linux_Show_mouse(bool show) {
    if (show) {
        XUndefineCursor(APP__x11_data.xdisplay, APP__x11_data.win);
    }
    else {
        XDefineCursor(APP__x11_data.xdisplay, APP__x11_data.win, APP__x11_data.hidden_cursor);
    }	
}

APP_INTERNAL void
APP__linux_Set_window_title(const char *title) {
	XStoreName(APP__x11_data.xdisplay, APP__x11_data.win, title);
	XFlush(APP__x11_data.xdisplay);
}


APP_INTERNAL void
APP__linux_Shutdown(void) {
	{
		// I readed somewhere that is important that all gl commands must finalize before clean
		// everything.
		glFinish();

		// This unbinds the context, must be done before destroy the context.
		if (EGL_TRUE != eglMakeCurrent(
					APP__x11_data.egl_display,
					EGL_NO_SURFACE,
					EGL_NO_SURFACE,
					EGL_NO_CONTEXT)) {

   		 	fprintf(stderr, "Error, cannot unbind context and surfaces.\n");
		}
		
		// Destroys the surface and the context
   		if (eglDestroySurface(APP__x11_data.egl_display, APP__x11_data.egl_surface) != EGL_TRUE) {
   		 	fprintf(stderr, "Error, cannot destroy egl surface.\n");
   		}
		if (eglDestroyContext(APP__x11_data.egl_display, APP__x11_data.egl_context) != EGL_TRUE) {
   		 	fprintf(stderr, "Error, cannot destroy egl context.\n");
   		}

		// Destroys the egl_display
   		if (eglTerminate(APP__x11_data.egl_display) != EGL_TRUE) {
   		 	fprintf(stderr, "Error, cannot terminate egl display connection.\n");
   		}
	}

	XDestroyIC(APP__x11_data.xic);
	XCloseIM(APP__x11_data.xim);
	XUnmapWindow(APP__x11_data.xdisplay, APP__x11_data.win);
	XDestroyWindow(APP__x11_data.xdisplay, APP__x11_data.win);
	XCloseDisplay(APP__x11_data.xdisplay);

    APP__Unload_libGLES();
    APP__Unload_libEGL();
    APP__Unload_libXcursor();
    APP__Unload_libX11();
}




APP_INTERNAL void
APP__linux_Set_swap_interval(unsigned int interval) {
	eglSwapInterval(APP__x11_data.egl_display, interval);
}

APP_INTERNAL void
APP__linux_Swap_buffers() {
	/* get rendered buffer to the screen */
	eglSwapBuffers (APP__x11_data.egl_display, APP__x11_data.egl_surface);
}

// Based on sokol_app.h
APP_INTERNAL APP_KeyCode
APP__Translate_key(int scancode) {
    int dummy;
    KeySym* keysyms = XGetKeyboardMapping(APP__x11_data.xdisplay, scancode, 1, &dummy);
    if (keysyms == NULL) {
		return APP_KEY_INVALID;
	}

    KeySym keysym = keysyms[0];
    XFree(keysyms);
    switch (keysym) {
        case 0xff1b /* XK_Escape */:         return APP_KEY_ESCAPE;
        case 0xff09 /* XK_Tab */:            return APP_KEY_TAB;
        case 0xffe1 /* XK_Shift_L */:        return APP_KEY_LEFT_SHIFT;
        case 0xffe2 /* XK_Shift_R */:        return APP_KEY_RIGHT_SHIFT;
        case 0xffe3 /* XK_Control_L */:      return APP_KEY_LEFT_CONTROL;
        case 0xffe4 /* XK_Control_R */:      return APP_KEY_RIGHT_CONTROL;
        case 0xffe7 /* XK_Meta_L */:
        case 0xffe9 /* XK_Alt_L */:          return APP_KEY_LEFT_ALT;
        case 0xff7e /* XK_Mode_switch */:    /* Mapped to Alt_R on many keyboards */
        case 0xfe03 /* XK_ISO_Level3_Shift */: /* AltGr on at least some machines */
        case 0xffe8 /* XK_Meta_R */:
        case 0xffea /* XK_Alt_R */:          return APP_KEY_RIGHT_ALT;
        case 0xffeb /* XK_Super_L */:        return APP_KEY_LEFT_SUPER;
        case 0xffec /* XK_Super_R */:        return APP_KEY_RIGHT_SUPER;
        case 0xff67 /* XK_Menu */:           return APP_KEY_MENU;
        case 0xff7f /* XK_Num_Lock */:       return APP_KEY_NUM_LOCK;
        case 0xffe5 /* XK_Caps_Lock */:      return APP_KEY_CAPS_LOCK;
        case 0xff61 /* XK_Print */:          return APP_KEY_PRINT_SCREEN;
        case 0xff14 /* XK_Scroll_Lock */:    return APP_KEY_SCROLL_LOCK;
        case 0xff13 /* XK_Pause */:          return APP_KEY_PAUSE;
        case 0xffff /* XK_Delete */:         return APP_KEY_DELETE;
        case 0xff08 /* XK_BackSpace */:      return APP_KEY_BACKSPACE;
        case 0xff0d /* XK_Return */:         return APP_KEY_ENTER;
        case 0xff50 /* XK_Home */:           return APP_KEY_HOME;
        case 0xff57 /* XK_End */:            return APP_KEY_END;
        case 0xff55 /* XK_Page_Up */:        return APP_KEY_PAGE_UP;
        case 0xff56 /* XK_Page_Down */:      return APP_KEY_PAGE_DOWN;
        case 0xff63 /* XK_Insert */:         return APP_KEY_INSERT;
        case 0xff51 /* XK_Left */:           return APP_KEY_LEFT;
        case 0xff53 /* XK_Right */:          return APP_KEY_RIGHT;
        case 0xff54 /* XK_Down */:           return APP_KEY_DOWN;
        case 0xff52 /* XK_Up */:             return APP_KEY_UP;
        case 0xffbe /* XK_F1 */:             return APP_KEY_F1;
        case 0xffbf /* XK_F2 */:             return APP_KEY_F2;
        case 0xffc0 /* XK_F3 */:             return APP_KEY_F3;
        case 0xffc1 /* XK_F4 */:             return APP_KEY_F4;
        case 0xffc2 /* XK_F5 */:             return APP_KEY_F5;
        case 0xffc3 /* XK_F6 */:             return APP_KEY_F6;
        case 0xffc4 /* XK_F7 */:             return APP_KEY_F7;
        case 0xffc5 /* XK_F8 */:             return APP_KEY_F8;
        case 0xffc6 /* XK_F9 */:             return APP_KEY_F9;
        case 0xffc7 /* XK_F10 */:            return APP_KEY_F10;
        case 0xffc8 /* XK_F11 */:            return APP_KEY_F11;
        case 0xffc9 /* XK_F12 */:            return APP_KEY_F12;
        case 0xffca /* XK_F13 */:            return APP_KEY_F13;
        case 0xffcb /* XK_F14 */:            return APP_KEY_F14;
        case 0xffcc /* XK_F15 */:            return APP_KEY_F15;
        case 0xffcd /* XK_F16 */:            return APP_KEY_F16;
        case 0xffce /* XK_F17 */:            return APP_KEY_F17;
        case 0xffcf /* XK_F18 */:            return APP_KEY_F18;
        case 0xffd0 /* XK_F19 */:            return APP_KEY_F19;
        case 0xffd1 /* XK_F20 */:            return APP_KEY_F20;
        case 0xffd2 /* XK_F21 */:            return APP_KEY_F21;
        case 0xffd3 /* XK_F22 */:            return APP_KEY_F22;
        case 0xffd4 /* XK_F23 */:            return APP_KEY_F23;
        case 0xffd5 /* XK_F24 */:            return APP_KEY_F24;
        case 0xffd6 /* XK_F25 */:            return APP_KEY_F25;

        case 0xffaf /* XK_KP_Divide */:      return APP_KEY_KP_DIVIDE;
        case 0xffaa /* XK_KP_Multiply */:    return APP_KEY_KP_MULTIPLY;
        case 0xffad /* XK_KP_Subtract */:    return APP_KEY_KP_SUBTRACT;
        case 0xffab /* XK_KP_Add */:         return APP_KEY_KP_ADD;

        case 0xff9e /* XK_KP_Insert */:      return APP_KEY_KP_0;
        case 0xff9c /* XK_KP_End */:         return APP_KEY_KP_1;
        case 0xff99 /* XK_KP_Down */:        return APP_KEY_KP_2;
        case 0xff9b /* XK_KP_Page_Down */:   return APP_KEY_KP_3;
        case 0xff96 /* XK_KP_Left */:        return APP_KEY_KP_4;
        case 0xff9d /* XK_KP_Begin */:       return APP_KEY_KP_5;
        case 0xff98 /* XK_KP_Right */:       return APP_KEY_KP_6;
        case 0xff95 /* XK_KP_Home */:        return APP_KEY_KP_7;
        case 0xff97 /* XK_KP_Up */:          return APP_KEY_KP_8;
        case 0xff9a /* XK_KP_Page_Up */:     return APP_KEY_KP_9;
        case 0xff9f /* XK_KP_Delete */:      return APP_KEY_KP_DECIMAL;
        case 0xffbd /* XK_KP_Equal */:       return APP_KEY_KP_EQUAL;
        case 0xff8d /* XK_KP_Enter */:       return APP_KEY_KP_ENTER;

        case 0x0061 /* XK_a */:              return APP_KEY_A;
        case 0x0062 /* XK_b */:              return APP_KEY_B;
        case 0x0063 /* XK_c */:              return APP_KEY_C;
        case 0x0064 /* XK_d */:              return APP_KEY_D;
        case 0x0065 /* XK_e */:              return APP_KEY_E;
        case 0x0066 /* XK_f */:              return APP_KEY_F;
        case 0x0067 /* XK_g */:              return APP_KEY_G;
        case 0x0068 /* XK_h */:              return APP_KEY_H;
        case 0x0069 /* XK_i */:              return APP_KEY_I;
        case 0x006a /* XK_j */:              return APP_KEY_J;
        case 0x006b /* XK_k */:              return APP_KEY_K;
        case 0x006c /* XK_l */:              return APP_KEY_L;
        case 0x006d /* XK_m */:              return APP_KEY_M;
        case 0x006e /* XK_n */:              return APP_KEY_N;
        case 0x006f /* XK_o */:              return APP_KEY_O;
        case 0x0070 /* XK_p */:              return APP_KEY_P;
        case 0x0071 /* XK_q */:              return APP_KEY_Q;
        case 0x0072 /* XK_r */:              return APP_KEY_R;
        case 0x0073 /* XK_s */:              return APP_KEY_S;
        case 0x0074 /* XK_t */:              return APP_KEY_T;
        case 0x0075 /* XK_u */:              return APP_KEY_U;
        case 0x0076 /* XK_v */:              return APP_KEY_V;
        case 0x0077 /* XK_w */:              return APP_KEY_W;
        case 0x0078 /* XK_x */:              return APP_KEY_X;
        case 0x0079 /* XK_y */:              return APP_KEY_Y;
        case 0x007a /* XK_z */:              return APP_KEY_Z;
        case 0x0031 /* XK_1 */:              return APP_KEY_1;
        case 0x0032 /* XK_2 */:              return APP_KEY_2;
        case 0x0033 /* XK_3 */:              return APP_KEY_3;
        case 0x0034 /* XK_4 */:              return APP_KEY_4;
        case 0x0035 /* XK_5 */:              return APP_KEY_5;
        case 0x0036 /* XK_6 */:              return APP_KEY_6;
        case 0x0037 /* XK_7 */:              return APP_KEY_7;
        case 0x0038 /* XK_8 */:              return APP_KEY_8;
        case 0x0039 /* XK_9 */:              return APP_KEY_9;
        case 0x0030 /* XK_0 */:              return APP_KEY_0;
        case 0x0020 /* XK_space */:          return APP_KEY_SPACE;
        case 0x002d /* XK_minus */:          return APP_KEY_MINUS;
        case 0x003d /* XK_equal */:          return APP_KEY_EQUAL;
        case 0x005b /* XK_bracketleft */:    return APP_KEY_LEFT_BRACKET;
        case 0x005d /* XK_bracketright */:   return APP_KEY_RIGHT_BRACKET;
        case 0x005c /* XK_backslash */:      return APP_KEY_BACKSLASH;
        case 0x003b /* XK_semicolon */:      return APP_KEY_SEMICOLON;
        case 0x0027 /* XK_apostrophe */:     return APP_KEY_APOSTROPHE;
        case 0x0060 /* XK_grave */:          return APP_KEY_GRAVE_ACCENT;
        case 0x002c /* XK_comma */:          return APP_KEY_COMMA;
        case 0x002e /* XK_period */:         return APP_KEY_PERIOD;
        case 0x002f /* XK_slash */:          return APP_KEY_SLASH;
        case 0x003c /* XK_less */:           return APP_KEY_WORLD_1; /* At least in some layouts... */
        default:                return APP_KEY_INVALID;
    }
}


void
APP__X11_parse_xdnd(const char *data, char *filepath, uint64_t cap) {

    // NOTE(Tano): We can put this inside a loop to get all the files, not only the first

    uint64_t cursor = 0;

    if (data[cursor+0] != 'f' ||
        data[cursor+1] != 'i' ||
        data[cursor+2] != 'l' ||
        data[cursor+3] != 'e' ||
        data[cursor+4] != ':' ||
        data[cursor+5] != '/' ||
        data[cursor+6] != '/') return;
    cursor += 7;
    uint64_t filepath_size = 0;

    for (;data[cursor] && data[cursor] != '\r' && data[cursor] != '\n'; cursor += 1) {
        
        // TODO(Tano): Do something if doesn't fit
        if (filepath_size < cap-1) {
            filepath[filepath_size] = data[cursor];
            filepath_size += 1;
        }

    }

    filepath[filepath_size] = 0;

    for (;data[cursor] == '\r' || data[cursor] == '\n'; cursor += 1);

}


APP_INTERNAL void
APP__linux_Process_events() {
	XEvent xevent;
	for (int remaining_events = XPending(APP__x11_data.xdisplay);
		remaining_events > 0;
		--remaining_events) {

        XNextEvent(APP__x11_data.xdisplay, &xevent);
		switch (xevent.type) {
			case ConfigureNotify: {
                APP__Emit_event(&(APP_Event){
                    .type = APP_EVENT_WINDOW_RESIZED,
                    .window = {
                        .width = xevent.xconfigure.width,
                        .height = xevent.xconfigure.height
                    }
                });
        	    break;
			}
			case FocusOut: {
                APP__Emit_event(&(APP_Event){
                    .type = APP_EVENT_WINDOW_FOCUS_LOST,
                });
				break;
			}
			case KeyPress: {
                int keycode = (int)xevent.xkey.keycode;
                APP_KeyCode key_id = APP__Translate_key(keycode);
                APP__Emit_event(&(APP_Event){
                    .type = APP_EVENT_KEY_PRESSED,
                    .key = {.id = key_id}
                });

                APP__Emit_event(&(APP_Event){
                    .type = APP_EVENT_KEY_REPEAT,
                    .key = {.id = key_id}
                });

				Status return_status;
                char input_data[5] = {0};
				int bytes_count = Xutf8LookupString(
                    APP__x11_data.xic,
                    (XKeyPressedEvent *)&xevent,
                    input_data, 5, NULL, &return_status);

				if ((return_status == XLookupChars || return_status == XLookupBoth) &&
                    bytes_count > 0 && bytes_count <= 4) {

                    APP__Emit_event(&(APP_Event){
                        .type       = APP_EVENT_TEXT_INPUT,
                        .text_input = {
                            .data[0] = input_data[0],
                            .data[1] = input_data[1],
                            .data[2] = input_data[2],
                            .data[3] = input_data[3],
                            .count   = bytes_count
                        }
                    });
				}
				break;
			}
			case KeyRelease: {
				// Checks if the key release is from an autorepeat
				// https://stackoverflow.com/questions/2100654/ignore-auto-repeat-in-x11-applications
				if (XEventsQueued(APP__x11_data.xdisplay, QueuedAfterReading)) {
					XEvent next_event;
					XPeekEvent(APP__x11_data.xdisplay, &next_event);
					if (next_event.type == KeyPress &&
						next_event.xkey.time == xevent.xkey.time &&
  					    next_event.xkey.keycode == xevent.xkey.keycode) {
                        break;
  					}
				}
                int keycode = (int)xevent.xkey.keycode;
                APP_KeyCode key_id = APP__Translate_key(keycode);
                APP__Emit_event(&(APP_Event){
                    .type = APP_EVENT_KEY_RELEASED,
                    .key = {.id = key_id}
                });
				break;
            }
			case ButtonPress: {
                if (xevent.xbutton.button == 1) {
                    APP__Emit_event(&(APP_Event){
                        .type  = APP_EVENT_MOUSE_BUTTON_PRESSED,
                        .mouse = {.button = APP_MOUSE_BUTTON_LEFT}
                    });
                }
                else if (xevent.xbutton.button == 2) {
                    APP__Emit_event(&(APP_Event){
                        .type  = APP_EVENT_MOUSE_BUTTON_PRESSED,
                        .mouse = {.button = APP_MOUSE_BUTTON_MIDDLE}
                    });
                }
                else if (xevent.xbutton.button == 3) {
                    APP__Emit_event(&(APP_Event){
                        .type  = APP_EVENT_MOUSE_BUTTON_PRESSED,
                        .mouse = {.button = APP_MOUSE_BUTTON_RIGHT}
                    });
                }
				/* might be a scroll event */
                else if (xevent.xbutton.button == 4) {
                    APP__Emit_event(&(APP_Event){
                        .type  = APP_EVENT_MOUSE_WHEEL_Y,
                        .mouse = {.wheel_y = 1.0f}
                    });
                }
                else if (xevent.xbutton.button == 5) {
                    APP__Emit_event(&(APP_Event){
                        .type  = APP_EVENT_MOUSE_WHEEL_Y,
                        .mouse = {.wheel_y = -1.0f}
                    });
                }
                else if (xevent.xbutton.button == 6) {
                    APP__Emit_event(&(APP_Event){
                        .type  = APP_EVENT_MOUSE_WHEEL_X,
                        .mouse = {.wheel_x = 1.0f}
                    });
                }
                else if (xevent.xbutton.button == 7) {
                    APP__Emit_event(&(APP_Event){
                        .type  = APP_EVENT_MOUSE_WHEEL_X,
                        .mouse = {.wheel_x = -1.0f}
                    });
                }
				
				break;
			}
			case ButtonRelease:
			{
                if (xevent.xbutton.button == 1) {
                    APP__Emit_event(&(APP_Event){
                        .type  = APP_EVENT_MOUSE_BUTTON_RELEASED,
                        .mouse = {.button = APP_MOUSE_BUTTON_LEFT}
                    });
                }
                else if (xevent.xbutton.button == 2) {
                    APP__Emit_event(&(APP_Event){
                        .type  = APP_EVENT_MOUSE_BUTTON_RELEASED,
                        .mouse = {.button = APP_MOUSE_BUTTON_MIDDLE}
                    });
                }
                else if (xevent.xbutton.button == 3) {
                    APP__Emit_event(&(APP_Event){
                        .type  = APP_EVENT_MOUSE_BUTTON_RELEASED,
                        .mouse = {.button = APP_MOUSE_BUTTON_RIGHT}
                    });
                }
			    break;
			}
			case MotionNotify: {
                APP__Emit_event(&(APP_Event){
                    .type  = APP_EVENT_MOUSE_MOVE,
                    .mouse = {
                        .x = (float)xevent.xmotion.x,
                        .y = (float)xevent.xmotion.y
                    }
                });
				break;
			}
			case ClientMessage: {
				if (xevent.xclient.message_type == APP__x11_data.WM_PROTOCOLS) {
            	    const Atom protocol = (Atom)xevent.xclient.data.l[0];
            	    if (protocol == APP__x11_data.WM_DELETE_WINDOW) {
                        APP__Emit_event(&(APP_Event){
                            .type  = APP_EVENT_QUIT
                        });
            	    }
            	}
                else if (xevent.xclient.message_type == APP__x11_data.xdnd.XdndEnter) {

                    const bool is_list = 0 != (xevent.xclient.data.l[1] & 1);
                    APP__x11_data.xdnd.version = xevent.xclient.data.l[1] >> 24;
                    APP__x11_data.xdnd.format  = None;
                    APP__x11_data.xdnd.source  = (Window)xevent.xclient.data.l[0];

                    if (APP__x11_data.xdnd.version > APP__X11_XDND_VERSION) break;

                    int count = 0;
                    Atom* formats = 0;
                    if (is_list) {
                        count = APP__X11_get_window_property(
                            APP__x11_data.xdnd.source,
                            APP__x11_data.xdnd.XdndTypeList,
                            XA_ATOM,
                            (unsigned char**)&formats);
                    }
                    else {
                        count = 3;
                        formats = (Atom*) xevent.xclient.data.l + 2;
                    }

                    // Why this???
                    for (int i = 0; i < count; i+=1) {
                        if (formats[i] == APP__x11_data.xdnd.text_uri_list) {
                            APP__x11_data.xdnd.format = APP__x11_data.xdnd.text_uri_list;
                            break;
                        }
                    }
                    if (is_list && formats) {
                        XFree(formats);
                    }
                }
                else if (xevent.xclient.message_type == APP__x11_data.xdnd.XdndPosition) {

                    // From sokol
                    /* drag operation has moved over the window
                       FIXME: we could track the mouse position here, but
                       this isn't implemented on other platforms either so far
                    */
                    if (APP__x11_data.xdnd.version > APP__X11_XDND_VERSION) break;
                    XEvent reply = {0};
                    reply.type                 = ClientMessage;
                    reply.xclient.window       = APP__x11_data.xdnd.source;
                    reply.xclient.message_type = APP__x11_data.xdnd.XdndStatus;
                    reply.xclient.format       = 32; // What?
                    reply.xclient.data.l[0]    = (long)APP__x11_data.win;
                    if (APP__x11_data.xdnd.format) {
                        /* reply that we are ready to copy the dragged data */
                        reply.xclient.data.l[1] = 1;    // accept with no rectangle
                        if (APP__x11_data.xdnd.version >= 2) {
                            reply.xclient.data.l[4] = (long)APP__x11_data.xdnd.XdndActionCopy;
                        }
                    }
                    XSendEvent(APP__x11_data.xdisplay, APP__x11_data.xdnd.source, False, NoEventMask, &reply);
                    XFlush(APP__x11_data.xdisplay);                
                }
                else if (xevent.xclient.message_type == APP__x11_data.xdnd.XdndDrop) {

                    if (APP__x11_data.xdnd.version > APP__X11_XDND_VERSION) break;
                    Time time = CurrentTime;
                    if (APP__x11_data.xdnd.format) {
                        if (APP__x11_data.xdnd.version >= 1) time = (Time)xevent.xclient.data.l[2];

                        XConvertSelection(
                            APP__x11_data.xdisplay,
                            APP__x11_data.xdnd.XdndSelection,
                            APP__x11_data.xdnd.format,
                            APP__x11_data.xdnd.XdndSelection,
                            APP__x11_data.win,
                            time);
                    }
                    else if (APP__x11_data.xdnd.version >= 2) { // In this case we reject the drag
                        XEvent reply = {0};
                        reply.type = ClientMessage;
                        reply.xclient.window       = APP__x11_data.win;
                        reply.xclient.message_type = APP__x11_data.xdnd.XdndFinished;
                        reply.xclient.format       = 32; // What?
                        reply.xclient.data.l[0]    = (long)APP__x11_data.win;
                        reply.xclient.data.l[1]    = 0;    // drag was rejected
                        reply.xclient.data.l[2]    = None;
                        XSendEvent(APP__x11_data.xdisplay, APP__x11_data.xdnd.source, False, NoEventMask, &reply);
                        XFlush(APP__x11_data.xdisplay);
                    }
                }
                break;
			}
            case SelectionNotify: {
                if (xevent.xselection.property == APP__x11_data.xdnd.XdndSelection) {

                    char* data = 0;
                    uint32_t result = APP__X11_get_window_property(
                        xevent.xselection.requestor,
                        xevent.xselection.property,
                        xevent.xselection.target,
                        (unsigned char**) &data);

                    if (result) {
                        APP__X11_parse_xdnd(data, APP__data.drag_and_drop_buffer, APP__DRAG_AND_DROP_BUFFER_MAX);
                        APP__Emit_event(&(APP_Event){
                            .type  = APP_EVENT_FILE_DROPPED,
                            .file_dropped = {.filepath = APP__data.drag_and_drop_buffer},
                        });
                        XFree(data);
                    }

                    if (APP__x11_data.xdnd.version >= 2) {
                        XEvent reply = {0};
                        reply.type = ClientMessage;
                        reply.xclient.window = APP__x11_data.win;
                        reply.xclient.message_type = APP__x11_data.xdnd.XdndFinished;
                        reply.xclient.format = 32;
                        reply.xclient.data.l[0] = (long)APP__x11_data.win;
                        reply.xclient.data.l[1] = result;
                        reply.xclient.data.l[2] = (long)APP__x11_data.xdnd.XdndActionCopy;
                        XSendEvent(APP__x11_data.xdisplay, APP__x11_data.xdnd.source, False, NoEventMask, &reply);
                        XFlush(APP__x11_data.xdisplay);
                    }
                }
                break; 
            }
            break;// TODO(Tano) HANDLE EVERYTHING!!!!!
		}
	}
}



APP_INTERNAL int64_t
APP__linux_Time(void) {
	struct timespec current;
	clock_gettime(CLOCK_MONOTONIC_RAW, &current);
	const int64_t SECOND = 1000*1000*1000;
	int64_t result = (current.tv_sec - APP__x11_data.initial_time.tv_sec) * (1 * SECOND);
	result += current.tv_nsec - APP__x11_data.initial_time.tv_nsec;
	return result;
}


APP_INTERNAL void
APP__linux_Sleep(int64_t ns) {
	struct timespec req;
    req.tv_sec  = ns/(1000*1000*1000);
    req.tv_nsec = ns%(1000*1000*1000);
    struct timespec rem = {0};
    nanosleep(&req, &rem);
}










/////////////////////////////////////////////////////////////////////////////////
//
//
//
//                     WASM IMPLEMENTATION ||||
//                                         VVVV
//
//
////////////////////////////////////////////////////////////////////////////////

#elif defined(APP_WASM)

// Macro to generate a JavaScript function that can be called from C


APP__WA_JS(void, APP__JS_Init, (void), {
	canvas.width = window.innerWidth;
	canvas.height = window.innerHeight;

	Module.ASM.WAFN_report_resize(canvas.width, canvas.height); // We call directly to set the correct size on the first frame

	// Sets the handler to report to the application the windows resizing.
	window.onresize = () => {
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
		Module.ASM.WAFN_report_resize(window.innerWidth, window.innerHeight);
	};

	// Key down
	window.addEventListener("keydown", (e) => {
		if (e.key.length === 1) { // Also fills the text input buffer
			var char_buffer_pointer = Module.ASM.WAFN_get_char_buffer();
			var char_len = stringToUTF8Array(e.key, char_buffer_pointer, 128);
			Module.ASM.WAFN_report_input_char(char_buffer_pointer, char_len);
		}
		// Prevent default browser's shortcuts
		e.preventDefault();
		Module.ASM.WAFN_report_keydown(e.keyCode);
	}, true);
	
	// Key up
	window.addEventListener("keyup", (e) => {
		Module.ASM.WAFN_report_keyup(e.keyCode);
	}, true);

	// Mouse position
	window.addEventListener("mousemove", function(e) {
		let rect = canvas.getBoundingClientRect();
		Module.ASM.WAFN_report_mouse_move(e.clientX - rect.left, e.clientY - rect.top);
	}, false);
	
	// Mouse button down
	window.addEventListener("mousedown", function(e) {
		if (e.buttons & 1) Module.ASM.WAFN_report_mouse_button_down(0); // left button
		if (e.buttons & 2) Module.ASM.WAFN_report_mouse_button_down(1); // right button
		if (e.buttons & 4) Module.ASM.WAFN_report_mouse_button_down(2); // middle button
	});

	// Mouse button up
	window.addEventListener("mouseup", function(e) {
		if      (e.button == 0) Module.ASM.WAFN_report_mouse_button_up(0); // left button
		else if (e.button == 2) Module.ASM.WAFN_report_mouse_button_up(1); // right button
		else if (e.button == 1) Module.ASM.WAFN_report_mouse_button_up(2); // middle button
	});

	// Mouse button up
	window.addEventListener("wheel", function(e) {
        if (e.deltaX != 0.0) {
		    Module.ASM.WAFN_report_wheel_x(e.deltaX);
        }
        if (e.deltaY != 0.0) {
		    Module.ASM.WAFN_report_wheel_y(-e.deltaY);
        }
	});

	// Touch start
	window.addEventListener("touchstart", function(e) {
        console.log("touch start");
		let touches     = e.changedTouches;
		let canvas_rect = canvas.getBoundingClientRect();
		for (var i = 0; i < touches.length; i++) {
			let pos_x = touches[i].clientX - canvas_rect.left;
			let pos_y = touches[i].clientY - canvas_rect.top;
			Module.ASM.WAFN_report_touch_event_begin(touches[i].identifier, pos_x, pos_y);
		}
	}, false);

	// Touch move
	window.addEventListener("touchmove", function(e) {
        console.log("touch move");
		let touches     = e.changedTouches;
		let canvas_rect = canvas.getBoundingClientRect();
		for (var i = 0; i < touches.length; i++) {
			let pos_x = touches[i].clientX - canvas_rect.left;
			let pos_y = touches[i].clientY - canvas_rect.top;
			Module.ASM.WAFN_report_touch_event_move(touches[i].identifier, pos_x, pos_y);
		}
	}, false);

	// Touch end
	window.addEventListener("touchend", function(e) {
        console.log("touch end");
		let touches     = e.changedTouches;
		let canvas_rect = canvas.getBoundingClientRect();
		for (var i = 0; i < touches.length; i++) {
			let pos_x = touches[i].clientX - canvas_rect.left;
			let pos_y = touches[i].clientY - canvas_rect.top;
			Module.ASM.WAFN_report_touch_event_end(touches[i].identifier, pos_x, pos_y);
		}
	}, false);

	// Touch cancel
	window.addEventListener("touchcancel", function(e) {
		let touches     = e.changedTouches;
		let canvas_rect = canvas.getBoundingClientRect();
		for (var i = 0; i < touches.length; i++) {
			let pos_x = touches[i].clientX - canvas_rect.left;
			let pos_y = touches[i].clientY - canvas_rect.top;
			Module.ASM.WAFN_report_touch_event_cancel(touches[i].identifier, pos_x, pos_y);
		}
	}, false);

	// Setups the frame handler
	function Application_frame() {
		var result = Module.ASM.APP__Handle_frame();
		if (result == 0) {
			window.requestAnimationFrame(Application_frame);
		}
	}
	window.requestAnimationFrame(Application_frame);

	Module.GLctx;
	Module.GLlastError;
	Module.GLcounter = 1;
	Module.GLbuffers = [];
	Module.GLprograms = [];
	Module.GLframebuffers = [];
	Module.GLtextures = [];
	Module.GLuniforms = [];
	Module.GLshaders = [];
	Module.GLprogramInfos = {};
	Module.GLpackAlignment = 4;
	Module.GLunpackAlignment = 4;
	Module.GLMINI_TEMP_BUFFER_SIZE = 256;
	Module.GLminiTempBuffer = null;
	Module.GLminiTempBufferViews = [0];
	Module.GLminiTempBuffer = new Float32Array(Module.GLMINI_TEMP_BUFFER_SIZE);
	for (var i = 0; i < Module.GLMINI_TEMP_BUFFER_SIZE; i++) Module.GLminiTempBufferViews[i] = Module.GLminiTempBuffer.subarray(0, i+1);

	var attr = { majorVersion: 1, minorVersion: 0, antialias: false, alpha: false};
	var errorInfo = "";
	try
	{
		let onContextCreationError = function(event) { errorInfo = event.statusMessage || errorInfo; };
		canvas.addEventListener('webglcontextcreationerror', onContextCreationError, false);
		try
		{
			Module.GLctx = canvas.getContext('webgl', attr) || canvas.getContext('experimental-webgl', attr);
		}
		finally { canvas.removeEventListener('webglcontextcreationerror', onContextCreationError, false); }
		if (!Module.GLctx) throw 'Could not create context';
	}
	catch (e)
	{
		abort("WEBGL", e + (errorInfo ? " (" + errorInfo + ")" : ""));
	}

	var exts = Module.GLctx.getSupportedExtensions();
	if (exts && exts.length > 0)
	{
        // These are the 'safe' feature-enabling extensions that don't add any performance impact
        // related to e.g. debugging, and should be enabled by default so that client GLES2/GL code
        // will not need to go through extra hoops to get its stuff working.  As new extensions are
        // ratified at http://www.khronos.org/registry/webgl/extensions/ , feel free to add your new
        // extensions here, as long as they don't produce a performance impact for users that might
        // not be using those extensions.  E.g. debugging-related extensions should probably be off
        // by default.
		var W = 'WEBGL_', O = 'OES_', E = 'EXT_', T = 'texture_', C = 'compressed_'+T;
		var automaticallyEnabledExtensions = [ // Khronos ratified WebGL extensions ordered by number (no debug extensions):
			O+T+'float', O+T+'half_float', O+'standard_derivatives',
			O+'vertex_array_object', W+C+'s3tc', W+'depth_texture',
			O+'element_index_uint', E+T+'filter_anisotropic', E+'frag_depth',
			W+'draw_buffers', 'ANGLE_instanced_arrays', O+T+'float_linear',
			O+T+'half_float_linear', E+'blend_minmax', E+'shader_texture_lod',
			// Community approved WebGL extensions ordered by number:
			W+C+'pvrtc', E+'color_buffer_half_float', W+'color_buffer_float',
			E+'sRGB', W+C+'etc1', E+'disjoint_timer_query',
			W+C+'etc', W+C+'astc', E+'color_buffer_float',
			W+C+'s3tc_srgb', E+'disjoint_timer_query_webgl2'];
		exts.forEach(function(ext)
		{
			if (automaticallyEnabledExtensions.indexOf(ext) != -1)
			{
				// Calling .getExtension enables that extension permanently, no need to store the return value to be enabled.
				Module.GLctx.getExtension(ext);
			}
		});
	}
	
	Module.GLgetNewId = function(table) {
		var ret = Module.GLcounter++;
		for (var i = table.length; i < ret; i++) table[i] = null;
		return ret;
	};

	Module.GLgetSource = function(shader, count, string, length) {
		var source = "";
		for (var i = 0; i < count; ++i)
		{
			var frag;
			if (length)
			{
				var len = HEAP32[(((length)+(i*4))>>2)];
				if (len < 0) frag = Pointer_stringify(HEAP32[(((string)+(i*4))>>2)]);
				else frag = Pointer_stringify(HEAP32[(((string)+(i*4))>>2)], len);
			}
			else frag = Pointer_stringify(HEAP32[(((string)+(i*4))>>2)]);
			source += frag;
		}
		return source;
	};

	Module.GLpopulateUniformTable = function(program) {
		var p = Module.GLprograms[program];
		Module.GLprogramInfos[program] =
		{
			uniforms: {},
			maxUniformLength: 0, // This is eagerly computed below, since we already enumerate all uniforms anyway.
			maxAttributeLength: -1, // This is lazily computed and cached, computed when/if first asked, '-1' meaning not computed yet.
			maxUniformBlockNameLength: -1 // Lazily computed as well
		};

		var ptable = Module.GLprogramInfos[program];
		var utable = ptable.uniforms;

        // A program's uniform table maps the string name of an uniform to an integer location of
        // that uniform.  The global GLuniforms map maps integer locations to WebGLUniformLocations.
		var numUniforms = Module.GLctx.getProgramParameter(p, Module.GLctx.ACTIVE_UNIFORMS);
		for (var i = 0; i < numUniforms; ++i)
		{
			var u = Module.GLctx.getActiveUniform(p, i);

			var name = u.name;
			ptable.maxUniformLength = Math.max(ptable.maxUniformLength, name.length+1);

			// Strip off any trailing array specifier we might have got, e.g. '[0]'.
			if (name.indexOf(']', name.length-1) !== -1)
			{
				var ls = name.lastIndexOf('[');
				name = name.slice(0, ls);
			}

            // Optimize memory usage slightly: If we have an array of uniforms, e.g. 'vec3
            // colors[3];', then only store the string 'colors' in utable, and 'colors[0]',
            // 'colors[1]' and 'colors[2]' will be parsed as 'colors'+i.  Note that for the
            // GLuniforms table, we still need to fetch the all WebGLUniformLocations for all the
            // indices.
			var loc = Module.GLctx.getUniformLocation(p, name);
			if (loc != null)
			{
				var id = Module.GLgetNewId(Module.GLuniforms);
				utable[name] = [u.size, id];
				Module.GLuniforms[id] = loc;

				for (var j = 1; j < u.size; ++j)
				{
					var n = name + '['+j+']';
					loc = Module.GLctx.getUniformLocation(p, n);
					id = Module.GLgetNewId(Module.GLuniforms);

					Module.GLuniforms[id] = loc;
				}
			}
		}
	};

	Module.GLrecordError = function(err) {
		if (!GLlastError) GLlastError = err;
	};

	Module.webGLGet = function(name_, p, type) {
		checkHeap();
		// Guard against user passing a null pointer.
		// Note that GLES2 spec does not say anything about how passing a null pointer should be treated.
		// Testing on desktop core GL 3, the application crashes on glGetIntegerv to a null pointer, but
		// better to report an error instead of doing anything random.
		if (!p) { Module.GLrecordError(0x0501); return; } // GL_INVALID_VALUE

		var ret = undefined;
		switch(name_) {
			// Handle a few trivial GLES values
			case 0x8DFA: ret = 1; break; // GL_SHADER_COMPILER
			case 0x8DF8: // GL_SHADER_BINARY_FORMATS
				if (type !== 'Integer' && type !== 'Integer64') Module.GLrecordError(0x0500); // GL_INVALID_ENUM
				return; // Do not write anything to the out pointer, since no binary formats are supported.
			case 0x8DF9: ret = 0; break; // GL_NUM_SHADER_BINARY_FORMATS
			case 0x86A2: // GL_NUM_COMPRESSED_TEXTURE_FORMATS
				// WebGL doesn't have GL_NUM_COMPRESSED_TEXTURE_FORMATS (it's obsolete since GL_COMPRESSED_TEXTURE_FORMATS returns a JS array that can be queried for length),
				// so implement it ourselves to allow C++ GLES2 code get the length.
				var formats = Module.GLctx.getParameter(0x86A3); // GL_COMPRESSED_TEXTURE_FORMATS
				ret = formats.length;
				break;
		}

		if (ret === undefined) {
			var result = Module.GLctx.getParameter(name_);
			switch (typeof(result)) {
				case 'number':
					ret = result;
					break;
				case 'boolean':
					ret = result ? 1 : 0;
					break;
				case 'string':
					Module.GLrecordError(0x0500); // GL_INVALID_ENUM
					return;
				case 'object':
					if (result === null) {
						// null is a valid result for some (e.g., which buffer is bound - perhaps nothing is bound), but otherwise
						// can mean an invalid name_, which we need to report as an error
						switch(name_)
						{
							case 0x8894: // ARRAY_BUFFER_BINDING
							case 0x8B8D: // CURRENT_PROGRAM
							case 0x8895: // ELEMENT_ARRAY_BUFFER_BINDING
							case 0x8CA6: // FRAMEBUFFER_BINDING
							case 0x8CA7: // RENDERBUFFER_BINDING
							case 0x8069: // TEXTURE_BINDING_2D
							case 0x8514: // TEXTURE_BINDING_CUBE_MAP
								ret = 0;
								break;
							default:
								Module.GLrecordError(0x0500); // GL_INVALID_ENUM
								return;
						}
					}
					else if (result instanceof Float32Array || result instanceof Uint32Array || result instanceof Int32Array || result instanceof Array)
					{
						for (var i = 0; i < result.length; ++i) {
							switch (type)
							{
								case 'Integer': HEAP32[(((p)+(i*4))>>2)]=result[i]; break;
								case 'Float':   HEAPF32[(((p)+(i*4))>>2)]=result[i]; break;
								case 'Boolean': HEAP8[(((p)+(i))>>0)]=result[i] ? 1 : 0; break;
								default: abort('WEBGL', 'internal glGet error, bad type: ' + type);
							}
						}
						return;
					}
					else if (result instanceof WebGLBuffer || result instanceof WebGLProgram || result instanceof WebGLFramebuffer || result instanceof WebGLRenderbuffer || result instanceof WebGLTexture)
					{
						ret = result.name | 0;
					}
					else
					{
						Module.GLrecordError(0x0500); // GL_INVALID_ENUM
						return;
					}
					break;
				default:
					Module.GLrecordError(0x0500); // GL_INVALID_ENUM
					return;
			}
		}

		switch (type)
		{
			case 'Integer64': (tempI64 = [ret>>>0,(tempDouble=ret,(+(Math.abs(tempDouble))) >= 1.0 ? (tempDouble > 0.0 ? ((Math.min((+(Math.floor((tempDouble)/4294967296.0))), 4294967295.0))|0)>>>0 : (~~((+(Math.ceil((tempDouble - +(((~~(tempDouble)))>>>0))/4294967296.0)))))>>>0) : 0)],HEAP32[((p)>>2)]=tempI64[0],HEAP32[(((p)+(4))>>2)]=tempI64[1]); break;
			case 'Integer': HEAP32[((p)>>2)] = ret; break;
			case 'Float':   HEAPF32[((p)>>2)] = ret; break;
			case 'Boolean': HEAP8[((p)>>0)] = ret ? 1 : 0; break;
			default: abort('WEBGL', 'internal glGet error, bad type: ' + type);
		}
	};

	Module.webGLGetTexPixelData = function(type, format, width, height, pixels, internalFormat) {
		checkHeap();
		var sizePerPixel;
		var numChannels;
		switch(format) {
			case 0x1906: case 0x1909: case 0x1902: numChannels = 1; break; //GL_ALPHA, GL_LUMINANCE, GL_DEPTH_COMPONENT
			case 0x190A: numChannels = 2; break; //GL_LUMINANCE_ALPHA
			case 0x1907: case 0x8C40: numChannels = 3; break; //GL_RGB, GL_SRGB_EXT
			case 0x1908: case 0x8C42: numChannels = 4; break; //GL_RGBA, GL_SRGB_ALPHA_EXT
			default: throw new Error();//GLrecordError(0x0500); return null; //GL_INVALID_ENUM
		}
		switch (type) {
			case 0x1401: sizePerPixel = numChannels*1; break; //GL_UNSIGNED_BYTE
			case 0x1403: case 0x8D61: sizePerPixel = numChannels*2; break; //GL_UNSIGNED_SHORT, GL_HALF_FLOAT_OES
			case 0x1405: case 0x1406: sizePerPixel = numChannels*4; break; //GL_UNSIGNED_INT, GL_FLOAT
			case 0x84FA: sizePerPixel = 4; break; //GL_UNSIGNED_INT_24_8_WEBGL/GL_UNSIGNED_INT_24_8
			case 0x8363: case 0x8033: case 0x8034: sizePerPixel = 2; break; //GL_UNSIGNED_SHORT_5_6_5, GL_UNSIGNED_SHORT_4_4_4_4, GL_UNSIGNED_SHORT_5_5_5_1
			default: throw new Error();//GLrecordError(0x0500); return null; //GL_INVALID_ENUM
		}

		function roundedToNextMultipleOf(x, y) { return Math.floor((x + y - 1) / y) * y; }
		var plainRowSize = width * sizePerPixel;
		var alignedRowSize = roundedToNextMultipleOf(plainRowSize, Module.GLunpackAlignment);
		var bytes = (height <= 0 ? 0 : ((height - 1) * alignedRowSize + plainRowSize));

		switch(type)
		{
			case 0x1401: return HEAPU8.subarray((pixels),(pixels+bytes)); //GL_UNSIGNED_BYTE
			case 0x1406: return HEAPF32.subarray((pixels)>>2,(pixels+bytes)>>2); //GL_FLOAT
			case 0x1405: case 0x84FA: return HEAPU32.subarray((pixels)>>2,(pixels+bytes)>>2); //GL_UNSIGNED_INT, GL_UNSIGNED_INT_24_8_WEBGL/GL_UNSIGNED_INT_24_8
			case 0x1403: case 0x8363: case 0x8033: case 0x8034: case 0x8D61: return HEAPU16.subarray((pixels)>>1,(pixels+bytes)>>1); //GL_UNSIGNED_SHORT, GL_UNSIGNED_SHORT_5_6_5, GL_UNSIGNED_SHORT_4_4_4_4, GL_UNSIGNED_SHORT_5_5_5_1, GL_HALF_FLOAT_OES
			default: throw new Error();//GLrecordError(0x0500); return null; //GL_INVALID_ENUM
		}
	}



});

APP__WA_JS(int32_t, APP__JS_Get_time, (void), {
	return Date.now();
});

APP__WA_JS(void, APP__JS_Set_window_title, (const char *title, int len), {
	const UTF8_decoder = new TextDecoder("utf-8");
	window.document.title = UTF8_decoder.decode(new Uint8Array(Module.ASM.memory.buffer, title, len));
});

APP__WA_JS(void, APP__JS_Set_fullscreen, (bool enable), {
	if(enable) {
		document.documentElement.requestFullscreen().catch(console.log);
	}else {
		document.exitFullscreen();
	}
});

APP__WA_JS(void, APP__JS_Show_mouse, (bool show), {
	if (show) {
		canvas.style.cursor = 'auto';
	}
	else {
		canvas.style.cursor = 'none';

	}
});

APP__WA_JS(bool, APP__JS_Window_has_focus, (void), {
	return document.hasFocus();
});



APP_INTERNAL int
APP__wasm_Init(const char *title, int width, int height) {

	APP__data.default_width  = width;
	APP__data.default_height = height;
	APP__data.w_width  = width;
	APP__data.w_height = height;

	APP__JS_Init();

	APP__wasm_Set_window_title(title);

	int64_t MSECOND = 1000 * 1000;
	APP__wasm_data.initial_time = MSECOND*(int64_t)APP__JS_Get_time();
	return 0;
}


APP_INTERNAL void
APP__wasm_Shutdown(void) {
	// We set the viewport black
	glClearColor(0.0, 0.0, 0.0, 1.0);// background color	
	glClear(GL_COLOR_BUFFER_BIT);
}

APP_INTERNAL void
APP__wasm_Set_window_title(const char *title) {
	int len = 0;
	while (title[len]) len += 1;
	APP__JS_Set_window_title(title, len);
}

APP_INTERNAL void
APP__wasm_Set_fullscreen(bool enable) {
	APP__JS_Set_fullscreen(enable);
}

APP_INTERNAL void
APP__wasm_Show_mouse(bool show) {
	APP__JS_Show_mouse(show);
}

APP_INTERNAL int64_t
APP__wasm_Time(void) {
	int64_t MSECOND = 1000 * 1000;
	return (MSECOND*(int64_t)APP__JS_Get_time()) - APP__wasm_data.initial_time;
}


#define APP__MAX_KEYMAPPING 512

static uint16_t APP__key_mapping[APP__MAX_KEYMAPPING] = {
	[8]   = APP_KEY_BACKSPACE,
	[9]   = APP_KEY_TAB,
	[13]  = APP_KEY_ENTER,
	[16]  = APP_KEY_LEFT_SHIFT,
	[17]  = APP_KEY_LEFT_CONTROL,
	[18]  = APP_KEY_LEFT_ALT,
	[19]  = APP_KEY_PAUSE,
	[27]  = APP_KEY_ESCAPE,
	[32]  = APP_KEY_SPACE,
	[33]  = APP_KEY_PAGE_UP,
	[34]  = APP_KEY_PAGE_DOWN,
	[35]  = APP_KEY_END,
	[36]  = APP_KEY_HOME,
	[37]  = APP_KEY_LEFT,
	[38]  = APP_KEY_UP,
	[39]  = APP_KEY_RIGHT,
	[40]  = APP_KEY_DOWN,
	[45]  = APP_KEY_INSERT,
	[46]  = APP_KEY_DELETE,
	[48]  = APP_KEY_0,
	[49]  = APP_KEY_1,
	[50]  = APP_KEY_2,
	[51]  = APP_KEY_3,
	[52]  = APP_KEY_4,
	[53]  = APP_KEY_5,
	[54]  = APP_KEY_6,
	[55]  = APP_KEY_7,
	[56]  = APP_KEY_8,
	[57]  = APP_KEY_9,
	[59]  = APP_KEY_SEMICOLON,
	[64]  = APP_KEY_EQUAL,
	[65]  = APP_KEY_A,
	[66]  = APP_KEY_B,
	[67]  = APP_KEY_C,
	[68]  = APP_KEY_D,
	[69]  = APP_KEY_E,
	[70]  = APP_KEY_F,
	[71]  = APP_KEY_G,
	[72]  = APP_KEY_H,
	[73]  = APP_KEY_I,
	[74]  = APP_KEY_J,
	[75]  = APP_KEY_K,
	[76]  = APP_KEY_L,
	[77]  = APP_KEY_M,
	[78]  = APP_KEY_N,
	[79]  = APP_KEY_O,
	[80]  = APP_KEY_P,
	[81]  = APP_KEY_Q,
	[82]  = APP_KEY_R,
	[83]  = APP_KEY_S,
	[84]  = APP_KEY_T,
	[85]  = APP_KEY_U,
	[86]  = APP_KEY_V,
	[87]  = APP_KEY_W,
	[88]  = APP_KEY_X,
	[89]  = APP_KEY_Y,
	[90]  = APP_KEY_Z,
	[91]  = APP_KEY_LEFT_SUPER,
	[93]  = APP_KEY_MENU,
	[96]  = APP_KEY_KP_0,
	[97]  = APP_KEY_KP_1,
	[98]  = APP_KEY_KP_2,
	[99]  = APP_KEY_KP_3,
	[100] = APP_KEY_KP_4,
	[101] = APP_KEY_KP_5,
	[102] = APP_KEY_KP_6,
	[103] = APP_KEY_KP_7,
	[104] = APP_KEY_KP_8,
	[105] = APP_KEY_KP_9,
	[106] = APP_KEY_KP_MULTIPLY,
	[107] = APP_KEY_KP_ADD,
	[109] = APP_KEY_KP_SUBTRACT,
	[110] = APP_KEY_KP_DECIMAL,
	[111] = APP_KEY_KP_DIVIDE,
	[112] = APP_KEY_F1,
	[113] = APP_KEY_F2,
	[114] = APP_KEY_F3,
	[115] = APP_KEY_F4,
	[116] = APP_KEY_F5,
	[117] = APP_KEY_F6,
	[118] = APP_KEY_F7,
	[119] = APP_KEY_F8,
	[120] = APP_KEY_F9,
	[121] = APP_KEY_F10,
	[122] = APP_KEY_F11,
	[123] = APP_KEY_F12,
	[144] = APP_KEY_NUM_LOCK,
	[145] = APP_KEY_SCROLL_LOCK,
	[173] = APP_KEY_MINUS,
	[186] = APP_KEY_SEMICOLON,
	[187] = APP_KEY_EQUAL,
	[188] = APP_KEY_COMMA,
	[189] = APP_KEY_MINUS,
	[190] = APP_KEY_PERIOD,
	[191] = APP_KEY_SLASH,
	[192] = APP_KEY_GRAVE_ACCENT,
	[219] = APP_KEY_LEFT_BRACKET,
	[220] = APP_KEY_BACKSLASH,
	[221] = APP_KEY_RIGHT_BRACKET,
	[222] = APP_KEY_APOSTROPHE,
	[224] = APP_KEY_LEFT_SUPER,
};





__attribute__((export_name("WAFN_report_resize"))) void
WAFN_report_resize(int w, int h) {
    APP__Emit_event(&(APP_Event){
        .type   = APP_EVENT_WINDOW_RESIZED,
        .window = {.width = w, .height = h},
    });
}


// Buffer to put which character was send from javascript
__attribute__((export_name("WAFN_get_char_buffer"))) char *
WAFN_get_char_buffer(void){
	static char buffer[128] = "";
	return buffer;
}


__attribute__((export_name("WAFN_report_input_char"))) void
WAFN_report_input_char(char *utf8_char, int char_len) {
    if (char_len > 0 && char_len <= 4) {
        APP__Emit_event(&(APP_Event){
            .type       = APP_EVENT_TEXT_INPUT,
            .text_input = {
                .data[0] = utf8_char[0],
                .data[1] = utf8_char[1],
                .data[2] = utf8_char[2],
                .data[3] = utf8_char[3],
                .count = char_len
            },
        });
    }
}

__attribute__((export_name("WAFN_report_keydown"))) void
WAFN_report_keydown(int keycode) {
	if (keycode >= 0 && keycode < APP__MAX_KEYMAPPING) {
		int key_id = APP__key_mapping[keycode];
        APP__Emit_event(&(APP_Event){
            .type = APP_EVENT_KEY_PRESSED,
            .key  = {.id = key_id},
        });
        APP__Emit_event(&(APP_Event){
            .type = APP_EVENT_KEY_REPEAT,
            .key  = {.id = key_id},
        });
	}
}

__attribute__((export_name("WAFN_report_keyup"))) void
WAFN_report_keyup(int keycode) {
	if (keycode >= 0 && keycode < APP__MAX_KEYMAPPING) {
		int key_id = APP__key_mapping[keycode];
        APP__Emit_event(&(APP_Event){
            .type = APP_EVENT_KEY_RELEASED,
            .key  = {.id = key_id},
        });
	}
}

__attribute__((export_name("WAFN_report_mouse_button_down"))) void
WAFN_report_mouse_button_down(APP_MouseButton button_id) {
    APP__Emit_event(&(APP_Event){
        .type  = APP_EVENT_MOUSE_BUTTON_PRESSED,
        .mouse = {.button = button_id},
    });
}

__attribute__((export_name("WAFN_report_mouse_button_up"))) void
WAFN_report_mouse_button_up(APP_MouseButton button_id) {
    APP__Emit_event(&(APP_Event){
        .type  = APP_EVENT_MOUSE_BUTTON_RELEASED,
        .mouse = {.button = button_id},
    });
}

__attribute__((export_name("WAFN_report_mouse_move"))) void
WAFN_report_mouse_move(float x, float y) {
    APP__Emit_event(&(APP_Event){
        .type  = APP_EVENT_MOUSE_MOVE,
        .mouse = {.x = x, .y = y},
    });
}

__attribute__((export_name("WAFN_report_wheel_y"))) void
WAFN_report_wheel_y(float y) {
    APP__Emit_event(&(APP_Event){
        .type  = APP_EVENT_MOUSE_WHEEL_Y,
        .mouse = {.wheel_y = y},
    });
}

__attribute__((export_name("WAFN_report_wheel_x"))) void
WAFN_report_wheel_x(float x) {
    APP__Emit_event(&(APP_Event){
        .type  = APP_EVENT_MOUSE_WHEEL_X,
        .mouse = {.wheel_x = x},
    });
}

__attribute__((export_name("WAFN_report_touch_event_begin"))) void
WAFN_report_touch_event_begin(int id, float x, float y) {
    APP__Emit_event(&(APP_Event){
        .type  = APP_EVENT_TOUCH_BEGIN,
        .touch = {
            .id = id,
            .x  = x,
            .y  = y
        },
    });
}

__attribute__((export_name("WAFN_report_touch_event_move"))) void
WAFN_report_touch_event_move(int id, float x, float y) {
    APP__Emit_event(&(APP_Event){
        .type  = APP_EVENT_TOUCH_MOVE,
        .touch = {
            .id = id,
            .x  = x,
            .y  = y
        },
    });
}

__attribute__((export_name("WAFN_report_touch_event_end"))) void
WAFN_report_touch_event_end(int id, float x, float y) {
    APP__Emit_event(&(APP_Event){
        .type  = APP_EVENT_TOUCH_END,
        .touch = {
            .id = id,
            .x  = x,
            .y  = y
        },
    });
}

__attribute__((export_name("WAFN_report_touch_event_cancel"))) void
WAFN_report_touch_event_cancel(int id, float x, float y) {
    APP__Emit_event(&(APP_Event){
        .type  = APP_EVENT_TOUCH_CANCEL,
        .touch = {
            .id = id,
            .x  = x,
            .y  = y
        },
    });
}

APP_INTERNAL void
APP__wasm_Process_events(void) {
	APP__data.focus = APP__JS_Window_has_focus();
}


#endif // defined (APP_WASM)




// Documented on the above, on the head section
#if defined(APP_WASM)

APP_PUBLIC int
APP_Run_application_loop(int(*application_frame)(void)) {
	APP__data.frame_callback = application_frame;
	return 0;
}

#else

APP_PUBLIC int
APP_Run_application_loop(int(*application_frame)(void)) {
	APP__ASSERT(application_frame);
	APP__data.frame_callback = application_frame;
	int loop_result = 0;
	for (;;) {
		loop_result = APP__Handle_frame();
		if (loop_result != 0) {break;}
	}
#if defined(APP_WINDOWS)
	// I'm not sure if this can be done here or must be always inside the windowProc :/ Tano.
	PostQuitMessage(0);
#endif
	if (loop_result == 1) {
		return 0;
	}
	return loop_result;
}

#endif


#endif // APP_IMPLEMENTATION_INCLUDED
#endif // APP_IMPLEMENTATION


