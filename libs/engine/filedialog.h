#ifndef FDLG_INCLUDED
#define FDLG_INCLUDED

#ifndef FDLG_PUBLIC
	#define FDLG_PUBLIC 
#endif

#ifndef FDLG_INTERNAL
	#define FDLG_INTERNAL static
#endif


typedef struct FDLG_Context FDLG_Context;

typedef void (*FDLG_Callback) (FDLG_Context *ctx, const char *filename);

struct FDLG_Context{
    const char *title;
    const char *path;
    const char *filter_name;
    const char **filter_extensions;
    const char *extension;
    void *user_data;
    FDLG_Callback callback;
};

enum {
    FDLG_OPEN = 0,
    FDLG_SAVE = 1
};

FDLG_PUBLIC const char*
FDLG_Get_last_error(void);

FDLG_PUBLIC void
FDLG_Dialog(FDLG_Context *opt, int type);

#ifdef __wasm__
FDLG_PUBLIC int
FDLG_Wasm_get_filesize(void);

FDLG_PUBLIC void
FDLG_Wasm_read_filedata(unsigned char *dst, int size);
#endif



#endif // FDLG_INCLUDED






#ifdef FDLG_IMPLEMENTATION
#ifndef FDLG_IMPLEMENTATION_INCLUDED
#define FDLG_IMPLEMENTATION_INCLUDED




#ifdef __wasm__
#define FDLG__ASSERT(x) do {if (!(x)) __builtin_trap();} while(0)
#else
#define FDLG__ASSERT(x) do {if (!(x)) *((volatile int *)(0)) = 1;} while(0)
#endif


#ifndef FDLG__SNPRINTF
    #include <string.h>
    #define FDLG__SNPRINTF snprintf
#endif


FDLG_INTERNAL const char *FDLG__last_error = NULL;


FDLG_INTERNAL int
FDLG__Strlen(const char *s) {
    if (s == NULL) return 0;
    int len = 0;
    for (; s[len]!=0; len+=1);
    return len;
}


FDLG_INTERNAL const char *
FDLG__Get_file_extension(const char *filename) {
    int slen   = FDLG__Strlen(filename);
    int cursor = slen-1;
    for (;
        cursor >= 0 &&
        filename[cursor] != '.' &&
        filename[cursor] != '/';
        cursor-=1);

    if (cursor > 0 && filename[cursor] == '.') {
        cursor += 1;
    }
    else {
        cursor = slen;
    }
    return &filename[cursor];
}



FDLG_PUBLIC const char*
FDLG_Get_last_error(void) {
    const char *res = FDLG__last_error;
    FDLG__last_error = NULL;
    return res;
}


// Windows

#ifdef _WIN32

#include <windows.h>


FDLG_INTERNAL const char*
FDLG__Make_filter_str(FDLG_Context *ctx) {
    static char buf[1024];
    int n;

    buf[0] = '\0';
    n = 0;
    
    const char *filter_name = ctx->filter_name ? ctx->filter_name : "Filter";
    n += sprintf(buf + n, "%s", filter_name) + 1;

    const char **extensions = ctx->filter_extensions;
    if (extensions == NULL) return buf;
    for (int ext_i = 0; extensions[ext_i] != NULL; ext_i+=1) {
        const char *ext = extensions[ext_i];
        n += sprintf(&buf[n], "*.%s;", ext);
    }
    n += 1;
    buf[n] = '\0';

    n += sprintf(&buf[n], "All Files") + 1;
    n += sprintf(&buf[n], "*.*");
    n += 1;
    buf[n] = '\0';

    return buf;
}


FDLG_PUBLIC void
FDLG_Dialog(FDLG_Context *ctx, int type) {
    
    FDLG__ASSERT(ctx->callback);
    FDLG__ASSERT(type == FDLG_OPEN || type == FDLG_SAVE);

    int ok = 0;
    OPENFILENAME ofn;
    static char result_buf[2048];
    result_buf[0] = '\0';
    
    memset(&ofn, 0, sizeof(OPENFILENAME));
    ofn.hwndOwner        = NULL;
    ofn.lStructSize      = sizeof(OPENFILENAME);
    ofn.lpstrFilter      = FDLG__Make_filter_str(ctx);
    ofn.nFilterIndex     = 1;
    ofn.lpstrFile        = result_buf;
    ofn.Flags            = OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT;
    ofn.nMaxFile         = sizeof(result_buf) - 1;
    ofn.lpstrInitialDir  = ctx->path;
    ofn.lpstrTitle       = ctx->title;
    ofn.lpstrDefExt      = ctx->extension;

    FDLG__last_error = NULL;
    if (type == FDLG_OPEN) {
        ok = GetOpenFileName(&ofn);
        if (ok) {
            ctx->callback(ctx, ofn.lpstrFile);
        }
        else {
            ctx->callback(ctx, NULL);
        }
    }
    else if (type == FDLG_SAVE) {
        ok = GetSaveFileName(&ofn);
        if (ok) {
            ctx->callback(ctx, ofn.lpstrFile);
        }
        else {
            ctx->callback(ctx, NULL);
        }
    }
}




/******************************************************************************
** Zenity
*******************************************************************************/

#elif defined(__linux__) || defined(__unix__)

#include <stdlib.h>

FDLG_PUBLIC void
FDLG_Dialog(FDLG_Context *ctx, int type) {

    FDLG__ASSERT(ctx->callback);
    FDLG__ASSERT(type == FDLG_OPEN || type == FDLG_SAVE);

    static char result_buf[2048];
    char buf[2048];
    char *p;
    const char *title;
    FILE *fp;
    int n, len;
    
    FDLG__last_error = NULL;
    
    fp = popen("zenity --version", "r");
    if (fp == NULL || pclose(fp) != 0) {
        FDLG__last_error = "could not open zenity";
        ctx->callback(ctx, NULL);
        return;
    }
    
    
    n = sprintf(buf, "zenity --file-selection");
    
    if (type == FDLG_SAVE) {
        n += sprintf(buf + n, " --save --confirm-overwrite");
    }
    
    if (ctx->title) {
        title = ctx->title;
    } if (type == FDLG_SAVE) {
        title = "Save File";
    }
    else {
        title = "Open File";
    }
    
    n += sprintf(buf + n, " --title=\"%s\"", title);
    
    if (ctx->path && ctx->path[0] != '\0') {
        n += sprintf(buf + n, " --filename=\"");
        p = realpath(ctx->path, buf + n);
        if (p == NULL) {
            FDLG__last_error = "call to realpath() failed";
            ctx->callback(ctx, NULL);
            return;
        }
        n += FDLG__Strlen(buf + n);
        n += sprintf(buf + n, "/\"");
    }
    
    if (ctx->filter_extensions && *(ctx->filter_extensions)) {
        n += sprintf(buf + n, " --file-filter=\"");
        
        if (ctx->filter_name) {
            n += sprintf(buf + n, "%s | ", ctx->filter_name);
        }

        const char **extensions = ctx->filter_extensions;
        for (int ext_i = 0; extensions[ext_i] != NULL; ext_i+=1) {
            const char *ext = extensions[ext_i];
            n += sprintf(&buf[n], "\"*.%s\" ", ext);
        }
        n += sprintf(buf + n, "\"");
    }
    
    n += sprintf(buf + n, " --file-filter=\"All Files | *\"");
    
    
    fp = popen(buf, "r");
    len = fread(result_buf, 1, sizeof(result_buf) - 1, fp);
    pclose(fp);
    
    if (len > 0) {
        result_buf[len - 1] = '\0';
        if (type == FDLG_SAVE && ctx->extension) {
            const char *file_extension = FDLG__Get_file_extension(result_buf);
            if (file_extension[0] == '\0') {
                sprintf(&result_buf[len - 1], ".%s", ctx->extension);
            }
        }
        ctx->callback(ctx, result_buf);
        return;
    }

    ctx->callback(ctx, NULL);
}


#elif defined(__wasm__)

#define FDLG__WA_JS(ret, name, args, ...) extern __attribute__((import_module("JS"), import_name(#name "|" #args "|" #__VA_ARGS__))) ret name args;


__attribute__((export_name("FDLG__WAFN_Run_callback"))) void
FDLG__WAFN_Run_callback(FDLG_Context *ctx, const char *filename) {
    ctx->callback(ctx, filename);
}

FDLG__WA_JS(void, FDLG__JS_Open_file_dialog, (FDLG_Context *ctx, char *accept, int accept_len,
    char *dst_filename, int max_size), {

    var file_chooser = document.createElement('input');
    file_chooser.setAttribute("type", "file");
    if (accept !== 0) {
	    const UTF8_decoder = new TextDecoder("utf-8");
        const accept_buffer = new Uint8Array(Module.ASM.memory.buffer, accept, accept_len);
	    const accept_string = UTF8_decoder.decode(accept_buffer);
        file_chooser.setAttribute("accept", accept_string);
    }
    file_chooser.onchange = function(e) {
        if (file_chooser.files.length > 0) {
			const encoder = new TextEncoder;
			var mem_u8 = new Uint8Array(Module.ASM.memory.buffer);
			const stats = encoder.encodeInto(
                file_chooser.files[0].name,
                mem_u8.subarray(dst_filename, dst_filename+max_size-1));
			mem_u8[dst_filename + stats.written] = 0;

            file_chooser.files[0].arrayBuffer()
            .then( filedata => {
                Module.FDLG__filedata = filedata;
                Module.ASM.FDLG__WAFN_Run_callback(ctx, dst_filename);
                Module.FDLG__filedata = null;
            })
            .catch( e => {
                Module.ASM.FDLG__WAFN_Run_callback(ctx, 0);
            });
		}
        else {
            Module.ASM.FDLG__WAFN_Run_callback(ctx, 0);
        }
    };
    file_chooser.click();
});


FDLG_PUBLIC void
FDLG_Dialog(FDLG_Context *ctx, int type) {
    FDLG__ASSERT(ctx->callback);
    FDLG__ASSERT(type == FDLG_OPEN || type == FDLG_SAVE);
    if (type == FDLG_OPEN) {
        static char filename[2048] = {0};
        static char accept_filter[2048] = {0};
        if (ctx->filter_extensions && *(ctx->filter_extensions)) {
            const char **extensions = ctx->filter_extensions;
            accept_filter[0] = 0;
            const char *sep = "";
            int n = 0;
            for (int ext_i = 0; extensions[ext_i] != NULL; ext_i+=1) {
                n += FDLG__SNPRINTF(&accept_filter[n], sizeof(accept_filter)-n, "%s.%s", sep, extensions[ext_i]);
                sep = ",";
            }
            FDLG__JS_Open_file_dialog(ctx, accept_filter, n, filename, sizeof(filename));
        }
        else {
            FDLG__JS_Open_file_dialog(ctx, NULL, 0, filename, sizeof(filename));
        }
    }

    return;
}

FDLG__WA_JS(int, FDLG__JS_Get_filesize, (void), {
    if (Module.FDLG__filedata) return Module.FDLG__filedata.byteLength;
    return 0;
});

FDLG_PUBLIC int
FDLG_Wasm_get_filesize(void) {
    return FDLG__JS_Get_filesize();
}

FDLG__WA_JS(int, FDLG__JS_Read_filedata, (unsigned char *dst, int size), {
    if (!Module.FDLG__filedata) return 0;
	var dst_buffer = new Uint8Array(Module.ASM.memory.buffer, dst, size);
    const bytes_to_copy = size > Module.FDLG__filedata.byteLength ?
        Module.FDLG__filedata.byteLength : size;
    dst_buffer.set(new Uint8Array(Module.FDLG__filedata, 0, bytes_to_copy));
    return;
});

FDLG_PUBLIC void
FDLG_Wasm_read_filedata(unsigned char *dst, int size) {
    FDLG__JS_Read_filedata(dst, size);
}


FDLG__WA_JS(void, FDLG__JS_Save_filedata, (const unsigned char *data, int datasize,
    const char *filename, int filename_size), {

    const data_array = new Uint8Array(Module.ASM.memory.buffer, data, datasize);

	const UTF8_decoder = new TextDecoder("utf-8");
    const filename_buffer = new Uint8Array(Module.ASM.memory.buffer, filename, filename_size);
	const filename_string = UTF8_decoder.decode(filename_buffer);

    if (Module.FDLG__download) {
        if (Module.FDLG__download.href) {
            URL.revokeObjectURL(Module.FDLG__download.href);
        }
    }

    Module.FDLG__download = document.createElement("a");
    Module.FDLG__download.href = window.URL.createObjectURL(new Blob([data_array]));
    Module.FDLG__download.download = filename_string;
    Module.FDLG__download.click(); 
});

FDLG_PUBLIC void
FDLG_Wasm_save_filedata(const unsigned char *dst, int size, const char *filename) {
    FDLG__JS_Save_filedata(dst, size, filename, FDLG__Strlen(filename));
}

#else

#error "filedialog: Unsuported platform"

#endif


#endif // FDLG_IMPLEMENTATION_INCLUDED
#endif // FDLG_IMPLEMENTATION

