#ifndef LOG_INCLUDED
#define LOG_INCLUDED

#ifndef LOG_PUBLIC
	#define LOG_PUBLIC 
#endif

#ifndef LOG_INTERNAL
	#define LOG_INTERNAL static
#endif


LOG_PUBLIC void
LOG_Debug(const char *msg);


#endif // LOG_INCLUDED


#ifdef LOG_IMPLEMENTATION
#ifndef LOG_IMPLEMENTATION_INCLUDED
#define LOG_IMPLEMENTATION_INCLUDED

#ifdef __wasm__

#define LOG__WA_JS(ret, name, args, ...) extern __attribute__((import_module("JS"), import_name(#name "|" #args "|" #__VA_ARGS__))) ret name args;

LOG__WA_JS(void, LOG__JS_Console_log, (const char *msg, int msg_len), {
	const UTF8_decoder = new TextDecoder("utf-8");
    const msg_buffer   = new Uint8Array(Module.ASM.memory.buffer, msg, msg_len);
	const msg_string = UTF8_decoder.decode(msg_buffer);
    console.log(msg_string);
});

LOG_PUBLIC void
LOG_Debug(const char *msg) {
    int len = 0;
    for (;msg[len];len+=1);
    LOG__JS_Console_log(msg, len);
}

#else

#include <stdio.h>

LOG_PUBLIC void
LOG_Debug(const char *msg) {
    puts(msg);
}

#endif

#endif // LOG_IMPLEMENTATION_INCLUDED
#endif // LOG_IMPLEMENTATION

