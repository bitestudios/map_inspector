
#ifndef ASSET_INCLUDED
#define ASSET_INCLUDED

#ifndef ASSET_PUBLIC
	#define ASSET_PUBLIC 
#endif

#ifndef ASSET_INTERNAL
	#define ASSET_INTERNAL static
#endif

#include <stdint.h>

enum {
    ASSET_RESPONSE_ERROR_UNKNOWN,
    ASSET_RESPONSE_LOADED,
    ASSET_RESPONSE_NOT_FOUND,
    ASSET_RESPONSE_OUT_OF_MEMORY,
};

typedef void (*ASSET_ResponseCallback)(
    void *user,
    uint8_t *filedata,
    int64_t filedata_len,
    int response_state);

ASSET_PUBLIC int
ASSET_Init(void *arena_data, int64_t arena_cap);

ASSET_PUBLIC void
ASSET_Deinit(void);

ASSET_PUBLIC int
ASSET_Request_fetch(const char *filename, void *user, ASSET_ResponseCallback callback);

ASSET_PUBLIC void
ASSET_Update(void);

ASSET_PUBLIC void
ASSET_Free(void *data_to_free);

enum {
    ASSET_STATE_NULL = 0,
    ASSET_QUEUED,
    ASSET_LOADING,
    ASSET_LOADED,
    ASSET_PARSING_ERROR,
    ASSET_NOT_FOUND,
    ASSET_OUT_OF_MEMORY,
    ASSET_SLOTS_FULL,
};

#ifndef ASSET_NO_SOUND

struct SND_Sound_t;
typedef struct SND_Sound_t SND_Sound;

ASSET_PUBLIC SND_Sound *
ASSET_SOUND(const char *name, int *state);

#endif // ASSET_NO_SOUND


#ifndef ASSET_NO_TEXTURE

ASSET_PUBLIC unsigned int
ASSET_TEXTURE(const char *name, int *state);

#endif // ASSET_NO_TEXTURE



#ifndef ASSET_NO_FONT

struct GFX_Font_t;
typedef struct GFX_Font_t GFX_Font;

ASSET_PUBLIC GFX_Font *
ASSET_FONT(const char *name, int height, int *state);


#endif // ASSET_NO_FONT




#endif // ASSET_INCLUDED



#ifdef ASSET_IMPLEMENTATION
#ifndef ASSET_IMPLEMENTATION_INCLUDED
#define ASSET_IMPLEMENTATION_INCLUDED

#ifndef ASSET_QUEUE_SIZE
    #define ASSET_QUEUE_SIZE 10
#endif

#ifndef ASSET__MAX_EXEC_PATH
    #define ASSET__MAX_EXEC_PATH 1024
#endif

enum {
    ASSET__STATE_IDLE = 0,
    ASSET__STATE_FETCHING,
    ASSET__STATE_FETCHED
};

typedef struct {
    const char *path;
    void       *user;
    ASSET_ResponseCallback callback;
} ASSET__Request;

typedef struct {
    uint8_t *data;
    int64_t  data_len;
    int      status;
} ASSET__Response;

struct {
    uint8_t *arena_data;
    int64_t  arena_cap;
    uint8_t *free;
    int64_t  free_cap;
    char exec_path[ASSET__MAX_EXEC_PATH];
    int64_t exec_path_len;
    ASSET__Request request_queue[ASSET_QUEUE_SIZE];
    int64_t request_queue_head;
    int64_t request_queue_count;
    ASSET__Request  current_request;
    ASSET__Response response;
    int state;
} ASSET__data = {0};


// Some dirty CRT replacements

#if defined(__wasm__) 
    #if defined(__clang__)
        #define ASSET__ASSERT(x) do {if(!(x)) __builtin_trap();} while(0)
    #else
        #error "Target wasm without clang???"
    #endif
#else
    #define ASSET__ASSERT(x) do {if(!(x)) *((volatile int *)NULL) = 0;} while(0)
#endif

ASSET_INTERNAL size_t
ASSET__strlen(const char *str) {
    size_t len = 0;
    for (;  str[len]; len+=1);
    return len;
}




ASSET_INTERNAL void *
ASSET__memset(void *dst_, int c, size_t n) {
    uint8_t *dst = dst_;
    for (size_t i = 0; i < n; i+=1) {
        dst[i] = (uint8_t)c;
    }
    return dst;
}

ASSET_INTERNAL int
ASSET__memcmp(const void *str1_, const void *str2_, size_t n) {
    const char *str1 = str1_;
    const char *str2 = str2_;
    for (size_t i = 0; i < n; i+=1) {
        int subs = str1[i] - str2[i];
        if (subs) return subs;
    }
    return 0;
}


ASSET_INTERNAL int
ASSET__strcmp(const char *str1, const char *str2) {
    size_t cursor = 0;
    for (; str1[cursor] && str2[cursor]; cursor+=1) {
        int subs = str1[cursor] - str2[cursor];
        if (subs) return subs;
    }
    return str1[cursor] - str2[cursor];
}

ASSET_INTERNAL int
ASSET__strncmp(const char *str1, const char *str2, size_t n) {
    size_t cursor = 0;
    for (; str1[cursor] && str2[cursor] && cursor < n-1; cursor+=1) {
        int subs = str1[cursor] - str2[cursor];
        if (subs) return subs;
    }
    return str1[cursor] - str2[cursor];
}

ASSET_INTERNAL void *
ASSET__memcpy(void *dest_, const void *src_, size_t n) {
    char *dest = dest_;
    const char *src = src_;
    for (size_t i = 0; i < n; i+=1) {
        dest[i] = src[i];
    }
    return dest_;
}


// A simple linear allocator to use in the external libraries that use malloc, after use the library
// and not require any of his resources we must call ASSET__malloc_clean to reset the allocator,
// This is NOT thread safe.
uint8_t ASSET__malloc_mem[64*1024*1024] __attribute__((aligned(16))) = {0};
size_t ASSET__malloc_mem_cursor = 0;
ASSET_INTERNAL void *
ASSET__malloc(size_t size) {
    size = (size+15)&(~15);
    void *result = NULL;
    if (ASSET__malloc_mem_cursor + size <= 64*1024*1024) {
        result = &ASSET__malloc_mem[ASSET__malloc_mem_cursor];
        ASSET__malloc_mem_cursor += size;
    }
    return result;
}

ASSET_INTERNAL void
ASSET__free(void *ptr) {
    (void)ptr;
}

ASSET_INTERNAL void *
ASSET__realloc(void *ptr, size_t old_size, size_t new_size) {
    (void)ptr;
    void *new_ptr = ASSET__malloc(new_size);
    if (!new_ptr) return NULL;
    if (ptr) ASSET__memcpy(new_ptr, ptr, old_size);
    return new_ptr;
}

ASSET_INTERNAL void
ASSET__malloc_clean(void) {
    ASSET__malloc_mem_cursor = 0;
}


ASSET_INTERNAL void
ASSET__qsort(void *base_, size_t nitems, size_t size, int (*compar)(const void *, const void*)) {
    unsigned char *base = base_;
    if (nitems < 2) return;

    unsigned char *pivot = &base[(nitems/2)*size];

    size_t i, j;
    for (i=0, j=nitems-1; ;i+=1, j-=1) {
        while (i < nitems && compar(&base[i*size], pivot) < 0) i+=1;
        while (0 < j      && compar(pivot, &base[j*size]) < 0) j-=1;

        if (i >= j) break;

        if (&base[i*size] == pivot) {
            pivot = &base[j*size];
        }else if (&base[j*size] == pivot) {
            pivot = &base[i*size];
        }
        for (size_t byte_i = 0; byte_i < size; byte_i+=1) {
            unsigned char tmp = base[i*size+byte_i];
            base[i*size+byte_i] = base[j*size+byte_i];
            base[j*size+byte_i] = tmp;
        }
    }
    
    ASSET__qsort(base, i, size, compar);
    ASSET__qsort(&base[i*size], nitems-i, size, compar);
}


ASSET_INTERNAL double
ASSET__ldexp(double x, int exp) {
    if (exp < -1022 || exp > 1023) return 0.0;
    uint64_t exp264 = (((uint64_t)(exp+1023)) << 52);
    double exp264f = *(volatile double *)&exp264;
    return x*exp264f;
}

// End of the CRT stuff

// djb2  http://www.cse.yorku.ca/~oz/hash.html
ASSET_INTERNAL  uint64_t
ASSET__String_hash(const char *str) {
    uint64_t hash = 5381;
    int cursor = 0;
    for (; str[cursor]; cursor+=1) {
        uint64_t c = str[cursor];
        hash = ((hash << 5) + hash) + c; /* hash * 33 + c */
    }
    return hash;
}


ASSET_PUBLIC int
ASSET_Init(void *arena_data, int64_t arena_cap) {
    uintptr_t pointer_size = sizeof(void*);
    if (((~((uintptr_t)arena_data))&(pointer_size-1)) != (pointer_size-1)) return -1; // Unaligned memory
    ASSET__memset(&ASSET__data, 0, sizeof(ASSET__data));
    ASSET__data.arena_data = arena_data;
    ASSET__data.arena_cap  = arena_cap;
    ((int64_t*)ASSET__data.arena_data)[0] = (int64_t)(ASSET__data.arena_data);
    ASSET__data.free      = &(((uint8_t*)arena_data)[8]);
    ASSET__data.free_cap  = arena_cap-8;
    return 0;
}

ASSET_PUBLIC void
ASSET_Deinit(void) {
    ASSET__memset(&ASSET__data, 0, sizeof(ASSET__data));
}


ASSET_INTERNAL void *
ASSET__Alloc(int64_t amount_) {
    void *result = NULL;
    // Make the amount multiple of 8 because alignment
    int64_t amount = ((amount_+7)&(~(int64_t)0x7));
    
    // We add 8 because we have to put a pointer to the begin of the allocation at the end
    if (ASSET__data.free_cap < amount+8) return result;

    // Set the pointer to the start of the allocation at the end
    ((int64_t*)&ASSET__data.free[amount])[0] = (int64_t)(ASSET__data.free);
    result = ASSET__data.free;
    ASSET__data.free      = &ASSET__data.free[amount+8];
    ASSET__data.free_cap -= (amount+8);

    return result;
}


ASSET_PUBLIC void
ASSET_Free(void *data_to_free) {
    // Note that we store the pointer as a 64 bits integer even when the pointer size is 32!!
    // in order to keep the sizes of everything the same
    void *last_alloc = (void *)*((int64_t*)(&ASSET__data.free[-8]));
    if (data_to_free == last_alloc) {
        int64_t allocation_size = ((int64_t)ASSET__data.free)-((int64_t)last_alloc);
        ASSET__data.free_cap -= allocation_size;
        ASSET__data.free      = last_alloc;
    }
}

ASSET_INTERNAL void
ASSET__Platform_send_request(const char *path);

ASSET_PUBLIC void
ASSET__Notify_response(const ASSET__Response *resp) {
    ASSET__ASSERT(ASSET__data.state == ASSET__STATE_FETCHING);
    ASSET__data.response = *resp;
    ASSET__data.state = ASSET__STATE_FETCHED;
}

ASSET_PUBLIC void
ASSET_Update(void) {
    if (ASSET__data.state == ASSET__STATE_FETCHED) {
        ASSET__Request  *creq = &(ASSET__data.current_request);
        ASSET__Response *resp = &(ASSET__data.response);
        creq->callback(creq->user, resp->data, resp->data_len, resp->status);
        ASSET__memset(creq, 0, sizeof(*creq));
        ASSET__memset(resp, 0, sizeof(*resp));
        ASSET__data.state = ASSET__STATE_IDLE;
    }
    if (ASSET__data.state == ASSET__STATE_IDLE && ASSET__data.request_queue_count > 0) {
        ASSET__Request *req = &(ASSET__data.request_queue[ASSET__data.request_queue_head]);
        ASSET__data.request_queue_head  = (ASSET__data.request_queue_head+1)%ASSET_QUEUE_SIZE;
        ASSET__data.request_queue_count -= 1;
        ASSET__data.state = ASSET__STATE_FETCHING;
        ASSET__data.current_request = *req;
        ASSET__Platform_send_request(req->path);
    }
}


ASSET_PUBLIC int
ASSET_Request_fetch(const char *filename, void *user, ASSET_ResponseCallback callback) {
    if (ASSET__data.request_queue_count == ASSET_QUEUE_SIZE) return -1;
    int cursor = (ASSET__data.request_queue_head+ASSET__data.request_queue_count)%ASSET_QUEUE_SIZE;
    ASSET__Request *request = &ASSET__data.request_queue[cursor];
    ASSET__data.request_queue_count += 1;
    request->path = filename;
    request->user = user;
    request->callback = callback;
    return 0;
}

#if defined(WIN32) || defined(_WIN32) || defined(__linux__) || defined(__unix__)

ASSET_INTERNAL void
ASSET__Platform_send_request(const char *path) {
    ASSET__Response response = {0};
    FILE    *file        = NULL;
    uint8_t *filedata    = NULL;
    int64_t  filedata_len = 0;

    file = fopen(path, "rb");
    if (!file) {
        response.status = ASSET_RESPONSE_NOT_FOUND;
        ASSET__Notify_response(&response);
        return;
    }

    fseek(file, 0, SEEK_END);
    filedata_len = ftell(file);
    fseek(file, 0, SEEK_SET);
    // TODO(Tano) race condition if we use threads
    filedata = ASSET__Alloc(filedata_len);
    if (filedata == NULL) {
        fclose(file);
        response.status = ASSET_RESPONSE_OUT_OF_MEMORY;
        ASSET__Notify_response(&response);
        return;
    }
    if (fread(filedata, 1, filedata_len, file) != filedata_len) {
        fclose(file);
        // TODO(Tano) this is not the right error...
        response.status = ASSET_RESPONSE_OUT_OF_MEMORY;
        ASSET__Notify_response(&response);
        return;
    }
    fclose(file);
    file = NULL;

    response.status   = ASSET_RESPONSE_LOADED;
    response.data     = filedata;
    response.data_len = filedata_len;
    ASSET__Notify_response(&response);
}





#elif defined(__wasm__)

#define ASSET__WA_JS(ret, name, args, ...) extern __attribute__((import_module("JS"), import_name(#name "|" #args "|" #__VA_ARGS__))) ret name args;


ASSET__WA_JS(void, ASSET__JS_Fetch, (const char *path, int path_len), {

    const path_buffer = new Uint8Array(Module.ASM.memory.buffer, path, path_len);
	const path_string = new TextDecoder("utf-8").decode(path_buffer);

    fetch(path_string)
        .then(response => {
            if (!response.ok) {
                Module.ASM.ASSET__WAFN_Response_end(0, 0);
            } else {
                response.arrayBuffer().then(arr => {
                    dst_addr = Module.ASM.ASSET__WAFN_Alloc(arr.byteLength);
                    if (dst_addr == 0) {
                        Module.ASM.ASSET__WAFN_Response_end(0, 0);
                        return;
                    }
                    var dst_buffer = new Uint8Array(Module.ASM.memory.buffer, dst_addr, arr.byteLength);
                    dst_buffer.set(new Uint8Array(arr));
                    Module.ASM.ASSET__WAFN_Response_end(dst_addr, arr.byteLength);
                });
            }
        }).catch(error => {
            Module.ASM.ASSET__WAFN_Response_end(0, 0);
        });
});


__attribute__((export_name("ASSET__WAFN_Response_end"))) void
ASSET__WAFN_Response_end(uint8_t *filedata, size_t filedata_len) {
    ASSET__Response response = {0};
    if (filedata) {
        response.status   = ASSET_RESPONSE_LOADED;
        response.data     = filedata;
        response.data_len = filedata_len;
    }
    ASSET__Notify_response(&response);
}

__attribute__((export_name("ASSET__WAFN_Alloc"))) void *
ASSET__WAFN_Alloc(size_t amount) {
    return ASSET__Alloc(amount);
}

ASSET_INTERNAL void
ASSET__Platform_send_request(const char *path) {
    int path_len = 0;
    for (; path[path_len]; path_len+=1);
    ASSET__JS_Fetch(path, path_len);
}



#endif


// Some dirty CRT replacements





//---------------------------------
//
// ASSET SOUND IMPLEMENTATION
//
//--------------------------------

#ifndef ASSET_NO_SOUND



#include "sound.h"



// ------------------------------------------------------
// VORBIS FORMAT
// ------------------------------------------------------

// #define ASSET_SOUND_VORBIS_USE_CRT

#ifndef ASSET_SOUND_NO_VORBIS

#define STB_VORBIS_NO_STDIO
#define STB_VORBIS_NO_INTEGER_CONVERSION

#ifndef ASSET_SOUND_VORBIS_USE_CRT
    #define STB_VORBIS_NO_CRT
    #define floor(x) ((Fract(x) < 0.0f) ? Trunc(x)-1.0f : Trunc(x))
    #define exp(x)   Exp(x)
    #define ldexp(x, y)   ASSET__ldexp(x, y);
    #define cos(x)   Cos(x)
    #define sin(x)   Sin(x)
    #define abs(x)   Abs(x)
    #define pow(x,y) Pow(x,y)
    #define log(x)   Ln(x)
    #define memset(a, b, c) ASSET__memset(a, b, c)
    #define memcpy(a, b, c) ASSET__memcpy(a, b, c)
    #define memcmp(a, b, c) ASSET__memcmp(a, b, c)
    #define alloca(a) (NULL)
    #define malloc(a) (NULL)
    #define free(a) (NULL)
    #define realloc(a,b) (NULL)
    #define assert(x) ASSET__ASSERT(x)
    #define qsort(a, b, c, d) ASSET__qsort(a, b, c, d)
#endif // ASSER_SOUND_VORBIS_USE_CRT

#define Floor Floor_
#include "third_party/stb_vorbis.h"
#undef floor

#ifndef ASSET_SOUND_VORBIS_USE_CRT
    #undef Floor
    #undef qsort
    #undef assert
    #undef realloc
    #undef free
    #undef malloc
    #undef alloca
    #undef memcmp
    #undef memcpy
    #undef memset
    #undef log
    #undef pow
    #undef abs
    #undef sin
    #undef cos
    #undef ldexp
    #undef exp
#endif // ASSER_SOUND_VORBIS_USE_CRT





typedef struct {
    int32_t (*fill_buffer)(
        void *sound_,
        int32_t cursor_start,
        float *dst_buffer,
        int32_t frames_count,
        int32_t channels_count,
        int32_t sample_rate,
        float volume);

	stb_vorbis *vorbis;
	stb_vorbis_info vorbis_info;
    uint8_t *data;
    int      data_len;
} ASSET__SoundVorbis;




int32_t
ASSET_SOUND__Vorbis_fill_buffer(
    void *sound_,
    int32_t cursor_start,
    float *dst_buffer,
    int32_t frames_count,
    int32_t channels_count,
    int32_t sample_rate,
    float volume) {

    float **outputs; // Dummy value
    ASSET__SoundVorbis *sound = (ASSET__SoundVorbis *)sound_;
    int64_t src_sample_rate = sound->vorbis_info.sample_rate;
    int64_t dst_sample_rate = sample_rate;
    int32_t src_channels    = sound->vorbis_info.channels;
    float   src_channels_inv = 1.0f/(float)src_channels;
    int32_t cursor = cursor_start;
    int64_t block_sample_start = cursor_start*src_sample_rate/dst_sample_rate;
    stb_vorbis_seek(sound->vorbis, block_sample_start);
    int frame_i = 0;
	for (;frame_i < frames_count;) {
        int block_start = sound->vorbis->channel_buffer_start;
        int block_end   = sound->vorbis->channel_buffer_end;
        int block_size  = block_end-block_start;
        
        int64_t src_sample_i = cursor*src_sample_rate/dst_sample_rate-block_sample_start+block_start;
        for (;src_sample_i < block_end && frame_i < frames_count;) {
            if (src_channels == channels_count) {
                for (int channel_i = 0; channel_i < channels_count; channel_i+=1) {
                    float f32_val = sound->vorbis->channel_buffers[channel_i][src_sample_i];
		            dst_buffer[frame_i*channels_count+channel_i] += f32_val * volume;
                }
            }
            else {
                float f32_val = 0;
                for (int channel_i = 0; channel_i < src_channels; channel_i+=1) {
                    f32_val += sound->vorbis->channel_buffers[channel_i][src_sample_i];
                }
                for (int channel_i = 0; channel_i < channels_count; channel_i+=1) {
		            dst_buffer[frame_i*channels_count+channel_i] += f32_val * src_channels_inv * volume;
                }
            }
            cursor += 1;
            frame_i = cursor-cursor_start;
            src_sample_i = cursor*src_sample_rate/dst_sample_rate-block_sample_start+block_start;
        }
        block_sample_start += block_size;
        if (!stb_vorbis_get_frame_float(sound->vorbis, NULL, &outputs)) break;
	}
    return cursor-cursor_start;
}



static bool
ASSET_SOUND_Load_vorbis_from_memory(uint8_t *filedata, int64_t filedata_len, ASSET__SoundVorbis *vorbis) {
	int error;
    // Buffer to store the memory needed by vorbis, Note that this must persist
    static size_t alloc_buffer_base = 0;
    static char alloc_buffer[1024*1024] = {0};
    stb_vorbis_alloc alloc = {&alloc_buffer[alloc_buffer_base], (sizeof(alloc_buffer))-alloc_buffer_base};
    stb_vorbis *v = stb_vorbis_open_memory(filedata, filedata_len, &error, &alloc);
    if (v != NULL) {
        stb_vorbis_info v_info = stb_vorbis_get_info(v);
        alloc_buffer_base   = v_info.setup_memory_required + v_info.temp_memory_required;
        vorbis->vorbis      = v;
        vorbis->vorbis_info = v_info;
        vorbis->fill_buffer = ASSET_SOUND__Vorbis_fill_buffer;
        vorbis->data        = filedata;
        vorbis->data_len    = filedata_len;
        return true;
    }
    return false;
}

#else

typedef struct {
    int32_t (*fill_buffer)(
        void *sound_,
        int32_t cursor_start,
        float *dst_buffer,
        int32_t frames_count,
        int32_t channels_count,
        int32_t sample_rate,
        float volume);
} ASSET__SoundVorbis;

static bool
ASSET_SOUND_Load_vorbis_from_memory(uint8_t *filedata, int64_t filedata_len, ASSET__SoundVorbis *vorbis) {
    return false;
}

#endif // ASSET_SOUND_NO_VORBIS


// ------------------------------------------------------
// WAV format
// ------------------------------------------------------


typedef struct wav_header {
    // RIFF Header
    char    riff_header[4]; // Contains "RIFF"
    int32_t wav_size; // Size of the wav portion of the file, which follows the first 8 bytes. File size - 8
    char    wave_header[4]; // Contains "WAVE"
    
    // Format Header
    char    fmt_header[4]; // Contains "fmt " (includes trailing space)
    int32_t fmt_chunk_size; // Should be 16 for PCM
    int16_t audio_format; // Should be 1 for PCM. 3 for IEEE Float
    int16_t num_channels;
    int32_t sample_rate;
    int32_t byte_rate; // Number of bytes per second. sample_rate * num_channels * Bytes Per Sample
    int16_t sample_alignment; // num_channels * Bytes Per Sample
    int16_t bit_depth; // Number of bits per sample
    
    // If found a weird "LIST" :/
    // Data
    // char data_header[4]; // Contains "data"
    // int data_bytes; // Number of bytes in data. Number of samples * num_channels * sample byte size
    // uint8_t bytes[]; // Remainder of wave file is bytes
    
} ASSET_SOUND__WavInfo;


typedef struct {
    int32_t (*fill_buffer)(
        void *sound_,
        int32_t cursor_start,
        float *dst_buffer,
        int32_t frames_count,
        int32_t channels_count,
        int32_t sample_rate,
        float volume);
    ASSET_SOUND__WavInfo *info;
    union {
        uint8_t *data_u8;
        int16_t *data_i16;
    };
    int32_t data_count;
} ASSET__SoundWav;


int32_t
ASSET_SOUND__Wav_fill_buffer(
    void *sound_,
    int32_t cursor_start,
    float *dst_buffer,
    int32_t frames_count,
    int32_t channels_count,
    int32_t sample_rate,
    float volume) {

#if 1
    ASSET__SoundWav *sound = (ASSET__SoundWav *)sound_;
    int64_t src_sample_rate = sound->info->sample_rate;
    int64_t dst_sample_rate = sample_rate;
    int32_t src_channels    = sound->info->num_channels;
    float   src_channels_inv = 1.0f/(float)src_channels;
    int64_t src_total_samples = sound->data_count/src_channels;
    int32_t cursor = cursor_start;
    int bits = sound->info->bit_depth;
    int frame_i = 0;
    int64_t src_sample_i = cursor*src_sample_rate/dst_sample_rate;

	for (;frame_i < frames_count && src_sample_i < src_total_samples;) {

        if (src_channels == channels_count) {
            for (int channel_i = 0; channel_i < channels_count; channel_i+=1) {
                float f32_val = 0.0f;
                if (bits == 8) {
                    uint8_t u8_val = sound->data_u8[src_sample_i*src_channels+channel_i];
                    f32_val = ((float)u8_val)*(1.0f/127.5f)-1.0f;
                }
                else if (bits == 16) {
                    int16_t i16_val = sound->data_i16[src_sample_i*src_channels+channel_i];
                    f32_val = ((float)i16_val)*(1.0f/32767.0f);
                }
		        dst_buffer[frame_i*channels_count+channel_i] += f32_val * volume;
            }
        }
        else {
            float f32_val = 0;
            for (int channel_i = 0; channel_i < src_channels; channel_i+=1) {
                if (bits == 8) {
                    uint8_t u8_val = sound->data_u8[src_sample_i*src_channels+channel_i];
                    f32_val = ((float)u8_val)*(1.0f/127.5f)-1.0f;
                }
                else if (bits == 16) {
                    int16_t i16_val = sound->data_i16[src_sample_i*src_channels+channel_i];
                    f32_val = ((float)i16_val)*(1.0f/32767.0f);
                }
            }
            for (int channel_i = 0; channel_i < channels_count; channel_i+=1) {
		        dst_buffer[frame_i*channels_count+channel_i] += f32_val * src_channels_inv * volume;
            }
        }

        cursor += 1;
        frame_i = cursor-cursor_start;
        src_sample_i = cursor*src_sample_rate/dst_sample_rate;
	}
    return cursor-cursor_start;
    #else
    return frames_count;
    #endif
}



static bool
ASSET_SOUND_Load_wav_from_memory(uint8_t *filedata, int64_t filedata_len, ASSET__SoundWav *wav) {

    if (((~((uintptr_t)filedata))&0x3) != 0x3) return false; // Unaligned memory
    
    if (filedata_len <= sizeof(ASSET_SOUND__WavInfo)) return false; // data too small

    ASSET_SOUND__WavInfo *wav_info = (ASSET_SOUND__WavInfo *)filedata;

    if (wav_info->riff_header[0] != 'R' ||
        wav_info->riff_header[1] != 'I' ||
        wav_info->riff_header[2] != 'F' ||
        wav_info->riff_header[3] != 'F') return false;

    if (wav_info->wav_size != filedata_len-8) return false;

    if (wav_info->wave_header[0] != 'W' ||
        wav_info->wave_header[1] != 'A' ||
        wav_info->wave_header[2] != 'V' ||
        wav_info->wave_header[3] != 'E') return false;

    if (wav_info->fmt_header[0] != 'f' ||
        wav_info->fmt_header[1] != 'm' ||
        wav_info->fmt_header[2] != 't' ||
        wav_info->fmt_header[3] != ' ') return false;

    if (wav_info->fmt_chunk_size != 16) return false;

    if (wav_info->audio_format != 1) return false;

    if (wav_info->bit_depth != 8 && wav_info->bit_depth != 16) return false;
    
    int32_t cursor = sizeof(ASSET_SOUND__WavInfo);
    int32_t data_len = 0;
    uint8_t *data    = NULL;
    while (true) {
        if (cursor + 8 >= filedata_len) return false;
        if (filedata[cursor+0] == 'd' &&
            filedata[cursor+1] == 'a' &&
            filedata[cursor+2] == 't' &&
            filedata[cursor+3] == 'a') {

            cursor += 4;
            ((uint8_t*)&data_len)[0] = filedata[cursor+0];
            ((uint8_t*)&data_len)[1] = filedata[cursor+1];
            ((uint8_t*)&data_len)[2] = filedata[cursor+2];
            ((uint8_t*)&data_len)[3] = filedata[cursor+3];
            cursor += 4;
            if (cursor+data_len != filedata_len) return false;
            data = &filedata[cursor];
            break;
        }
        // Skip other kinds of data
        cursor += 4;
        ((uint8_t*)&data_len)[0] = filedata[cursor+0];
        ((uint8_t*)&data_len)[1] = filedata[cursor+1];
        ((uint8_t*)&data_len)[2] = filedata[cursor+2];
        ((uint8_t*)&data_len)[3] = filedata[cursor+3];
        cursor += 4+data_len;
    }
    if (wav_info->bit_depth == 16) {
        if (((~((uintptr_t)filedata))&0x1) != 0x1) return false; // Unaligned memory, CRY
        if (data_len % 2 != 0) return false;
        data_len /= 2;
        if (data_len % wav_info->num_channels != 0) return false;
    }

    wav->info        = wav_info;
    wav->data_u8     = data;
    wav->data_count  = data_len;
    wav->fill_buffer = ASSET_SOUND__Wav_fill_buffer;

    return true;
}







enum {
    ASSET_SOUND__TYPE_UNKNOWN = 0,
    ASSET_SOUND__TYPE_VORBIS,
    ASSET_SOUND__TYPE_WAV,
};

typedef struct {
    int type;
    union {
        SND_Sound         mixer_sound;
        ASSET__SoundVorbis vorbis;
        ASSET__SoundWav    wav;
    };
} ASSET__Sound;


typedef struct {
    uint64_t hash;
    int      state;
    ASSET__Sound sound;
} ASSET__SoundSlot;


#define ASSET__MAX_SOUND_SLOTS 32

ASSET__SoundSlot ASSET__sound_slots[ASSET__MAX_SOUND_SLOTS] = {0};


#if 0
static const char *
ASSET_SOUND__Get_ext(const char *filename) {
    int last_dot = 0;
    int cursor   = 0;
    for (;;) {
        char c = filename[cursor];
        if (c == 0) break;
        if (c == '.') last_dot = cursor;
        cursor+=1;
    }
    if (last_dot == 0) last_dot = cursor;
    return &filename[last_dot];
}

static bool
ASSET_SOUND__Strings_match(const char *str1, const char *str2) {
    int cursor = 0;
    for (;str1[cursor] && str2[cursor]; cursor+=1) {
        if (str1[cursor] != str2[cursor]) return false;
    }
    if (str1[cursor] != str2[cursor]) return false;
    return true;
}
#endif





static void
ASSET_SOUND_Response_callback(void *user, uint8_t *filedata, int64_t filedata_len, int response_state) {
    
    ASSET__SoundSlot  *slot = (ASSET__SoundSlot *)user;
    ASSET__Sound *sound = &(slot->sound);
    if (response_state == ASSET_RESPONSE_LOADED) {
        if (0) {
        }
        else if (filedata_len > 4 &&
            filedata[0] == 'O' &&
            filedata[1] == 'g' &&
            filedata[2] == 'g' &&
            filedata[3] == 'S') {
            
            if (ASSET_SOUND_Load_vorbis_from_memory(filedata, filedata_len, &(sound->vorbis))) {
                sound->type = ASSET_SOUND__TYPE_VORBIS;
                slot->state = ASSET_LOADED;
                return;
            }
            else {
                slot->state = ASSET_PARSING_ERROR;
            }
        }
        else if (filedata_len > 4 &&
            filedata[0] == 'R' &&
            filedata[1] == 'I' &&
            filedata[2] == 'F' &&
            filedata[3] == 'F') {

            if (ASSET_SOUND_Load_wav_from_memory(filedata, filedata_len, &(sound->wav))) {
                sound->type = ASSET_SOUND__TYPE_WAV;
                slot->state = ASSET_LOADED;
                return;
            }
            else {
                slot->state = ASSET_PARSING_ERROR;
            }

        }
        else {
            slot->state = ASSET_PARSING_ERROR;
        }
    }
    else if (response_state == ASSET_RESPONSE_NOT_FOUND) {
        slot->state = ASSET_NOT_FOUND;
    }
    else if (response_state == ASSET_RESPONSE_OUT_OF_MEMORY) {
        slot->state = ASSET_OUT_OF_MEMORY;
    }

    if (filedata) ASSET_Free(filedata);
}





SND_Sound *
ASSET_SOUND(const char *filename, int *state) {

    uint64_t hash = ASSET__String_hash(filename);
    
    int  slot_found = -1;
    bool cached     = false;
    for (int slot_i = 0; slot_i < ASSET__MAX_SOUND_SLOTS; slot_i+=1) {
        if (slot_found == -1 && ASSET__sound_slots[slot_i].state == ASSET_STATE_NULL) {
            slot_found = slot_i;
        }
        if (ASSET__sound_slots[slot_i].hash == hash) {
            cached = true;
            slot_found = slot_i;
            break;
        }
    }

    if (!cached) {
        if (slot_found == -1) {
            if (state) *state = ASSET_SLOTS_FULL;
            return NULL;
        }
        ASSET__SoundSlot *slot = &ASSET__sound_slots[slot_found];
        slot->hash  = hash;
        slot->state = ASSET_LOADING;
        ASSET_Request_fetch(filename, (void *)slot,  ASSET_SOUND_Response_callback);
    }

    ASSET__SoundSlot *slot = &ASSET__sound_slots[slot_found];

    if (state) *state = slot->state;
    if (slot->state == ASSET_LOADED) {
        
        SND_Sound *snd = &(slot->sound.mixer_sound);
        return snd;
    }
    else return NULL;
}



#endif // ASSET_NO_SOUND







//---------------------------------
//
// ASSET TEXTURE IMPLEMENTATION
//
//--------------------------------

#ifndef ASSET_NO_TEXTURE

#include "graphics.h"

//#define ASSET_TEXTURE_STBI_USE_CRT

#ifndef ASSET_TEXTURE_STBI_USE_CRT
    #define STBI_NO_SIMD // We should enable this, but it includes CRT stuff that colides with this macros
    #define STBI_NO_STDIO
    #define STBI_NO_CRT
    #define STBI_MALLOC  ASSET__malloc
    #define STBI_FREE    ASSET__free
    #define STBI_REALLOC_SIZED ASSET__realloc
    #define STBI_ASSERT  ASSET__ASSERT
    #define pow    Pow
    #define abs    Abs
    #define ldexp  ASSET__ldexp
    #define memcmp ASSET__memcmp
    #define strcmp  ASSET__strcmp
    #define strncmp ASSET__strncmp
    #define memset  ASSET__memset
    #define memcpy  ASSET__memcpy
    #define STBI_NO_HDR // We need a strtod implementation
#endif // ASSER_TEXTURE_STBI_USE_CRT

#define STB_IMAGE_IMPLEMENTATION
#include "third_party/stb_image.h"

#ifndef ASSET_TEXTURE_STBI_USE_CRT
    #undef pow
    #undef abs
    #undef ldexp
    #undef memcmp
    #undef strcmp
    #undef strncmp
    #undef memset
    #undef memcpy
#endif // ASSER_TEXTURE_STBI_USE_CRT


typedef struct {
    uint64_t hash;
    int      state;
    unsigned int texture;
} ASSET__TextureSlot;


#define ASSET__MAX_TEXTURE_SLOTS 32

ASSET__TextureSlot ASSET__texture_slots[ASSET__MAX_TEXTURE_SLOTS] = {0};

ASSET_INTERNAL void
ASSET_TEXTURE_Response_callback(void *user, uint8_t *filedata, int64_t filedata_len, int response_state) {

    ASSET__TextureSlot  *slot = (ASSET__TextureSlot *)user;
    if (response_state == ASSET_RESPONSE_LOADED) {
        int iw = 0;
        int ih = 0;
        int ic = 0;
        uint8_t *image_data = stbi_load_from_memory(filedata, filedata_len, &iw, &ih, &ic, 4);
        if (image_data) {
            GLuint texture;
    	    glGenTextures(1, &texture);
    	    glBindTexture(GL_TEXTURE_2D, texture);
		    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    	    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); // On webgl we need a pow
		    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); // of 2 texture or set this
    	    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, iw, ih, 0, GL_RGBA, GL_UNSIGNED_BYTE, image_data);
    	    //glGenerateMipmap(GL_TEXTURE_2D);
            slot->state   = ASSET_LOADED;
            slot->texture = texture;
        }
        else {
            slot->state = ASSET_PARSING_ERROR;
        }
        ASSET__malloc_clean();
    }
    else if (response_state == ASSET_RESPONSE_NOT_FOUND) {
        slot->state = ASSET_NOT_FOUND;
    }
    else if (response_state == ASSET_RESPONSE_OUT_OF_MEMORY) {
        slot->state = ASSET_OUT_OF_MEMORY;
    }

    if (filedata) ASSET_Free(filedata);
}

// TODO(Tano) this will need more parameters, if we want linear interpolation or not, generate
// mipmaps...
// We can also use this to build a big atlas and just return  the UVs where the loaded image
// resides.
ASSET_PUBLIC unsigned int
ASSET_TEXTURE(const char *filename, int *state) {

    uint64_t hash = ASSET__String_hash(filename);
    
    int  slot_found = -1;
    bool cached     = false;
    for (int slot_i = 0; slot_i < ASSET__MAX_TEXTURE_SLOTS; slot_i+=1) {
        if (slot_found == -1 && ASSET__texture_slots[slot_i].state == ASSET_STATE_NULL) {
            slot_found = slot_i;
        }
        if (ASSET__texture_slots[slot_i].hash == hash) {
            cached = true;
            slot_found = slot_i;
            break;
        }
    }

    if (!cached) {
        if (slot_found == -1) {
            if (state) *state = ASSET_SLOTS_FULL;
            return GFX_Default_texture();
        }
        ASSET__TextureSlot *slot = &ASSET__texture_slots[slot_found];
        slot->hash  = hash;
        slot->state = ASSET_LOADING;
        ASSET_Request_fetch(filename, (void *)slot,  ASSET_TEXTURE_Response_callback);
    }

    ASSET__TextureSlot *slot = &ASSET__texture_slots[slot_found];

    if (state) *state = slot->state;
    if (slot->state == ASSET_LOADED) return slot->texture;
    else return GFX_Default_texture();
}

#endif // ASSET_NO_TEXTURE










//---------------------------------
//
// ASSET FONT IMPLEMENTATION
//
//--------------------------------

#ifndef ASSET_NO_FONT


#define STBTT_ifloor(x)   ((int) Floor(x))
#define STBTT_iceil(x)    ((int) Ceil(x))
#define STBTT_sqrt(x)      Sqrt(x)
#define STBTT_pow(x,y)     Pow(x,y)
#define STBTT_fmod(x,y)    Mod(x,y)
#define STBTT_cos(x)       Cos(x)
#define STBTT_acos(x)      Acos(x)
#define STBTT_fabs(x)      Abs(x)

#define STBTT_malloc(x,u)  ((void)(u),ASSET__malloc(x))
#define STBTT_free(x,u)    ((void)(u),ASSET__free(x))

#define STBTT_assert(x)    ASSET__ASSERT(x)

#define STBTT_strlen(x)    ASSET__strlen(x)

#define STBTT_memcpy(a, b, c) ASSET__memcpy(a, b, c)
#define STBTT_memset(a, b, c) ASSET__memset(a, b, c)

#define STB_TRUETYPE_IMPLEMENTATION
#include "third_party/stb_truetype.h"

typedef struct {
    uint64_t hash;
    int      state;
    int      height;
    GFX_Font font;
    int64_t ranges[2];
    GFX_GlyphInfo glyph_infos[96];
} ASSET__FontSlot;


#define ASSET__MAX_FONT_SLOTS  32
#define ASSET__FONT_ATLAS_SIZE 1024

ASSET__FontSlot ASSET__font_slots[ASSET__MAX_FONT_SLOTS] = {0};


ASSET_INTERNAL void
ASSET_FONT_Response_callback(void *user, uint8_t *filedata, int64_t filedata_len, int response_state) {

    static uint8_t atlas8[ASSET__FONT_ATLAS_SIZE*ASSET__FONT_ATLAS_SIZE];
    static uint8_t atlas32[ASSET__FONT_ATLAS_SIZE*ASSET__FONT_ATLAS_SIZE*4];

    ASSET__FontSlot  *slot = (ASSET__FontSlot *)user;
    if (response_state == ASSET_RESPONSE_LOADED) {
    
        // We lie saying that the height is ASSET__FONT_ATLAS_SIZE-4 because we reserve the last
        // 4 rows to store a tiny white square
	    int result = stbtt_BakeFontBitmap(
            filedata, 0, slot->height, atlas8, ASSET__FONT_ATLAS_SIZE, ASSET__FONT_ATLAS_SIZE-4,
            32, 96, (stbtt_bakedchar*)(slot->glyph_infos));

        ASSET__malloc_clean();

        if (result > 0) {
	        for (int i = 0; i < ASSET__FONT_ATLAS_SIZE; i+=1) {
	        	for (int j = 0; j < ASSET__FONT_ATLAS_SIZE; j+=1) {
	        		atlas32[i*ASSET__FONT_ATLAS_SIZE*4 + j*4 + 0] = 0xFF; // R
	        		atlas32[i*ASSET__FONT_ATLAS_SIZE*4 + j*4 + 1] = 0xFF; // G
	        		atlas32[i*ASSET__FONT_ATLAS_SIZE*4 + j*4 + 2] = 0xFF; // B
	        		uint8_t A = atlas8[i*ASSET__FONT_ATLAS_SIZE + j];
	        		atlas32[i*ASSET__FONT_ATLAS_SIZE*4 + j*4 + 3] = A; // A
	        	}
	        }

            for (int i = ASSET__FONT_ATLAS_SIZE-4; i < ASSET__FONT_ATLAS_SIZE; i+=1) {
                for (int j = 0; j < 4; j+=1) {
	        		atlas32[i*ASSET__FONT_ATLAS_SIZE*4 + j*4 + 0] = 0xFF; // R
	        		atlas32[i*ASSET__FONT_ATLAS_SIZE*4 + j*4 + 1] = 0xFF; // G
	        		atlas32[i*ASSET__FONT_ATLAS_SIZE*4 + j*4 + 2] = 0xFF; // B
	        		atlas32[i*ASSET__FONT_ATLAS_SIZE*4 + j*4 + 3] = 0xFF; // A
                }
            }

	        // Make the texture
            GLuint texture;
	        glGenTextures(1, &texture);
            glBindTexture(GL_TEXTURE_2D, texture);
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, ASSET__FONT_ATLAS_SIZE, ASSET__FONT_ATLAS_SIZE, 0,
                GL_RGBA, GL_UNSIGNED_BYTE, atlas32);
	        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            
            slot->ranges[0] = 32;
            slot->ranges[1] = 126;
            slot->font.ranges = slot->ranges;
            slot->font.total_ranges = 1;
            slot->font.count     = 96;
            slot->font.fallback  = 0;
            slot->font.texture_w = ASSET__FONT_ATLAS_SIZE;
            slot->font.texture_h = ASSET__FONT_ATLAS_SIZE;
            slot->font.texture   = texture;
            slot->font.height = slot->height;
            slot->font.has_white = true;
            slot->font.white_x = 1.0f/(float)ASSET__FONT_ATLAS_SIZE;
            slot->font.white_y = ((float)ASSET__FONT_ATLAS_SIZE-3)/(float)ASSET__FONT_ATLAS_SIZE;
            slot->font.white_w = 2.0f/(float)ASSET__FONT_ATLAS_SIZE;
            slot->font.white_h = 2.0f/(float)ASSET__FONT_ATLAS_SIZE;
            slot->font.glyph_infos = slot->glyph_infos;
            slot->state   = ASSET_LOADED;
            // NOTE(Tano) the yoff on stbtt ajust the position to the baseline, but the default font
            // on graphics.h adjusts to the maximum top, this is why we iterate hera and add the
            // height, but this stills not giving the same result...
            // TODO(Tano) fix this or change the graphics.h default font to adjust to baseline
            for (int i = 0; i < 96; i+=1) slot->glyph_infos[i].yoff += slot->height;
        }
        else {
            slot->state = ASSET_PARSING_ERROR;
        }
    }
    else if (response_state == ASSET_RESPONSE_NOT_FOUND) {
        slot->state = ASSET_NOT_FOUND;
    }
    else if (response_state == ASSET_RESPONSE_OUT_OF_MEMORY) {
        slot->state = ASSET_OUT_OF_MEMORY;
    }

    if (filedata) ASSET_Free(filedata);
}

ASSET_PUBLIC GFX_Font *
ASSET_FONT(const char *filename, int height, int *state) {

    uint64_t hash = ASSET__String_hash(filename);

    int  slot_found = -1;
    bool cached     = false;
    for (int slot_i = 0; slot_i < ASSET__MAX_FONT_SLOTS; slot_i+=1) {
        if (slot_found == -1 && ASSET__font_slots[slot_i].state == ASSET_STATE_NULL) {
            slot_found = slot_i;
        }
        if (ASSET__font_slots[slot_i].hash == hash) {
            cached = true;
            slot_found = slot_i;
            break;
        }
    }

    if (!cached) {
        if (slot_found == -1) {
            if (state) *state = ASSET_SLOTS_FULL;
            return GFX_Default_font();
        }
        ASSET__FontSlot *slot = &ASSET__font_slots[slot_found];
        slot->hash  = hash;
        slot->state = ASSET_LOADING;
        slot->height = height;
        ASSET_Request_fetch(filename, (void *)slot,  ASSET_FONT_Response_callback);
    }

    ASSET__FontSlot *slot = &ASSET__font_slots[slot_found];

    if (state) *state = slot->state;
    if (slot->state == ASSET_LOADED) return &(slot->font);
    else return GFX_Default_font();

}

#endif // ASSET_NO_FONT





#endif // ASSET_IMPLEMENTATION_INCLUDED
#endif // ASSET_IMPLEMENTATION


