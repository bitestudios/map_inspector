#ifndef SOUND_INCLUDED
#define SOUND_INCLUDED

#ifndef SND_PUBLIC
	#define SND_PUBLIC 
#endif

#ifndef SND_INTERNAL
	#define SND_INTERNAL static
#endif


typedef void (*SND_SoundPlayerCallback) (
    void *user_data,
    float* buffer,
    int n_frames,
    int n_channels,
    int sample_rate);


//
// Description with the desired when the player is launched
//
typedef struct {
	int sample_rate;        /* requested sample rate */
	int num_channels;       /* number of channels, default: 1 (mono) */
	int buffer_frames;      /* number of frames in streaming buffer */
    void *user_data;        /* optional user data argument for stream_userdata_cb */
	SND_SoundPlayerCallback stream_cb; /* Streaming callback */
} SND_SoundParams;


// Launchs a thread (On linux and windows) or a async rutine (wasm)
// Which will play the audio.
//
// When it needs a new chunk of samples will call the callback
SND_PUBLIC int
SND_Init(const SND_SoundParams *params);

SND_PUBLIC void
SND_Deinit(void);

// Terminates the player routine.
SND_PUBLIC void
SND_Terminate_sound_player(void);

SND_PUBLIC int
SND_Get_sample_rate(void);

SND_PUBLIC int
SND_Get_num_channels(void);


// Builtin mixer API

struct SND_Sound_t{
    int32_t (*fill_buffer)(
        void *sound_,
        int32_t cursor_start,
        float *dst_buffer,
        int32_t frames_count,
        int32_t channels_count,
        int32_t sample_rate,
        float volume);
};

typedef struct SND_Sound_t SND_Sound;

enum {
	SND_FLAG_NONE    = 0x0,
	SND_FLAG_INVALID = 0x1,
	SND_FLAG_LOOP    = 0x2,
	SND_FLAG_PAUSED  = 0x4,
};

SND_PUBLIC void
SND_Play_sound(SND_Sound *sound, uint8_t flags, float volume, uint64_t handle);


SND_PUBLIC void
SND_Clear_tracks(void);


SND_PUBLIC void
SND_Pause_all_tracks(void);


SND_PUBLIC void
SND_Resume_all_tracks(void);


SND_PUBLIC void
SND_Toggle_all_tracks(void);


SND_PUBLIC bool
SND_Pause_track(uint64_t handle);


SND_PUBLIC bool
SND_Resume_track(uint64_t handle);


SND_PUBLIC bool
SND_Remove_track(uint64_t handle);


SND_PUBLIC bool
SND_Set_track_volume(uint64_t handle, float volume);








#endif // SOUND_INCLUDED




#ifdef SOUND_IMPLEMENTATION
#ifndef SOUND_IMPLEMENTATION_INCLUDED
#define SOUND_IMPLEMENTATION_INCLUDED



#ifndef SND_MAX_BUFFER_SIZE
    #define SND_MAX_BUFFER_SIZE 8192
#endif


// Sound player information
struct {
	int sample_rate;
	int num_channels;
	int buffer_frames;
	void *user_data;
	SND_SoundPlayerCallback stream_cb;
    float buffer[SND_MAX_BUFFER_SIZE];
} SND__sound_player_info;


SND_PUBLIC int
SND_Get_sample_rate(void) {
    return SND__sound_player_info.sample_rate;
}

SND_PUBLIC int
SND_Get_num_channels(void) {
    return SND__sound_player_info.num_channels;
}

SND_INTERNAL int
SND__Init_platform(const SND_SoundParams *params);

SND_INTERNAL void
SND__Play_frames(void *user, float* buffer, int n_frames, int n_channels, int sample_rate);

SND_INTERNAL void
SND__Clear_tracks_no_lock(void);

SND_PUBLIC int
SND_Init(const SND_SoundParams *params_) {

    SND__Clear_tracks_no_lock();

    SND_SoundParams params = {0};

    if (params_ && params_->sample_rate) params.sample_rate = params_->sample_rate;
    else params.sample_rate = 44100;

    if (params_ && params_->num_channels) params.num_channels = params_->num_channels;
    else params.num_channels = 2;

    if (params_ && params_->buffer_frames) params.buffer_frames = params_->buffer_frames;
    else params.buffer_frames = 2048;

    if (params_ && params_->user_data) params.user_data = params_->user_data;

    if (params_ && params_->stream_cb) params.stream_cb = params_->stream_cb;
    else params.stream_cb = SND__Play_frames;

    int result = SND__Init_platform(&params);
    return result;
}

SND_INTERNAL void
SND__Deinit_platform(void);

SND_PUBLIC void
SND_Deinit(void) {
    SND__Deinit_platform();
}


typedef struct {
	SND_Sound *sound;
    uint64_t handle;
    uint64_t flags;
	uint32_t current_sample;
	float volume;
} SND__Track;

SND_INTERNAL void
SND__Lock(void);

SND_INTERNAL void
SND__Unlock(void);


#ifndef SND_MAX_TRACKS
    #define SND_MAX_TRACKS 16
#endif


static struct {
    SND__Track tracks[SND_MAX_TRACKS];
} SND__mixer_data = {0};



SND_INTERNAL void
SND__Play_frames(void *user, float* buffer, int n_frames, int n_channels, int sample_rate) {

    (void)user;
    
	// Fills the buffer with 0.0
	for (int i = 0; i < n_frames*n_channels; ++i) {
		buffer[i] = 0.0f;
	}

	SND__Lock();
	{
		for (int track_i = 0; track_i < SND_MAX_TRACKS; track_i+=1) {
			// If is invalid or paused we do nothing
			if (SND__mixer_data.tracks[track_i].flags & (SND_FLAG_INVALID | SND_FLAG_PAUSED)) {
				continue;
			}
            SND__Track *track = &SND__mixer_data.tracks[track_i];
            SND_Sound *sound = track->sound;

            bool wrap = ((track->flags & SND_FLAG_LOOP) != 0);

            if (wrap) {
                int32_t frames_written = 0;
                int32_t buffer_cursor  = 0;
                while(true) {
                    frames_written = sound->fill_buffer(
                        sound,
                        track->current_sample,
                        &buffer[buffer_cursor],
                        n_frames-buffer_cursor,
                        n_channels,
                        sample_rate,
                        track->volume);

                    if (frames_written+buffer_cursor < n_frames) {
                        track->current_sample = 0;
                    }
                    else {
                        track->current_sample += frames_written;
                        break;
                    }
                }
            }
            else {
                int32_t frames_written = sound->fill_buffer(
                    sound,
                    track->current_sample,
                    buffer,
                    n_frames,
                    n_channels,
                    sample_rate,
                    track->volume);

                if (frames_written < n_frames) {
		            track->flags = SND_FLAG_INVALID;
		            track->handle = 0;
                }
                else {
                    track->current_sample += frames_written;
                }

            }
		}
	}
	SND__Unlock();

	// Clamps the result
	for (uint32_t i = 0; i < n_frames*n_channels; ++i) {
		float val = buffer[i];
		if (val < -1.0f)     { buffer[i] = -1.0f; }
		else if (val > 1.0f) { buffer[i] = 1.0f; }
	}
}


SND_PUBLIC void
SND_Play_sound(SND_Sound *sound, uint8_t flags, float volume, uint64_t handle) {
    if (!sound) return;

    SND__Lock();
	{
		for (int i = 0; i < SND_MAX_TRACKS; ++i) {
			if (SND__mixer_data.tracks[i].flags & SND_FLAG_INVALID) {
				SND__mixer_data.tracks[i].flags          = flags;
				SND__mixer_data.tracks[i].sound          = sound;
				SND__mixer_data.tracks[i].current_sample = 0;
				SND__mixer_data.tracks[i].volume         = volume;
				SND__mixer_data.tracks[i].handle         = handle;
                SND__Unlock();
				return;
			}
		}
	}
    SND__Unlock();
}


SND_INTERNAL void
SND__Clear_tracks_no_lock(void) {
	for (int i = 0; i < SND_MAX_TRACKS; ++i) {
		SND__mixer_data.tracks[i].flags  = SND_FLAG_INVALID;
		SND__mixer_data.tracks[i].handle = 0;
	}
}

SND_PUBLIC void
SND_Clear_tracks(void) {
    SND__Lock();
    SND__Clear_tracks_no_lock();
    SND__Unlock();
}


SND_PUBLIC void
SND_Pause_all_tracks(void) {
	SND__Lock();
	{
		for (int i = 0; i < SND_MAX_TRACKS; ++i) {
			if (SND__mixer_data.tracks[i].flags & SND_FLAG_INVALID) {
				continue;
			}
			SND__mixer_data.tracks[i].flags |= SND_FLAG_PAUSED;
		}
	}
	SND__Unlock();
}


SND_PUBLIC void
SND_Resume_all_tracks(void) {
	SND__Lock();
	{
		for (int i = 0; i < SND_MAX_TRACKS; ++i) {
			if (SND__mixer_data.tracks[i].flags & SND_FLAG_INVALID) {
				continue;
			}
			SND__mixer_data.tracks[i].flags &= (~SND_FLAG_PAUSED);
		}
	}
    SND__Unlock();
}


SND_PUBLIC void
SND_Toggle_all_tracks(void) {
    SND__Lock();
	{
		for (int i = 0; i < SND_MAX_TRACKS; ++i) {
			if (SND__mixer_data.tracks[i].flags & SND_FLAG_INVALID) {
				continue;
			}
			if (SND__mixer_data.tracks[i].flags & SND_FLAG_PAUSED) {
				SND__mixer_data.tracks[i].flags &= (~SND_FLAG_PAUSED);
			}
			else {
				SND__mixer_data.tracks[i].flags |= SND_FLAG_PAUSED;
			}
		}
	}
    SND__Unlock();
}


SND_PUBLIC bool
SND_Pause_track(uint64_t handle) {
    if (!handle) return false;
    SND__Lock();
	{
		for (int i = 0; i < SND_MAX_TRACKS; ++i) {
			if (SND__mixer_data.tracks[i].handle == handle) {
				SND__mixer_data.tracks[i].flags |= SND_FLAG_PAUSED;
                SND__Unlock();
				return true;
			}
		}
	}
    SND__Unlock();
	return false;
}


SND_PUBLIC bool
SND_Resume_track(uint64_t handle) {
    if (!handle) return false;
    SND__Lock();
	{
		for (int i = 0; i < SND_MAX_TRACKS; ++i) {
			if (SND__mixer_data.tracks[i].handle == handle) {
				SND__mixer_data.tracks[i].flags &= (~SND_FLAG_PAUSED);
                SND__Unlock();
				return true;
			}
		}
	}
    SND__Unlock();
	return false;
}


SND_PUBLIC bool
SND_Remove_track(uint64_t handle) {
    if (!handle) return false;
    SND__Lock();
	{
		for (int i = 0; i < SND_MAX_TRACKS; ++i) {
			if (SND__mixer_data.tracks[i].handle == handle) {
				SND__mixer_data.tracks[i].flags  = SND_FLAG_INVALID;
				SND__mixer_data.tracks[i].handle = 0;
                SND__Unlock();
				return true;
			}
		}
	}
    SND__Unlock();
	return false;
}


SND_PUBLIC bool
SND_Set_track_volume(uint64_t handle, float volume) {
    if (!handle) return false;
    SND__Lock();
	{
		for (int i = 0; i < SND_MAX_TRACKS; ++i) {
			if (SND__mixer_data.tracks[i].handle == handle) {
				SND__mixer_data.tracks[i].volume = volume;
                SND__Unlock();
				return true;
			}
		}
	}
    SND__Unlock();
	return false;
}


#if defined(WIN32)

#include <windows.h>
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#ifndef NOMINMAX
#define NOMINMAX
#endif
#include <synchapi.h>
#ifndef CINTERFACE
#define CINTERFACE
#endif
#ifndef COBJMACROS
#define COBJMACROS
#endif
#ifndef CONST_VTABLE
#define CONST_VTABLE
#endif
#include <mmdeviceapi.h>
#include <audioclient.h>
static const IID splayer_IID_IAudioClient = { 0x1cb9ad4c, 0xdbfa, 0x4c32, { 0xb1, 0x78, 0xc2, 0xf5, 0x68, 0xa7, 0x03, 0xb2 } };
static const IID splayer_IID_IMMDeviceEnumerator = { 0xa95664d2, 0x9614, 0x4f35, { 0xa7, 0x46, 0xde, 0x8d, 0xb6, 0x36, 0x17, 0xe6 } };
static const CLSID splayer_CLSID_IMMDeviceEnumerator = { 0xbcde0395, 0xe52f, 0x467c, { 0x8e, 0x3d, 0xc4, 0x57, 0x92, 0x91, 0x69, 0x2e } };
static const IID splayer_IID_IAudioRenderClient = { 0xf294acfc, 0x3146, 0x4483,{ 0xa7, 0xbf, 0xad, 0xdc, 0xa7, 0xc2, 0x60, 0xe2 } };
static const IID splayer_IID_Devinterface_Audio_Render = { 0xe6327cad, 0xdcec, 0x4949, {0xae, 0x8a, 0x99, 0x1e, 0x97, 0x6a, 0x79, 0xd2 } };
static const IID splayer_IID_IActivateAudioInterface_Completion_Handler = { 0x94ea2b94, 0xe9cc, 0x49e0, {0xc0, 0xff, 0xee, 0x64, 0xca, 0x8f, 0x5b, 0x90} };
#define AUDIO_WIN32COM_ID(x) (&x)
/* fix for Visual Studio 2015 SDKs */



static struct {
	IMMDeviceEnumerator* device_enumerator;
	IMMDevice* device;
	IAudioClient* audio_client;
	IAudioRenderClient* render_client;
	struct {
	    HANDLE thread_handle;
	    HANDLE buffer_end_event;
        HANDLE mutex;
	    volatile bool stop;
	    UINT32 dst_buffer_frames;
	    int src_buffer_frames;
	    int src_buffer_byte_size;
	    int src_buffer_pos;
	} thread;
} SND__sound_player_backend = {0};





SND_INTERNAL void
SND__Sound_player_submit_buffer(int num_frames) {
    BYTE* wasapi_buffer = 0;
    if (FAILED(IAudioRenderClient_GetBuffer(SND__sound_player_backend.render_client, num_frames, &wasapi_buffer))) {
        return;
    }

    /* convert float samples to int16_t, refill float buffer if needed */
    const int num_samples = num_frames * SND__sound_player_info.num_channels;
    int16_t* dst = (int16_t*) wasapi_buffer;
    int buffer_pos = SND__sound_player_backend.thread.src_buffer_pos;
    const int buffer_float_size = SND__sound_player_backend.thread.src_buffer_byte_size / (int)sizeof(float);
    float* src = SND__sound_player_info.buffer;
    for (int i = 0; i < num_samples; i++) {
        if (0 == buffer_pos) {
			SND__sound_player_info.stream_cb(
					SND__sound_player_info.user_data,
					SND__sound_player_info.buffer,
					SND__sound_player_info.buffer_frames,
					SND__sound_player_info.num_channels,
					SND__sound_player_info.sample_rate
			);
        }
		int value = (int)(src[buffer_pos] * 32767.0f);
		value = value < -32768 ? -32768 : (value > 32767 ? 32767 : value); // Clamp the value to prevent overflow
        dst[i] = (int16_t) (value);
        buffer_pos += 1;
        if (buffer_pos == buffer_float_size) {
            buffer_pos = 0;
        }
    }
    SND__sound_player_backend.thread.src_buffer_pos = buffer_pos;

    IAudioRenderClient_ReleaseBuffer(SND__sound_player_backend.render_client, num_frames, 0);
}

SND_INTERNAL DWORD WINAPI
SND__Sound_player_thread(LPVOID param) {
    (void)param;
    SND__Sound_player_submit_buffer(SND__sound_player_backend.thread.src_buffer_frames);
    IAudioClient_Start(SND__sound_player_backend.audio_client);
    while (!SND__sound_player_backend.thread.stop) {
        WaitForSingleObject(SND__sound_player_backend.thread.buffer_end_event, INFINITE);
        UINT32 padding = 0;
        if (FAILED(IAudioClient_GetCurrentPadding(SND__sound_player_backend.audio_client, &padding))) {
            continue;
        }
        if (SND__sound_player_backend.thread.dst_buffer_frames < padding) {
			fprintf(stderr, "Error, on sound player thread, buffer frames less than padding");
			return 1;
		}
        int num_frames = (int)SND__sound_player_backend.thread.dst_buffer_frames - (int)padding;
        if (num_frames > 0) {
            SND__Sound_player_submit_buffer(num_frames);
        }
    }
    return 0;
}


SND_INTERNAL int
SND__Init_platform(const SND_SoundParams *params) {
	REFERENCE_TIME dur;
	/* CoInitializeEx could have been called elsewhere already, in which
	    case the function returns with S_FALSE (thus it does not make much
	    sense to check the result)
	*/
	CoInitializeEx(0, COINIT_MULTITHREADED);
	SND__sound_player_backend.thread.buffer_end_event = CreateEvent(0, FALSE, FALSE, 0);
	if (0 == SND__sound_player_backend.thread.buffer_end_event) {
	    fprintf(stderr, "failed to create buffer_end_event.\n");
		SND_Deinit();
		return -1;
	}
	if (FAILED(CoCreateInstance(
					&splayer_CLSID_IMMDeviceEnumerator,
					0,
					CLSCTX_ALL,
					&splayer_IID_IMMDeviceEnumerator,
					(void**)&SND__sound_player_backend.device_enumerator))
	) {
	    fprintf(stderr, "failed to create device enumerator.\n");
		SND_Deinit();
		return -1;
	}

	if (FAILED(IMMDeviceEnumerator_GetDefaultAudioEndpoint(SND__sound_player_backend.device_enumerator,
			eRender, eConsole,
			&SND__sound_player_backend.device))) {
	    fprintf(stderr, "GetDefaultAudioEndPoint failed.\n");
		SND_Deinit();
		return -1;
	}

	if (FAILED(IMMDevice_Activate(SND__sound_player_backend.device, &splayer_IID_IAudioClient, CLSCTX_ALL, 0,
	    (void**)&SND__sound_player_backend.audio_client)))
	{
	    fprintf(stderr, "device activate failed\n");
		SND_Deinit();
		return -1;
	}

	WAVEFORMATEX fmt;
	memset(&fmt, 0, sizeof(fmt));
	fmt.nChannels = (WORD)params->num_channels;
	fmt.nSamplesPerSec = (DWORD)params->sample_rate;
	fmt.wFormatTag = WAVE_FORMAT_PCM;
	fmt.wBitsPerSample = 16;
	fmt.nBlockAlign = (fmt.nChannels * fmt.wBitsPerSample) / 8;
	fmt.nAvgBytesPerSec = fmt.nSamplesPerSec * fmt.nBlockAlign;
	dur = (REFERENCE_TIME)
	    (((double)params->buffer_frames) / (((double)params->sample_rate) * (1.0/10000000.0)));
	if (FAILED(IAudioClient_Initialize(SND__sound_player_backend.audio_client,
	    AUDCLNT_SHAREMODE_SHARED,
	    AUDCLNT_STREAMFLAGS_EVENTCALLBACK|AUDCLNT_STREAMFLAGS_AUTOCONVERTPCM|AUDCLNT_STREAMFLAGS_SRC_DEFAULT_QUALITY,
	    dur, 0, &fmt, 0)))
	{
	    fprintf(stderr, "audio client initialize failed.\n");
		SND_Deinit();
		return -1;
	}
	if (FAILED(IAudioClient_GetBufferSize(SND__sound_player_backend.audio_client, &SND__sound_player_backend.thread.dst_buffer_frames))) {
	    fprintf(stderr, "audio client get buffer size failed.\n");
		SND_Deinit();
		return -1;
	}
	if (FAILED(IAudioClient_GetService(SND__sound_player_backend.audio_client, &splayer_IID_IAudioRenderClient,
			(void**)&SND__sound_player_backend.render_client))) {
	    fprintf(stderr, "audio client GetService failed.\n");
		SND_Deinit();
		return -1;
	}
	if (FAILED(IAudioClient_SetEventHandle(SND__sound_player_backend.audio_client,
			SND__sound_player_backend.thread.buffer_end_event))) {
	    fprintf(stderr, "audio client SetEventHandle failed.\n");
		SND_Deinit();
		return -1;
	}
	size_t bytes_per_frame = params->num_channels * (int)sizeof(float);
	SND__sound_player_backend.thread.src_buffer_frames = params->buffer_frames;
	SND__sound_player_backend.thread.src_buffer_byte_size = SND__sound_player_backend.thread.src_buffer_frames * bytes_per_frame;
	
	/* allocate an intermediate buffer for sample format conversion */
    if (params->num_channels*params->buffer_frames > SND_MAX_BUFFER_SIZE) {
	    fprintf(stderr, "Buffer to small!\n");
		SND_Deinit();
		return -1;
    }
	for (int i = 0; i < params->num_channels*params->buffer_frames; i+=1) SND__sound_player_info.buffer[i] = 0.0f;


    // Create the mutex
    SND__sound_player_backend.thread.mutex = CreateMutex( 
        NULL,              // default security attributes
        FALSE,             // initially not owned
        NULL);             // unnamed mutex    return pthread_mutex_init(m, NULL);
    if (SND__sound_player_backend.thread.mutex == NULL) {
	    fprintf(stderr, "audio wasapi: Failed to create mutex\n");
		SND_Deinit();
        return -1;
    }

	/* create streaming thread */
	SND__sound_player_backend.thread.thread_handle = CreateThread(NULL, 0, SND__Sound_player_thread, 0, 0, 0);
	if (0 == SND__sound_player_backend.thread.thread_handle) {
	    fprintf(stderr, "audio wasapi: CreateThread failed\n");
		SND_Deinit();
		return -1;
	}

	SND__sound_player_info.sample_rate   = params->sample_rate;
	SND__sound_player_info.buffer_frames = params->buffer_frames;
	SND__sound_player_info.user_data     = params->user_data;
	SND__sound_player_info.stream_cb     = params->stream_cb;
	SND__sound_player_info.num_channels  = params->num_channels;

	return 0;
}


SND_INTERNAL void
SND__Lock(void) {
    WaitForSingleObject( 
        SND__sound_player_backend.thread.mutex,    // handle to mutex
        INFINITE);  // no time-out intervalturn pthread_mutex_lock(m);
}

SND_INTERNAL void
SND__Unlock(void) {
    ReleaseMutex(SND__sound_player_backend.thread.mutex);
}


SND_PUBLIC void
SND__Deinit_platform(void) {
	if (SND__sound_player_backend.thread.thread_handle) {
	    SND__sound_player_backend.thread.stop = true;
	    SetEvent(SND__sound_player_backend.thread.buffer_end_event);
	    WaitForSingleObject(SND__sound_player_backend.thread.thread_handle, INFINITE);
	    CloseHandle(SND__sound_player_backend.thread.thread_handle);
	    SND__sound_player_backend.thread.thread_handle = 0;
	}
    if (SND__sound_player_backend.thread.mutex) {
        CloseHandle(SND__sound_player_backend.thread.mutex);
        SND__sound_player_backend.thread.mutex = 0;
    }
	if (SND__sound_player_backend.audio_client) {
	    IAudioClient_Stop(SND__sound_player_backend.audio_client);
	}
    if (SND__sound_player_backend.render_client) {
        IAudioRenderClient_Release(SND__sound_player_backend.render_client);
        SND__sound_player_backend.render_client = 0;
    }
    if (SND__sound_player_backend.audio_client) {
        IAudioClient_Release(SND__sound_player_backend.audio_client);
        SND__sound_player_backend.audio_client = 0;
    }
	if (SND__sound_player_backend.device) {
	    IMMDevice_Release(SND__sound_player_backend.device);
	    SND__sound_player_backend.device = 0;
	}
	if (SND__sound_player_backend.device_enumerator) {
	    IMMDeviceEnumerator_Release(SND__sound_player_backend.device_enumerator);
	    SND__sound_player_backend.device_enumerator = 0;
	}
	if (0 != SND__sound_player_backend.thread.buffer_end_event) {
	    CloseHandle(SND__sound_player_backend.thread.buffer_end_event);
	    SND__sound_player_backend.thread.buffer_end_event = 0;
	}
	CoUninitialize();
}

#elif defined(__wasm__)

#define SND__WA_JS(ret, name, args, ...) extern __attribute__((import_module("JS"), import_name(#name "|" #args "|" #__VA_ARGS__))) ret name args;

APP__WA_JS(void, SND__JS_Start_audio, (uint32_t sample_rate, uint32_t n_channels, uint32_t n_frames, float *buffer_ptr), {
    checkHeap();

    function findAlias(el, a, b, c) { return el[a+c] || el['moz'+b+c] || el['webkit'+b+c] || el['ms'+b+c]; }
    try { Module.audioCtx = new (findAlias(window,"","","AudioContext"))({
        sampleRate: sample_rate,
        latencyHint: "interactive",
    }); } catch (e) { }
    if (!Module.audioCtx) { console,log('Warning: WebAudio not supported'); return; }

    Module.audio_node = Module.audioCtx.createScriptProcessor(n_frames, 0, n_channels);
    Module.audio_node.onaudioprocess = function (event) {
        checkHeap();
        var num_frames = event.outputBuffer.length;
        var num_channels = event.outputBuffer.numberOfChannels;
        Module.ASM.WAFN_audio_cb(num_frames, num_channels, buffer_ptr);
        for (var chn = 0; chn < num_channels; chn++) {
            var chan = event.outputBuffer.getChannelData(chn);
            for (var i = 0; i < num_frames; i++) {
                chan[i] = HEAPF32[(buffer_ptr>>2) + ((num_channels*i)+chn)];
            }
        }

    };
    Module.audio_node.connect(Module.audioCtx.destination);

    // in some browsers, WebAudio needs to be activated on a user action
    var resume_webaudio = function() {
        if (Module.audioCtx) {
            if (Module.audioCtx.state === 'suspended') {
                Module.audioCtx.resume();
            }
        }
    };
    document.addEventListener('click', resume_webaudio, {once:true});
    document.addEventListener('touchstart', resume_webaudio, {once:true});
    document.addEventListener('keydown', resume_webaudio, {once:true});
});

APP__WA_JS(void, SND__JS_Terminate_audio, (void), {
    if (Module.audio_node) {
        Module.audio_node.disconnect();
    }
});


__attribute__((export_name("WAFN_audio_cb"))) void
WAFN_audio_cb(uint32_t n_frames, uint32_t n_channels, float *buffer) {
	SND__sound_player_info.stream_cb(
			SND__sound_player_info.user_data,
			buffer,
			n_frames,
			n_channels,
			SND__sound_player_info.sample_rate
	);
}


SND_INTERNAL void
SND__Lock(void) {
}

SND_INTERNAL void
SND__Unlock(void) {
}


SND_INTERNAL int
SND__Init_platform(const SND_SoundParams *params) {
    if (params->num_channels*params->buffer_frames > SND_MAX_BUFFER_SIZE) {
        SND_Deinit();
		return -1;
    }
	SND__JS_Start_audio(
			params->sample_rate,
			params->num_channels,
			params->buffer_frames,
			SND__sound_player_info.buffer
	);

	SND__sound_player_info.sample_rate   = params->sample_rate;
	SND__sound_player_info.buffer_frames = params->buffer_frames;
	SND__sound_player_info.user_data     = params->user_data;
	SND__sound_player_info.stream_cb     = params->stream_cb;
	SND__sound_player_info.num_channels  = params->num_channels;
	return 0;
}


SND_INTERNAL void
SND__Deinit_platform(void) {
	SND__JS_Terminate_audio();
}

#elif defined(__linux__) || defined(__unix__)

#include <dlfcn.h>
#include <pthread.h>

typedef struct _snd_pcm snd_pcm_t;
typedef struct _snd_pcm_hw_params snd_pcm_hw_params_t;
typedef long snd_pcm_sframes_t;
typedef unsigned long snd_pcm_uframes_t;

/** PCM stream (direction) */
typedef enum _snd_pcm_stream {
	/** Playback stream */
	SND_PCM_STREAM_PLAYBACK = 0,
	/** Capture stream */
	SND_PCM_STREAM_CAPTURE,
	SND_PCM_STREAM_LAST = SND_PCM_STREAM_CAPTURE
} snd_pcm_stream_t;

/** PCM access type */
typedef enum _snd_pcm_access {
	/** mmap access with simple interleaved channels */
	SND_PCM_ACCESS_MMAP_INTERLEAVED = 0,
	/** mmap access with simple non interleaved channels */
	SND_PCM_ACCESS_MMAP_NONINTERLEAVED,
	/** mmap access with complex placement */
	SND_PCM_ACCESS_MMAP_COMPLEX,
	/** snd_pcm_readi/snd_pcm_writei access */
	SND_PCM_ACCESS_RW_INTERLEAVED,
	/** snd_pcm_readn/snd_pcm_writen access */
	SND_PCM_ACCESS_RW_NONINTERLEAVED,
	SND_PCM_ACCESS_LAST = SND_PCM_ACCESS_RW_NONINTERLEAVED
} snd_pcm_access_t;

/** PCM sample format */
typedef enum _snd_pcm_format {
	/** Unknown */
	SND_PCM_FORMAT_UNKNOWN = -1,
	/** Signed 8 bit */
	SND_PCM_FORMAT_S8 = 0,
	/** Unsigned 8 bit */
	SND_PCM_FORMAT_U8,
	/** Signed 16 bit Little Endian */
	SND_PCM_FORMAT_S16_LE,
	/** Signed 16 bit Big Endian */
	SND_PCM_FORMAT_S16_BE,
	/** Unsigned 16 bit Little Endian */
	SND_PCM_FORMAT_U16_LE,
	/** Unsigned 16 bit Big Endian */
	SND_PCM_FORMAT_U16_BE,
	/** Signed 24 bit Little Endian using low three bytes in 32-bit word */
	SND_PCM_FORMAT_S24_LE,
	/** Signed 24 bit Big Endian using low three bytes in 32-bit word */
	SND_PCM_FORMAT_S24_BE,
	/** Unsigned 24 bit Little Endian using low three bytes in 32-bit word */
	SND_PCM_FORMAT_U24_LE,
	/** Unsigned 24 bit Big Endian using low three bytes in 32-bit word */
	SND_PCM_FORMAT_U24_BE,
	/** Signed 32 bit Little Endian */
	SND_PCM_FORMAT_S32_LE,
	/** Signed 32 bit Big Endian */
	SND_PCM_FORMAT_S32_BE,
	/** Unsigned 32 bit Little Endian */
	SND_PCM_FORMAT_U32_LE,
	/** Unsigned 32 bit Big Endian */
	SND_PCM_FORMAT_U32_BE,
	/** Float 32 bit Little Endian, Range -1.0 to 1.0 */
	SND_PCM_FORMAT_FLOAT_LE,
	/** Float 32 bit Big Endian, Range -1.0 to 1.0 */
	SND_PCM_FORMAT_FLOAT_BE,
	/** Float 64 bit Little Endian, Range -1.0 to 1.0 */
	SND_PCM_FORMAT_FLOAT64_LE,
	/** Float 64 bit Big Endian, Range -1.0 to 1.0 */
	SND_PCM_FORMAT_FLOAT64_BE,
	/** IEC-958 Little Endian */
	SND_PCM_FORMAT_IEC958_SUBFRAME_LE,
	/** IEC-958 Big Endian */
	SND_PCM_FORMAT_IEC958_SUBFRAME_BE,
	/** Mu-Law */
	SND_PCM_FORMAT_MU_LAW,
	/** A-Law */
	SND_PCM_FORMAT_A_LAW,
	/** Ima-ADPCM */
	SND_PCM_FORMAT_IMA_ADPCM,
	/** MPEG */
	SND_PCM_FORMAT_MPEG,
	/** GSM */
	SND_PCM_FORMAT_GSM,
	/** Signed 20bit Little Endian in 4bytes format, LSB justified */
	SND_PCM_FORMAT_S20_LE,
	/** Signed 20bit Big Endian in 4bytes format, LSB justified */
	SND_PCM_FORMAT_S20_BE,
	/** Unsigned 20bit Little Endian in 4bytes format, LSB justified */
	SND_PCM_FORMAT_U20_LE,
	/** Unsigned 20bit Big Endian in 4bytes format, LSB justified */
	SND_PCM_FORMAT_U20_BE,
	/** Special */
	SND_PCM_FORMAT_SPECIAL = 31,
	/** Signed 24bit Little Endian in 3bytes format */
	SND_PCM_FORMAT_S24_3LE = 32,
	/** Signed 24bit Big Endian in 3bytes format */
	SND_PCM_FORMAT_S24_3BE,
	/** Unsigned 24bit Little Endian in 3bytes format */
	SND_PCM_FORMAT_U24_3LE,
	/** Unsigned 24bit Big Endian in 3bytes format */
	SND_PCM_FORMAT_U24_3BE,
	/** Signed 20bit Little Endian in 3bytes format */
	SND_PCM_FORMAT_S20_3LE,
	/** Signed 20bit Big Endian in 3bytes format */
	SND_PCM_FORMAT_S20_3BE,
	/** Unsigned 20bit Little Endian in 3bytes format */
	SND_PCM_FORMAT_U20_3LE,
	/** Unsigned 20bit Big Endian in 3bytes format */
	SND_PCM_FORMAT_U20_3BE,
	/** Signed 18bit Little Endian in 3bytes format */
	SND_PCM_FORMAT_S18_3LE,
	/** Signed 18bit Big Endian in 3bytes format */
	SND_PCM_FORMAT_S18_3BE,
	/** Unsigned 18bit Little Endian in 3bytes format */
	SND_PCM_FORMAT_U18_3LE,
	/** Unsigned 18bit Big Endian in 3bytes format */
	SND_PCM_FORMAT_U18_3BE,
	/* G.723 (ADPCM) 24 kbit/s, 8 samples in 3 bytes */
	SND_PCM_FORMAT_G723_24,
	/* G.723 (ADPCM) 24 kbit/s, 1 sample in 1 byte */
	SND_PCM_FORMAT_G723_24_1B,
	/* G.723 (ADPCM) 40 kbit/s, 8 samples in 3 bytes */
	SND_PCM_FORMAT_G723_40,
	/* G.723 (ADPCM) 40 kbit/s, 1 sample in 1 byte */
	SND_PCM_FORMAT_G723_40_1B,
	/* Direct Stream Digital (DSD) in 1-byte samples (x8) */
	SND_PCM_FORMAT_DSD_U8,
	/* Direct Stream Digital (DSD) in 2-byte samples (x16) */
	SND_PCM_FORMAT_DSD_U16_LE,
	/* Direct Stream Digital (DSD) in 4-byte samples (x32) */
	SND_PCM_FORMAT_DSD_U32_LE,
	/* Direct Stream Digital (DSD) in 2-byte samples (x16) */
	SND_PCM_FORMAT_DSD_U16_BE,
	/* Direct Stream Digital (DSD) in 4-byte samples (x32) */
	SND_PCM_FORMAT_DSD_U32_BE,
	SND_PCM_FORMAT_LAST = SND_PCM_FORMAT_DSD_U32_BE,

#if __BYTE_ORDER == __LITTLE_ENDIAN
	/** Signed 16 bit CPU endian */
	SND_PCM_FORMAT_S16 = SND_PCM_FORMAT_S16_LE,
	/** Unsigned 16 bit CPU endian */
	SND_PCM_FORMAT_U16 = SND_PCM_FORMAT_U16_LE,
	/** Signed 24 bit CPU endian */
	SND_PCM_FORMAT_S24 = SND_PCM_FORMAT_S24_LE,
	/** Unsigned 24 bit CPU endian */
	SND_PCM_FORMAT_U24 = SND_PCM_FORMAT_U24_LE,
	/** Signed 32 bit CPU endian */
	SND_PCM_FORMAT_S32 = SND_PCM_FORMAT_S32_LE,
	/** Unsigned 32 bit CPU endian */
	SND_PCM_FORMAT_U32 = SND_PCM_FORMAT_U32_LE,
	/** Float 32 bit CPU endian */
	SND_PCM_FORMAT_FLOAT = SND_PCM_FORMAT_FLOAT_LE,
	/** Float 64 bit CPU endian */
	SND_PCM_FORMAT_FLOAT64 = SND_PCM_FORMAT_FLOAT64_LE,
	/** IEC-958 CPU Endian */
	SND_PCM_FORMAT_IEC958_SUBFRAME = SND_PCM_FORMAT_IEC958_SUBFRAME_LE,
	/** Signed 20bit in 4bytes format, LSB justified, CPU Endian */
	SND_PCM_FORMAT_S20 = SND_PCM_FORMAT_S20_LE,
	/** Unsigned 20bit in 4bytes format, LSB justified, CPU Endian */
	SND_PCM_FORMAT_U20 = SND_PCM_FORMAT_U20_LE,
#elif __BYTE_ORDER == __BIG_ENDIAN
	/** Signed 16 bit CPU endian */
	SND_PCM_FORMAT_S16 = SND_PCM_FORMAT_S16_BE,
	/** Unsigned 16 bit CPU endian */
	SND_PCM_FORMAT_U16 = SND_PCM_FORMAT_U16_BE,
	/** Signed 24 bit CPU endian */
	SND_PCM_FORMAT_S24 = SND_PCM_FORMAT_S24_BE,
	/** Unsigned 24 bit CPU endian */
	SND_PCM_FORMAT_U24 = SND_PCM_FORMAT_U24_BE,
	/** Signed 32 bit CPU endian */
	SND_PCM_FORMAT_S32 = SND_PCM_FORMAT_S32_BE,
	/** Unsigned 32 bit CPU endian */
	SND_PCM_FORMAT_U32 = SND_PCM_FORMAT_U32_BE,
	/** Float 32 bit CPU endian */
	SND_PCM_FORMAT_FLOAT = SND_PCM_FORMAT_FLOAT_BE,
	/** Float 64 bit CPU endian */
	SND_PCM_FORMAT_FLOAT64 = SND_PCM_FORMAT_FLOAT64_BE,
	/** IEC-958 CPU Endian */
	SND_PCM_FORMAT_IEC958_SUBFRAME = SND_PCM_FORMAT_IEC958_SUBFRAME_BE,
	/** Signed 20bit in 4bytes format, LSB justified, CPU Endian */
	SND_PCM_FORMAT_S20 = SND_PCM_FORMAT_S20_BE,
	/** Unsigned 20bit in 4bytes format, LSB justified, CPU Endian */
	SND_PCM_FORMAT_U20 = SND_PCM_FORMAT_U20_BE,
#else
#error "Unknown endian"
#endif
} snd_pcm_format_t;

typedef int (*PFN_snd_pcm_open) (snd_pcm_t **pcm, const char *name, snd_pcm_stream_t stream, int mode);
typedef int (*PFN_snd_pcm_close) (snd_pcm_t *pcm);
typedef int (*PFN_snd_config_update_free_global) (void);
typedef snd_pcm_sframes_t (*PFN_snd_pcm_writei) (snd_pcm_t *pcm, const void *buffer, snd_pcm_uframes_t size);
typedef int (*PFN_snd_pcm_prepare) (snd_pcm_t *pcm);
typedef int (*PFN_snd_pcm_hw_params_malloc) (snd_pcm_hw_params_t **ptr);
typedef void (*PFN_snd_pcm_hw_params_free) (snd_pcm_hw_params_t *obj);
typedef int (*PFN_snd_pcm_hw_params_any) (snd_pcm_t *pcm, snd_pcm_hw_params_t *params);
typedef int (*PFN_snd_pcm_hw_params_set_access) (snd_pcm_t *pcm, snd_pcm_hw_params_t *params, snd_pcm_access_t access);
typedef int (*PFN_snd_pcm_hw_params_set_format) (snd_pcm_t *pcm, snd_pcm_hw_params_t *params, snd_pcm_format_t val);
typedef int (*PFN_snd_pcm_hw_params_set_rate_near) (snd_pcm_t *pcm, snd_pcm_hw_params_t *params, unsigned int *val, int *dir);
typedef int (*PFN_snd_pcm_hw_params_set_buffer_size_near) (snd_pcm_t *pcm, snd_pcm_hw_params_t *params, snd_pcm_uframes_t *val);
typedef int (*PFN_snd_pcm_hw_params_set_channels) (snd_pcm_t *pcm, snd_pcm_hw_params_t *params, unsigned int val);
typedef int (*PFN_snd_pcm_hw_params) (snd_pcm_t *pcm, snd_pcm_hw_params_t *params);
typedef const char * (*PFN_snd_strerror) (int errnum);

PFN_snd_pcm_open                           snd_pcm_open   = 0;
PFN_snd_pcm_close                          snd_pcm_close = 0;
PFN_snd_config_update_free_global          snd_config_update_free_global = 0;
PFN_snd_pcm_writei                         snd_pcm_writei = 0;
PFN_snd_pcm_prepare                        snd_pcm_prepare = 0;
PFN_snd_pcm_hw_params_malloc               snd_pcm_hw_params_malloc = 0;
PFN_snd_pcm_hw_params_free                 snd_pcm_hw_params_free = 0;
PFN_snd_pcm_hw_params_any                  snd_pcm_hw_params_any = 0;
PFN_snd_pcm_hw_params_set_access           snd_pcm_hw_params_set_access  = 0;
PFN_snd_pcm_hw_params_set_format           snd_pcm_hw_params_set_format = 0;
PFN_snd_pcm_hw_params_set_rate_near        snd_pcm_hw_params_set_rate_near = 0;
PFN_snd_pcm_hw_params_set_buffer_size_near snd_pcm_hw_params_set_buffer_size_near = 0;
PFN_snd_pcm_hw_params_set_channels         snd_pcm_hw_params_set_channels = 0;
PFN_snd_pcm_hw_params                      snd_pcm_hw_params = 0;
PFN_snd_strerror                           snd_strerror = 0;

typedef struct {
	snd_pcm_t* device;
    bool thread_created;
	pthread_t thread;
    bool mutex_created;
    pthread_mutex_t mutex;
	volatile bool thread_stop;
    void *libasound;
} SND__SoundPlayerBackend;

static SND__SoundPlayerBackend SND__sound_player_backend = {0};


SND_INTERNAL int
SND__Load_alsa_procs(void) {
    const char* candidates[] = {"libasound.so", "libasound.so.2", "libasound.so.2.0.0"};
    void *lib = 0;
    for (int i = 0; i < 3; i+=1) {
        lib = dlopen(candidates[i], RTLD_LAZY);
        if (lib) break;
    }
    if (!lib) return -1;

    SND__sound_player_backend.libasound = lib;

    snd_pcm_open                           = (PFN_snd_pcm_open                          )dlsym(lib, "snd_pcm_open");
    snd_pcm_close                          = (PFN_snd_pcm_close                         )dlsym(lib, "snd_pcm_close");
    snd_config_update_free_global          = (PFN_snd_config_update_free_global         )dlsym(lib, "snd_config_update_free_global");
    snd_pcm_writei                         = (PFN_snd_pcm_writei                        )dlsym(lib, "snd_pcm_writei");
    snd_pcm_prepare                        = (PFN_snd_pcm_prepare                       )dlsym(lib, "snd_pcm_prepare");
    snd_pcm_hw_params_malloc               = (PFN_snd_pcm_hw_params_malloc              )dlsym(lib, "snd_pcm_hw_params_malloc");
    snd_pcm_hw_params_free                 = (PFN_snd_pcm_hw_params_free                )dlsym(lib, "snd_pcm_hw_params_free");
    snd_pcm_hw_params_any                  = (PFN_snd_pcm_hw_params_any                 )dlsym(lib, "snd_pcm_hw_params_any");
    snd_pcm_hw_params_set_access           = (PFN_snd_pcm_hw_params_set_access          )dlsym(lib, "snd_pcm_hw_params_set_access");
    snd_pcm_hw_params_set_format           = (PFN_snd_pcm_hw_params_set_format          )dlsym(lib, "snd_pcm_hw_params_set_format");
    snd_pcm_hw_params_set_rate_near        = (PFN_snd_pcm_hw_params_set_rate_near       )dlsym(lib, "snd_pcm_hw_params_set_rate_near");
    snd_pcm_hw_params_set_buffer_size_near = (PFN_snd_pcm_hw_params_set_buffer_size_near)dlsym(lib, "snd_pcm_hw_params_set_buffer_size_near");
    snd_pcm_hw_params_set_channels         = (PFN_snd_pcm_hw_params_set_channels        )dlsym(lib, "snd_pcm_hw_params_set_channels");
    snd_pcm_hw_params                      = (PFN_snd_pcm_hw_params                     )dlsym(lib, "snd_pcm_hw_params");
    snd_strerror                           = (PFN_snd_strerror                          )dlsym(lib, "snd_strerror");

    if (!snd_pcm_open)                           return -1;
    if (!snd_pcm_close)                          return -1;
    if (!snd_config_update_free_global)          return -1;
    if (!snd_pcm_writei)                         return -1;
    if (!snd_pcm_prepare)                        return -1;
    if (!snd_pcm_hw_params_malloc)               return -1;
    if (!snd_pcm_hw_params_free)                 return -1;
    if (!snd_pcm_hw_params_any)                  return -1;
    if (!snd_pcm_hw_params_set_access)           return -1;
    if (!snd_pcm_hw_params_set_format)           return -1;
    if (!snd_pcm_hw_params_set_rate_near)        return -1;
    if (!snd_pcm_hw_params_set_buffer_size_near) return -1;
    if (!snd_pcm_hw_params_set_channels)         return -1;
    if (!snd_pcm_hw_params)                      return -1;
    if (!snd_strerror)                           return -1;

    return 0;
}

SND_INTERNAL void
SND__Unload_alsa_procs(void) {

    if (SND__sound_player_backend.libasound) dlclose(SND__sound_player_backend.libasound);
    SND__sound_player_backend.libasound = 0;

    snd_pcm_open                           = 0;
    snd_pcm_close                          = 0;
    snd_config_update_free_global          = 0;
    snd_pcm_writei                         = 0;
    snd_pcm_prepare                        = 0;
    snd_pcm_hw_params_malloc               = 0;
    snd_pcm_hw_params_free                 = 0;
    snd_pcm_hw_params_any                  = 0;
    snd_pcm_hw_params_set_access           = 0;
    snd_pcm_hw_params_set_format           = 0;
    snd_pcm_hw_params_set_rate_near        = 0;
    snd_pcm_hw_params_set_buffer_size_near = 0;
    snd_pcm_hw_params_set_channels         = 0;
    snd_pcm_hw_params                      = 0;
    snd_strerror                           = 0;
}



/* the streaming callback runs in a separate thread */
SND_INTERNAL void *
SND__Sound_player_thread(void *param) {
    snd_pcm_prepare(SND__sound_player_backend.device);
    while (!SND__sound_player_backend.thread_stop) {
        /* snd_pcm_writei() will be blocking until it needs data */
        int write_res = snd_pcm_writei(
				SND__sound_player_backend.device,
				SND__sound_player_info.buffer,
				(snd_pcm_uframes_t)SND__sound_player_info.buffer_frames
		);
        if (write_res < 0) {
            /* underrun occurred */
            snd_pcm_prepare(SND__sound_player_backend.device);
        }
        else {
            /* fill the streaming buffer with new data */
			SND__sound_player_info.stream_cb(
					SND__sound_player_info.user_data,
					SND__sound_player_info.buffer,
					SND__sound_player_info.buffer_frames,
					SND__sound_player_info.num_channels,
					SND__sound_player_info.sample_rate
			);
        }
    }
    return 0;
}



SND_PUBLIC int
SND__Init_platform(const SND_SoundParams *params) {

    snd_pcm_hw_params_t* hw_params = 0;

    if (SND__Load_alsa_procs() != 0) {
        goto error;
    }

    int dir; uint32_t rate;
    int rc = snd_pcm_open(&SND__sound_player_backend.device, "default", SND_PCM_STREAM_PLAYBACK, 0);
    if (rc < 0) {
        fprintf(stderr, "Cannot open the audio device.\n");
        return -1;
    }

    /* configuration works by restricting the 'configuration space' step
       by step, we require all parameters except the sample rate and buffer size to
       match perfectly
    */
    snd_pcm_hw_params_malloc(&hw_params);
    int error_code = snd_pcm_hw_params_any(SND__sound_player_backend.device, hw_params);
    if (0 > error_code) {
        printf("Broken configuration for %s PCM: no configurations available\n", snd_strerror(error_code));
        goto error;
    }
    error_code = snd_pcm_hw_params_set_access(SND__sound_player_backend.device, hw_params, SND_PCM_ACCESS_RW_INTERLEAVED);
    if (0 > error_code) {
        printf("snd_pcm_hw_params_set_access() failed. Code: %d. Error: %s\n", error_code, snd_strerror(error_code));
        goto error;
    }
    if (0 > snd_pcm_hw_params_set_format(SND__sound_player_backend.device, hw_params, SND_PCM_FORMAT_FLOAT_LE)) {
        fprintf(stderr, "Cannot device doesn't support floats\n");
        goto error;
    }

    /* let ALSA pick a nearby sampling rate */
    rate = (uint32_t) params->sample_rate;
    dir = 0;
    error_code = snd_pcm_hw_params_set_rate_near(SND__sound_player_backend.device, hw_params, &rate, &dir);
    if (0 > error_code) {
        const char *error = snd_strerror(error_code);
        fprintf(stderr, "snd_pcm_hw_params_set_rate_near() failed. Code: %d, Error: %s\n", error_code, error);
        goto error;
    }

	/* Pick a near buffer size */
	snd_pcm_uframes_t buffer_frames = params->buffer_frames;
    if (0 > snd_pcm_hw_params_set_buffer_size_near(SND__sound_player_backend.device, hw_params, &buffer_frames)) {
        fprintf(stderr, "Requested buffer size not supported\n");
        goto error;
    }

    if (0 > snd_pcm_hw_params_set_channels(SND__sound_player_backend.device, hw_params,
    (uint32_t)params->num_channels)) {
        fprintf(stderr, "Requested channel count not supported\n");
        goto error;
    }

    if (0 > snd_pcm_hw_params(SND__sound_player_backend.device, hw_params)) {
        fprintf(stderr, "snd_pcm_hw_params() failed\n");
        goto error;
    }


    /* read back actual sample rate and channels */
    SND__sound_player_info.sample_rate   = rate;
	SND__sound_player_info.buffer_frames = buffer_frames;
	SND__sound_player_info.user_data     = params->user_data;
	SND__sound_player_info.stream_cb     = params->stream_cb;
	SND__sound_player_info.num_channels  = params->num_channels;

    /* allocate the streaming buffer */
	size_t buffer_nelems = SND__sound_player_info.buffer_frames * SND__sound_player_info.num_channels;
    if (buffer_nelems > SND_MAX_BUFFER_SIZE) {
        fprintf(stderr, "Out of memory!\n");
        goto error;
    }
	for (int i = 0; i < buffer_nelems; i+=1) SND__sound_player_info.buffer[i] = 0.0f;


    if (pthread_mutex_init(&(SND__sound_player_backend.mutex), NULL) != 0) {
        fprintf(stderr, "Mutex creation failed\n");
        goto error;
    }
    SND__sound_player_backend.mutex_created = true;

    /* create the buffer-streaming start thread */
    if (0 != pthread_create(&SND__sound_player_backend.thread, 0, SND__Sound_player_thread, 0)) {
        fprintf(stderr, "pthread_create() failed\n");
        goto error;
    }
    SND__sound_player_backend.thread_created = true;


    if (hw_params) snd_pcm_hw_params_free(hw_params);
    return 0;

    error: {
        if (hw_params) snd_pcm_hw_params_free(hw_params);
		SND_Deinit();
        return -1;
    }
}

SND_INTERNAL void
SND__Lock(void) {
    pthread_mutex_lock(&SND__sound_player_backend.mutex);
}

SND_INTERNAL void
SND__Unlock(void) {
    pthread_mutex_unlock(&SND__sound_player_backend.mutex);
}


SND_INTERNAL void
SND__Deinit_platform(void) {
    SND__sound_player_backend.thread_created = true;
    if (SND__sound_player_backend.thread_created) {
	    SND__sound_player_backend.thread_stop = true;
	    void *dummy;
	    pthread_join(SND__sound_player_backend.thread, &dummy);
    }
    if (SND__sound_player_backend.mutex_created) {
        pthread_mutex_destroy(&(SND__sound_player_backend.mutex));
    }
	if (SND__sound_player_backend.device) {
		snd_pcm_close(SND__sound_player_backend.device);
		SND__sound_player_backend.device = NULL;
		snd_config_update_free_global();
	}
    SND__Unload_alsa_procs();
    SND__sound_player_backend = (SND__SoundPlayerBackend){0};
}


#endif

#endif // SOUND_IMPLEMENTATION_INCLUDED
#endif // SOUND_IMPLEMENTATION

