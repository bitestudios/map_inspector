
#ifndef GRAPHICS_INCLUDED
#define GRAPHICS_INCLUDED

#include "madmath.h"

#ifndef GFX_PUBLIC
	#define GFX_PUBLIC 
#endif

#ifndef GFX_INTERNAL
	#define GFX_INTERNAL static
#endif

typedef struct {
    Vec3 position;
    Color normal;
    Vec2 tex_coord;
    Color color;
} GFX_Vertex;

#define GFX_VERTEX_SIZE   (sizeof(GFX_Vertex))

typedef enum {
    GFX_BUFFER_INDEX_TYPE_16,
    GFX_BUFFER_INDEX_TYPE_32,
} GFX_BufferIndexType;

typedef struct {

    // Vertices
    GFX_Vertex *vertices;
    int64_t vertices_count;
    int64_t vertices_cap;
    GLuint VBO;
    
    GFX_BufferIndexType index_type;
    
    // Indices
    void *indices;
    int64_t	indices_count;
    int64_t indices_cap;
    GLuint EBO;
    
} GFX_Buffer;

typedef struct {
    uint16_t x0, y0, x1, y1; // coordinates of bbox in bitmap
    float xoff, yoff, xadvance;
} GFX_GlyphInfo;

struct GFX_Font_t {
    int64_t  *ranges;
    int64_t  total_ranges;
    int64_t  count; // Total amount of glyphs
    int64_t  fallback; // Index (of the glyph_infos, no the codepoint) of a fallback if the desired
                       // glyph was not found
    uint16_t texture_w;
    uint16_t texture_h;
    GLuint   texture;
    float height;

    // The atlas can have a white portion, this is useful because we dont have to flush and switch
    // textures in most API calls. Is we call GFX_Set_white() it will determine if should change
    // the texture or only change the texture subwindow calling GFX_Set_texture_window()
    bool has_white;
    float white_x;
    float white_y;
    float white_w;
    float white_h;

    GFX_GlyphInfo *glyph_infos;
};

typedef struct GFX_Font_t GFX_Font;


GFX_PUBLIC int
GFX_Create_buffer(GFX_Buffer *buffer_out, void *vertices_mem, int64_t vertices_mem_size, void *indices_mem, int64_t indices_mem_size, GFX_BufferIndexType);

GFX_PUBLIC void
GFX_Destroy_buffer(GFX_Buffer *buffer);

GFX_PUBLIC void
GFX_Clear_buffer_data(GFX_Buffer *buffer);

GFX_PUBLIC void
GFX_Upload_buffer_to_gpu(GFX_Buffer *buffer);

GFX_PUBLIC GFX_Vertex *
GFX_Alloc_vertices(GFX_Buffer *buffer, int64_t count, uint32_t *base_index);

GFX_PUBLIC void *
GFX_Alloc_indices(GFX_Buffer *buffer, int64_t count);

GFX_PUBLIC int64_t
GFX_Get_remaining_vertices(GFX_Buffer *buffer);

GFX_PUBLIC int64_t
GFX_Get_remaining_indices(GFX_Buffer *buffer);

GFX_PUBLIC void
GFX_Draw_buffer(GFX_Buffer *buffer);



// Setups the basic renderer, it will allow you to draw various types of figures,
// moreover, is possible that other kinds of renderer are dependant of this.
// To clean the resources GFX_Deinit has to be called.
GFX_PUBLIC int
GFX_Init(void);

// Cleans all the resources used by the renderer.
GFX_PUBLIC void
GFX_Deinit(void);

GFX_PUBLIC void
GFX_Begin(void);

GFX_PUBLIC void
GFX_End(void);

GFX_PUBLIC const char *
GFX_Last_error(void);


// Sets the 4x4 matrix to apply over each vertex position on the shader NOTE that this call will
// flush the buffer with the previus matrix values
GFX_PUBLIC void
GFX_Set_matrix(Mat4 matrix);

// Gets the current matrix
GFX_PUBLIC Mat4
GFX_Get_matrix(void);

GFX_PUBLIC void
GFX_Set_light_dir(Vec3 light_dir);

GFX_PUBLIC GLuint
GFX_Default_texture(void);

// Sets the current texture to be used. NOTE that if the render was currently drawing something
// with texture, it will be flushed.
GFX_PUBLIC void
GFX_Set_texture(GLuint texture);

// Gets the current texture
GFX_PUBLIC GLuint
GFX_Get_texture(void);

GFX_PUBLIC void
GFX_Set_texture_window(float x, float y, float w, float h);

GFX_PUBLIC void
GFX_Set_white(void);

GFX_PUBLIC void
GFX_Flush(void);

GFX_PUBLIC void
GFX_Draw_triangle_ex(Vec3 v0, Vec3 v1, Vec3 v2, Color n0, Color n1, Color n2, Vec2 uv0, Vec2 uv1,
		Vec2 uv2, Color c0, Color c1, Color c2);

GFX_PUBLIC void
GFX_Draw_quad_ex(Vec3 v0, Vec3 v1, Vec3 v2, Vec3 v3, Color n0, Color n1, Color n2, Color n3,
		Vec2 uv0, Vec2 uv1, Vec2 uv2, Vec2 uv3, Color c0, Color c1, Color c2, Color c3);

GFX_PUBLIC void
GFX_Draw_quad(Vec2 v0, Vec2 v1, Vec2 v2, Vec2 v3, Color color);

GFX_PUBLIC void
GFX_Draw_rect(Vec2 pos, float width, float height, Color color);

GFX_PUBLIC void
GFX_Draw_triangle(Vec2 v0, Vec2 v1, Vec2 v2, Color color);

GFX_PUBLIC void
GFX_Draw_line(Vec2 src, Vec2 dst, float thickness, Color color);

GFX_PUBLIC void
GFX_Draw_textured_quad(Vec2 v0, Vec2 v1, Vec2 v2, Vec2 v3, Vec2 uv0, Vec2 uv1, Vec2 uv2, Vec2 uv3, Color color);

GFX_PUBLIC void
GFX_Draw_textured_triangle(Vec2 v0, Vec2 v1, Vec2 v2, Vec2 uv0, Vec2 uv1, Vec2 uv2, Color color);

GFX_PUBLIC void
GFX_Draw_circle(Vec2 pos, float radius, Color color);

GFX_PUBLIC void
GFX_Draw_triangle_3D(Vec3 v0, Vec3 v1, Vec3 v2, Color color);

GFX_PUBLIC void
GFX_Draw_quad_3D(Vec3 v0, Vec3 v1, Vec3 v2, Vec3 v3, Color color);

GFX_PUBLIC void
GFX_Draw_cube(Vec3 pos, float width, float height, float length, Color color);

GFX_PUBLIC void
GFX_Draw_cylinder(Vec3 pos, float up_radius, float down_radius, float length, Color color);

GFX_PUBLIC void
GFX_Draw_sphere(Vec3 pos, float radius, Color color);

GFX_PUBLIC void
GFX_Draw_torus(Vec3 pos, float radius0, float radius1, Color color);


// Text rendering stuff

GFX_PUBLIC GFX_Font *
GFX_Default_font(void);

GFX_PUBLIC void
GFX_Set_font(GFX_Font *font);

GFX_PUBLIC GFX_Font *
GFX_Get_font(void);

GFX_PUBLIC float
GFX_Get_text_width(GFX_Font *font, const char *text, int len);

GFX_PUBLIC void
GFX_Draw_text(const char *text, Vec2 pos, float scale, Color color);

GFX_PUBLIC void
GFX_Draw_text_sized(const char *text, int text_len, Vec2 pos, float scale, Color color);

GFX_PUBLIC void
GFX_Draw_text_view(const char *text, int text_len, Vec2 pos, float offset, float max_width, float scale, Color color);

#endif // GRAPHICS_INCLUDED

#ifdef GRAPHICS_IMPLEMENTATION
#ifndef GRAPHICS_IMPLEMENTATION_INCLUDED
#define GRAPHICS_IMPLEMENTATION_INCLUDED


///////////////////////////////////////////////////////////////////////////////////////
//
//
//                              IMPLEMENTATION STARTS
//
//
///////////////////////////////////////////////////////////////////////////////////////



#include <stddef.h>


#ifndef GFX__MAX_ERROR_STR_SIZE
    #define GFX__MAX_ERROR_STR_SIZE 1024
#endif


// 64K vertices and indices
#ifndef GFX__VERTICES_BUFFER_MAX_SIZE_BYTES
    #define GFX__VERTICES_BUFFER_MAX_SIZE_BYTES (GFX_VERTEX_SIZE*64*1024)
#endif
#ifndef GFX__INDICES_BUFFER_MAX_SIZE_BYTES
    #define GFX__INDICES_BUFFER_MAX_SIZE_BYTES (2*64*1024)
#endif


#define GFX__MAX_BUFFER_VERTICES  (VERTICES_BUFFER_MAX_SIZE_BYTES/GFX_VERTEX_SIZE)

#define GFX__DEFAULT_FONT_COMPRESSED_SIZE 3266
#define GFX__DEFAULT_TEXTURE_W 512
#define GFX__DEFAULT_TEXTURE_H 64
#define GFX__DEFAULT_FONT_TOTAL_GLYPHS 319
#define GFX__DEFAULT_FONT_TOTAL_RANGES 3


// 
// All the internal data used by the renderer
//
GFX_INTERNAL struct GFX__Data {

    Mat4 matrix; // Current matrix that will be send to the shader
	Vec3 light_dir;
	GLuint texture;
    float texture_window_x;
    float texture_window_y;
    float texture_window_w;
    float texture_window_h;
    GFX_Font *font;

	uint8_t vertices_mem[GFX__VERTICES_BUFFER_MAX_SIZE_BYTES];
	uint8_t indices_mem[GFX__INDICES_BUFFER_MAX_SIZE_BYTES];
	
	GFX_Buffer buffer;


	// Shader used to draw the triangles
	struct {
		GLuint id;
		GLint position;
		GLint normal;
		GLint tex_coord;
		GLint color;
		GLint vmat;
		GLint texture;
		GLint light_dir;
	} default_shader;

	GLuint default_texture;
    GFX_Font default_font;
    GFX_GlyphInfo default_glyph_infos[GFX__DEFAULT_FONT_TOTAL_GLYPHS];
    int64_t       default_font_ranges[GFX__DEFAULT_FONT_TOTAL_RANGES];

    char last_error_str[GFX__MAX_ERROR_STR_SIZE];

    union {
	    unsigned char default_font_texture_data[GFX__DEFAULT_TEXTURE_W*GFX__DEFAULT_TEXTURE_H*4];
    } tmp;

} GFX__data = {0};



// Sweet16Font (c) 2018 Martin Sedlák
// https://github.com/kmar/Sweet16Font

// The atlas of the font is codified as a bitarray, you can see the decoding of it in the init
// funciton. Because the atlas has a lot of white space at the end, this only encodes until the
// last not null element, the rest is assumed to be 0.
// Link to the original atlas: https://github.com/kmar/Sweet16Font/blob/master/Sweet16_bmfont_0.png

GFX_INTERNAL unsigned char GFX__default_font_texture_data_compress[GFX__DEFAULT_FONT_COMPRESSED_SIZE] = {
    0x88, 0x3F, 0x86, 0xC7, 0xC3, 0x60, 0x48, 0x3C, 0x01, 0x08, 0x93, 0x20, 0x0F, 0x08, 0x11, 0x84,
    0x1F, 0x0A, 0x84, 0x01, 0x09, 0x85, 0xF1, 0x41, 0x30, 0x18, 0x12, 0x8A, 0x47, 0x22, 0x61, 0x90,
    0x18, 0x21, 0x84, 0x04, 0x22, 0x61, 0x48, 0x18, 0x0C, 0x86, 0x04, 0x42, 0xA0, 0x30, 0x08, 0x10,
    0x09, 0x81, 0x21, 0xF1, 0x48, 0x08, 0x04, 0x08, 0x83, 0x22, 0x81, 0x84, 0x24, 0x12, 0x06, 0x43,
    0x04, 0x04, 0x49, 0x28, 0xC4, 0x90, 0x78, 0x42, 0x01, 0x84, 0x14, 0x80, 0x10, 0x04, 0x00, 0x8A,
    0x00, 0x04, 0x8A, 0x02, 0x8F, 0x42, 0x12, 0x22, 0x48, 0x24, 0x0C, 0x05, 0x80, 0xE1, 0x61, 0x48,
    0x24, 0x21, 0x02, 0x03, 0xC1, 0x60, 0x30, 0x18, 0x0C, 0x06, 0x00, 0x41, 0x50, 0x48, 0x10, 0x08,
    0x00, 0x42, 0xC2, 0x00, 0x00, 0x10, 0x08, 0x84, 0x44, 0x01, 0x40, 0x8C, 0x00, 0x1E, 0x86, 0x24,
    0x0C, 0x04, 0x40, 0x20, 0x00, 0x00, 0x00, 0x02, 0x01, 0x00, 0x90, 0xA0, 0x90, 0xA0, 0x20, 0x80,
    0x00, 0x00, 0x80, 0x02, 0x00, 0x00, 0x10, 0x02, 0x00, 0x00, 0x00, 0x40, 0x08, 0x00, 0x00, 0x00,
    0x00, 0x21, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x07, 0x00, 0x00, 0x00, 0x00, 0x9E,
    0x1F, 0x00, 0x00, 0xF0, 0x84, 0x42, 0x00, 0x00, 0x00, 0xE0, 0x09, 0x95, 0x42, 0x00, 0x00, 0x00,
    0x00, 0x04, 0x8F, 0x20, 0xE0, 0x09, 0x79, 0x02, 0x81, 0x10, 0x94, 0xA0, 0x90, 0xA0, 0xA0, 0xA0,
    0x80, 0xBF, 0xA0, 0x81, 0x5F, 0xE8, 0x17, 0xF2, 0x78, 0x3C, 0x9E, 0x50, 0xE8, 0x17, 0x0A, 0x85,
    0x42, 0xA1, 0xDF, 0xC3, 0xE3, 0xF9, 0xFD, 0x7E, 0x9E, 0x5F, 0xE8, 0xE7, 0xF1, 0x78, 0x3C, 0xA1,
    0xC0, 0xCF, 0xE3, 0x09, 0x85, 0x42, 0x1E, 0x8F, 0xC7, 0x13, 0x0A, 0xA5, 0x42, 0x1E, 0x4F, 0x28,
    0x1E, 0x84, 0x10, 0x23, 0x10, 0x0A, 0x85, 0x02, 0x81, 0x10, 0x94, 0xA0, 0x10, 0x11, 0x91, 0xA0,
    0x07, 0x84, 0x20, 0x80, 0xC0, 0x28, 0xF0, 0x09, 0x85, 0x42, 0xA1, 0x50, 0x28, 0x10, 0x0A, 0x85,
    0x42, 0x21, 0x50, 0x24, 0x14, 0x02, 0x81, 0x02, 0xA1, 0x40, 0x28, 0x10, 0x0A, 0x85, 0x42, 0x81,
    0x40, 0x20, 0x14, 0x0A, 0x85, 0x42, 0xA1, 0x50, 0x28, 0x14, 0x0A, 0xC5, 0x42, 0xA1, 0x50, 0x28,
    0x21, 0x84, 0x10, 0x24, 0x17, 0x0A, 0x85, 0x02, 0x81, 0x10, 0x94, 0xA0, 0x1F, 0x11, 0x11, 0x91,
    0x00, 0x84, 0x20, 0x86, 0x40, 0x29, 0x10, 0x09, 0x04, 0x02, 0x81, 0x50, 0x28, 0x10, 0x0A, 0x85,
    0x42, 0x21, 0x48, 0x28, 0x14, 0x80, 0x40, 0x02, 0xA1, 0x40, 0x28, 0x10, 0x08, 0x05, 0x42, 0x81,
    0x40, 0x20, 0x14, 0x0A, 0x85, 0x42, 0xA1, 0x50, 0x28, 0x14, 0x0A, 0x85, 0x42, 0x81, 0x40, 0x28,
    0x21, 0x84, 0x10, 0x28, 0x14, 0x0A, 0x85, 0x02, 0x81, 0x10, 0x94, 0xA0, 0x10, 0x0A, 0x0A, 0x91,
    0x00, 0x84, 0x24, 0x86, 0x47, 0xEA, 0x11, 0x12, 0x08, 0x02, 0x82, 0x50, 0xE8, 0x11, 0x0A, 0x85,
    0x42, 0x21, 0x44, 0x28, 0x14, 0x40, 0x20, 0x1E, 0xA1, 0x47, 0xE8, 0x11, 0x08, 0x05, 0x42, 0x81,
    0xC7, 0x23, 0x14, 0x0A, 0x85, 0x42, 0xA1, 0x50, 0x28, 0x14, 0x0A, 0x85, 0x42, 0x81, 0xC0, 0x2F,
    0x21, 0x84, 0x10, 0x28, 0x14, 0x0A, 0x85, 0x02, 0x81, 0x10, 0x94, 0xA0, 0x10, 0x04, 0x04, 0x8A,
    0x00, 0x84, 0x24, 0x82, 0x40, 0x2C, 0x10, 0x62, 0x30, 0x72, 0x8C, 0x50, 0x28, 0x10, 0x0A, 0x85,
    0x42, 0x21, 0x42, 0xE8, 0x17, 0x20, 0x10, 0x02, 0xBF, 0xC0, 0x2F, 0x10, 0xF8, 0x05, 0x7E, 0x81,
    0x40, 0xE0, 0xF7, 0xFB, 0xC5, 0x42, 0xA1, 0x50, 0x28, 0x14, 0x0A, 0x85, 0x42, 0xB9, 0x5C, 0x28,
    0x21, 0x84, 0x58, 0x28, 0x14, 0x8B, 0xC5, 0x42, 0x81, 0x18, 0x94, 0xA0, 0x10, 0x04, 0x04, 0x84,
    0x00, 0x84, 0x24, 0x96, 0x40, 0x28, 0x10, 0x82, 0x40, 0x42, 0x90, 0x50, 0x28, 0x10, 0x0A, 0x85,
    0x42, 0x21, 0x41, 0x28, 0x14, 0x10, 0x08, 0x02, 0xA1, 0x40, 0x28, 0x10, 0x08, 0x05, 0x42, 0x81,
    0x40, 0x20, 0x14, 0x0A, 0xB9, 0x42, 0xA1, 0x50, 0x28, 0x14, 0x0A, 0x85, 0x42, 0xA1, 0x50, 0x28,
    0x31, 0x04, 0x97, 0xC7, 0xE3, 0x72, 0xB9, 0x3C, 0x3F, 0x17, 0x24, 0xA2, 0x10, 0x04, 0x04, 0x84,
    0x1F, 0x84, 0x2A, 0x90, 0x40, 0x28, 0x10, 0x02, 0x81, 0x42, 0xA0, 0x50, 0x28, 0x10, 0x0A, 0x85,
    0x42, 0x9E, 0x40, 0x28, 0x14, 0x08, 0x04, 0x02, 0xA1, 0x40, 0x28, 0x10, 0x08, 0x05, 0x42, 0x81,
    0x40, 0x20, 0x14, 0x0A, 0x81, 0x42, 0xA1, 0x50, 0x28, 0x14, 0x0A, 0x85, 0x42, 0xA1, 0x50, 0x28,
    0x2E, 0x00, 0x10, 0x00, 0x00, 0x02, 0x81, 0x08, 0x00, 0x50, 0x04, 0x22, 0x08, 0x04, 0x04, 0x04,
    0x08, 0x84, 0x31, 0x90, 0x40, 0x28, 0x80, 0x08, 0x85, 0x42, 0xA1, 0x50, 0x28, 0x10, 0x0A, 0x85,
    0x42, 0x84, 0x40, 0x24, 0x14, 0x0A, 0x04, 0x02, 0xA1, 0x40, 0x28, 0x10, 0x0A, 0x85, 0x42, 0xA1,
    0x40, 0x20, 0x14, 0x0A, 0x41, 0x42, 0xA1, 0x50, 0x28, 0x14, 0x0A, 0x21, 0x42, 0xA1, 0x50, 0x28,
    0x20, 0x84, 0x10, 0x82, 0x11, 0x82, 0x84, 0x10, 0x04, 0x48, 0x24, 0x1C, 0x30, 0x04, 0x04, 0x04,
    0x30, 0x84, 0x20, 0xB0, 0x5F, 0xE8, 0xC7, 0xF0, 0x78, 0x3C, 0x1E, 0x8F, 0xE7, 0xE7, 0xF1, 0x78,
    0x3C, 0x8C, 0xDF, 0x23, 0xE4, 0xF9, 0xFD, 0x7E, 0xA1, 0x5F, 0xE8, 0xE7, 0x09, 0x79, 0x42, 0x9E,
    0xDF, 0x2F, 0x14, 0x0A, 0x3D, 0x3C, 0x1E, 0x8F, 0xC7, 0xE3, 0xF1, 0x30, 0x3C, 0x1E, 0x4F, 0x28,
    0x21, 0x06, 0x0F, 0x83, 0xE0, 0x79, 0x78, 0x18, 0x86, 0x87, 0x33, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x1E, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x20, 0x20, 0x42, 0xA1, 0x92,
    0x2B, 0x59, 0x04, 0x04, 0x02, 0x03, 0x84, 0x27, 0x00, 0x1A, 0x30, 0x44, 0x71, 0x72, 0xA0, 0xF8,
    0x09, 0xF1, 0xC7, 0x8B, 0x01, 0xC4, 0x23, 0x28, 0x48, 0xE0, 0x2F, 0x28, 0x28, 0x20, 0xC8, 0x23,
    0x48, 0xE0, 0x0F, 0xC1, 0xE3, 0x61, 0x48, 0x3C, 0x1E, 0xCF, 0xC7, 0xF3, 0xF8, 0xFD, 0x42, 0x20,
    0x00, 0x09, 0x23, 0x14, 0x80, 0x20, 0x24, 0x21, 0x8F, 0x84, 0x04, 0x71, 0x00, 0xA2, 0x10, 0x28,
    0x38, 0x99, 0x0A, 0x04, 0x22, 0x92, 0x44, 0x28, 0x02, 0x22, 0x88, 0x24, 0x42, 0x10, 0xA0, 0x24,
    0xFC, 0x8B, 0x20, 0x4C, 0x02, 0x4F, 0x64, 0x2C, 0xE8, 0x03, 0x21, 0x28, 0x28, 0x20, 0x48, 0x24,
    0x48, 0x00, 0x81, 0x22, 0x14, 0x62, 0x78, 0x42, 0xA1, 0x50, 0x28, 0x14, 0x09, 0x04, 0x42, 0x20,
    0x18, 0x86, 0x24, 0x14, 0x40, 0x10, 0x18, 0x23, 0x80, 0x47, 0x82, 0x88, 0xA0, 0x0F, 0x04, 0x01,
    0x01, 0x01, 0x8A, 0x02, 0x22, 0x93, 0x42, 0x28, 0x42, 0x21, 0x88, 0x12, 0x44, 0x12, 0xA0, 0x22,
    0x08, 0x89, 0x20, 0x44, 0x02, 0x44, 0xA8, 0x2A, 0x48, 0x00, 0x41, 0x24, 0x28, 0x20, 0x48, 0x48,
    0x44, 0x01, 0x01, 0x20, 0x14, 0x02, 0x00, 0x02, 0xA1, 0x50, 0x28, 0x10, 0x0A, 0x04, 0x42, 0x20,
    0x24, 0x00, 0x20, 0x12, 0x08, 0x84, 0x42, 0x25, 0x0F, 0x00, 0x00, 0x88, 0x20, 0x42, 0x04, 0x11,
    0x11, 0x89, 0x44, 0x00, 0x12, 0x8A, 0x40, 0x28, 0x41, 0x20, 0x88, 0x10, 0x44, 0x12, 0x3E, 0x22,
    0x08, 0x89, 0x20, 0x46, 0xC2, 0xE7, 0x29, 0x29, 0xC8, 0x07, 0x41, 0x24, 0x28, 0x2C, 0xE8, 0x89,
    0xC2, 0x00, 0x21, 0x28, 0x14, 0xF2, 0x78, 0x02, 0xA0, 0x50, 0x28, 0x10, 0x0A, 0x04, 0x42, 0x20,
    0x81, 0x0F, 0x28, 0x11, 0x0A, 0x8C, 0x46, 0xA9, 0x90, 0xC7, 0xF3, 0x09, 0x20, 0x42, 0x04, 0x11,
    0x11, 0x89, 0x20, 0x7C, 0x10, 0x8B, 0x40, 0x08, 0xF1, 0x21, 0x88, 0x10, 0x44, 0x12, 0x21, 0x22,
    0xF8, 0x89, 0x23, 0x85, 0x29, 0x44, 0x28, 0x49, 0x44, 0xC8, 0x87, 0x22, 0x29, 0x2C, 0x48, 0x08,
    0x41, 0x00, 0x21, 0xC8, 0xE3, 0x0B, 0x85, 0x02, 0x90, 0xD0, 0x27, 0x10, 0x7A, 0x3C, 0x7E, 0x20,
    0x81, 0x10, 0xE8, 0x10, 0x0A, 0x94, 0x4A, 0xB1, 0x50, 0x28, 0x14, 0x72, 0x20, 0x42, 0x04, 0x11,
    0x11, 0x89, 0x10, 0x84, 0x08, 0x84, 0x40, 0x88, 0x40, 0xC0, 0x86, 0x10, 0x44, 0x12, 0x21, 0xFE,
    0x08, 0x89, 0xA0, 0x44, 0x29, 0x44, 0x28, 0x48, 0x44, 0x08, 0x01, 0x21, 0x29, 0x22, 0x48, 0xC8,
    0x67, 0x00, 0x21, 0x29, 0x04, 0x0A, 0x85, 0x72, 0x88, 0x5F, 0x28, 0x10, 0x0A, 0x04, 0x42, 0x20,
    0x81, 0x10, 0x28, 0x11, 0x09, 0xA4, 0x52, 0xA1, 0x50, 0x28, 0x14, 0x82, 0x20, 0x42, 0x04, 0x11,
    0x11, 0x89, 0x08, 0x84, 0x68, 0xA4, 0x40, 0x88, 0x44, 0x20, 0x88, 0x10, 0x44, 0x12, 0x21, 0x22,
    0x08, 0x89, 0x60, 0x24, 0x26, 0x44, 0x28, 0x88, 0x42, 0x08, 0x01, 0x21, 0xE9, 0x21, 0x48, 0x08,
    0x41, 0x00, 0x21, 0x29, 0x04, 0xFA, 0xFD, 0x42, 0x84, 0x50, 0x28, 0x10, 0x0A, 0x04, 0x42, 0x20,
    0x9F, 0x10, 0x28, 0xF2, 0x08, 0xC4, 0x62, 0xA1, 0x50, 0x28, 0x14, 0x82, 0x20, 0x42, 0x04, 0x11,
    0x11, 0x89, 0x44, 0x84, 0x44, 0x92, 0x40, 0x4A, 0x42, 0x20, 0x88, 0x10, 0x44, 0x12, 0x21, 0x22,
    0x08, 0x89, 0x20, 0x24, 0x24, 0x44, 0x28, 0x88, 0x42, 0x08, 0x01, 0xA1, 0x2A, 0x22, 0x48, 0xC8,
    0x47, 0x00, 0x21, 0x29, 0x04, 0x0A, 0x04, 0x42, 0x80, 0x50, 0x28, 0x10, 0x0A, 0x04, 0x42, 0x21,
    0xA1, 0x0F, 0x28, 0x14, 0x09, 0x84, 0x42, 0xA1, 0x50, 0x28, 0xF4, 0x89, 0x20, 0x4C, 0x04, 0x11,
    0x11, 0x89, 0xA2, 0x84, 0x24, 0x8A, 0x40, 0x4C, 0x41, 0x20, 0x88, 0x10, 0x44, 0x12, 0x21, 0x22,
    0x08, 0x89, 0x30, 0x24, 0x26, 0x44, 0x24, 0x08, 0x41, 0x08, 0x01, 0x61, 0x2C, 0xA4, 0x48, 0x04,
    0x41, 0x00, 0xA1, 0x2A, 0x14, 0x0A, 0x85, 0x42, 0x84, 0x50, 0x28, 0x14, 0x09, 0x04, 0x42, 0x21,
    0xA1, 0x08, 0x28, 0x14, 0x0A, 0x84, 0x42, 0xA1, 0x50, 0x28, 0x14, 0x89, 0x22, 0x40, 0x04, 0x11,
    0x11, 0x89, 0xA0, 0x84, 0x12, 0xB9, 0x80, 0x2F, 0x47, 0x20, 0x88, 0x20, 0x42, 0x11, 0x3E, 0xE2,
    0x09, 0xF1, 0xD7, 0xC3, 0xC9, 0xC7, 0x23, 0x08, 0x41, 0x08, 0x01, 0x21, 0x28, 0x28, 0xC7, 0x03,
    0xC1, 0x0F, 0x41, 0xC4, 0xE3, 0xF1, 0x78, 0x3C, 0x84, 0xD0, 0xC7, 0xF3, 0xF8, 0x05, 0x42, 0x1E,
    0xA1, 0x50, 0x28, 0x14, 0x0A, 0x84, 0x42, 0xA1, 0x50, 0x28, 0x14, 0x72, 0x22, 0x44, 0x04, 0x11,
    0x91, 0x88, 0x40, 0x84, 0x72, 0xA1, 0x1F, 0x28, 0x84, 0x19, 0x30, 0x41, 0x71, 0x76, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0xA1, 0x50, 0x08, 0x41, 0x08, 0x84, 0x42, 0xA0, 0x50, 0x28, 0x14, 0x22, 0x1C, 0x46, 0x08, 0x11,
    0x11, 0x8B, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xC2,
    0x20, 0x78, 0x28, 0x92, 0x08, 0x03, 0x42, 0x20, 0x30, 0x28, 0x0C, 0x84, 0xC4, 0xC1, 0x90, 0x90,
    0x18, 0x1E, 0x0F, 0xC4, 0x03, 0xF9, 0x21, 0x38, 0x8C, 0x9F, 0xC4, 0x81, 0x18, 0x44, 0x0A, 0x02,
    0xA1, 0x90, 0x87, 0x61, 0xF8, 0x85, 0x42, 0x18, 0x8F, 0xC7, 0x13, 0x02, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0xF8, 0x48, 0x7C, 0x92, 0x10, 0x43, 0x02, 0x21, 0x79, 0x20, 0x04, 0x06, 0x02, 0x21,
    0x41, 0x84, 0x10, 0x8C, 0x88, 0x04, 0x81, 0x40, 0x30, 0x14, 0x12, 0x84, 0x27, 0xC2, 0x60, 0x48,
    0x18, 0xA1, 0x10, 0x22, 0x84, 0x09, 0x10, 0x04, 0x0C, 0x10, 0x23, 0x82, 0x04, 0x82, 0x09, 0x02,
    0x21, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xE0, 0x78, 0x02, 0x01, 0x42,
    0x00, 0xC1, 0x08, 0x31, 0x42, 0x8C, 0x90, 0xC4, 0xE3, 0x91, 0x84, 0x10, 0x08, 0x09, 0x01, 0x00,
    0x00, 0x04, 0x28, 0x80, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x20, 0x02, 0x00, 0x00,
    0x00, 0x31, 0x10, 0x00, 0x44, 0x09, 0x00, 0x02, 0x00, 0x10, 0x20, 0x40, 0x04, 0x00, 0x68, 0x04,
    0x80, 0x40, 0xC8, 0xF3, 0xF9, 0x78, 0x42, 0xA1, 0x5F, 0x40, 0x02, 0x12, 0x85, 0x02, 0x01, 0x42,
    0x80, 0x20, 0x09, 0x01, 0x42, 0x80, 0x10, 0x00, 0x90, 0x02, 0x04, 0x00, 0x00, 0x00, 0xC0, 0xE3,
    0xF1, 0x78, 0x20, 0x9E, 0x88, 0xC7, 0xE3, 0xF1, 0x78, 0x42, 0x1E, 0x44, 0x28, 0x12, 0xF2, 0x84,
    0x3C, 0x29, 0x88, 0x07, 0x24, 0x09, 0xFC, 0x02, 0x3F, 0xD0, 0x2F, 0x40, 0x84, 0xAE, 0x9B, 0x04,
    0xA1, 0xC0, 0x28, 0x14, 0x0A, 0x85, 0x42, 0x21, 0x50, 0x80, 0x01, 0x12, 0x84, 0x02, 0x01, 0x40,
    0x00, 0x00, 0x08, 0x7D, 0x42, 0x9E, 0x50, 0xC8, 0x93, 0xF0, 0x78, 0x42, 0x21, 0x8F, 0x27, 0x14,
    0x0A, 0x81, 0x5C, 0xA1, 0x48, 0x28, 0x14, 0x02, 0x81, 0x42, 0x20, 0x42, 0x28, 0x11, 0x0A, 0x85,
    0x42, 0x29, 0x04, 0x88, 0x13, 0xF9, 0x40, 0x3E, 0x10, 0x08, 0x24, 0x20, 0x8E, 0x61, 0x88, 0x08,
    0xA1, 0x40, 0x29, 0x14, 0x0A, 0x05, 0x42, 0x21, 0x50, 0x00, 0x00, 0x12, 0x84, 0x3E, 0x01, 0x4F,
    0xE8, 0xE3, 0x09, 0x85, 0x42, 0xA0, 0x50, 0x28, 0x94, 0x08, 0x85, 0x42, 0xA1, 0x50, 0x28, 0x14,
    0x08, 0x85, 0x62, 0x81, 0x48, 0x28, 0x14, 0xF2, 0xF9, 0x42, 0x3E, 0x41, 0x28, 0x12, 0x0A, 0x85,
    0x42, 0x25, 0x82, 0x0F, 0xF4, 0x03, 0x21, 0x42, 0x08, 0x04, 0x22, 0x20, 0x84, 0x20, 0x88, 0x08,
    0x91, 0x40, 0x2A, 0x14, 0x0A, 0x09, 0x42, 0x12, 0xC8, 0xC7, 0xE3, 0x7B, 0x84, 0x42, 0x8D, 0x50,
    0x28, 0x14, 0x0A, 0x85, 0x42, 0xBE, 0x58, 0x28, 0x94, 0x08, 0x85, 0x42, 0xA1, 0x50, 0x20, 0xE4,
    0x09, 0x79, 0x42, 0x9E, 0xD7, 0xEF, 0xF7, 0x0B, 0x85, 0x42, 0xA1, 0x40, 0x28, 0x14, 0xFA, 0x85,
    0x7E, 0x25, 0x41, 0x08, 0x04, 0x01, 0x11, 0x42, 0x04, 0x02, 0x21, 0x10, 0x84, 0x20, 0x98, 0x10,
    0x89, 0x40, 0x2C, 0xF4, 0xF9, 0x30, 0x42, 0x0C, 0x44, 0x28, 0x14, 0x12, 0xC4, 0x42, 0x8D, 0x40,
    0x24, 0x14, 0xF8, 0x84, 0x7C, 0x21, 0x57, 0x28, 0x54, 0x0A, 0x79, 0x42, 0xA1, 0x90, 0x27, 0x04,
    0x0A, 0x01, 0x42, 0xA0, 0x40, 0x20, 0x10, 0x08, 0x85, 0x42, 0xA1, 0x50, 0x28, 0x14, 0x0A, 0x84,
    0x02, 0xA3, 0x40, 0x08, 0x04, 0x01, 0x09, 0x42, 0x02, 0x82, 0x20, 0x10, 0x84, 0x20, 0x68, 0x10,
    0x87, 0x40, 0x28, 0x14, 0x88, 0x40, 0x42, 0x0C, 0x42, 0x28, 0x10, 0x12, 0xB8, 0x42, 0x81, 0xC0,
    0x23, 0x14, 0x08, 0x84, 0x40, 0x21, 0x50, 0x28, 0xE4, 0x09, 0x81, 0x42, 0xA1, 0x10, 0x28, 0x14,
    0x0A, 0x21, 0x42, 0xA1, 0x40, 0x28, 0x14, 0x0A, 0x85, 0x42, 0xA1, 0x50, 0x28, 0x14, 0x0A, 0x85,
    0x42, 0xA1, 0x40, 0x28, 0x04, 0x09, 0x05, 0x42, 0x01, 0x42, 0x20, 0x08, 0x84, 0x20, 0x08, 0x20,
    0x89, 0x40, 0x28, 0x14, 0x08, 0x81, 0x42, 0x12, 0x41, 0x28, 0x10, 0x12, 0x80, 0x42, 0x81, 0x40,
    0x24, 0x14, 0x08, 0x84, 0x40, 0x21, 0x48, 0x28, 0x44, 0x08, 0x85, 0x42, 0xA1, 0x50, 0xC8, 0xE3,
    0xF1, 0x30, 0x7C, 0x9E, 0x80, 0xC7, 0xE3, 0xF1, 0xF9, 0x7C, 0x3E, 0x8F, 0x2F, 0xE3, 0xF3, 0xF8,
    0x3C, 0x9E, 0x9F, 0xCF, 0x03, 0xF1, 0xFC, 0x3C, 0x3F, 0xC2, 0x2F, 0x08, 0xBA, 0x20, 0x08, 0x20,
    0x91, 0x40, 0x28, 0x14, 0x08, 0x81, 0x42, 0xA1, 0x40, 0x28, 0x10, 0x12, 0x84, 0x42, 0x81, 0x50,
    0x28, 0x14, 0x0A, 0x84, 0x40, 0xBE, 0x87, 0xCF, 0x23, 0xF0, 0x78, 0x7C, 0x3E, 0x8F, 0x07, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0xA1, 0x40, 0x28, 0x14, 0x08, 0x85, 0x42, 0xA1, 0x40, 0x28, 0x14, 0x12, 0x78, 0x42, 0x3F, 0x4F,
    0x28, 0xE4, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xBA,
    0xBA, 0xF4, 0xE7, 0x83, 0x64, 0x93, 0x70, 0x13, 0x14, 0x14, 0x14, 0xF2, 0x78, 0x3C, 0x1E, 0xCF,
    0xE7, 0x47, 0x10, 0x5D, 0x85, 0xCC, 0xFD, 0x24, 0x84, 0x31, 0x66, 0xF4, 0x23, 0xA4, 0xA4, 0x24,
    0xA1, 0x5F, 0xC8, 0x13, 0x08, 0x79, 0x3C, 0xA1, 0xDF, 0xC7, 0xE3, 0x03, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0xE0, 0x91, 0x7C, 0x3C, 0x1E, 0x89, 0xC4, 0xF3, 0xF1, 0x78, 0x02, 0x02, 0x22, 0x06,
    0x00, 0x24, 0x82, 0x44, 0x92, 0x24, 0x91, 0x14, 0x14, 0x24, 0x12, 0x02, 0x85, 0x42, 0xA1, 0x50,
    0x08, 0x42, 0x10, 0x43, 0x49, 0x12, 0x81, 0x18, 0x44, 0x4A, 0x95, 0x04, 0x50, 0xA8, 0xA4, 0x24,
    0x21, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x3C, 0x3C, 0x24, 0x10,
    0x8F, 0x24, 0x14, 0x02, 0x84, 0x00, 0x00, 0x00, 0x00, 0x10, 0x02, 0x00, 0x04, 0x02, 0x21, 0x02,
    0x91, 0x20, 0xE2, 0x27, 0x91, 0x44, 0x92, 0x24, 0x92, 0x44, 0x11, 0xF2, 0x05, 0x42, 0xA1, 0x40,
    0x08, 0xF1, 0x01, 0x01, 0x31, 0xD2, 0x81, 0x7E, 0x1F, 0x21, 0x94, 0xF4, 0x8B, 0xA6, 0x12, 0x01,
    0x00, 0x08, 0x15, 0xA4, 0x94, 0x54, 0xC5, 0x83, 0x07, 0x20, 0x1E, 0x81, 0x42, 0x40, 0x24, 0x08,
    0x00, 0x20, 0x14, 0xF0, 0x84, 0x3E, 0x1E, 0x4F, 0x28, 0x14, 0xF2, 0x78, 0x88, 0x8F, 0xF8, 0x02,
    0x91, 0x20, 0x92, 0x90, 0x90, 0x87, 0x94, 0x24, 0x92, 0x84, 0x10, 0x0A, 0x05, 0x7E, 0x21, 0x4F,
    0x88, 0x40, 0x7C, 0x01, 0x31, 0xCC, 0x81, 0x18, 0x84, 0x48, 0x64, 0x02, 0x00, 0x00, 0x00, 0x00,
    0x28, 0x88, 0x12, 0xEA, 0xA2, 0x54, 0x25, 0x44, 0x08, 0x1F, 0x21, 0x5A, 0x42, 0x7C, 0x7E, 0x1E,
    0x8F, 0x27, 0x14, 0x08, 0x85, 0x42, 0xA1, 0x50, 0x28, 0x14, 0x02, 0x85, 0x10, 0x42, 0x20, 0x02,
    0x91, 0x20, 0x92, 0x20, 0x91, 0x40, 0x92, 0x44, 0x91, 0x44, 0x11, 0x0A, 0x05, 0x02, 0x21, 0x50,
    0x48, 0x40, 0x00, 0x01, 0x49, 0x00, 0x80, 0x24, 0xC4, 0x33, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x2C, 0x28, 0x10, 0x00, 0x80, 0x54, 0x94, 0xA9, 0x93, 0x98, 0x4C, 0x24, 0x7E, 0x42, 0x24, 0x21,
    0x10, 0x28, 0x14, 0x08, 0x85, 0x42, 0xA1, 0x50, 0x28, 0x14, 0xF2, 0x85, 0x20, 0x22, 0x20, 0x02,
    0x91, 0x24, 0x92, 0x44, 0x92, 0x24, 0x91, 0x44, 0x51, 0x25, 0x12, 0x0A, 0x85, 0x42, 0xA1, 0x50,
    0x28, 0x00, 0x10, 0x41, 0x85, 0xDE, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x2A, 0x18, 0x71, 0x44, 0xA2, 0x54, 0x15, 0xAA, 0x94, 0x94, 0x52, 0x42, 0x02, 0x42, 0x24, 0x01,
    0x9F, 0x2F, 0x14, 0xFA, 0x85, 0x42, 0xA1, 0x50, 0x28, 0x14, 0x0A, 0x85, 0x10, 0x47, 0x20, 0x02,
    0x91, 0x24, 0xE2, 0x83, 0x64, 0x93, 0x10, 0x84, 0x20, 0x12, 0xE4, 0xF3, 0x79, 0x3C, 0x1E, 0x4F,
    0xE8, 0xF7, 0x11, 0x41, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x29, 0x08, 0x91, 0x44, 0xA2, 0x14, 0x95, 0xAB, 0x93, 0x92, 0x42, 0x42, 0x42, 0x42, 0x24, 0x81,
    0x50, 0x28, 0xE4, 0x09, 0x84, 0x42, 0xA1, 0x50, 0x28, 0x14, 0x0A, 0x85, 0x08, 0x82, 0x20, 0x08,
    0x91, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0xA8, 0x0D, 0x91, 0x44, 0xA2, 0x54, 0x55, 0xAA, 0x94, 0x92, 0x52, 0x42, 0x3C, 0x7C, 0x7E, 0x81,
    0x50, 0xC8, 0x87, 0x08, 0x21, 0x42, 0xA1, 0x50, 0x28, 0x04, 0x0A, 0x85, 0x04, 0x02, 0x21, 0x0C,
    0x91, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0xA8, 0x09, 0x71, 0x44, 0xA2, 0x54, 0x95, 0xA7, 0x94, 0x91, 0x4C, 0x24, 0x40, 0x40, 0x24, 0xA1,
    0x50, 0x08, 0xC2, 0xF0, 0x30, 0x42, 0x1E, 0x8F, 0xCF, 0x87, 0xF1, 0x79, 0x02, 0x0C, 0xC2, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x28, 0x08, 0x11, 0x44, 0xA2, 0x50, 0x25, 0x40, 0x88, 0x0F, 0x21, 0x5A, 0xC0, 0xC0, 0x24, 0x1E,
    0x9F, 0x0F, 0x06, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x28, 0x08, 0x11, 0x44, 0xA2, 0x54, 0xC5, 0x87, 0x47, 0x00, 0x1E, 0x81, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x28, 0x10, 0x11, 0x44, 0x22, 0x55, 0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x48, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0xF8, 0x7D, 0x0F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0xA6, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x99, 0x02
};


GFX_INTERNAL int64_t GFX__default_font_ranges[GFX__DEFAULT_FONT_TOTAL_RANGES*2] = {
    32, 127,
    160, 172,
    174, 383
};


typedef struct {
    uint16_t x, y;
    uint8_t w, h, xoff, yoff, xadv;
} GFX__CompressedGlyphInfo;

GFX_INTERNAL GFX__CompressedGlyphInfo
GFX__default_compressed_glyph_info[GFX__DEFAULT_FONT_TOTAL_GLYPHS] = {
    {.x = 31, .y = 49, .w = 1, .h = 1, .xoff = 0, .yoff = 0, .xadv = 4},
    {.x = 42, .y = 38, .w = 1, .h = 10, .xoff = 0, .yoff = 2, .xadv = 2},
    {.x = 493, .y = 35, .w = 3, .h = 3, .xoff = 0, .yoff = 2, .xadv = 4},
    {.x = 113, .y = 37, .w = 6, .h = 9, .xoff = 0, .yoff = 3, .xadv = 7},
    {.x = 91, .y = 13, .w = 5, .h = 12, .xoff = 0, .yoff = 1, .xadv = 6},
    {.x = 145, .y = 13, .w = 7, .h = 11, .xoff = 0, .yoff = 2, .xadv = 8},
    {.x = 285, .y = 13, .w = 7, .h = 10, .xoff = 0, .yoff = 2, .xadv = 8},
    {.x = 497, .y = 35, .w = 2, .h = 3, .xoff = 0, .yoff = 2, .xadv = 3},
    {.x = 220, .y = 13, .w = 3, .h = 11, .xoff = 0, .yoff = 2, .xadv = 4},
    {.x = 224, .y = 13, .w = 3, .h = 11, .xoff = 0, .yoff = 2, .xadv = 4},
    {.x = 441, .y = 35, .w = 6, .h = 5, .xoff = 0, .yoff = 5, .xadv = 7},
    {.x = 448, .y = 35, .w = 5, .h = 5, .xoff = 0, .yoff = 5, .xadv = 6},
    {.x = 500, .y = 35, .w = 2, .h = 3, .xoff = 0, .yoff = 10, .xadv = 3},
    {.x = 24, .y = 49, .w = 4, .h = 1, .xoff = 0, .yoff = 7, .xadv = 5},
    {.x = 7, .y = 50, .w = 1, .h = 2, .xoff = 0, .yoff = 10, .xadv = 2},
    {.x = 475, .y = 24, .w = 5, .h = 10, .xoff = 0, .yoff = 2, .xadv = 6},
    {.x = 392, .y = 24, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 0, .y = 39, .w = 4, .h = 10, .xoff = 0, .yoff = 2, .xadv = 5},
    {.x = 399, .y = 24, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 413, .y = 24, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 420, .y = 24, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 427, .y = 24, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 441, .y = 24, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 455, .y = 24, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 413, .y = 13, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 420, .y = 13, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 414, .y = 35, .w = 1, .h = 7, .xoff = 0, .yoff = 5, .xadv = 2},
    {.x = 265, .y = 35, .w = 2, .h = 8, .xoff = 0, .yoff = 5, .xadv = 3},
    {.x = 237, .y = 36, .w = 5, .h = 9, .xoff = 0, .yoff = 3, .xadv = 6},
    {.x = 476, .y = 35, .w = 6, .h = 3, .xoff = 0, .yoff = 6, .xadv = 7},
    {.x = 225, .y = 36, .w = 5, .h = 9, .xoff = 0, .yoff = 3, .xadv = 6},
    {.x = 448, .y = 13, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 52, .y = 38, .w = 8, .h = 9, .xoff = 0, .yoff = 3, .xadv = 9},
    {.x = 455, .y = 13, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 462, .y = 13, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 469, .y = 13, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 476, .y = 13, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 483, .y = 13, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 490, .y = 13, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 441, .y = 13, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 497, .y = 13, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 50, .y = 38, .w = 1, .h = 10, .xoff = 0, .yoff = 2, .xadv = 2},
    {.x = 504, .y = 13, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 0, .y = 28, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 7, .y = 27, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 309, .y = 13, .w = 7, .h = 10, .xoff = 0, .yoff = 2, .xadv = 8},
    {.x = 14, .y = 27, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 21, .y = 27, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 28, .y = 27, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 182, .y = 13, .w = 6, .h = 11, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 35, .y = 27, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 42, .y = 27, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 397, .y = 13, .w = 7, .h = 10, .xoff = 0, .yoff = 2, .xadv = 8},
    {.x = 49, .y = 27, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 317, .y = 13, .w = 7, .h = 10, .xoff = 0, .yoff = 2, .xadv = 8},
    {.x = 349, .y = 13, .w = 7, .h = 10, .xoff = 0, .yoff = 2, .xadv = 8},
    {.x = 56, .y = 27, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 341, .y = 13, .w = 7, .h = 10, .xoff = 0, .yoff = 2, .xadv = 8},
    {.x = 63, .y = 27, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 236, .y = 13, .w = 3, .h = 11, .xoff = 0, .yoff = 2, .xadv = 4},
    {.x = 505, .y = 24, .w = 5, .h = 10, .xoff = 0, .yoff = 2, .xadv = 6},
    {.x = 228, .y = 13, .w = 3, .h = 11, .xoff = 0, .yoff = 2, .xadv = 4},
    {.x = 483, .y = 35, .w = 5, .h = 3, .xoff = 0, .yoff = 1, .xadv = 6},
    {.x = 11, .y = 49, .w = 6, .h = 1, .xoff = 0, .yoff = 13, .xadv = 7},
    {.x = 503, .y = 35, .w = 2, .h = 3, .xoff = 0, .yoff = 2, .xadv = 3},
    {.x = 347, .y = 35, .w = 6, .h = 7, .xoff = 0, .yoff = 5, .xadv = 7},
    {.x = 70, .y = 27, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 354, .y = 35, .w = 6, .h = 7, .xoff = 0, .yoff = 5, .xadv = 7},
    {.x = 84, .y = 27, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 361, .y = 35, .w = 6, .h = 7, .xoff = 0, .yoff = 5, .xadv = 7},
    {.x = 91, .y = 26, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 98, .y = 26, .w = 6, .h = 10, .xoff = 0, .yoff = 5, .xadv = 7},
    {.x = 105, .y = 26, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 263, .y = 35, .w = 1, .h = 9, .xoff = 0, .yoff = 3, .xadv = 2},
    {.x = 97, .y = 13, .w = 5, .h = 12, .xoff = 0, .yoff = 3, .xadv = 6},
    {.x = 126, .y = 26, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 39, .y = 38, .w = 2, .h = 10, .xoff = 0, .yoff = 2, .xadv = 3},
    {.x = 308, .y = 35, .w = 7, .h = 7, .xoff = 0, .yoff = 5, .xadv = 8},
    {.x = 382, .y = 35, .w = 6, .h = 7, .xoff = 0, .yoff = 5, .xadv = 7},
    {.x = 368, .y = 35, .w = 6, .h = 7, .xoff = 0, .yoff = 5, .xadv = 7},
    {.x = 147, .y = 25, .w = 6, .h = 10, .xoff = 0, .yoff = 5, .xadv = 7},
    {.x = 161, .y = 25, .w = 6, .h = 10, .xoff = 0, .yoff = 5, .xadv = 7},
    {.x = 408, .y = 35, .w = 5, .h = 7, .xoff = 0, .yoff = 5, .xadv = 6},
    {.x = 375, .y = 35, .w = 6, .h = 7, .xoff = 0, .yoff = 5, .xadv = 7},
    {.x = 243, .y = 36, .w = 5, .h = 9, .xoff = 0, .yoff = 3, .xadv = 6},
    {.x = 340, .y = 35, .w = 6, .h = 7, .xoff = 0, .yoff = 5, .xadv = 7},
    {.x = 316, .y = 35, .w = 7, .h = 7, .xoff = 0, .yoff = 5, .xadv = 8},
    {.x = 324, .y = 35, .w = 7, .h = 7, .xoff = 0, .yoff = 5, .xadv = 8},
    {.x = 332, .y = 35, .w = 7, .h = 7, .xoff = 0, .yoff = 5, .xadv = 8},
    {.x = 175, .y = 25, .w = 6, .h = 10, .xoff = 0, .yoff = 5, .xadv = 7},
    {.x = 389, .y = 35, .w = 6, .h = 7, .xoff = 0, .yoff = 5, .xadv = 7},
    {.x = 209, .y = 13, .w = 5, .h = 11, .xoff = 0, .yoff = 2, .xadv = 6},
    {.x = 46, .y = 38, .w = 1, .h = 10, .xoff = 0, .yoff = 2, .xadv = 2},
    {.x = 203, .y = 13, .w = 5, .h = 11, .xoff = 0, .yoff = 2, .xadv = 6},
    {.x = 0, .y = 50, .w = 6, .h = 2, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 151, .y = 0, .w = 7, .h = 12, .xoff = 0, .yoff = 1, .xadv = 8},
    {.x = 29, .y = 49, .w = 1, .h = 1, .xoff = 0, .yoff = 0, .xadv = 4},
    {.x = 48, .y = 38, .w = 1, .h = 10, .xoff = 0, .yoff = 2, .xadv = 2},
    {.x = 196, .y = 25, .w = 6, .h = 10, .xoff = 0, .yoff = 4, .xadv = 7},
    {.x = 481, .y = 24, .w = 5, .h = 10, .xoff = 0, .yoff = 2, .xadv = 6},
    {.x = 88, .y = 38, .w = 8, .h = 9, .xoff = 0, .yoff = 3, .xadv = 9},
    {.x = 381, .y = 13, .w = 7, .h = 10, .xoff = 0, .yoff = 2, .xadv = 8},
    {.x = 44, .y = 38, .w = 1, .h = 10, .xoff = 0, .yoff = 2, .xadv = 2},
    {.x = 210, .y = 25, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 506, .y = 35, .w = 4, .h = 2, .xoff = 0, .yoff = 2, .xadv = 5},
    {.x = 79, .y = 38, .w = 8, .h = 9, .xoff = 0, .yoff = 3, .xadv = 9},
    {.x = 430, .y = 35, .w = 3, .h = 6, .xoff = 0, .yoff = 2, .xadv = 4},
    {.x = 284, .y = 35, .w = 7, .h = 7, .xoff = 0, .yoff = 4, .xadv = 8},
    {.x = 434, .y = 35, .w = 6, .h = 5, .xoff = 0, .yoff = 6, .xadv = 7},
    {.x = 61, .y = 38, .w = 8, .h = 9, .xoff = 0, .yoff = 3, .xadv = 9},
    {.x = 18, .y = 49, .w = 5, .h = 1, .xoff = 0, .yoff = 3, .xadv = 6},
    {.x = 468, .y = 35, .w = 4, .h = 4, .xoff = 0, .yoff = 2, .xadv = 5},
    {.x = 396, .y = 35, .w = 5, .h = 7, .xoff = 0, .yoff = 5, .xadv = 6},
    {.x = 454, .y = 35, .w = 4, .h = 5, .xoff = 0, .yoff = 2, .xadv = 5},
    {.x = 459, .y = 35, .w = 4, .h = 5, .xoff = 0, .yoff = 2, .xadv = 5},
    {.x = 473, .y = 35, .w = 2, .h = 4, .xoff = 0, .yoff = 1, .xadv = 3},
    {.x = 287, .y = 24, .w = 6, .h = 10, .xoff = 0, .yoff = 5, .xadv = 7},
    {.x = 268, .y = 35, .w = 7, .h = 7, .xoff = 0, .yoff = 5, .xadv = 8},
    {.x = 9, .y = 50, .w = 1, .h = 2, .xoff = 0, .yoff = 6, .xadv = 2},
    {.x = 489, .y = 35, .w = 3, .h = 3, .xoff = 0, .yoff = 10, .xadv = 4},
    {.x = 464, .y = 35, .w = 3, .h = 5, .xoff = 0, .yoff = 2, .xadv = 4},
    {.x = 425, .y = 35, .w = 4, .h = 6, .xoff = 0, .yoff = 2, .xadv = 5},
    {.x = 300, .y = 35, .w = 7, .h = 7, .xoff = 0, .yoff = 4, .xadv = 8},
    {.x = 189, .y = 13, .w = 6, .h = 11, .xoff = 0, .yoff = 1, .xadv = 7},
    {.x = 161, .y = 13, .w = 6, .h = 11, .xoff = 0, .yoff = 1, .xadv = 7},
    {.x = 168, .y = 13, .w = 6, .h = 11, .xoff = 0, .yoff = 1, .xadv = 7},
    {.x = 343, .y = 24, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 369, .y = 0, .w = 6, .h = 12, .xoff = 0, .yoff = 0, .xadv = 7},
    {.x = 285, .y = 0, .w = 6, .h = 12, .xoff = 0, .yoff = 0, .xadv = 7},
    {.x = 397, .y = 0, .w = 6, .h = 12, .xoff = 0, .yoff = 0, .xadv = 7},
    {.x = 355, .y = 0, .w = 6, .h = 12, .xoff = 0, .yoff = 0, .xadv = 7},
    {.x = 334, .y = 0, .w = 6, .h = 12, .xoff = 0, .yoff = 0, .xadv = 7},
    {.x = 320, .y = 0, .w = 6, .h = 12, .xoff = 0, .yoff = 0, .xadv = 7},
    {.x = 249, .y = 13, .w = 8, .h = 10, .xoff = 0, .yoff = 2, .xadv = 9},
    {.x = 57, .y = 0, .w = 6, .h = 13, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 390, .y = 0, .w = 6, .h = 12, .xoff = 0, .yoff = 0, .xadv = 7},
    {.x = 341, .y = 0, .w = 6, .h = 12, .xoff = 0, .yoff = 0, .xadv = 7},
    {.x = 173, .y = 0, .w = 6, .h = 12, .xoff = 0, .yoff = 0, .xadv = 7},
    {.x = 383, .y = 0, .w = 6, .h = 12, .xoff = 0, .yoff = 0, .xadv = 7},
    {.x = 142, .y = 13, .w = 2, .h = 12, .xoff = 0, .yoff = 0, .xadv = 3},
    {.x = 509, .y = 0, .w = 2, .h = 12, .xoff = 0, .yoff = 0, .xadv = 3},
    {.x = 123, .y = 13, .w = 3, .h = 12, .xoff = 0, .yoff = 0, .xadv = 4},
    {.x = 119, .y = 13, .w = 3, .h = 12, .xoff = 0, .yoff = 0, .xadv = 4},
    {.x = 301, .y = 13, .w = 7, .h = 10, .xoff = 0, .yoff = 2, .xadv = 8},
    {.x = 166, .y = 0, .w = 6, .h = 12, .xoff = 0, .yoff = 0, .xadv = 7},
    {.x = 432, .y = 0, .w = 6, .h = 12, .xoff = 0, .yoff = 0, .xadv = 7},
    {.x = 439, .y = 0, .w = 6, .h = 12, .xoff = 0, .yoff = 0, .xadv = 7},
    {.x = 446, .y = 0, .w = 6, .h = 12, .xoff = 0, .yoff = 0, .xadv = 7},
    {.x = 453, .y = 0, .w = 6, .h = 12, .xoff = 0, .yoff = 0, .xadv = 7},
    {.x = 460, .y = 0, .w = 6, .h = 12, .xoff = 0, .yoff = 0, .xadv = 7},
    {.x = 418, .y = 35, .w = 6, .h = 6, .xoff = 0, .yoff = 6, .xadv = 7},
    {.x = 276, .y = 13, .w = 8, .h = 10, .xoff = 0, .yoff = 2, .xadv = 9},
    {.x = 425, .y = 0, .w = 6, .h = 12, .xoff = 0, .yoff = 0, .xadv = 7},
    {.x = 467, .y = 0, .w = 6, .h = 12, .xoff = 0, .yoff = 0, .xadv = 7},
    {.x = 257, .y = 0, .w = 6, .h = 12, .xoff = 0, .yoff = 0, .xadv = 7},
    {.x = 481, .y = 0, .w = 6, .h = 12, .xoff = 0, .yoff = 0, .xadv = 7},
    {.x = 103, .y = 0, .w = 7, .h = 12, .xoff = 0, .yoff = 0, .xadv = 8},
    {.x = 499, .y = 24, .w = 5, .h = 10, .xoff = 0, .yoff = 2, .xadv = 6},
    {.x = 357, .y = 24, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 315, .y = 24, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 406, .y = 24, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 336, .y = 24, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 127, .y = 37, .w = 6, .h = 9, .xoff = 0, .yoff = 3, .xadv = 7},
    {.x = 134, .y = 37, .w = 6, .h = 9, .xoff = 0, .yoff = 3, .xadv = 7},
    {.x = 322, .y = 24, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 276, .y = 35, .w = 7, .h = 7, .xoff = 0, .yoff = 5, .xadv = 8},
    {.x = 148, .y = 36, .w = 6, .h = 9, .xoff = 0, .yoff = 5, .xadv = 7},
    {.x = 308, .y = 24, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 301, .y = 24, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 294, .y = 24, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 155, .y = 36, .w = 6, .h = 9, .xoff = 0, .yoff = 3, .xadv = 7},
    {.x = 36, .y = 38, .w = 2, .h = 10, .xoff = 0, .yoff = 2, .xadv = 3},
    {.x = 33, .y = 38, .w = 2, .h = 10, .xoff = 0, .yoff = 2, .xadv = 3},
    {.x = 25, .y = 38, .w = 3, .h = 10, .xoff = 0, .yoff = 2, .xadv = 4},
    {.x = 255, .y = 35, .w = 3, .h = 9, .xoff = 0, .yoff = 3, .xadv = 4},
    {.x = 273, .y = 24, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 169, .y = 36, .w = 6, .h = 9, .xoff = 0, .yoff = 3, .xadv = 7},
    {.x = 259, .y = 24, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 245, .y = 24, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 231, .y = 25, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 176, .y = 36, .w = 6, .h = 9, .xoff = 0, .yoff = 3, .xadv = 7},
    {.x = 183, .y = 36, .w = 6, .h = 9, .xoff = 0, .yoff = 3, .xadv = 7},
    {.x = 402, .y = 35, .w = 5, .h = 7, .xoff = 0, .yoff = 4, .xadv = 6},
    {.x = 70, .y = 38, .w = 8, .h = 9, .xoff = 0, .yoff = 4, .xadv = 9},
    {.x = 224, .y = 25, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 217, .y = 25, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 182, .y = 25, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 190, .y = 36, .w = 6, .h = 9, .xoff = 0, .yoff = 3, .xadv = 7},
    {.x = 71, .y = 0, .w = 6, .h = 13, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 20, .y = 38, .w = 4, .h = 10, .xoff = 0, .yoff = 2, .xadv = 5},
    {.x = 418, .y = 0, .w = 6, .h = 12, .xoff = 0, .yoff = 3, .xadv = 7},
    {.x = 411, .y = 0, .w = 6, .h = 12, .xoff = 0, .yoff = 0, .xadv = 7},
    {.x = 211, .y = 36, .w = 6, .h = 9, .xoff = 0, .yoff = 3, .xadv = 7},
    {.x = 404, .y = 0, .w = 6, .h = 12, .xoff = 0, .yoff = 0, .xadv = 7},
    {.x = 168, .y = 25, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 95, .y = 0, .w = 7, .h = 12, .xoff = 0, .yoff = 2, .xadv = 8},
    {.x = 105, .y = 37, .w = 7, .h = 9, .xoff = 0, .yoff = 5, .xadv = 8},
    {.x = 376, .y = 0, .w = 6, .h = 12, .xoff = 0, .yoff = 0, .xadv = 7},
    {.x = 120, .y = 37, .w = 6, .h = 9, .xoff = 0, .yoff = 3, .xadv = 7},
    {.x = 362, .y = 0, .w = 6, .h = 12, .xoff = 0, .yoff = 0, .xadv = 7},
    {.x = 140, .y = 26, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 348, .y = 0, .w = 6, .h = 12, .xoff = 0, .yoff = 0, .xadv = 7},
    {.x = 119, .y = 26, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 292, .y = 0, .w = 6, .h = 12, .xoff = 0, .yoff = 0, .xadv = 7},
    {.x = 77, .y = 27, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 278, .y = 0, .w = 6, .h = 12, .xoff = 0, .yoff = 0, .xadv = 7},
    {.x = 240, .y = 13, .w = 8, .h = 10, .xoff = 0, .yoff = 2, .xadv = 9},
    {.x = 373, .y = 13, .w = 7, .h = 10, .xoff = 0, .yoff = 2, .xadv = 8},
    {.x = 293, .y = 13, .w = 7, .h = 10, .xoff = 0, .yoff = 2, .xadv = 8},
    {.x = 159, .y = 0, .w = 6, .h = 12, .xoff = 0, .yoff = 0, .xadv = 7},
    {.x = 434, .y = 13, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 327, .y = 0, .w = 6, .h = 12, .xoff = 0, .yoff = 0, .xadv = 7},
    {.x = 427, .y = 13, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 313, .y = 0, .w = 6, .h = 12, .xoff = 0, .yoff = 0, .xadv = 7},
    {.x = 385, .y = 24, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 127, .y = 0, .w = 7, .h = 12, .xoff = 0, .yoff = 2, .xadv = 8},
    {.x = 97, .y = 37, .w = 7, .h = 9, .xoff = 0, .yoff = 5, .xadv = 8},
    {.x = 229, .y = 0, .w = 6, .h = 12, .xoff = 0, .yoff = 0, .xadv = 7},
    {.x = 371, .y = 24, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 201, .y = 0, .w = 6, .h = 12, .xoff = 0, .yoff = 0, .xadv = 7},
    {.x = 15, .y = 0, .w = 6, .h = 13, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 488, .y = 0, .w = 6, .h = 12, .xoff = 0, .yoff = 0, .xadv = 7},
    {.x = 50, .y = 0, .w = 6, .h = 13, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 495, .y = 0, .w = 6, .h = 12, .xoff = 0, .yoff = 0, .xadv = 7},
    {.x = 36, .y = 0, .w = 6, .h = 13, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 29, .y = 0, .w = 6, .h = 13, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 0, .y = 0, .w = 6, .h = 14, .xoff = 0, .yoff = 1, .xadv = 7},
    {.x = 502, .y = 0, .w = 6, .h = 12, .xoff = 0, .yoff = 0, .xadv = 7},
    {.x = 0, .y = 15, .w = 6, .h = 12, .xoff = 0, .yoff = 0, .xadv = 7},
    {.x = 258, .y = 13, .w = 8, .h = 10, .xoff = 0, .yoff = 2, .xadv = 9},
    {.x = 325, .y = 13, .w = 7, .h = 10, .xoff = 0, .yoff = 2, .xadv = 8},
    {.x = 109, .y = 13, .w = 4, .h = 12, .xoff = 0, .yoff = 0, .xadv = 5},
    {.x = 15, .y = 38, .w = 4, .h = 10, .xoff = 0, .yoff = 2, .xadv = 5},
    {.x = 127, .y = 13, .w = 3, .h = 12, .xoff = 0, .yoff = 0, .xadv = 4},
    {.x = 259, .y = 35, .w = 3, .h = 9, .xoff = 0, .yoff = 3, .xadv = 4},
    {.x = 131, .y = 13, .w = 3, .h = 12, .xoff = 0, .yoff = 0, .xadv = 4},
    {.x = 29, .y = 38, .w = 3, .h = 10, .xoff = 0, .yoff = 2, .xadv = 4},
    {.x = 135, .y = 13, .w = 3, .h = 12, .xoff = 0, .yoff = 2, .xadv = 4},
    {.x = 232, .y = 13, .w = 3, .h = 11, .xoff = 0, .yoff = 3, .xadv = 4},
    {.x = 139, .y = 13, .w = 2, .h = 12, .xoff = 0, .yoff = 0, .xadv = 3},
    {.x = 416, .y = 35, .w = 1, .h = 7, .xoff = 0, .yoff = 5, .xadv = 2},
    {.x = 365, .y = 13, .w = 7, .h = 10, .xoff = 0, .yoff = 2, .xadv = 8},
    {.x = 87, .y = 0, .w = 7, .h = 12, .xoff = 0, .yoff = 3, .xadv = 8},
    {.x = 14, .y = 14, .w = 6, .h = 12, .xoff = 0, .yoff = 0, .xadv = 7},
    {.x = 78, .y = 0, .w = 5, .h = 13, .xoff = 0, .yoff = 2, .xadv = 6},
    {.x = 21, .y = 14, .w = 6, .h = 12, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 28, .y = 14, .w = 6, .h = 12, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 357, .y = 13, .w = 7, .h = 10, .xoff = 0, .yoff = 2, .xadv = 8},
    {.x = 35, .y = 14, .w = 6, .h = 12, .xoff = 0, .yoff = 0, .xadv = 7},
    {.x = 114, .y = 13, .w = 4, .h = 12, .xoff = 0, .yoff = 0, .xadv = 5},
    {.x = 64, .y = 0, .w = 6, .h = 13, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 84, .y = 0, .w = 2, .h = 13, .xoff = 0, .yoff = 2, .xadv = 3},
    {.x = 175, .y = 13, .w = 6, .h = 11, .xoff = 0, .yoff = 1, .xadv = 7},
    {.x = 215, .y = 13, .w = 4, .h = 11, .xoff = 0, .yoff = 1, .xadv = 5},
    {.x = 112, .y = 26, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 5, .y = 39, .w = 4, .h = 10, .xoff = 0, .yoff = 2, .xadv = 5},
    {.x = 389, .y = 13, .w = 7, .h = 10, .xoff = 0, .yoff = 2, .xadv = 8},
    {.x = 10, .y = 38, .w = 4, .h = 10, .xoff = 0, .yoff = 2, .xadv = 5},
    {.x = 42, .y = 14, .w = 6, .h = 12, .xoff = 0, .yoff = 0, .xadv = 7},
    {.x = 133, .y = 26, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 474, .y = 0, .w = 6, .h = 12, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 162, .y = 36, .w = 6, .h = 9, .xoff = 0, .yoff = 5, .xadv = 7},
    {.x = 49, .y = 14, .w = 6, .h = 12, .xoff = 0, .yoff = 0, .xadv = 7},
    {.x = 154, .y = 25, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 153, .y = 13, .w = 7, .h = 11, .xoff = 0, .yoff = 1, .xadv = 8},
    {.x = 56, .y = 14, .w = 6, .h = 12, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 204, .y = 36, .w = 6, .h = 9, .xoff = 0, .yoff = 5, .xadv = 7},
    {.x = 63, .y = 14, .w = 6, .h = 12, .xoff = 0, .yoff = 0, .xadv = 7},
    {.x = 218, .y = 36, .w = 6, .h = 9, .xoff = 0, .yoff = 3, .xadv = 7},
    {.x = 70, .y = 14, .w = 6, .h = 12, .xoff = 0, .yoff = 0, .xadv = 7},
    {.x = 189, .y = 25, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 77, .y = 14, .w = 6, .h = 12, .xoff = 0, .yoff = 0, .xadv = 7},
    {.x = 203, .y = 25, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 267, .y = 13, .w = 8, .h = 10, .xoff = 0, .yoff = 2, .xadv = 9},
    {.x = 292, .y = 35, .w = 7, .h = 7, .xoff = 0, .yoff = 5, .xadv = 8},
    {.x = 84, .y = 14, .w = 6, .h = 12, .xoff = 0, .yoff = 0, .xadv = 7},
    {.x = 487, .y = 24, .w = 5, .h = 10, .xoff = 0, .yoff = 2, .xadv = 6},
    {.x = 180, .y = 0, .w = 6, .h = 12, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 249, .y = 35, .w = 5, .h = 9, .xoff = 0, .yoff = 5, .xadv = 6},
    {.x = 7, .y = 14, .w = 6, .h = 12, .xoff = 0, .yoff = 0, .xadv = 7},
    {.x = 493, .y = 24, .w = 5, .h = 10, .xoff = 0, .yoff = 2, .xadv = 6},
    {.x = 187, .y = 0, .w = 6, .h = 12, .xoff = 0, .yoff = 0, .xadv = 7},
    {.x = 238, .y = 25, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 194, .y = 0, .w = 6, .h = 12, .xoff = 0, .yoff = 0, .xadv = 7},
    {.x = 252, .y = 24, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 22, .y = 0, .w = 6, .h = 13, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 266, .y = 24, .w = 6, .h = 10, .xoff = 0, .yoff = 5, .xadv = 7},
    {.x = 208, .y = 0, .w = 6, .h = 12, .xoff = 0, .yoff = 0, .xadv = 7},
    {.x = 280, .y = 24, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 7, .y = 0, .w = 7, .h = 13, .xoff = 0, .yoff = 2, .xadv = 8},
    {.x = 103, .y = 13, .w = 5, .h = 12, .xoff = 0, .yoff = 3, .xadv = 6},
    {.x = 135, .y = 0, .w = 7, .h = 12, .xoff = 0, .yoff = 0, .xadv = 8},
    {.x = 196, .y = 13, .w = 6, .h = 11, .xoff = 0, .yoff = 1, .xadv = 7},
    {.x = 333, .y = 13, .w = 7, .h = 10, .xoff = 0, .yoff = 2, .xadv = 8},
    {.x = 231, .y = 36, .w = 5, .h = 9, .xoff = 0, .yoff = 3, .xadv = 6},
    {.x = 215, .y = 0, .w = 6, .h = 12, .xoff = 0, .yoff = 0, .xadv = 7},
    {.x = 329, .y = 24, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 222, .y = 0, .w = 6, .h = 12, .xoff = 0, .yoff = 0, .xadv = 7},
    {.x = 197, .y = 36, .w = 6, .h = 9, .xoff = 0, .yoff = 3, .xadv = 7},
    {.x = 236, .y = 0, .w = 6, .h = 12, .xoff = 0, .yoff = 0, .xadv = 7},
    {.x = 350, .y = 24, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 243, .y = 0, .w = 6, .h = 12, .xoff = 0, .yoff = 0, .xadv = 7},
    {.x = 364, .y = 24, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 250, .y = 0, .w = 6, .h = 12, .xoff = 0, .yoff = 0, .xadv = 7},
    {.x = 378, .y = 24, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 264, .y = 0, .w = 6, .h = 12, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 141, .y = 37, .w = 6, .h = 9, .xoff = 0, .yoff = 5, .xadv = 7},
    {.x = 143, .y = 0, .w = 7, .h = 12, .xoff = 0, .yoff = 0, .xadv = 8},
    {.x = 405, .y = 13, .w = 7, .h = 10, .xoff = 0, .yoff = 2, .xadv = 8},
    {.x = 119, .y = 0, .w = 7, .h = 12, .xoff = 0, .yoff = 0, .xadv = 8},
    {.x = 43, .y = 0, .w = 6, .h = 13, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 111, .y = 0, .w = 7, .h = 12, .xoff = 0, .yoff = 0, .xadv = 8},
    {.x = 271, .y = 0, .w = 6, .h = 12, .xoff = 0, .yoff = 0, .xadv = 7},
    {.x = 434, .y = 24, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 299, .y = 0, .w = 6, .h = 12, .xoff = 0, .yoff = 0, .xadv = 7},
    {.x = 448, .y = 24, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 306, .y = 0, .w = 6, .h = 12, .xoff = 0, .yoff = 0, .xadv = 7},
    {.x = 462, .y = 24, .w = 6, .h = 10, .xoff = 0, .yoff = 2, .xadv = 7},
    {.x = 469, .y = 24, .w = 5, .h = 10, .xoff = 0, .yoff = 2, .xadv = 6}
};


GFX_INTERNAL int
GFX__Copy_string(char *dst, int dst_cap, const char *src, int src_count) {
    int i = 0;
    for (; i < dst_cap-1 && src[i] && (src_count == -1 || i < src_count); i+=1) {
        dst[i] = src[i];
    }
    dst[i] = 0;
    return i;
}


// Makes a shader from a string (Documented above)
GFX_INTERNAL int
Make_shader_from_string(GLuint *shader_result, const char *shader_source, GLenum shader_type) {
	GLuint shader = glCreateShader(shader_type);
	glShaderSource(shader, 1, &shader_source, NULL);
	glCompileShader(shader);
	GLint shader_compiled;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &shader_compiled);
	if (shader_compiled != GL_TRUE){
		GLsizei log_length = 0;
		static GLchar message[1024];

		glGetShaderInfoLog(shader, 1024, &log_length, message);

        int cursor = 0;

        cursor += GFX__Copy_string(
            &GFX__data.last_error_str[cursor],
            GFX__MAX_ERROR_STR_SIZE-cursor,
            "--- SHADER ---\n\n '", -1);

        cursor += GFX__Copy_string(
            &GFX__data.last_error_str[cursor],
            GFX__MAX_ERROR_STR_SIZE-cursor,
            shader_source, -1);

        cursor += GFX__Copy_string(
            &GFX__data.last_error_str[cursor],
            GFX__MAX_ERROR_STR_SIZE-cursor,
            "\n\n Compilation error:\n", -1);

        cursor += GFX__Copy_string(
            &GFX__data.last_error_str[cursor],
            GFX__MAX_ERROR_STR_SIZE-cursor,
            message, log_length);

		return -1;
	}

	*shader_result = shader;
	return 0;
}


// Makes a shader program from 2 shader strings (Documented above)
GFX_INTERNAL int
Make_program_from_strings(GLuint *program, const char *vertex_shader_source, const char *fragment_shader_source) {
	GLuint vertex_shader;
	if (Make_shader_from_string(&vertex_shader, vertex_shader_source, GL_VERTEX_SHADER) != 0) {
		return -1;
	}
	GLuint fragment_shader;
	if (Make_shader_from_string(&fragment_shader, fragment_shader_source, GL_FRAGMENT_SHADER) != 0) {
		return -1;
	}

	GLuint shader_program  = glCreateProgram();      // create program object
	glAttachShader(shader_program, vertex_shader);   // and attach both...
	glAttachShader(shader_program, fragment_shader); // ... shaders to it

	glLinkProgram(shader_program);   // link the program

	// We can delete de shaders here
	glDetachShader(shader_program, vertex_shader);
	glDeleteShader(vertex_shader);
	glDetachShader(shader_program, fragment_shader);
	glDeleteShader(fragment_shader);

	*program = shader_program;
	return 0;
}



typedef enum {
	LOC_TYPE_ATTRIB,
	LOC_TYPE_UNIFORM,
} LocationType;

GFX_INTERNAL bool
Program_get_location(GLint *result, GLuint program_id, LocationType location_type, const char *location_name) {
	if (location_type == LOC_TYPE_ATTRIB) {
		GLint loc = glGetAttribLocation(program_id, location_name);
		if (loc < 0) {
            int cursor = 0;

            cursor += GFX__Copy_string(
                &GFX__data.last_error_str[cursor],
                GFX__MAX_ERROR_STR_SIZE-cursor,
                "Unable to get '", -1);

            cursor += GFX__Copy_string(
                &GFX__data.last_error_str[cursor],
                GFX__MAX_ERROR_STR_SIZE-cursor,
                location_name, -1);

            cursor += GFX__Copy_string(
                &GFX__data.last_error_str[cursor],
                GFX__MAX_ERROR_STR_SIZE-cursor,
                "' attrib location\n", -1);

			return false;
		}
		else {
			*result = loc;
			return true;
		}
	}
	else if (location_type == LOC_TYPE_UNIFORM) {
		GLint loc = glGetUniformLocation(program_id, location_name);
		if (loc < 0) {
            int cursor = 0;

            cursor += GFX__Copy_string(
                &GFX__data.last_error_str[cursor],
                GFX__MAX_ERROR_STR_SIZE-cursor,
                "Unable to get '", -1);

            cursor += GFX__Copy_string(
                &GFX__data.last_error_str[cursor],
                GFX__MAX_ERROR_STR_SIZE-cursor,
                location_name, -1);

            cursor += GFX__Copy_string(
                &GFX__data.last_error_str[cursor],
                GFX__MAX_ERROR_STR_SIZE-cursor,
                "' uniform location\n", -1);

			return false;
		}
		else {
			*result = loc;
			return true;
		}
	}
	return false;
}


// Documented above
GFX_PUBLIC int
GFX_Init(void) {

	GFX_Create_buffer(
		&GFX__data.buffer,
		GFX__data.vertices_mem,
		GFX__VERTICES_BUFFER_MAX_SIZE_BYTES,
		GFX__data.indices_mem,
		GFX__INDICES_BUFFER_MAX_SIZE_BYTES,
		GFX_BUFFER_INDEX_TYPE_16);

	//
	//
	// ALL THIS FOR TEXTURED TRIANGLES
	//
	//

	if (0 != Make_program_from_strings(
			&GFX__data.default_shader.id,

			// Vertex shader
			"#version 100\n"
			
			"uniform mat4 vmat;\n"
			
			"attribute vec3 position;\n"
			"attribute vec4 normal;\n"
			"attribute vec2 tex_coord;\n"
			"attribute mediump vec4 color;\n"
			
			"varying mediump vec2 pixel_uv;\n"
			"varying mediump vec3 pixel_normal;\n"
			"varying mediump vec4 pixel_color;\n"
			
			"void main()\n"
			"{\n"
				"gl_Position = vec4(position, 1.0) * vmat;\n"
				"pixel_uv    = tex_coord;\n"
				"pixel_normal= normalize(normal.xyz*2.0-vec3(1,1,1));\n"
				"pixel_color = color;\n"
			"}\n",

			// Fragment shader
			"#version 100\n"
			
			"varying mediump vec2 pixel_uv;\n"
			"varying mediump vec3 pixel_normal;\n"
			"varying mediump vec4 pixel_color;\n"
			"uniform sampler2D texture;\n"
			"uniform mediump vec3 light_dir;\n"
			
			"void main()\n"
			"{\n"
				"mediump float intensity = max(-dot(light_dir, normalize(pixel_normal)), 0.3);\n"
				"mediump vec4  tex_color = texture2D(texture, pixel_uv);\n"
				"gl_FragColor = tex_color * pixel_color * vec4(vec3(intensity), 1.0);\n"
			"}\n"

			)) goto render_setup_error;


	glUseProgram(GFX__data.default_shader.id);

	{ // Populate all the shader locations
		GLuint prog_id = GFX__data.default_shader.id;

		if (!Program_get_location(&GFX__data.default_shader.position, prog_id, LOC_TYPE_ATTRIB, "position"))
			goto render_setup_error;
		if (!Program_get_location(&GFX__data.default_shader.normal, prog_id, LOC_TYPE_ATTRIB, "normal"))
			goto render_setup_error;
		if (!Program_get_location(&GFX__data.default_shader.tex_coord, prog_id, LOC_TYPE_ATTRIB, "tex_coord"))
			goto render_setup_error;
		if (!Program_get_location(&GFX__data.default_shader.color, prog_id, LOC_TYPE_ATTRIB, "color"))
			goto render_setup_error;
		if (!Program_get_location(&GFX__data.default_shader.vmat, prog_id, LOC_TYPE_UNIFORM, "vmat"))
			goto render_setup_error;
		if (!Program_get_location(&GFX__data.default_shader.light_dir, prog_id, LOC_TYPE_UNIFORM, "light_dir"))
			goto render_setup_error;
		if (!Program_get_location(&GFX__data.default_shader.texture, prog_id, LOC_TYPE_UNIFORM, "texture"))
			goto render_setup_error;
	}

	static uint8_t default_texture[] = {
		0xFF, 0xFF, 0xFF, 0xFF,   0xFF, 0xFF, 0xFF, 0xFF,
		0xFF, 0xFF, 0xFF, 0xFF,   0xFF, 0xFF, 0xFF, 0xFF
	};

	// Generates the default texture
	glGenTextures(1, &GFX__data.default_texture);
    glBindTexture(GL_TEXTURE_2D, GFX__data.default_texture);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 2, 2, 0, GL_RGBA, GL_UNSIGNED_BYTE, default_texture);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	GFX__data.texture = GFX__data.default_texture;



    int texture_w = GFX__DEFAULT_TEXTURE_W;
    int texture_h = GFX__DEFAULT_TEXTURE_H;

	// Generates the default font texture

	for (int i = 0; i < texture_h; i+=1) {
		for (int j = 0; j < texture_w; j+=1) {

            int index = i*texture_w+j;
            int compress_index = index/8;
            int compress_bit   = index%8;
			unsigned char A = 0;
            if (compress_index < GFX__DEFAULT_FONT_COMPRESSED_SIZE) {
                if (GFX__default_font_texture_data_compress[compress_index] & (0x1 << compress_bit)) {
                    A = 0xFF;
                }
            }

			GFX__data.tmp.default_font_texture_data[i*texture_w*4 + j*4 + 0] = 0xFF; // R
			GFX__data.tmp.default_font_texture_data[i*texture_w*4 + j*4 + 1] = 0xFF; // G
			GFX__data.tmp.default_font_texture_data[i*texture_w*4 + j*4 + 2] = 0xFF; // B
			GFX__data.tmp.default_font_texture_data[i*texture_w*4 + j*4 + 3] = A; // A
		}
	}
    for (int i = 0; i < 3; i+=1) {
        for (int j = 0; j < 3; j+=1) {
            GFX__data.tmp.default_font_texture_data[(60+i)*texture_w*4+(508+j)*4+3] = 0xFF;
        }
    }


	glGenTextures(1, &GFX__data.default_font.texture);
    glBindTexture(GL_TEXTURE_2D, GFX__data.default_font.texture);
    glTexImage2D(
        GL_TEXTURE_2D, 0, GL_RGBA, texture_w, texture_h, 0, GL_RGBA, GL_UNSIGNED_BYTE,
        GFX__data.tmp.default_font_texture_data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    GFX__data.default_font.ranges = GFX__default_font_ranges;
    GFX__data.default_font.total_ranges = 3;
    GFX__data.default_font.count  = 319;
    GFX__data.default_font.fallback  = 0;
    GFX__data.default_font.texture_w = texture_w;
    GFX__data.default_font.texture_h = texture_h;
    GFX__data.default_font.glyph_infos = GFX__data.default_glyph_infos;

    for (int glyph_i = 0; glyph_i < 319; glyph_i+=1) {
        GFX__CompressedGlyphInfo g = GFX__default_compressed_glyph_info[glyph_i];
        GFX__data.default_font.glyph_infos[glyph_i].x0 = g.x;
        GFX__data.default_font.glyph_infos[glyph_i].x1 = g.x+g.w;
        GFX__data.default_font.glyph_infos[glyph_i].y0 = g.y;
        GFX__data.default_font.glyph_infos[glyph_i].y1 = g.y+g.h;
        GFX__data.default_font.glyph_infos[glyph_i].xoff = (float)g.xoff;
        GFX__data.default_font.glyph_infos[glyph_i].yoff = (float)g.yoff;
        GFX__data.default_font.glyph_infos[glyph_i].xadvance = (float)g.xadv;
    }
    GFX__data.default_font.height = 16.0f;
    GFX__data.default_font.has_white = true;
    GFX__data.default_font.white_x   = 508.0f/(float)GFX__DEFAULT_TEXTURE_W;
    GFX__data.default_font.white_y   = 60.0f/(float)GFX__DEFAULT_TEXTURE_H;
    GFX__data.default_font.white_w   = 3.0f/(float)GFX__DEFAULT_TEXTURE_W;
    GFX__data.default_font.white_h   = 3.0f/(float)GFX__DEFAULT_TEXTURE_H;

    GFX__data.font = &GFX__data.default_font;
    GFX__data.texture = GFX__data.default_font.texture;
    GFX__data.texture_window_x = GFX__data.default_font.white_x;
    GFX__data.texture_window_y = GFX__data.default_font.white_y;
    GFX__data.texture_window_w = GFX__data.default_font.white_w;
    GFX__data.texture_window_h = GFX__data.default_font.white_h;

    GFX_Set_matrix(M4_Diagonal(1.0f));
	GFX_Set_light_dir(V3(0, 0, -1));

	return 0;
	
	render_setup_error:
		GFX_Deinit();
		return -1;
}

// Documented above
GFX_PUBLIC void
GFX_Deinit(void) {
	// Clean the shader program
	glDeleteProgram(GFX__data.default_shader.id);
	// Clean VBO
	GFX_Destroy_buffer(&GFX__data.buffer);
	glDeleteTextures(1, &GFX__data.default_texture);
	GFX__data = (struct GFX__Data){0};
}



// Documented above
GFX_PUBLIC void
GFX_Flush(void) {
	GFX_Upload_buffer_to_gpu(&GFX__data.buffer);
	GFX_Draw_buffer(&GFX__data.buffer);
	GFX_Clear_buffer_data(&GFX__data.buffer);
}


GFX_PUBLIC void
GFX_Begin(void) {
	glUseProgram(GFX__data.default_shader.id);

	glActiveTexture(GL_TEXTURE0);
    glUniform1i(GFX__data.default_shader.texture, 0);
	glBindTexture(GL_TEXTURE_2D, GFX__data.texture);

	glUniform3f(GFX__data.default_shader.light_dir, 0.0f, 0.0f, -1.0f);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);
}



GFX_PUBLIC void
GFX_End(void) {
	GFX_Flush();
}

GFX_PUBLIC const char *
GFX_Last_error(void) {
    return GFX__data.last_error_str;
}


GFX_PUBLIC int
GFX_Create_buffer(GFX_Buffer *buffer_out, void *vertices_mem, int64_t vertices_mem_size, void *indices_mem, int64_t indices_mem_size, GFX_BufferIndexType index_type) {
	GFX_Buffer result = {0};
	GLuint buffer_objects[2];
	glGenBuffers(2, buffer_objects);
	result.VBO = buffer_objects[0];
	result.EBO = buffer_objects[1];

	glBindBuffer(GL_ARRAY_BUFFER, result.VBO);
	glBufferData(GL_ARRAY_BUFFER, vertices_mem_size, NULL, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, result.EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices_mem_size, NULL, GL_DYNAMIC_DRAW);

	result.vertices       = vertices_mem;
	result.vertices_cap   = vertices_mem_size / GFX_VERTEX_SIZE;
	result.vertices_count = 0;

	result.indices       = indices_mem;
	if (index_type == GFX_BUFFER_INDEX_TYPE_16) {
		result.indices_cap   = indices_mem_size / 2;
	}
	else {
		result.indices_cap   = indices_mem_size / 4;
	}

	result.indices_count = 0;

	result.index_type = index_type;

	*buffer_out = result;

	return 0;
}

GFX_PUBLIC void
GFX_Destroy_buffer(GFX_Buffer *buffer) {
	GLuint buffer_objects[] = {buffer->VBO, buffer->EBO};
	glDeleteBuffers(0, buffer_objects);
	*buffer = (GFX_Buffer){0};
}


GFX_PUBLIC void
GFX_Clear_buffer_data(GFX_Buffer *buffer) {
	buffer->vertices_count  = 0;
	buffer->indices_count = 0;
}

GFX_PUBLIC void
GFX_Upload_buffer_to_gpu(GFX_Buffer *buffer) {

	uint32_t bytes_of_vertices  = GFX_VERTEX_SIZE * buffer->vertices_count;
	glBindBuffer(GL_ARRAY_BUFFER, buffer->VBO);
	glBufferSubData(GL_ARRAY_BUFFER, 0, bytes_of_vertices, buffer->vertices);
	uint32_t bytes_of_indices;
	if (buffer->index_type == GFX_BUFFER_INDEX_TYPE_16) {
		bytes_of_indices = 2 * buffer->indices_count;
	}
	else {
		bytes_of_indices = 4 * buffer->indices_count;
	}
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer->EBO);
	glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, bytes_of_indices, buffer->indices);

}


GFX_PUBLIC GFX_Vertex *
GFX_Alloc_vertices(GFX_Buffer *buffer, int64_t count, uint32_t *base_index) {
	if ((buffer->vertices_cap - buffer->vertices_count) < count) return NULL;
	GFX_Vertex *result = &buffer->vertices[buffer->vertices_count];
	if (base_index) *base_index = buffer->vertices_count;
	buffer->vertices_count += count;
	return result;
}

GFX_PUBLIC void *
GFX_Alloc_indices(GFX_Buffer *buffer, int64_t count) {
	if (buffer->indices_cap - buffer->indices_count < count) return NULL;
	void *result;
	if (buffer->index_type == GFX_BUFFER_INDEX_TYPE_16) {
		uint16_t *indices16 = (uint16_t *)buffer->indices;
		result = &indices16[buffer->indices_count];
	}
	else {
		uint32_t *indices32 = (uint32_t *)buffer->indices;
		result = &indices32[buffer->indices_count];
	}

	buffer->indices_count += count;

	return result;
}


GFX_PUBLIC int64_t
GFX_Get_remaining_vertices(GFX_Buffer *buffer) {
	return buffer->vertices_cap - buffer->vertices_count;
}


GFX_PUBLIC int64_t
GFX_Get_remaining_indices(GFX_Buffer *buffer) {
	return buffer->indices_cap - buffer->indices_count;
}


GFX_PUBLIC void
GFX_Draw_buffer(GFX_Buffer *buffer) {
	glBindBuffer(GL_ARRAY_BUFFER, buffer->VBO);
	unsigned int stride = 3*4 + 4 + 2*4 + 4; // 3 floats + 4 bytes + 2 floats + 4 bytes
    glEnableVertexAttribArray(GFX__data.default_shader.position);
	glVertexAttribPointer(GFX__data.default_shader.position, 3, GL_FLOAT, false, stride, (void*)(0*sizeof(float)));
    glEnableVertexAttribArray(GFX__data.default_shader.normal);
	glVertexAttribPointer(GFX__data.default_shader.normal, 3, GL_UNSIGNED_BYTE, true, stride, (void*)(3*sizeof(float)));
    glEnableVertexAttribArray(GFX__data.default_shader.tex_coord);
	glVertexAttribPointer(GFX__data.default_shader.tex_coord, 2, GL_FLOAT, false, stride, (void*)(4*sizeof(float)));
    glEnableVertexAttribArray(GFX__data.default_shader.color);
	glVertexAttribPointer(GFX__data.default_shader.color, 4, GL_UNSIGNED_BYTE, true, stride, (void*)(6*sizeof(float)));
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer->EBO);
	GLenum index_type = (buffer->index_type == GFX_BUFFER_INDEX_TYPE_16) ? GL_UNSIGNED_SHORT : GL_UNSIGNED_INT;
	glDrawElements(GL_TRIANGLES, buffer->indices_count, index_type, 0);
}


GFX_PUBLIC GLuint
GFX_Default_texture(void) {
	return GFX__data.default_texture;
}



// Doc above
GFX_PUBLIC void
GFX_Set_texture(GLuint texture) {
	if (GFX__data.texture != texture) {
		GFX_Flush();
		GFX__data.texture = texture;
		glBindTexture(GL_TEXTURE_2D, GFX__data.texture);
	}
}



// Doc above
GFX_PUBLIC GLuint
GFX_Get_texture(void) {
	return GFX__data.texture;
}


GFX_PUBLIC void
GFX_Set_texture_window(float x, float y, float w, float h) {
    GFX__data.texture_window_x = x;
    GFX__data.texture_window_y = y;
    GFX__data.texture_window_w = w;
    GFX__data.texture_window_h = h;
}

GFX_PUBLIC void
GFX_Set_white(void) {
    if (GFX__data.font->has_white) {
        GFX_Set_texture(GFX__data.font->texture);
        GFX_Set_texture_window(GFX__data.font->white_x, GFX__data.font->white_y,
            GFX__data.font->white_w, GFX__data.font->white_h);
    }
    else {
        GFX_Set_texture(GFX__data.default_font.texture);
        GFX_Set_texture_window(GFX__data.default_font.white_x, GFX__data.default_font.white_y,
            GFX__data.default_font.white_w, GFX__data.default_font.white_h);
    }
}


//Documented above
GFX_PUBLIC void
GFX_Set_matrix(Mat4 matrix) {
	GFX_Flush();
    GFX__data.matrix = matrix;
    glUniformMatrix4fv(GFX__data.default_shader.vmat, 1, GL_FALSE, (GLfloat *)&GFX__data.matrix);
}


//Documented above
GFX_PUBLIC Mat4
GFX_Get_matrix(void) {
    return GFX__data.matrix;
}

//Documented above
GFX_PUBLIC void
GFX_Set_light_dir(Vec3 light_dir) {
	GFX_Flush();
	glUniform3f(GFX__data.default_shader.light_dir, light_dir.x, light_dir.y, light_dir.z);
    GFX__data.light_dir = light_dir;
}



GFX_PUBLIC void
GFX_Draw_triangle_ex(Vec3 v0, Vec3 v1, Vec3 v2, Color n0, Color n1, Color n2, Vec2 uv0, Vec2 uv1, Vec2 uv2, Color c0, Color c1, Color c2) {

	if (GFX_Get_remaining_vertices(&GFX__data.buffer) < 3 || GFX_Get_remaining_indices(&GFX__data.buffer) < 3) GFX_Flush();

    Vec2 uv_scale = V2(GFX__data.texture_window_w, GFX__data.texture_window_h);
    Vec2 uv_off   = V2(GFX__data.texture_window_x, GFX__data.texture_window_y);

	uint32_t base_index;
	GFX_Vertex *verts = GFX_Alloc_vertices(&GFX__data.buffer, 3, &base_index);

	verts[0].position  = v0;
	verts[0].normal    = n0;
	verts[0].tex_coord = V2_Add(V2_Mul(uv0, uv_scale), uv_off);
	verts[0].color     = c0;

	verts[1].position  = v1;
	verts[1].normal    = n1;
	verts[1].tex_coord = V2_Add(V2_Mul(uv1, uv_scale), uv_off);
	verts[1].color     = c1;

	verts[2].position  = v2;
	verts[2].normal    = n2;
	verts[2].tex_coord = V2_Add(V2_Mul(uv2, uv_scale), uv_off);
	verts[2].color     = c2;

	uint16_t *indices = (uint16_t *)GFX_Alloc_indices(&GFX__data.buffer, 3);
	indices[0] = (uint16_t)base_index+0;
	indices[1] = (uint16_t)base_index+1;
	indices[2] = (uint16_t)base_index+2;
}


GFX_PUBLIC void
GFX_Draw_quad_ex(Vec3 v0, Vec3 v1, Vec3 v2, Vec3 v3, Color n0, Color n1, Color n2, Color n3, Vec2 uv0, Vec2 uv1, Vec2 uv2, Vec2 uv3, Color c0, Color c1, Color c2, Color c3) {
	
	if (GFX_Get_remaining_vertices(&GFX__data.buffer) < 4 || GFX_Get_remaining_indices(&GFX__data.buffer) < 6) GFX_Flush();

    Vec2 uv_scale = V2(GFX__data.texture_window_w, GFX__data.texture_window_h);
    Vec2 uv_off   = V2(GFX__data.texture_window_x, GFX__data.texture_window_y);

	uint32_t base_index;
	GFX_Vertex *verts = GFX_Alloc_vertices(&GFX__data.buffer, 4, &base_index);

	verts[0].position  = v0;
	verts[0].normal    = n0;
	verts[0].tex_coord = V2_Add(V2_Mul(uv0, uv_scale), uv_off);
	verts[0].color     = c0;

	verts[1].position  = v1;
	verts[1].normal    = n1;
	verts[1].tex_coord = V2_Add(V2_Mul(uv1, uv_scale), uv_off);
	verts[1].color     = c1;

	verts[2].position  = v2;
	verts[2].normal    = n2;
	verts[2].tex_coord = V2_Add(V2_Mul(uv2, uv_scale), uv_off);
	verts[2].color     = c2;

	verts[3].position  = v3;
	verts[3].normal    = n3;
	verts[3].tex_coord = V2_Add(V2_Mul(uv3, uv_scale), uv_off);
	verts[3].color     = c3;

	uint16_t *indices = (uint16_t *)GFX_Alloc_indices(&GFX__data.buffer, 6);
	indices[0] = (uint16_t)base_index+0;
	indices[1] = (uint16_t)base_index+1;
	indices[2] = (uint16_t)base_index+2;
	indices[3] = (uint16_t)base_index+0;
	indices[4] = (uint16_t)base_index+2;
	indices[5] = (uint16_t)base_index+3;

}



// Documented above
GFX_PUBLIC void
GFX_Draw_triangle(Vec2 v0, Vec2 v1, Vec2 v2, Color color) {
	GFX_Draw_triangle_ex(
		V3(v0.x, v0.y, 0),
		V3(v1.x, v1.y, 0),
		V3(v2.x, v2.y, 0),
		V4_To_Color_renorm(V4(0, 0, 1, 0)),
		V4_To_Color_renorm(V4(0, 0, 1, 0)),
		V4_To_Color_renorm(V4(0, 0, 1, 0)),
		V2(0, 0),
		V2(1, 0),
		V2(1, 1),
		color,
		color,
		color
	);
}

GFX_PUBLIC void
GFX_Draw_textured_triangle(Vec2 v0, Vec2 v1, Vec2 v2, Vec2 uv0, Vec2 uv1, Vec2 uv2, Color color) {
	GFX_Draw_triangle_ex(
		V3(v0.x, v0.y, 0),
		V3(v1.x, v1.y, 0),
		V3(v2.x, v2.y, 0),
		V4_To_Color_renorm(V4(0, 0, 1, 0)),
		V4_To_Color_renorm(V4(0, 0, 1, 0)),
		V4_To_Color_renorm(V4(0, 0, 1, 0)),
		uv0,
		uv1,
		uv2,
		color,
		color,
		color
	);
}

// Documented above
GFX_PUBLIC void
GFX_Draw_quad(Vec2 v0, Vec2 v1, Vec2 v2, Vec2 v3, Color color) {
	GFX_Draw_quad_ex(
		V3(v0.x, v0.y, 0),
		V3(v1.x, v1.y, 0),
		V3(v2.x, v2.y, 0),
		V3(v3.x, v3.y, 0),
		V4_To_Color_renorm(V4(0, 0, 1, 0)),
		V4_To_Color_renorm(V4(0, 0, 1, 0)),
		V4_To_Color_renorm(V4(0, 0, 1, 0)),
		V4_To_Color_renorm(V4(0, 0, 1, 0)),
		V2(0, 0),
		V2(1, 0),
		V2(1, 1),
		V2(0, 1),
		color,
		color,
		color,
		color
	);
}

// Documented above
GFX_PUBLIC void
GFX_Draw_line(Vec2 src, Vec2 dst, float thickness, Color color) {
	
	Vec2 norm_dir = V2_Normalize(V2_Sub(dst, src));

	float thick_half = thickness * 0.5f;
	Vec2 up   = V2_Mulf(V2(-norm_dir.y, norm_dir.x), thick_half);
	Vec2 down = V2_Mulf(V2(norm_dir.y, -norm_dir.x), thick_half);

	Vec2 v0 = V2_Add(src, up);
	Vec2 v1 = V2_Add(src, down);
	Vec2 v2 = V2_Add(dst, down);
	Vec2 v3 = V2_Add(dst, up);
	GFX_Draw_quad(v0, v1, v2, v3, color);
}

// Documented above
GFX_PUBLIC void
GFX_Draw_rect(Vec2 pos, float width, float height, Color color) {
	GFX_Draw_quad(
		pos,
		V2(pos.x + width, pos.y),
		V2(pos.x + width, pos.y + height),
		V2(pos.x,         pos.y + height),
		color);
}




// Documented above
GFX_PUBLIC void
GFX_Draw_textured_quad(Vec2 v0, Vec2 v1, Vec2 v2, Vec2 v3, Vec2 uv0, Vec2 uv1, Vec2 uv2, Vec2 uv3, Color color) {
	Vec3 v3_0 = V3(v0.x, v0.y, 0);
	Vec3 v3_1 = V3(v1.x, v0.y, 0);
	Vec3 v3_2 = V3(v2.x, v2.y, 0);
	Vec3 v3_3 = V3(v3.x, v3.y, 0);
	Color n = V4_To_Color_renorm(V4(0, 0, 1, 0));
	Color c = color;
	GFX_Draw_quad_ex(v3_0, v3_1, v3_2, v3_3, n, n, n, n, uv0, uv1, uv2, uv3, c, c, c, c);
}



GFX_PUBLIC void
GFX_Draw_circle(Vec2 center, float radius, Color color) {
	static const int SUBDIVISIONS = 30;
	float theta = 2*PI32/(float)SUBDIVISIONS;
	float cos_theta = Cos(theta);
	float sin_theta = Sin(theta);

	Vec2 p0 = V2(radius, 0);
	for (int i = 0; i < SUBDIVISIONS; ++i) {
		Vec2 p1 = V2(p0.x * cos_theta - p0.y * sin_theta, p0.x * sin_theta + p0.y * cos_theta);
		GFX_Draw_triangle(center, V2_Add(center, p0), V2_Add(center, p1), color);
		p0 = p1;
	}
}


GFX_PUBLIC void
GFX_Draw_triangle_3D(Vec3 v0, Vec3 v1, Vec3 v2, Color color) {
	Vec3 n = V3_Cross3(v0, v1, v2);
	n = V3(n.x, -n.y, n.z); // FIXME(Tano) Why is the y inverteeeeed
	Color nc = V4_To_Color_renorm((Vec4){.xyz = n});
	GFX_Draw_triangle_ex(v0, v1, v2, nc, nc, nc, V2(0, 0), V2(1, 0), V2(1, 1), color, color, color);
}


GFX_PUBLIC void
GFX_Draw_quad_3D(Vec3 v0, Vec3 v1, Vec3 v2, Vec3 v3, Color color) {
	GFX_Draw_triangle_3D(v0, v1, v2, color);
	GFX_Draw_triangle_3D(v0, v2, v3, color);
}

GFX_PUBLIC void
GFX_Draw_cube(Vec3 pos, float width, float height, float length, Color color) {

		Vec3 corner_front_bottom_left  = V3_Add(V3(-0.5f * width, -0.5f * height,  0.5f * length), pos);
		Vec3 corner_front_bottom_right = V3_Add(V3( 0.5f * width, -0.5f * height,  0.5f * length), pos);
		Vec3 corner_front_up_right     = V3_Add(V3( 0.5f * width,  0.5f * height,  0.5f * length), pos);
		Vec3 corner_front_up_left      = V3_Add(V3(-0.5f * width,  0.5f * height,  0.5f * length), pos);
		Vec3 corner_back_bottom_left   = V3_Add(V3(-0.5f * width, -0.5f * height, -0.5f * length), pos);
		Vec3 corner_back_bottom_right  = V3_Add(V3( 0.5f * width, -0.5f * height, -0.5f * length), pos);
		Vec3 corner_back_up_right      = V3_Add(V3( 0.5f * width,  0.5f * height, -0.5f * length), pos);
		Vec3 corner_back_up_left       = V3_Add(V3(-0.5f * width,  0.5f * height, -0.5f * length), pos);

		Color normal_front = V4_To_Color_renorm(V4( 0,  0,  1,  0));
		Color normal_back  = V4_To_Color_renorm(V4( 0,  0, -1,  0));
		Color normal_up    = V4_To_Color_renorm(V4( 0,  1,  0,  0));
		Color normal_down  = V4_To_Color_renorm(V4( 0, -1,  0,  0));
		Color normal_left  = V4_To_Color_renorm(V4(-1,  0,  0,  0));
		Color normal_right = V4_To_Color_renorm(V4( 1,  0,  0,  0));

		GFX_Draw_quad_ex(
				corner_front_bottom_left,
				corner_front_bottom_right,
				corner_front_up_right,
				corner_front_up_left,
				normal_front, normal_front, normal_front, normal_front,
				V2(1, 1), V2(0, 1), V2(0, 0), V2(1, 0),
				color, color, color, color);

		GFX_Draw_quad_ex(
				corner_front_bottom_right,
				corner_back_bottom_right,
				corner_back_up_right,
				corner_front_up_right,
				normal_right, normal_right, normal_right, normal_right,
				V2(1, 1), V2(0, 1), V2(0, 0), V2(1, 0),
				color, color, color, color);
		
		GFX_Draw_quad_ex(
				corner_back_bottom_right,
				corner_back_bottom_left,
				corner_back_up_left,
				corner_back_up_right,
				normal_back, normal_back, normal_back, normal_back,
				V2(1, 1), V2(0, 1), V2(0, 0), V2(1, 0),
				color, color, color, color);

		GFX_Draw_quad_ex(
				corner_back_bottom_left,
				corner_front_bottom_left,
				corner_front_up_left,
				corner_back_up_left,
				normal_left, normal_left, normal_left, normal_left,
				V2(1, 1), V2(0, 1), V2(0, 0), V2(1, 0),
				color, color, color, color);

		GFX_Draw_quad_ex(
				corner_front_up_left,
				corner_front_up_right,
				corner_back_up_right,
				corner_back_up_left,
				normal_up, normal_up, normal_up, normal_up,
				V2(1, 1), V2(0, 1), V2(0, 0), V2(1, 0),
				color, color, color, color);

		GFX_Draw_quad_ex(
				corner_front_bottom_right,
				corner_front_bottom_left,
				corner_back_bottom_left,
				corner_back_bottom_right,
				normal_down, normal_down, normal_down, normal_down,
				V2(1, 1), V2(0, 1), V2(0, 0), V2(1, 0),
				color, color, color, color);
}


GFX_PUBLIC void
GFX_Draw_cylinder(Vec3 pos, float up_radius, float down_radius, float length, Color color) {
	int SUBDIVISIONS = 40;
	float theta = 2*PI32/(float)SUBDIVISIONS;
	float cos_theta = Cos(theta);
	float sin_theta = Sin(theta);
	
	Vec3 up_center   = V3_Add(pos, V3(0,  0.5f * length, 0));
	Vec3 down_center = V3_Add(pos, V3(0, -0.5f * length, 0));
	
	float slope = (down_radius - up_radius) / length;

	Color normal_up   = V4_To_Color_renorm(V4(0,  1, 0, 0));
	Color normal_down = V4_To_Color_renorm(V4(0, -1, 0, 0));

	Vec3 p0 = V3(1, 0, 0);
	Color n0 = V4_To_Color_renorm(V4(p0.x, slope, p0.z, 0));
	Vec3 up0   = V3_Add(up_center, V3_Mulf(p0, up_radius));
	Vec3 down0 = V3_Add(down_center, V3_Mulf(p0, down_radius));
	for (int i = 1; i <= SUBDIVISIONS; ++i) {
		Vec3 p1 = V3(0, 0, 0);
		p1.x = p0.x * cos_theta - p0.z * sin_theta;
		p1.z = p0.x * sin_theta + p0.z * cos_theta;

		Color n1   = V4_To_Color_renorm(V4(p1.x, slope, p1.z, 0));
		Vec3 up1   = V3_Add(up_center, V3_Mulf(p1, up_radius));
		Vec3 down1 = V3_Add(down_center, V3_Mulf(p1, down_radius));
		
		Vec2 cap_uv0 = V2(0.5f, 0.5f);
		Vec2 cap_uv1; {
			float m = 0.5f/Max(Abs(p0.x), Abs(p0.z));
			cap_uv1 = V2(p0.x*m+0.5f, p0.z*m+0.5f);
		}
		Vec2 cap_uv2; {
			float m = 0.5f/Max(Abs(p1.x), Abs(p1.z));
			cap_uv2 = V2(p1.x*m+0.5f, p1.z*m+0.5f);
		}
		
		GFX_Draw_triangle_ex(
				up_center, up1, up0,
				normal_up, normal_up, normal_up,
				cap_uv0, cap_uv2, cap_uv1,
				color, color, color);

		Vec2 uv0 = V2((float)(i)/(float)SUBDIVISIONS, 1);
		Vec2 uv1 = V2((float)(i-1)/(float)SUBDIVISIONS, 1);
		Vec2 uv2 = V2((float)(i-1)/(float)SUBDIVISIONS, 0);
		Vec2 uv3 = V2((float)(i)/(float)SUBDIVISIONS, 0);
		GFX_Draw_quad_ex(down1, down0, up0, up1, n1, n0, n0, n1, uv0, uv1, uv2, uv3, color, color, color, color);
		//GFX_Draw_quad_3D(down1, down0, up0, up1, color);

		GFX_Draw_triangle_ex(
				down_center, down0, down1,
				normal_down, normal_down, normal_down,
				cap_uv0, cap_uv1, cap_uv2,
				color, color, color);

		p0 = p1;
		n0 = n1;
		up0 = up1;
		down0 = down1;
	}
}

GFX_PUBLIC void
GFX_Draw_sphere(Vec3 pos, float radius, Color color) {
	int SUBDIVISIONS0 = 20;
	int SUBDIVISIONS1 = 40;
	float theta0 = PI32/(float)SUBDIVISIONS0;
	float theta1 = 2*PI32/(float)SUBDIVISIONS1;
	float cos_theta0 = Cos(theta0);
	float sin_theta0 = Sin(theta0);
	float cos_theta1 = Cos(theta1);
	float sin_theta1 = Sin(theta1);
	
	Vec3 half_circle_p0 = V3(0, radius, 0);
	for (int i0 = 1; i0 <= SUBDIVISIONS0; i0+=1) {
		Vec3 half_circle_p1 = V3(0, 0, 0);
		// Rotate around the z axis
		half_circle_p1.x = half_circle_p0.x * cos_theta0 - half_circle_p0.y * sin_theta0;
		half_circle_p1.y = half_circle_p0.x * sin_theta0 + half_circle_p0.y * cos_theta0;

		Vec3 p_up_0   = half_circle_p0;
		Vec3 p_down_0 = half_circle_p1;
		Color n_up_0   = V4_To_Color_renorm((Vec4){.xyz=p_up_0});
		Color n_down_0 = V4_To_Color_renorm((Vec4){.xyz=p_down_0});

		for (int i1 = 1; i1 <= SUBDIVISIONS1; i1+=1) {
			Vec3 p_down_1 = V3(0, p_down_0.y, 0);
			p_down_1.x = p_down_0.x * cos_theta1 - p_down_0.z * sin_theta1;
			p_down_1.z = p_down_0.x * sin_theta1 + p_down_0.z * cos_theta1;
			Color n_down_1 = V4_To_Color_renorm((Vec4){.xyz=p_down_1});

			Vec3 p_up_1   = V3(0, p_up_0.y, 0);
			p_up_1.x = p_up_0.x * cos_theta1 - p_up_0.z * sin_theta1;
			p_up_1.z = p_up_0.x * sin_theta1 + p_up_0.z * cos_theta1;
			Color n_up_1 = V4_To_Color_renorm((Vec4){.xyz=p_up_1});

			Vec2 uv0 = V2((float)(i1-1)/SUBDIVISIONS1, (float)(i0-1)/(float)SUBDIVISIONS0);
			Vec2 uv1 = V2((float)(i1)/SUBDIVISIONS1, (float)(i0-1)/(float)SUBDIVISIONS0);
			Vec2 uv2 = V2((float)(i1)/SUBDIVISIONS1, (float)(i0)/(float)SUBDIVISIONS0);
			Vec2 uv3 = V2((float)(i1-1)/SUBDIVISIONS1, (float)(i0)/(float)SUBDIVISIONS0);

			GFX_Draw_quad_ex(
				V3_Add(pos, p_up_0),
				V3_Add(pos, p_up_1),
				V3_Add(pos, p_down_1),
				V3_Add(pos, p_down_0),
				n_up_0, n_up_1, n_down_1, n_down_0,
				uv0, uv1, uv2, uv3, color, color, color, color);
			//GFX_Draw_quad_3D(
			//	V3_Add(p_down_0, pos),
			//	V3_Add(p_down_1, pos),
			//	V3_Add(p_up_1, pos),
			//	V3_Add(p_up_0, pos),
			//	color
			//);
			p_down_0 = p_down_1;
			p_up_0 = p_up_1;
			n_up_0 = n_up_1;
			n_down_0 = n_down_1;
		}

		half_circle_p0 = half_circle_p1;
	}
}

GFX_PUBLIC void
GFX_Draw_torus(Vec3 pos, float radius0, float radius1, Color color) {

	int SUBDIVISIONS0 = 20;
	float theta0 = -2*PI32/(float)SUBDIVISIONS0;
	float cos_theta0 = Cos(theta0);
	float sin_theta0 = Sin(theta0);

	int SUBDIVISIONS1 = 40;
	float theta1 = -2*PI32/(float)SUBDIVISIONS1;
	float cos_theta1 = Cos(theta1);
	float sin_theta1 = Sin(theta1);
	
	Vec3 circle_p0 = V3(0, 0, radius0);
	for (int i0 = 1; i0 <= SUBDIVISIONS0; i0+=1) {
		Vec3 circle_p1 = V3(0, 0, 0);
		// Rotate across the z axis
		circle_p1.x = circle_p0.x * cos_theta0 - circle_p0.z * sin_theta0;
		circle_p1.z = circle_p0.x * sin_theta0 + circle_p0.z * cos_theta0;

		Vec3 p_down_0 = V3_Add(V3(radius1, 0, 0), circle_p0);
		Vec3 p_down_1 = V3_Add(V3(radius1, 0, 0), circle_p1);
		Vec3 n_down_0 = circle_p0;
		Vec3 n_down_1 = circle_p1;
		Color nc_down_0 = V4_To_Color_renorm((Vec4){.xyz=n_down_0});
		Color nc_down_1 = V4_To_Color_renorm((Vec4){.xyz=n_down_1});

		circle_p0 = circle_p1;
		
		for (int i1 = 1; i1 <= SUBDIVISIONS1; i1+=1) {
			Vec3 p_up_0;
			p_up_0.x = p_down_0.x * cos_theta1 - p_down_0.y * sin_theta1;
			p_up_0.y = p_down_0.x * sin_theta1 + p_down_0.y * cos_theta1;
			p_up_0.z = p_down_0.z;

			Vec3 p_up_1;
			p_up_1.x = p_down_1.x * cos_theta1 - p_down_1.y * sin_theta1;
			p_up_1.y = p_down_1.x * sin_theta1 + p_down_1.y * cos_theta1;
			p_up_1.z = p_down_1.z;

			Vec3 n_up_0;
			n_up_0.x = n_down_0.x * cos_theta1 - n_down_0.y * sin_theta1;
			n_up_0.y = n_down_0.x * sin_theta1 + n_down_0.y * cos_theta1;
			n_up_0.z = n_down_0.z;
			Color nc_up_0 = V4_To_Color_renorm((Vec4){.xyz=n_up_0});

			Vec3 n_up_1;
			n_up_1.x = n_down_1.x * cos_theta1 - n_down_1.y * sin_theta1;
			n_up_1.y = n_down_1.x * sin_theta1 + n_down_1.y * cos_theta1;
			n_up_1.z = n_down_1.z;
			Color nc_up_1 = V4_To_Color_renorm((Vec4){.xyz=n_up_1});

			Vec2 uv0 = V2((float)(i0-1)/SUBDIVISIONS0, (float)(i1-1)/(float)SUBDIVISIONS1);
			Vec2 uv1 = V2((float)(i0-1)/SUBDIVISIONS0, (float)(i1)/(float)SUBDIVISIONS1);
			Vec2 uv2 = V2((float)(i0)/SUBDIVISIONS0, (float)(i1)/(float)SUBDIVISIONS1);
			Vec2 uv3 = V2((float)(i0)/SUBDIVISIONS0, (float)(i1-1)/(float)SUBDIVISIONS1);

			GFX_Draw_quad_ex(
				V3_Add(p_down_0, pos),
				V3_Add(p_up_0, pos),
				V3_Add(p_up_1, pos),
				V3_Add(p_down_1, pos),
				nc_down_0, nc_up_0, nc_up_1, nc_down_1,
				uv0, uv1, uv2, uv3,
				color, color, color, color);
			//GFX_Draw_quad_3D(
			//	V3_Add(p_down_0, pos),
			//	V3_Add(p_up_0, pos),
			//	V3_Add(p_up_1, pos),
			//	V3_Add(p_down_1, pos),
			//	color
			//);
			p_down_0 = p_up_0;
			p_down_1 = p_up_1;
			n_down_0 = n_up_0;
			n_down_1 = n_up_1;
			nc_down_0 = nc_up_0;
			nc_down_1 = nc_up_1;
		}

	}
}



// TEXT RENDERING


GFX_PUBLIC GFX_Font *
GFX_Default_font(void) {
	return &GFX__data.default_font;
}

GFX_PUBLIC void
GFX_Set_font(GFX_Font *font) {
    GFX__data.font = font;
    GFX_Set_texture(font->texture);
    GFX_Set_texture_window(0, 0, 1, 1);
}

GFX_PUBLIC GFX_Font *
GFX_Get_font(void) {
    return GFX__data.font;
}

GFX_INTERNAL int64_t
GFX__Get_next_codepoint(const char *text, int len, int *chars_consumed) {
    int64_t codepoint = -1;
    *chars_consumed   = 1;

    // NOTE(Tano) if the codepoint is invalid this will return -1 but in that case only consumes
    // 1 char, maybe we should consume the amount of chars indicated???
    if (text[0] & 0x80) {
        if ((text[0] & 0xe0) == 0xc0 && 1 < len && text[1]) { // 2 bytes
            codepoint = ((0x1f & text[0]) << 6) | (0x3f & text[1]);
            (*chars_consumed)+=1;
        }
        else if ((text[0] & 0xf0) == 0xe0 && 2 < len && text[1] && text[2]) { // 3 bytes
            codepoint = ((0x0f & text[0]) << 12)  |
                        ((0x3f & text[1]) << 6) |
                        (0x3f & text[2]);
            (*chars_consumed)+=2;
        }
        else if ((text[0] & 0xf8) == 0xf0 && 3 < len && text[1] && text[2] && text[3]) { // 4 bytes
            codepoint = ((0x07 & text[0]) << 18)   |
                        ((0x3f & text[1]) << 12) |
                        ((0x3f & text[2]) << 6)  |
                         (0x3f & text[3]);
            (*chars_consumed)+=3;
        }
    }
    else {
        codepoint = text[0];
    }
    return codepoint;
}


GFX_INTERNAL int64_t
GFX__Get_glyph_index(GFX_Font *font, uint32_t codepoint) {
    int64_t total_ranges = font->total_ranges;
    int64_t *ranges      = font->ranges;
    int64_t index        = 0;
    for (int range_i = 0; range_i < total_ranges; range_i+=1) {
        int64_t start = ranges[range_i*2];
        int64_t end   = ranges[range_i*2+1];
        if (codepoint >= start && codepoint <= end) {
            index += (codepoint-start);
            return index;
        }
        else {
            index += (end-start)+1;
        }
    }
    return font->fallback;
}


GFX_PUBLIC float
GFX_Get_text_width(GFX_Font *font, const char *text, int len) {
	float res = 0;
    if (len == -1) len = 0x0fffffff;
    
    for (int i = 0; i < len;) {

        int chars_consumed = 0;
        int64_t codepoint = GFX__Get_next_codepoint(&text[i], len-i, &chars_consumed);
        i += chars_consumed;

        if (codepoint == 0) break;


        int64_t index = GFX__Get_glyph_index(font, codepoint);
        res += font->glyph_infos[index].xadvance;
    }

	return res;
}


GFX_PUBLIC float
GFX_Draw_glyph(GFX_Font *font, uint32_t codepoint, Vec2 pos, float scale, Color color) {
    // Compute the UVs
    float invx_size = 1.0f/font->texture_w;
    float invy_size = 1.0f/font->texture_h;
    int64_t index = GFX__Get_glyph_index(font, codepoint);

    GFX_GlyphInfo gi = GFX__data.font->glyph_infos[index];
    Vec2 uv0 = V2(((float)gi.x0)*invx_size, (((float)gi.y0)*invy_size));
    Vec2 uv1 = V2(((float)gi.x1)*invx_size, (((float)gi.y0)*invy_size));
    Vec2 uv2 = V2(((float)gi.x1)*invx_size, (((float)gi.y1)*invy_size));
    Vec2 uv3 = V2(((float)gi.x0)*invx_size, (((float)gi.y1)*invy_size));
    
    float w = ((float)(gi.x1-gi.x0))*scale;
    float h = ((float)(gi.y1-gi.y0))*scale;
    
    float x = pos.x + gi.xoff*scale;
    float y = pos.y + gi.yoff*scale;

    // Compute the vertices
    Vec3 v0 = V3(x,   y, 0.0f);
    Vec3 v1 = V3(x+w, y, 0.0f);
    Vec3 v2 = V3(x+w, y+h, 0.0f);
    Vec3 v3 = V3(x,   y+h, 0.0f);
    
    const Color n = COLOR(127, 127, 255, 127);
    GFX_Draw_quad_ex(v0, v1, v2, v3, n, n, n, n, uv0, uv1, uv2, uv3, color, color, color, color);

    return gi.xadvance*scale;
}


GFX_PUBLIC void
GFX_Draw_text(const char *text, Vec2 pos, float scale, Color color) {
    int index = 0;
    float x = pos.x;
    float y = pos.y;
    for (;;) {
        int chars_consumed = 0;
        int64_t codepoint = GFX__Get_next_codepoint(&text[index], 0x0fffffff, &chars_consumed);
        index += chars_consumed;

        if (codepoint == 0) break;

        x += GFX_Draw_glyph(GFX__data.font, codepoint, V2(x, y), scale, color);
    }
}

GFX_PUBLIC void
GFX_Draw_text_sized(const char *text, int text_len, Vec2 pos, float scale, Color color) {
    float x = pos.x;
    float y = pos.y;
    for (int index = 0; index < text_len;) {
        int chars_consumed = 0;
        int64_t codepoint = GFX__Get_next_codepoint(&text[index], text_len-index, &chars_consumed);
        index += chars_consumed;

        if (codepoint == 0) break;

        x += GFX_Draw_glyph(GFX__data.font, codepoint, V2(x, y), scale, color);
    }
}

GFX_PUBLIC void
GFX_Draw_text_view(const char *text, int text_len, Vec2 pos, float offset, float max_width, float scale, Color color) {
    float x_advanced = 0.0f;
    float x = 0.0f;

    int index = 0;
    for (; index < text_len;) {
        if (x_advanced >= offset) {
            x = x_advanced - offset;
            break;
        }
        int chars_consumed = 0;
        int64_t codepoint = GFX__Get_next_codepoint(&text[index], text_len-index, &chars_consumed);
        index += chars_consumed;

        if (codepoint == 0) break;

        int64_t index = GFX__Get_glyph_index(GFX__data.font, codepoint);
        x_advanced += GFX__data.font->glyph_infos[index].xadvance * scale;
    }
    for (; index < text_len;) {
        if (x >= max_width) {
            break;
        }
        int chars_consumed = 0;
        int64_t codepoint = GFX__Get_next_codepoint(&text[index], text_len-index, &chars_consumed);
        index += chars_consumed;

        if (codepoint == 0) break;

        x += GFX_Draw_glyph(GFX__data.font, codepoint, V2(x+pos.x, pos.y), scale, color);
    }
}


#endif // GRAPHICS_IMPLEMENTATION_INCLUDED
#endif // GRAPHICS_IMPLEMENTATION


