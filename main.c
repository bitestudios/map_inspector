
#include "libs/engine/app.h"
#include "libs/engine/madmath.h"
#include "libs/engine/graphics.h"
#include "libs/engine/log.h"
#include "libs/engine/filedialog.h"
#include "libs/stb_sprintf.h"
#include "util.h"
#include "ui.h"
#include "inspector.h"
#include "inconsolata.ttf.inl"
#include "font.h"




#ifndef __wasm__
#include <stdio.h>
#include <stdlib.h>

uint8_t *
Read_all_file(Arena *arena, const char *filepath, int64_t *filesize) {
    bool error = false;
    uint8_t *filedata = NULL;
    int64_t  size = 0;
    
    FILE *f = fopen(filepath, "rb");

    if (!f) error = true;
    
    // Determine the file size and allocate the data
    if (!error) {
        fseek(f, 0, SEEK_END);
        size = ftell(f);
        fseek(f, 0, SEEK_SET);
        filedata = ARENA_Array(arena, uint8_t, size);
        if (!filedata) error = true;
    }

    // Read all the data
    if (!error) {
        fread(filedata, 1, size, f);
    }

    // Release the file handle
    if (f) fclose(f);
    
    if (filesize) *filesize = size;
    return filedata;
}

#endif






#define FONT_HEIGHT 24
UI_Context *ui_ctx;
Arena tmp_arena = {0};
Arena inspector_arena = {0};
const char  *filename = "";
bool    file_loaded = 0;
int64_t filesize    = 0;
char *filedata  = NULL;
Arena filedata_arena = {0};
bool binary_map_loaded   = false;


void
Open_file_cb(FDLG_Context *ctx, const char *new_filename) {
    if (new_filename) {
        file_loaded       = false;
        binary_map_loaded = false;
        filename = new_filename;
        ARENA_Clear(&filedata_arena);
        #ifdef __wasm__
            filesize = FDLG_Wasm_get_filesize();
            filedata = ARENA_Array(&filedata_arena, char, filesize);
            FDLG_Wasm_read_filedata((unsigned char *)filedata, filesize);
            file_loaded = true;
        #else
            filedata = (char *)Read_all_file(&filedata_arena, filename, &filesize);
            if (filedata) file_loaded = true;
        #endif
    }
}

int64_t
Change_sort(int64_t current_order) {
    int64_t result = current_order + 1;
    if (result > SORT_GREATER_FIRST) {
        result = SORT_LESS_FIRST;
    }
    return result;
}




String
Sprintf(Arena *arena, const char *fmt, ...) {
    String result = {0};

    int64_t arena_free_space = arena->cap - arena->cursor;
    if (arena_free_space > 0) {
        char *buffer = (char *)&(arena->memory[arena->cursor]);
        va_list va;
        va_start(va, fmt);
        int64_t string_len = stbsp_vsnprintf(buffer, arena_free_space, fmt, va);
        va_end(va);
        arena->cursor += string_len+1;
        result.data = buffer;
        result.len  = string_len;
    }

    return result;
}


int64_t
Filter_symbols(int64_t filtered_symbols[], String filter_string, Symbol *symbols[], int64_t symbols_count) {
    int64_t filtered_symbols_count = 0;
    for (int64_t symbol_i = 0; symbol_i < symbols_count; symbol_i+=1) {
        if (STR_Contains(symbols[symbol_i]->name, filter_string, true) ||
            STR_Contains(symbols[symbol_i]->area->name, filter_string, true) ||
            STR_Contains(symbols[symbol_i]->module, filter_string, true)) {
            
            filtered_symbols[filtered_symbols_count] = symbol_i;
            filtered_symbols_count += 1;
        }
    }
    return filtered_symbols_count;
}

int64_t
Count_filtered_symbols_bytes(int64_t filtered_symbols[], int64_t filtered_symbols_count, Symbol *symbols[]) {
    int64_t result = 0;
    for (int64_t filtered_symbol_i = 0; filtered_symbol_i < filtered_symbols_count; filtered_symbol_i += 1) {
        Symbol *symbol = symbols[filtered_symbols[filtered_symbol_i]];
        result += symbol->estimated_size;
    }
    return result;
}



int
Frame(void) {
    if (APP_Quit_requested()) return 1;

    static const float font_scales[5] = {0.6f, 0.8f, 1.0f, 1.2f, 1.4f};
    static int64_t current_font_scale = 1;
    static BinaryMap binary_map;
    static int64_t  *filtered_symbols = NULL;
    static int64_t   filtered_symbols_count = 0;
    static int64_t   filtered_symbols_bytes = 0;

    static char input_buffer[128];
    static UI_Input filter_input = {.buffer = input_buffer, .buffer_cap = 128, .width = 200.0f};
    static TableHeaderItem thi_address = {.label = STR_LIT("Addr"), .sorter = true};
    static TableHeaderItem thi_size = {.label = STR_LIT("Size"), .sorter = true};
    static bool reset_table = true;
    static int64_t selected_symbol_index = 0;

    ARENA_Clear(&tmp_arena);

        
    const char *file_dropped = APP_Get_file_dropped_path();
    if (file_dropped) {
        filename = file_dropped;
        #ifndef __wasm__
            file_loaded       = false;
            binary_map_loaded = false;
            filedata = (char *)Read_all_file(&filedata_arena, filename, &filesize);
            if (filedata) file_loaded = true;
        #endif
    }

    static float max_danger_rate = 1.0f;

    if (file_loaded) {
        file_loaded = 0;
        filtered_symbols = NULL;
        filtered_symbols_count = 0;
        ARENA_Clear(&inspector_arena);
        bool success = Gen_binary_map_from_memory(&inspector_arena, &binary_map, filedata, filesize);
        Sort_symbols_by(&binary_map, SORT_KIND_SIZE_GREATER_FIRST);
        thi_address.sort_order = NO_SORT;
        thi_size.sort_order    = SORT_GREATER_FIRST;
        if (success) {
            binary_map_loaded = true;
            reset_table = true;
            float total_size = (float)binary_map.size;
            float mean_size = 0.0f;
            int64_t percentil_count = (int64_t)((float)binary_map.symbols_count*0.01f);
            for (int64_t symbol_i = 0; symbol_i < percentil_count; symbol_i+=1) {
                float symbol_size = (float)binary_map.symbols[symbol_i]->estimated_size;
                mean_size += symbol_size/total_size;
            }
            mean_size /= (float)percentil_count;
            max_danger_rate = mean_size;
            
            String filter_string   = UI_Get_input_string(&filter_input);
            filtered_symbols       = ARENA_Array(&inspector_arena, int64_t, binary_map.symbols_count);
            filtered_symbols_count = Filter_symbols(filtered_symbols, filter_string, binary_map.symbols, binary_map.symbols_count);
            filtered_symbols_bytes = Count_filtered_symbols_bytes(filtered_symbols, filtered_symbols_count, binary_map.symbols);
        }
    }

    if ((APP_Get_key_state(APP_KEY_LEFT_CONTROL) & APP_STATE_DOWN) ||
        (APP_Get_key_state(APP_KEY_RIGHT_CONTROL) & APP_STATE_DOWN)) {
        
        if (((APP_Get_key_state(APP_KEY_MINUS) & APP_STATE_PRESSED) ||
            (APP_Get_key_state(APP_KEY_1) & APP_STATE_PRESSED)) &&
            current_font_scale > 0) {
            current_font_scale -= 1;
        }

        if (((APP_Get_key_state(APP_KEY_EQUAL) & APP_STATE_PRESSED) ||
            (APP_Get_key_state(APP_KEY_2) & APP_STATE_PRESSED)) &&
            current_font_scale < 4) {
            current_font_scale += 1;
        }
    }


    ui_ctx->font_scale = font_scales[current_font_scale];
    ui_ctx->pointer_pos   = V2(APP_Get_pointer_x(), APP_Get_pointer_y());
    ui_ctx->pointer_state = APP_Get_pointer_state();
    ui_ctx->delta_time    = ((float)APP_Frame_duration(5))*1e-9f;

    glViewport(0, 0, APP_Get_window_width(), APP_Get_window_height());
    glClearColor(0.40, 0.40, 0.40, 1);
    glClear(GL_COLOR_BUFFER_BIT);

    GFX_Begin();

    Mat4 ortho = M4_Orthographic(0, APP_Get_window_width(), APP_Get_window_height(), 0, 0, 1);
    GFX_Set_matrix(ortho);
    
    UI_Begin(ui_ctx, 0, 0, APP_Get_window_width(), APP_Get_window_height());
    
    GFX_Set_white();

    UI_Begin_row(ui_ctx, UI_Key(STR_LIT("first_row")), COLOR(200, 80, 0, 255));
        static UI_Button open = {.label = STR_LIT("Open file")};
        if (UI_Do_button(ui_ctx, &open)) {
            static FDLG_Context ctx = {
                .title       = "Load file",
                .callback    = Open_file_cb
            };
            FDLG_Dialog(&ctx, 0);
        }
        if (binary_map_loaded) {
            
            if (UI_Do_input(ui_ctx, &filter_input)) {
                String filter_string   = UI_Get_input_string(&filter_input);
                filtered_symbols_count = Filter_symbols(filtered_symbols, filter_string, binary_map.symbols, binary_map.symbols_count);
                filtered_symbols_bytes = Count_filtered_symbols_bytes(filtered_symbols, filtered_symbols_count, binary_map.symbols);
            }

            static UI_Button clear_filter = {.label = STR_LIT("X")};
            if (UI_Do_button(ui_ctx, &clear_filter)) {
                UI_Clear_input(&filter_input);
                filtered_symbols_count = Filter_symbols(filtered_symbols, STR_LIT(""), binary_map.symbols, binary_map.symbols_count);
                filtered_symbols_bytes = Count_filtered_symbols_bytes(filtered_symbols, filtered_symbols_count, binary_map.symbols);
            }
            
            float bytes_rate = ((float)filtered_symbols_bytes)/((float)binary_map.size)*100.0f;
            String items_found_str = Sprintf(
                &tmp_arena,
                "%lld/%lld Syms | %lld/%lld Bytes (%.2f%%)",
                filtered_symbols_count,
                binary_map.symbols_count,
                filtered_symbols_bytes,
                binary_map.size,
                bytes_rate);
            UI_Label(ui_ctx, items_found_str, UI_Key(STR_LIT("items_found_str")));
        }
    UI_End_row(ui_ctx);

    if (binary_map_loaded) {
        
        String selected_symbol_name    = STR_LIT("");
        String selected_symbol_address = STR_LIT("");
        String selected_symbol_size    = STR_LIT("");
        String selected_area_name      = STR_LIT("");
        String selected_module_name    = STR_LIT("");

        if (selected_symbol_index >= 0 && selected_symbol_index < filtered_symbols_count) {
            Symbol *symbol = binary_map.symbols[filtered_symbols[selected_symbol_index]];
            selected_symbol_name    = symbol->name;
            selected_symbol_address = Sprintf(&tmp_arena, "0x%llX", symbol->value);
            selected_symbol_size    = Sprintf(&tmp_arena, "%lld",   symbol->estimated_size);
            selected_area_name      = symbol->area->name;
            selected_module_name    = symbol->module;
        }

        UI_Begin_row(ui_ctx, UI_Key(STR_LIT("second_row")), COLOR(127, 80, 0, 255));
            UI_Label(ui_ctx, Sprintf(&tmp_arena, "File: %s", filename), UI_Key(STR_LIT("File")));
        UI_End_row(ui_ctx);

        UI_Begin_row(ui_ctx, UI_Key(STR_LIT("third_row")), COLOR(127, 80, 0, 255));
            String lbl = Sprintf(
                &tmp_arena,
                "Symbol: \"%.*s\" | Address: %.*s | Size: %.*s",
                STR_VARG(selected_symbol_name),
                STR_VARG(selected_symbol_address),
                STR_VARG(selected_symbol_size)
            );
            UI_Label(ui_ctx, lbl, UI_Key(STR_LIT("Symbol")));
        UI_End_row(ui_ctx);

        UI_Begin_row(ui_ctx, UI_Key(STR_LIT("forth_row")), COLOR(127, 80, 0, 255));
            lbl = Sprintf(
                &tmp_arena,
                "Area: \"%.*s\" | Module: \"%.*s\"",
                STR_VARG(selected_area_name),
                STR_VARG(selected_module_name)
            );
            UI_Label(ui_ctx, lbl, UI_Key(STR_LIT("Area")));
        UI_End_row(ui_ctx);

        float padding = 5.0f;
        float item_height = ui_ctx->font->height*ui_ctx->font_scale + padding*2.0f;

        static UI_Table table = {0};
        if (reset_table) {
            void *
            memset(void *dst_, int c, size_t n);
            memset(&table, 0, sizeof(table));
            reset_table = false;
            String filter_string   = UI_Get_input_string(&filter_input);
            filtered_symbols_count = Filter_symbols(filtered_symbols, filter_string, binary_map.symbols, binary_map.symbols_count);
            filtered_symbols_bytes = Count_filtered_symbols_bytes(filtered_symbols, filtered_symbols_count, binary_map.symbols);
        }

        UI_Begin_table(
            ui_ctx,
            &table, 
            1200.0f,
            6,
            (float[6]){10.f/30.f, 4.f/30.f, 4.f/30.f, 4.f/30.f, 4.f/30.f, 4.f/30.f},
            item_height,
            filtered_symbols_count
        );

        {
            UI_Begin_table_header(ui_ctx, &table, DARKGRAY);
            {
                static TableHeaderItem thi_name = {.label = STR_LIT("Name")};
                static TableHeaderItem thi_area = {.label = STR_LIT("Area")};
                static TableHeaderItem thi_module = {.label = STR_LIT("Module")};
                static TableHeaderItem thi_rate = {.label = STR_LIT("Rate")};

                UI_Table_header_item(ui_ctx, &table, &thi_name);

                UI_Table_header_item(ui_ctx, &table, &thi_area);

                UI_Table_header_item(ui_ctx, &table, &thi_module);

                if (UI_Table_header_item(ui_ctx, &table, &thi_address)) {
                    thi_size.sort_order = NO_SORT;
                    thi_address.sort_order = Change_sort(thi_address.sort_order);
                    if (thi_address.sort_order == SORT_LESS_FIRST) {
                        Sort_symbols_by(&binary_map, SORT_KIND_ADDRESS_LESS_FIRST);
                    }
                    else {
                        Sort_symbols_by(&binary_map, SORT_KIND_ADDRESS_GREATER_FIRST);
                    }
                    reset_table = true;
                }

                if (UI_Table_header_item(ui_ctx, &table, &thi_size)) {
                    thi_address.sort_order = NO_SORT;
                    thi_size.sort_order = Change_sort(thi_size.sort_order);
                    if (thi_size.sort_order == SORT_LESS_FIRST) {
                        Sort_symbols_by(&binary_map, SORT_KIND_SIZE_LESS_FIRST);
                    }
                    else {
                        Sort_symbols_by(&binary_map, SORT_KIND_SIZE_GREATER_FIRST);
                    }
                    reset_table = true;
                }

                UI_Table_header_item(ui_ctx, &table, &thi_rate);
            }
            UI_End_table_header(ui_ctx, &table);

            for (int i = table.first_item_index; i <= table.last_item_index; i+=1) {

                Symbol *symbol = binary_map.symbols[filtered_symbols[i]];

                String name   = symbol->name;

                String area   = symbol->area->name;

                String module = symbol->module;

                String address = Sprintf(&tmp_arena, "0x%llX", symbol->value);

                String size = Sprintf(&tmp_arena, "%lld", symbol->estimated_size);

                float rate_flt = ((float)(symbol->estimated_size)) / (float)binary_map.size;
                Color row_color = Color_lerp(COLOR(0, 128, 0, 255), RED, Min(1.0f, rate_flt/max_danger_rate));
                String rate = Sprintf(&tmp_arena, "%.2f%%", rate_flt * 100.0f);

                bool row_hovered = UI_Begin_table_row(ui_ctx, &table, row_color);
                {
                    UI_Table_item(ui_ctx, &table, name);
                    UI_Table_item(ui_ctx, &table, area);
                    UI_Table_item(ui_ctx, &table, module);
                    UI_Table_item(ui_ctx, &table, address);
                    UI_Table_item(ui_ctx, &table, size);
                    UI_Table_item(ui_ctx, &table, rate);
                }
                UI_End_table_row(ui_ctx, &table);

                if (row_hovered) {
                    selected_symbol_index = i;
                }
            }
        }
        UI_End_table(ui_ctx, &table);
    }

    

    UI_End(ui_ctx);

    GFX_End();
    APP_Swap_buffers();
    return 0;
}


#ifdef __wasm__
APP_ENTRY int
main() {
#else
int
main(int argc, const char **argv) {
#endif
    static uint8_t filedata_arena_memory[16*1024*1024];
    ARENA_Init(&filedata_arena, filedata_arena_memory, sizeof(filedata_arena_memory));
    
    // If we aren't on the web version we check the second argument as a input file
    #ifndef __wasm__
    if (argc == 2) {
        filename = argv[1];
        file_loaded       = false;
        binary_map_loaded = false;
        filedata = (char *)Read_all_file(&filedata_arena, filename, &filesize);
        if (filedata) file_loaded = true;
    }
    #endif

    if (APP_Init("Map inspector", 600, 480) != 0) return -1;
    if (GFX_Init() != 0) return -1;
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);
    glEnable(GL_MULTISAMPLE);

    static uint8_t tmp_arena_memory[16*1024*1024];
    ARENA_Init(&tmp_arena, tmp_arena_memory, sizeof(tmp_arena_memory));

    static uint8_t inspector_arena_memory[16*1024*1024];
    ARENA_Init(&inspector_arena, inspector_arena_memory, sizeof(inspector_arena_memory));
    
    static Arena   ui_arena;
    static uint8_t ui_arena_memory[4*1024*1024];
    ARENA_Init(&ui_arena, ui_arena_memory, sizeof(ui_arena_memory));
    ui_ctx = UI_Create(&ui_arena);

    ui_ctx->clickable_default_fill_color     = Color_To_Vec4(DARKGRAY);
    ui_ctx->clickable_default_outline_color  = Color_To_Vec4(BLACK);
    ui_ctx->clickable_hovered_fill_color     = Color_To_Vec4(GRAY);
    ui_ctx->clickable_hovered_outline_color  = Color_To_Vec4(DARKGRAY);
    ui_ctx->clickable_clicked_fill_color     = Color_To_Vec4(DARKGRAY);
    ui_ctx->clickable_clicked_outline_color  = Color_To_Vec4(WHITE);
    ui_ctx->font_scale = 0.8f;
    GFX_Font *font = Load_font(&tmp_arena, inconsolata_ttf, sizeof(inconsolata_ttf), FONT_HEIGHT);
    ASSERT(font);
    ui_ctx->font = font;


    APP_Run_application_loop(Frame);

    return 0;
}



#ifdef __wasm__


void *
memcpy(void *dest_, const void *src_, size_t n) {
    char *dest = dest_;
    const char *src = src_;
    for (size_t i = 0; i < n; i+=1) {
        dest[i] = src[i];
    }
    return dest_;
}


void *
memset(void *dst_, int c, size_t n) {

    size_t cursor = 0;

    uint8_t *restrict dst = dst_;
    uint8_t val8 = (uint8_t)c;
    if (!val8) {
        if (n > 7) {
            for (; cursor < n-7; cursor+=8) {
                dst[cursor+0] = 0;
                dst[cursor+1] = 0;
                dst[cursor+2] = 0;
                dst[cursor+3] = 0;

                dst[cursor+4] = 0;
                dst[cursor+5] = 0;
                dst[cursor+6] = 0;
                dst[cursor+7] = 0;
            }
        }
        for (; cursor < n; cursor+=1) {
            dst[cursor] = 0;
        }
        return dst;
    }
    else {
        if (n > 7) {
            for (; cursor < n-7; cursor+=8) {
                dst[cursor+0] = val8;
                dst[cursor+1] = val8;
                dst[cursor+2] = val8;
                dst[cursor+3] = val8;

                dst[cursor+4] = val8;
                dst[cursor+5] = val8;
                dst[cursor+6] = val8;
                dst[cursor+7] = val8;
            }
        }
        for (; cursor < n; cursor+=1) {
            dst[cursor] = val8;
        }
        return dst;
    }

}

int
memcmp(const void *str1_, const void *str2_, size_t n) {
    const char *str1 = str1_;
    const char *str2 = str2_;
    for (size_t i = 0; i < n; i+=1) {
        int subs = str1[i] - str2[i];
        if (subs) return subs;
    }
    return 0;
}
#endif


#define APP_IMPLEMENTATION
#include "libs/engine/app.h"
#define GRAPHICS_IMPLEMENTATION
#include "libs/engine/graphics.h"
#define LOG_IMPLEMENTATION
#include "libs/engine/log.h"
#define STB_SPRINTF_IMPLEMENTATION
#include "libs/stb_sprintf.h"
#define FDLG_IMPLEMENTATION
#define FDLG__SNPRINTF stbsp_snprintf
#include "libs/engine/filedialog.h"
#define UTIL_IMPLEMENTATION
#include "util.h"
#define INSPECTOR_IMPLEMENTATION
#include "inspector.h"
#define UI_IMPLEMENTATION
#include "ui.h"
#define FONT_IMPLEMENTATION
#include "font.h"



