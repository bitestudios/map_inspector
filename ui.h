
#ifndef UI_INCLUDED
#define UI_INCLUDED

#include "util.h"
#include "libs/engine/madmath.h"
#include "libs/engine/graphics.h"
#include "libs/engine/app.h"


enum {
    UI_WIDTH_FIXED  = 0x2,
    UI_WIDTH_ADAPTS_TO_PARENT = 0x4,
    UI_WIDTH_ADAPTS_TO_CHILDS = 0x8,
    UI_HEIGHT_FIXED  = 0x10,
    UI_HEIGHT_ADAPTS_TO_PARENT = 0x20,
    UI_HEIGHT_ADAPTS_TO_CHILDS = 0x40,
    UI_VERTICAL_CENTER = 0x80,
    UI_HORIZONTAL_CENTER = 0x100,
};

enum {
    UI_HOVERED  = 0x1,
    UI_PRESSED  = 0x2,
    UI_RELEASED = 0x4,
};

typedef struct UI_Element UI_Element;
struct UI_Element{
    uint64_t flags;
    UI_Element *parent;
    UI_Element *childs;
    UI_Element *next;

    uint64_t status;

    float padding;
    float x;
    float y;
    float width;
    float height;
    float min_width;
    float min_height;
    float max_width;
    float max_height;
    float empty_x_offset;
    float empty_y_offset;
    float empty_y_max;
};


typedef struct {
    uint64_t flags;

    float padding;
    float x;
    float y;
    float width;
    float height;
    float min_width;
    float min_height;
    float max_width;
    float max_height;
} UI_ElementDesc;



typedef struct UI_ElementNode UI_ElementNode;
struct UI_ElementNode {
    UI_ElementNode *next;
    UI_Element element;
};

typedef struct {
    UI_ElementNode *first;
    UI_ElementNode *last;
    UI_ElementNode *first_free;
} UI_ElementNodeList;


typedef struct {
    HashMap *elements_map;
    UI_Element         *root;
    UI_ElementNodeList elements_list;
} UI_TreeInfo;

#define UI_MAX_PARENTS_STACK 16

typedef struct {
    GFX_Font *font;
    float font_scale;

    float delta_time;

    Vec4 clickable_default_fill_color;
    Vec4 clickable_default_outline_color;
    Vec4 clickable_hovered_fill_color;
    Vec4 clickable_hovered_outline_color;
    Vec4 clickable_clicked_fill_color;
    Vec4 clickable_clicked_outline_color;

    Vec2     pointer_pos;
    int      pointer_state;

    int64_t parents_stack_count;
    UI_Element *parents_stack[UI_MAX_PARENTS_STACK];
    UI_Element *last_child;

    int64_t     current_tree_info;
    UI_TreeInfo tree_infos[2];
    Arena *arena;

} UI_Context;


static inline uint64_t
UI_Key(String s) {
    return STR_Hash(s);
}


UI_Context *
UI_Create(Arena *arena);


void
UI_Begin(UI_Context *ctx, float x, float y, float w, float h);

void
UI_End(UI_Context *ctx);



void
UI_Begin_row(UI_Context *ctx, uint64_t id, Color color);

void
UI_End_row(UI_Context *ctx);



typedef struct {
    String label;
    bool colors_init;
    Vec4 current_fill_color;
    Vec4 target_fill_color;
    Vec4 current_outline_color;
    Vec4 target_outline_color;
} UI_Button;

bool
UI_Do_button(UI_Context *ctx, UI_Button *button);



typedef struct {
    int64_t columns;
    float  *columns_width_percent;
    UI_Element *element;
    int64_t items_count;
    uint64_t current_element;
    uint64_t current_column;
    uint64_t current_row;
    int64_t first_item_index;
    int64_t last_item_index;
    float item_height;
    float current_height_percent;
    float current_scroll_offset;
    float target_scroll_offset;
    float items_draw_offset;
    float items_area_start;
    float items_area_height;
    bool  scrollbar_holded;
    float scrollbar_cursor_offset;
    float scrollbar_current_alpha;
    float scrollbar_target_alpha;
} UI_Table;


void
UI_Begin_table(
    UI_Context *ctx,
    UI_Table *table,
    float width,
    int64_t columns,
    float *columns_width_percent,
    float item_height,
    int64_t items_count);

void
UI_End_table(UI_Context *ctx, UI_Table *table);

void
UI_Begin_table_header(UI_Context *ctx, UI_Table *table, Color color);

void
UI_End_table_header(UI_Context *ctx, UI_Table *table);


enum {
    NO_SORT = 0,
    SORT_LESS_FIRST,
    SORT_GREATER_FIRST,
};

typedef struct {
    String label;
    bool    sorter;
    int64_t sort_order;
    bool colors_init;
    float current_tri_angle;
    float target_tri_angle;
    Vec4 current_fill_color;
    Vec4 target_fill_color;
    Vec4 current_outline_color;
    Vec4 target_outline_color;
} TableHeaderItem;

bool
UI_Table_header_item(UI_Context *ctx, UI_Table *table, TableHeaderItem *header_item);

bool
UI_Begin_table_row(UI_Context *ctx, UI_Table *table, Color color);

void
UI_End_table_row(UI_Context *ctx, UI_Table *table);

void
UI_Table_item(UI_Context *ctx, UI_Table *table, String s);



void
UI_Label(UI_Context *ctx, String s, uint64_t key);





typedef struct {
    char   *buffer;
    int64_t buffer_cap;
    int64_t buffer_bytes_used;
    int64_t cursor_byte_pos;
    float   width;
    float   text_shift;
    Vec4    current_outline_color;
    bool focused;
} UI_Input;

void
UI_Clear_input(UI_Input *input);

String
UI_Get_input_string(UI_Input *input);

bool
UI_Do_input(UI_Context *ctx, UI_Input *input);


#endif // UI_INCLUDED





#ifdef UI_IMPLEMENTATION
#ifndef UI_IMPLEMENTATION_INCLUDED
#define UI_IMPLEMENTATION_INCLUDED


// We need memset later..
void *
memset(void *dst_, int c, size_t n);

UI_Element *
UI_ElementNodeList_New_element(Arena *arena, UI_ElementNodeList *list) {
    UI_ElementNode *new_node = NULL;
    if (list->first_free) {
        new_node = list->first_free;
        list->first_free = new_node->next;
    }
    else {
        new_node = ARENA_New(arena, UI_ElementNode);
    }

    if (!new_node) return NULL;

    memset(new_node, 0, sizeof(*new_node));
    if (list->first && list->last) {
        list->last->next = new_node;
        list->last = new_node;
    }
    else {
        list->first = new_node;
        list->last  = new_node;
    }

    return &(new_node->element);
}


void
UI_ElementNodeList_Delete_all_elements(UI_ElementNodeList *list) {
    if (list->last) {
        if (list->first_free) {
            list->last->next = list->first_free;
        }
        list->first_free = list->first;
        list->last  = NULL;
        list->first = NULL;
    }
}





UI_Context *
UI_Create(Arena *arena) {
    UI_Context *result = ARENA_New(arena, UI_Context);
    if (!result) return NULL;
    memset(result, 0, sizeof(*result));
    result->arena = arena;
    result->tree_infos[0].elements_map = HashMap_New(arena, 1024*32);
    result->tree_infos[1].elements_map = HashMap_New(arena, 1024*32);
    return result;
}


UI_Element *
UI_Current_parent(UI_Context *ctx) {
    if (ctx->parents_stack_count > 0) {
        return ctx->parents_stack[ctx->parents_stack_count-1];
    }
    return NULL;
}


void
UI_Push_parent(UI_Context *ctx, UI_Element *parent) {
    ASSERT(ctx->parents_stack_count < UI_MAX_PARENTS_STACK);
    ctx->parents_stack[ctx->parents_stack_count] = parent;
    ctx->parents_stack_count += 1;
    ctx->last_child = NULL;
}

UI_Element *
UI_Pop_parent(UI_Context *ctx) {
    ASSERT(ctx->parents_stack_count > 0);
    ctx->parents_stack_count -= 1;
    UI_Element *result = ctx->parents_stack[ctx->parents_stack_count];
    ctx->last_child = result;
    return result;
}




UI_Element *
UI_Add_element(UI_Context *ctx, uint64_t element_id, const UI_ElementDesc *element_desc) {
    UI_TreeInfo *old_tree_info = &(ctx->tree_infos[(ctx->current_tree_info+1)%2]);
    UI_TreeInfo *tree_info     = &(ctx->tree_infos[ctx->current_tree_info]);

    UI_Element *element_old = NULL;

    uint64_t *element_old_handle = HashMap_Lookup(old_tree_info->elements_map, element_id);
    if (element_old_handle) {
        element_old = ((UI_Element *)(*element_old_handle));
    }
    
    UI_Element *element = UI_ElementNodeList_New_element(ctx->arena, &(tree_info->elements_list));
    ASSERT(HashMap_Insert(ctx->arena, tree_info->elements_map, element_id, (uint64_t)element, false));

    element->flags   = element_desc->flags;
    element->padding = element_desc->padding;
    element->x       = element_desc->x;
    element->y       = element_desc->y;
    element->min_width  = element_desc->min_width;
    element->max_width  = element_desc->max_width;
    element->min_height = element_desc->min_height;
    element->max_height = element_desc->max_height;

    if (element_old) {
        element->status = element_old->status;
        element->x = element_old->x;
        element->y = element_old->y;
        element->width  = element_old->width;
        element->height = element_old->height;
    }
    
    if (element_desc->flags & UI_WIDTH_FIXED) {
        element->width = element_desc->width;
    }
    if (element_desc->flags & UI_HEIGHT_FIXED) {
        element->height = element_desc->height;
    }

    UI_Element *current_parent = UI_Current_parent(ctx);
    element->parent = current_parent;

    if (ctx->last_child) {
        ctx->last_child->next = element;
    }
    else if (current_parent) {
        current_parent->childs = element;
    }
    ctx->last_child = element;

    float x0 = element->x;
    float x1 = x0 + element->width;
    float y0 = element->y;
    float y1 = y0 + element->height;

    // Test if is focused
    if (ctx->pointer_pos.x > x0 && ctx->pointer_pos.x < x1 && 
        ctx->pointer_pos.y > y0 && ctx->pointer_pos.y < y1) {

        if (element->status & (UI_HOVERED|UI_PRESSED)) {
            if (ctx->pointer_state & APP_STATE_DOWN) {
                element->status = UI_PRESSED;
            }
            else if (ctx->pointer_state & APP_STATE_RELEASED) {
                element->status = UI_RELEASED;
            }
        }
        else if (ctx->pointer_state & APP_STATE_UP) {
            element->status = UI_HOVERED;
        }

    }
    else {
        element->status = 0;
    }

    return element;
}

void
UI_Begin(UI_Context *ctx, float x, float y, float w, float h) {

    ASSERT(ctx->parents_stack_count == 0);

    ctx->current_tree_info = (ctx->current_tree_info+1)%2;
    UI_TreeInfo *tree_info = &(ctx->tree_infos[ctx->current_tree_info]);
    HashMap_Delete_all_values(tree_info->elements_map);
    UI_ElementNodeList_Delete_all_elements(&(tree_info->elements_list));
    
    uint64_t element_id = 0;
    UI_Element *element = UI_Add_element(ctx, element_id, &(UI_ElementDesc){
        .flags  = UI_WIDTH_FIXED|UI_HEIGHT_FIXED,
        .x      = x,
        .y      = y,
        .width  = w,
        .height = h,
    });

    UI_Push_parent(ctx, element);
    tree_info->root = element;
}


void
UI_Build_layout(UI_Element *element) {
    
    UI_Element *parent = element->parent;
    float padding = element->padding;
    
    float computed_width = element->width;
    if (element->flags & UI_WIDTH_ADAPTS_TO_PARENT) {
        if (parent) {
            float parent_empty_width = parent->width - parent->empty_x_offset;
            computed_width = parent_empty_width - padding * 2.0f;
        }
    }
    
    computed_width = Max(computed_width, element->min_width);
    if (element->max_width > 0.0f) {
        computed_width = Min(computed_width, element->max_width);
    }
    element->width = computed_width;

    if (parent) {
        if (element->flags & UI_HORIZONTAL_CENTER) {
            float parent_empty_width = parent->width - parent->empty_x_offset;
            element->x = parent->x + parent->empty_x_offset + (parent_empty_width-computed_width)*0.5f;
        }
        else {
            element->x = parent->x + parent->empty_x_offset + padding;
        }
    }


    float computed_height = element->height;
    if (element->flags & UI_HEIGHT_ADAPTS_TO_PARENT) {
        if (parent) {
            float height    = parent->height - parent->empty_y_offset;
            computed_height = height - padding * 2.0f;
        }
    }
    if (element->flags & UI_HEIGHT_ADAPTS_TO_CHILDS) {
        float max_h  = 0.0f;
        for (UI_Element *child = element->childs; child; child = child->next) {
            float h = child->height + (child->y - element->y) + child->padding;
            if (h > max_h) {
                max_h = h;
            }
        }
        computed_height = max_h;
    }

    computed_height = Max(computed_height, element->min_height);
    if (element->max_height > 0.0f) {
        computed_height = Min(computed_height, element->max_height);
    }
    element->height = computed_height;


    if (parent) {
        element->y = parent->y + parent->empty_y_offset + padding;
    }

    if (parent) {
        // Update the empty y max
        parent->empty_y_max = Max(parent->empty_y_max, parent->empty_y_offset + element->height + padding);
        
        parent->empty_x_offset += element->width + padding;

        // If we arrive to the x limit we go down 1 row
        float parent_rem_width = parent->width - parent->empty_x_offset;
        if (parent_rem_width <= padding) {
            parent->empty_x_offset = 0.0f;
            parent->empty_y_offset = parent->empty_y_max;
        }
    }

    for (UI_Element *child = element->childs; child; child = child->next) {
        UI_Build_layout(child);
    }
}

void
UI_End(UI_Context *ctx) {
    UI_Pop_parent(ctx);
    ASSERT(ctx->parents_stack_count == 0);
    UI_TreeInfo *tree_info = &(ctx->tree_infos[ctx->current_tree_info]);
    UI_Build_layout(tree_info->root);
}






// Draws rounded outlined rectangles
void
Draw_rectangle_ex(
    Vec2 pos, float width, float height, float radius_percent, float outline_thickness,
    Color fill_color, Color outline_color) {

    const int subdivisions = 4*5;
    const float theta = 2*PI32/(float)subdivisions;
    const float cos_theta = Cos(theta);
    const float sin_theta = Sin(theta);
    
    float radius;
    if (width > height) {
        radius = height*0.5f*radius_percent;
    }
    else {
        radius = width*0.5f*radius_percent;
    }

    Vec2 dir0 = V2(-1.0f, 0.0f);
    if (radius > 0.0f) {
        for (int i = 0; i < subdivisions; i += 1) {
	        Vec2 dir1 = V2(dir0.x * cos_theta - dir0.y * sin_theta, dir0.x * sin_theta + dir0.y * cos_theta);
            int corner = (i/(subdivisions/4));
            Vec2 center;
            if (corner == 0) {
                center = V2(pos.x+radius, pos.y+radius);
            }
            else if (corner == 1) {
                center = V2(pos.x+width-radius, pos.y+radius);
            }
            else if (corner == 2) {
                center = V2(pos.x+width-radius, pos.y+height-radius);
            }
            else {
                center = V2(pos.x+radius, pos.y+height-radius);
            }
            Vec2 p0_in = V2_Add(V2_Mulf(dir0, radius-outline_thickness), center);
            Vec2 p1_in = V2_Add(V2_Mulf(dir1, radius-outline_thickness), center);
	    	GFX_Draw_triangle(center, p0_in, p1_in, fill_color);
            Vec2 p0_out = V2_Add(V2_Mulf(dir0, radius), center);
            Vec2 p1_out = V2_Add(V2_Mulf(dir1, radius), center);
	        GFX_Draw_quad(p0_in, p1_in, p1_out, p0_out, outline_color);
            dir0 = dir1;
        }
    }

    { // Draw the inner top block
        Vec2  p = V2(pos.x+radius, pos.y+outline_thickness);
        float w = width-radius*2.0f;
        float h = radius-outline_thickness;
        if (h > 0.0f && w > 0.0f)  GFX_Draw_rect(p, w, h, fill_color);
    }
    
    { // Draw the inner center block
        float v_pad = Max(radius, outline_thickness);
        Vec2  p = V2(pos.x+outline_thickness, pos.y+v_pad);
        float w = width-outline_thickness*2.0f;
        float h = height-v_pad*2.0f;
        if (h > 0.0f && w > 0.0f) GFX_Draw_rect(p, w, h, fill_color);
    }

    { // Draw the inner bottom block
        Vec2  p = V2(pos.x+radius, pos.y+height-radius);
        float w = width-radius*2.0f;
        float h = radius-outline_thickness;
        if (h > 0.0f && w > 0.0f) GFX_Draw_rect(p, w, h, fill_color);
    }

    { // Draw the out top block
        Vec2  p = V2(pos.x+radius, pos.y);
        float w = width-radius*2.0f;
        float h = outline_thickness;
        if (h > 0.0f && w > 0.0f) GFX_Draw_rect(p, w, h, outline_color);
    }
    
    { // Draw the out center left block
        float v_pad = Max(radius, outline_thickness);
        Vec2  p = V2(pos.x, pos.y+v_pad);
        float w = outline_thickness;
        float h = height-v_pad*2.0f;
        if (h > 0.0f && w > 0.0f) GFX_Draw_rect(p, w, h, outline_color);
    }

    { // Draw the out center right block
        float v_pad = Max(radius, outline_thickness);
        Vec2  p = V2(pos.x+width-outline_thickness, pos.y+v_pad);
        float w = outline_thickness;
        float h = height-v_pad*2.0f;
        if (h > 0.0f && w > 0.0f) GFX_Draw_rect(p, w, h, outline_color);
    }

    { // Draw the out bottom block
        Vec2  p = V2(pos.x+radius, pos.y+height-outline_thickness);
        float w = width-radius*2.0f;
        float h = outline_thickness;
        if (h > 0.0f && w > 0.0f) GFX_Draw_rect(p, w, h, outline_color);
    }
}




float
Exp_animation_float(float current, float target, float dt, float amount, float time) {

    // x = dt*Log2((amount+epsilon)/epsilon)/time
    //
    // f(x) = (-(amount+epsilon) * 2^-x + amount + epsilon) / amount
    // 
    // f(0) = 0
    // f(1) = 1
    
    const float epsilon = 0.01f;
    float x  = dt*Log2((amount+epsilon)/epsilon)/time;
    float fx = ((-(amount+epsilon) * Exp2(-x)) + amount + epsilon) / amount;
    fx = Clamp(fx, 0.0f, 1.0f);

    float result = current + (target-current)*fx;
    return result;
}


Vec4
Exp_animation_color(Vec4 a, Vec4 b, float dt, float amount, float time) {
    Vec4 result;
    result.r = Exp_animation_float(a.r, b.r, dt, amount, time);
    result.g = Exp_animation_float(a.g, b.g, dt, amount, time);
    result.b = Exp_animation_float(a.b, b.b, dt, amount, time);
    result.a = Exp_animation_float(a.a, b.a, dt, amount, time);
    return result;
}






void
UI_Begin_row(UI_Context *ctx, uint64_t id, Color color) {
    UI_Element *element = UI_Add_element(ctx, id, &(UI_ElementDesc){
        .flags  = UI_WIDTH_ADAPTS_TO_PARENT|UI_HEIGHT_ADAPTS_TO_CHILDS,
        .height = 100.0f
    });
    UI_Push_parent(ctx, element);
    GFX_Set_white();
    GFX_Draw_rect(V2(element->x, element->y), element->width, element->height, color);
}

void
UI_End_row(UI_Context *ctx) {
    UI_Pop_parent(ctx);
}



bool
UI_Do_button(UI_Context *ctx, UI_Button *button) {
    
    uint64_t key = (uint64_t)button;

    float padding = 5.0f;

    float tw = GFX_Get_text_width(ctx->font, button->label.data, button->label.len)*ctx->font_scale;
    float r_w = tw+padding*4.0f;
    float r_h = ctx->font->height*ctx->font_scale+padding*2.0f;

    UI_Element *element = UI_Add_element(ctx, key, &(UI_ElementDesc){
        .flags  = UI_WIDTH_FIXED|UI_HEIGHT_FIXED,
        .width  = r_w,
        .height = r_h,
        .padding = padding,
    });

    if (!button->colors_init) {
        button->current_fill_color = ctx->clickable_default_fill_color;
        button->target_fill_color  = ctx->clickable_default_fill_color;
        button->current_outline_color = ctx->clickable_default_outline_color;
        button->target_outline_color  = ctx->clickable_default_outline_color;
        button->colors_init = true;
    }

    Vec2  r_pos = V2(element->x, element->y);

    GFX_Set_white();

    if (element->status & UI_PRESSED) {
        button->target_fill_color    = ctx->clickable_clicked_fill_color;
        button->target_outline_color = ctx->clickable_clicked_outline_color;
    }
    else if (element->status & UI_HOVERED) {
        button->target_fill_color    = ctx->clickable_hovered_fill_color;
        button->target_outline_color = ctx->clickable_hovered_outline_color;
    }
    else {
        button->target_fill_color    = ctx->clickable_default_fill_color;
        button->target_outline_color = ctx->clickable_default_outline_color;
    }

    button->current_fill_color    = Exp_animation_color(
        button->current_fill_color, button->target_fill_color, ctx->delta_time, 1.0f, 0.5f);
    button->current_outline_color = Exp_animation_color(
        button->current_outline_color, button->target_outline_color, ctx->delta_time, 1.0f, 0.5f);
    
    Color r_fill_color    = V4_To_Color(button->current_fill_color);
    Color r_outline_color = V4_To_Color(button->current_outline_color);
    Draw_rectangle_ex(r_pos, r_w, r_h, 0.6f, 2.0f, r_fill_color, r_outline_color);

    GFX_Set_font(ctx->font);
    Vec2 t_pos = V2(r_pos.x+padding*2, r_pos.y);
    GFX_Draw_text_sized(button->label.data, button->label.len, V2(t_pos.x+1, t_pos.y+1), ctx->font_scale, BLACK);
    GFX_Draw_text_sized(button->label.data, button->label.len, t_pos, ctx->font_scale, WHITE);
    
    bool result = (element->status & UI_RELEASED) != 0;
    return result;
}




void
UI_Begin_table(
    UI_Context *ctx,
    UI_Table *table,
    float max_width,
    int64_t columns,
    float *columns_width_percent,
    float item_height,
    int64_t items_count) {

    uint64_t key = (uint64_t)table;
    UI_Element *table_element = UI_Add_element(ctx, key, &(UI_ElementDesc){
        //.flags  = UI_WIDTH_FIXED|UI_HEIGHT_ADAPTS_TO_PARENT|UI_HORIZONTAL_CENTER,
        .flags  = UI_WIDTH_ADAPTS_TO_PARENT|UI_HEIGHT_ADAPTS_TO_PARENT|UI_HORIZONTAL_CENTER,
        .max_width = max_width,
        .padding = 4.0f,
    });
    UI_Push_parent(ctx, table_element);
    table->items_count = items_count;
    table->columns = columns;
    table->columns_width_percent = columns_width_percent;
    table->current_element = 0;
    table->current_row     = 0;
    table->current_column  = 0;
    table->element = table_element;
    table->item_height = item_height;

    float total_items_height = item_height * (float)table->items_count;

    float max_scroll = total_items_height-table->items_area_height+1.0f;
    float min_scroll = 0.0f;
    if (max_scroll < 0.0f) max_scroll = 0.0f;

    if (APP_Get_key_state(APP_KEY_DOWN)&APP_STATE_DOWN) {
        table->target_scroll_offset += 10.0f;
    }
    if (APP_Get_key_state(APP_KEY_UP)&APP_STATE_DOWN) {
        table->target_scroll_offset -= 10.0f;
    }
    float wheel_y = APP_Get_wheel_y();
    if (wheel_y > 0.0f) table->target_scroll_offset -= 30.0f;
    if (wheel_y < 0.0f) table->target_scroll_offset += 30.0f;

    table->target_scroll_offset = Clamp(table->target_scroll_offset, min_scroll, max_scroll);

    table->current_scroll_offset = Exp_animation_float(
        table->current_scroll_offset, table->target_scroll_offset, ctx->delta_time, 15.0f, 0.5f);

    int64_t first_item_index = (int64_t)(table->current_scroll_offset / item_height);
    float scroll_end = table->current_scroll_offset+table->items_area_height;
    int64_t last_item_index  = (int64_t)(scroll_end / item_height);
    
    if (table->items_count) {
        table->first_item_index = Clamp(first_item_index, 0, table->items_count-1);
        table->last_item_index  = Clamp(last_item_index, 0, table->items_count-1);
    }
    else {
        table->first_item_index = 0;
        table->last_item_index  = -1;
    }

    table->items_draw_offset = (table->first_item_index * item_height)-table->current_scroll_offset;
}

void
UI_End_table(UI_Context *ctx, UI_Table *table) {

    float total_items_height = table->item_height * (float)table->items_count;
    float max_scroll = total_items_height-table->items_area_height+1.0f;
    float scroll_percent = table->target_scroll_offset / max_scroll;
    scroll_percent = Clamp(scroll_percent, 0.0f, 1.0f);

    float scrollbar_w = 20.0f;
    float scrollbar_h = table->items_area_height;
    float scrollbar_x = table->element->x + table->element->width - scrollbar_w;
    float scrollbar_y = table->items_area_start;
    float scrollbar_x_end = scrollbar_x + scrollbar_w;
    float scrollbar_y_end = scrollbar_y + scrollbar_h;


    float scroll_thing_pad  = 2.0f;
    float scroll_thing_area = scrollbar_h - scroll_thing_pad*2.0f;


    float scroll_thing_w = scrollbar_w - scroll_thing_pad*2.0f;
    float scroll_thing_h = scroll_thing_area*((table->items_area_height)/total_items_height);
    scroll_thing_h = Max(scroll_thing_h, 10.0f);
    float scroll_thing_x = scrollbar_x + scroll_thing_pad;
    float scroll_thing_min_y = scrollbar_y + scroll_thing_pad;
    float scroll_thing_max_y = scroll_thing_min_y + (scroll_thing_area-scroll_thing_h);
    float scroll_thing_y = scroll_thing_min_y + scroll_percent * (scroll_thing_area-scroll_thing_h);
    float scroll_thing_x_end = scroll_thing_x + scroll_thing_w;
    float scroll_thing_y_end = scroll_thing_y + scroll_thing_h;

    if (Abs(table->target_scroll_offset - table->current_scroll_offset) <= 1.0f) {
        table->scrollbar_target_alpha = 0.0f;
    }
    else {
        table->scrollbar_target_alpha = 1.0f;
    }

    if (!table->scrollbar_holded) {
        // Test if is hovered
        if (ctx->pointer_pos.x > scrollbar_x && ctx->pointer_pos.x < scrollbar_x_end && 
            ctx->pointer_pos.y > scrollbar_y && ctx->pointer_pos.y < scrollbar_y_end) {

            table->scrollbar_target_alpha = 1.0f;

            bool in_scroll_thing = false;
            if (ctx->pointer_pos.x > scroll_thing_x && ctx->pointer_pos.x < scroll_thing_x_end && 
                ctx->pointer_pos.y > scroll_thing_y && ctx->pointer_pos.y < scroll_thing_y_end) {
                in_scroll_thing = true;
            }

            
            // Test if went down
            if (ctx->pointer_state & APP_STATE_PRESSED) {
                table->scrollbar_holded = true;
                if (in_scroll_thing) {
                    table->scrollbar_cursor_offset = ctx->pointer_pos.y - scroll_thing_y;
                }
                else {
                    table->scrollbar_cursor_offset = scroll_thing_h * 0.5f;
                }
            }
        }
    }
    else {

        if (ctx->pointer_state & APP_STATE_UP) {
            table->scrollbar_holded = false;
        }
        else {
            table->scrollbar_target_alpha = 1.0f;

            float cursor_offset      = table->scrollbar_cursor_offset;
            float new_scroll_thing_y = ctx->pointer_pos.y - cursor_offset;
            scroll_thing_y = Clamp(new_scroll_thing_y, scroll_thing_min_y, scroll_thing_max_y);
            float rate_pos   = (scroll_thing_y-scroll_thing_min_y)/(scroll_thing_area-scroll_thing_h);
            float max_scroll = total_items_height-table->items_area_height+1.0f;
            table->target_scroll_offset = rate_pos * max_scroll;
        }
    }

    table->scrollbar_current_alpha = Exp_animation_float(
        table->scrollbar_current_alpha, table->scrollbar_target_alpha, ctx->delta_time, 1.f, 0.5f);

    float current_alpha = table->scrollbar_current_alpha;

    GFX_Set_white();

    Color black_alpha = COLOR(0, 0, 0, Round(current_alpha * 255.0f));

    Draw_rectangle_ex(
        V2(scrollbar_x, scrollbar_y),
        scrollbar_w, scrollbar_h,
        0.0f, 1.0f,
        COLOR(80, 80, 80, Round(current_alpha * 180.0f)),
        black_alpha);
    
    Color scroll_thing_color = DARKGRAY;
    scroll_thing_color.a = Round(current_alpha * 255.0f);
    Draw_rectangle_ex(
        V2(scroll_thing_x, scroll_thing_y), scroll_thing_w, scroll_thing_h, 0.0f, 1.0f,
        scroll_thing_color, black_alpha);

    UI_Pop_parent(ctx);
    GFX_Flush();
    glDisable(GL_SCISSOR_TEST);
}

void
Draw_text_trunc(GFX_Font *font,
                String s,
                String on_trunc_text,
                float font_scale,
                float max_width,
                Vec2 pos,
                Color color,
                bool shadow) {

    int64_t text_len = s.len;
    float end_width  = GFX_Get_text_width(font, on_trunc_text.data, on_trunc_text.len) * font_scale;
    float text_width = GFX_Get_text_width(font, s.data, text_len) * font_scale;
    if (text_width > max_width) {
        for (; text_len > 0 && text_width+end_width > max_width;) {
            text_len -= 1;
            text_width = GFX_Get_text_width(font, s.data, text_len) * font_scale;
        }
    }

    if (shadow) {
        GFX_Draw_text_sized(s.data, text_len, V2(pos.x+1, pos.y+1), font_scale, BLACK);
        if (text_len < s.len) {
            GFX_Draw_text_sized(on_trunc_text.data, on_trunc_text.len, V2(pos.x+text_width+1, pos.y+1), font_scale, BLACK);
        }
    }
    GFX_Draw_text_sized(s.data, text_len, pos, font_scale, color);
    if (text_len < s.len) {
        GFX_Draw_text_sized(on_trunc_text.data, on_trunc_text.len, V2(pos.x+text_width, pos.y), font_scale, color);
    }
}


void
UI_Begin_table_header(UI_Context *ctx, UI_Table *table, Color color) {
    uint64_t header_key = UI_Key(STR_LIT("_table_header"))^(uint64_t)table;
    UI_Element *table_header = UI_Add_element(ctx, header_key, &(UI_ElementDesc){
        .flags  = UI_WIDTH_ADAPTS_TO_PARENT|UI_HEIGHT_ADAPTS_TO_CHILDS,
        .min_height = 30.0f
    });
    GFX_Set_white();
    GFX_Draw_rect(V2(table_header->x, table_header->y), table_header->width, table_header->height, color);
    GFX_Draw_rect(V2(table_header->x, table_header->y), table_header->width, 1.0f, BLACK);
    GFX_Draw_rect(V2(table_header->x, table_header->y+table_header->height-1.0f), table_header->width, 1.0f, BLACK);

    float bar_x = 0.0f;
    for (int64_t i = 0; i <= table->columns; i += 1) {
        GFX_Draw_rect(V2(table_header->x+bar_x, table_header->y+1), 1.0f, table_header->height-2.0f, BLACK);
        if (i < table->columns) {
            if (table->columns_width_percent) {
                float col_w = (table_header->width-1.0f)*table->columns_width_percent[i];
                bar_x += col_w;
            }
            else {
                bar_x += (table_header->width-1.0f)/(float)table->columns;
            }
        }
    }
    UI_Push_parent(ctx, table_header);
}

void
UI_End_table_header(UI_Context *ctx, UI_Table *table) {
    table->current_height_percent += (1.0f-table->current_height_percent)*0.1f;
    UI_Element *header_element = UI_Current_parent(ctx);
    float x = header_element->x;
    float y = header_element->y+header_element->height-1.0f;
    float w = table->element->width;
    float h = (table->element->height - header_element->height)*table->current_height_percent;
    GFX_Flush();
    glEnable(GL_SCISSOR_TEST);
    // Y is inverted
    glScissor(x, APP_Get_window_height() - (y + h), w, h);

    table->items_area_start  = y;
    table->items_area_height = h;

    UI_Pop_parent(ctx);
}




bool
UI_Table_header_item(UI_Context *ctx, UI_Table *table, TableHeaderItem *header_item) {
    ASSERT(table->current_column < table->columns);

    float padding = 5.0f;

    bool result = false;

    float r_w;
    if (table->columns_width_percent) {
        r_w = (table->columns_width_percent[table->current_column] * table->element->width);
    }
    else {
        r_w = (table->element->width)/(float)table->columns;
    }
    float r_h = ctx->font->height*ctx->font_scale+padding*2.0f;

    uint64_t key = UI_Key(STR_LIT("_header_item"))^((uint64_t)table)^table->current_column;
    table->current_column += 1;

    UI_Element *element = UI_Add_element(ctx, key, &(UI_ElementDesc){
        .flags  = UI_WIDTH_FIXED|UI_HEIGHT_FIXED,
        .width  = r_w,
        .height = r_h,
    });

    Vec2  r_pos = V2(element->x, element->y);
    Vec2  t_pos = V2(r_pos.x+padding*2, r_pos.y);

    if (header_item->sorter) {
        if (!header_item->colors_init) {
            header_item->current_fill_color = ctx->clickable_default_fill_color;
            header_item->target_fill_color  = ctx->clickable_default_fill_color;
            header_item->current_outline_color = Color_To_Vec4(BLANK);
            header_item->target_outline_color  = Color_To_Vec4(BLANK);
            header_item->colors_init = true;
        }

        GFX_Set_white();

        if (element->status & UI_PRESSED) {
            header_item->target_fill_color    = ctx->clickable_clicked_fill_color;
            header_item->target_outline_color = ctx->clickable_clicked_outline_color;
        }
        else if (element->status & UI_HOVERED) {
            header_item->target_fill_color    = ctx->clickable_hovered_fill_color;
            header_item->target_outline_color = ctx->clickable_hovered_outline_color;
        }
        else {
            header_item->target_fill_color    = ctx->clickable_default_fill_color;
            header_item->target_outline_color = Color_To_Vec4(BLANK);
        }

        header_item->current_fill_color    = Exp_animation_color(
            header_item->current_fill_color, header_item->target_fill_color, ctx->delta_time, 1.f, 0.5f);
        header_item->current_outline_color = Exp_animation_color(
            header_item->current_outline_color, header_item->target_outline_color, ctx->delta_time, 1.f, 0.5f);
        
        Color r_fill_color    = V4_To_Color(header_item->current_fill_color);
        Color r_outline_color = V4_To_Color(header_item->current_outline_color);
        Draw_rectangle_ex(r_pos, r_w, r_h, 0.0f, 1.0f, r_fill_color, r_outline_color);

        if (header_item->sort_order == NO_SORT) {
            header_item->target_tri_angle = 0.f;
        }
        else if (header_item->sort_order == SORT_LESS_FIRST) {
            header_item->target_tri_angle = -90.f;
        }
        else if (header_item->sort_order == SORT_GREATER_FIRST) {
            header_item->target_tri_angle =  90.f;
        }

        header_item->current_tri_angle = Exp_animation_float(
            header_item->current_tri_angle, header_item->target_tri_angle, ctx->delta_time, 1.f, 0.5f);

        float tri_size = ctx->font->height*ctx->font_scale*0.5f;
        Vec2 tri_pos = V2(t_pos.x+tri_size*0.5f, r_pos.y + r_h*0.5f);

        Vec2 tri_v0 = V2(-tri_size*0.5f, -tri_size*0.5f);
        Vec2 tri_v1 = V2(-tri_size*0.5f,  tri_size*0.5f);
        Vec2 tri_v2 = V2(tri_size*0.5f, 0.0f);

        // Rotate the points
        Mat2 rot = M2_Rotate(Radians(header_item->current_tri_angle));
        tri_v0 = Mul_v2_m2(tri_v0, rot);
        tri_v1 = Mul_v2_m2(tri_v1, rot);
        tri_v2 = Mul_v2_m2(tri_v2, rot);

        tri_v0 = V2_Add(tri_pos, tri_v0);
        tri_v1 = V2_Add(tri_pos, tri_v1);
        tri_v2 = V2_Add(tri_pos, tri_v2);

        t_pos.x += padding + tri_size;

        GFX_Draw_triangle(tri_v0, tri_v1, tri_v2, WHITE);

        result = (element->status & UI_RELEASED) != 0;
    }


    GFX_Set_font(ctx->font);
    GFX_Draw_text_sized(header_item->label.data, header_item->label.len, V2(t_pos.x+1, t_pos.y+1), ctx->font_scale, BLACK);
    GFX_Draw_text_sized(header_item->label.data, header_item->label.len, t_pos, ctx->font_scale, WHITE);

    return result;
}





bool
UI_Begin_table_row(UI_Context *ctx, UI_Table *table, Color color) {

    bool is_hovered = false;

    table->current_column = 0;
    uint64_t row_key = UI_Key(STR_LIT("_table_row"))^(uint64_t)table^table->current_row;
    UI_Element *table_row = UI_Add_element(ctx, row_key, &(UI_ElementDesc){
        .flags  = UI_WIDTH_ADAPTS_TO_PARENT|UI_HEIGHT_FIXED,
        .height = table->item_height
    });
    UI_Push_parent(ctx, table_row);


    float row_x  = table_row->x;
    float row_y  = table_row->y + table->items_draw_offset;
    float row_width  = table_row->width;
    float row_height = table_row->height;

    Color borders_color = BLACK;


    if (ctx->pointer_pos.x > row_x && ctx->pointer_pos.x <= row_x+row_width && 
        ctx->pointer_pos.y > row_y && ctx->pointer_pos.y <= row_y+row_height &&
        !table->scrollbar_holded) {
        is_hovered = true;
        borders_color = WHITE;
    }


    GFX_Set_white();
    GFX_Draw_rect(V2(row_x, row_y), row_width, row_height, color);
    GFX_Draw_rect(V2(row_x, row_y), row_width, 1.0f, borders_color);
    GFX_Draw_rect(V2(row_x, row_y+row_height-1.0f), row_width, 1.0f, borders_color);


    float bar_x = 0.0f;
    for (int64_t i = 0; i <= table->columns; i += 1) {
        GFX_Draw_rect(V2(row_x+bar_x, row_y+1), 1.0f, row_height-2.0f, borders_color);
        if (i < table->columns) {
            if (table->columns_width_percent) {
                float col_w = (row_width-1.0f)*table->columns_width_percent[i];
                bar_x += col_w;
            }
            else {
                bar_x += (row_width-1.0f)/(float)table->columns;
            }
        }
    }
    return is_hovered;
}

void
UI_End_table_row(UI_Context *ctx, UI_Table *table) {
    table->current_row += 1;
    UI_Pop_parent(ctx);
}

void
UI_Table_item(UI_Context *ctx, UI_Table *table, String s) {
    ASSERT(table->current_column < table->columns);
    float text_padding = 5.0f;
    float r_w;
    if (table->columns_width_percent) {
        r_w = (table->columns_width_percent[table->current_column] * table->element->width) - 2.0f;
    }
    else {
        r_w = (table->element->width-((table->columns+1)*2.0f))/(float)table->columns;
    }
    float r_h = ctx->font->height*ctx->font_scale+text_padding*2.0f;

    uint64_t key =
        UI_Key(STR_LIT("_table_item"))^
        ((uint64_t)table)^
        (table->current_row*table->columns+table->current_column);

    table->current_column += 1;

    UI_Element *element = UI_Add_element(ctx, key, &(UI_ElementDesc){
        .flags  = UI_WIDTH_FIXED|UI_HEIGHT_FIXED,
        .width  = r_w,
        .height = r_h,
        .padding = 2.0f,
    });
    
    float r_pos_x = element->x;
    float r_pos_y = element->y + table->items_draw_offset;
    
    float max_width = r_w - (text_padding * 4.0f);
    GFX_Set_white();
    
    GFX_Set_font(ctx->font);
    Vec2 t_pos = V2(r_pos_x+text_padding*2, r_pos_y);

    Draw_text_trunc(ctx->font, s, STR_LIT(".."), ctx->font_scale, max_width, t_pos, RAYWHITE, true);
}


void
UI_Label(UI_Context *ctx, String s, uint64_t key) {
    float padding = 8.0f;
    float tw = GFX_Get_text_width(ctx->font, s.data, s.len)*ctx->font_scale;
    float r_w = tw+padding*2.0f;
    float r_h = ctx->font->height*ctx->font_scale+padding*2.0f;

    UI_Element *element = UI_Add_element(ctx, key, &(UI_ElementDesc){
        .flags  = UI_WIDTH_FIXED|UI_HEIGHT_FIXED,
        .width  = r_w,
        .height = r_h,
    });

    Vec2  r_pos = V2(element->x, element->y+4.0f);

    GFX_Set_font(ctx->font);
    Vec2 t_pos = V2(r_pos.x+padding*2, r_pos.y);
    GFX_Draw_text_sized(s.data, s.len, V2(t_pos.x+1, t_pos.y+1), ctx->font_scale, BLACK);
    GFX_Draw_text_sized(s.data, s.len, t_pos, ctx->font_scale, WHITE);
}



void
UI_Clear_input(UI_Input *input) {
    input->buffer_bytes_used = 0;
    input->cursor_byte_pos = 0;
    input->focused = false;
    input->text_shift = 0.0f;
}

String
UI_Get_input_string(UI_Input *input) {
    String result = STR(input->buffer, input->buffer_bytes_used);
    return result;
}

bool
UI_Do_input(UI_Context *ctx, UI_Input *input) {

    bool result = false;

    uint64_t key = (uint64_t)input;

    float padding = 5.0f;

    float r_w = input->width;
    float r_h = ctx->font->height*ctx->font_scale+padding*2.0f;

    UI_Element *element = UI_Add_element(ctx, key, &(UI_ElementDesc){
        .flags  = UI_WIDTH_FIXED|UI_HEIGHT_FIXED,
        .width  = input->width,
        .height = r_h,
        .padding = padding,
    });

    Color target_outline_color = BLACK;

    if (input->focused) {
        target_outline_color = BLUE;
        
        int bytes = 0;
        const char *new_text_input = APP_Get_text_input(&bytes);
        if (bytes && input->buffer_cap - input->buffer_bytes_used > bytes) {
            result = true;
            for (int64_t i = 0; i < bytes; i += 1) {
                input->buffer[input->buffer_bytes_used+i] = new_text_input[i];
            }
            input->buffer_bytes_used += bytes;
            input->cursor_byte_pos   += bytes;
        }

        if (APP_Get_key_state(APP_KEY_BACKSPACE) & (APP_STATE_REPEAT|APP_STATE_PRESSED)) {
            if (input->buffer_bytes_used > 0) {
                result = true;
                // TODO(Tano): Handle UTF8 encoding
                // WARNING WARNING
                input->buffer_bytes_used -= 1;
                input->cursor_byte_pos   -= 1;
            }
        }

        // If isn't hovered and we click outside we lose focus
        if ((element->status & UI_HOVERED) == 0) {
            if (ctx->pointer_state & APP_STATE_PRESSED) {
                input->focused = false;
            }
        }
    }
    else if (element->status & UI_HOVERED) {
        target_outline_color = GRAY;
    }

    if (element->status & UI_PRESSED) input->focused = true;


    input->current_outline_color = Exp_animation_color(
        input->current_outline_color, Color_To_Vec4(target_outline_color), ctx->delta_time, 1.f, 0.5f);

    Color outline_color = V4_To_Color(input->current_outline_color);

    Vec2 r_pos = V2(element->x, element->y);
    GFX_Set_white();
    Draw_rectangle_ex(r_pos, r_w, r_h, 0.6f, 2.0f, WHITE, outline_color);
    float max_input_width = r_w - (padding * 2.0f);

    float text_width_until_cursor = GFX_Get_text_width(ctx->font, input->buffer, input->cursor_byte_pos)*ctx->font_scale;

    float text_shift = 0.0f;
    float cursor_x_off = text_width_until_cursor;
    if (cursor_x_off > max_input_width) {
        text_shift = (cursor_x_off-max_input_width);
        cursor_x_off = max_input_width;
    }
    
    float text_x = r_pos.x + padding;
    float text_y = r_pos.y;
    String input_text = STR(input->buffer, input->buffer_bytes_used);
    GFX_Set_font(ctx->font);
    GFX_Draw_text_view(input_text.data, input_text.len, V2(text_x, text_y), text_shift, max_input_width, ctx->font_scale, BLACK);

    if (input->focused) {
        float cursor_height = ctx->font->height*ctx->font_scale;
        float cursor_width  = 2.0f;
        float cursor_x = r_pos.x + padding + cursor_x_off;
        float cursor_y = r_pos.y + padding;
        GFX_Set_white();
        GFX_Draw_rect(V2(cursor_x, cursor_y), cursor_width, cursor_height, BLACK);
    }

    return result;
}


#endif // UI_IMPLEMENTATION_INCLUDED
#endif // UI_IMPLEMENTATION

